package com.bingcoo.fileexplorer;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.webkit.MimeTypeMap;

import com.github.junrar.Archive;
import com.github.junrar.exception.RarException;
import com.github.junrar.rarfile.FileHeader;

import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;
import org.junit.Test;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;
import java.util.Stack;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;

import static java.io.File.separator;
import static junit.framework.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void extractRarFile() {
        extractRarFiles();

    }


    public void extractRarFiles() {
        //URL url = Main.class.getResource("/logRar.rar");
        //String filename = url.getFile();
        //File archive = new File(filename);
        File archive = new File("C:/Users/ii/Desktop/test3English.rar");
        File destination = new File("C:/Users/ii/Desktop/MyTest");

        Archive arch = null;
        try {
            arch = new Archive(archive);
        } catch (RarException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (arch != null) {
            if (arch.isEncrypted()) {
                System.out.println("archive is encrypted cannot extreact");
                return;
            }
            FileHeader fh = null;
            while (true) {
                fh = arch.nextFileHeader();
                if (fh == null) {
                    break;
                }
                if (fh.isEncrypted()) {
                    System.out.println("file is encrypted cannot extract: "
                            + fh.getFileNameString());
                    continue;
                }
                System.out.println("extracting: " + fh.getFileNameString());
                try {
                    if (fh.isDirectory()) {
                        createDirectory(fh, destination);
                    } else {
                        File f = createFile(fh, destination);
                        OutputStream stream = new FileOutputStream(f);
                        arch.extractFile(fh, stream);
                        stream.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private File createFile(FileHeader fh, File destination) {
        File f = null;
        String name = null;
        if (fh.isFileHeader() && fh.isUnicode()) {
            name = fh.getFileNameW();
        } else {
            name = fh.getFileNameString();
        }
        f = new File(destination, name);
        if (!f.exists()) {
            try {
                f = makeFile(destination, name);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return f;
    }

    private File makeFile(File destination, String name) throws IOException {
        String[] dirs = name.split("\\\\");
        if (dirs == null) {
            return null;
        }
        String path = "";
        int size = dirs.length;
        if (size == 1) {
            return new File(destination, name);
        } else if (size > 1) {
            for (int i = 0; i < dirs.length - 1; i++) {
                path = path + separator + dirs[i];
                new File(destination, path).mkdir();
            }
            path = path + separator + dirs[dirs.length - 1];
            File f = new File(destination, path);
            f.createNewFile();
            return f;
        } else {
            return null;
        }
    }

    private void createDirectory(FileHeader fh, File destination) {
        File f = null;
        if (fh.isDirectory() && fh.isUnicode()) {
            f = new File(destination, fh.getFileNameW());
            if (!f.exists()) {
                makeDirectory(destination, fh.getFileNameW());
            }
        } else if (fh.isDirectory() && !fh.isUnicode()) {
            f = new File(destination, fh.getFileNameString());
            if (!f.exists()) {
                makeDirectory(destination, fh.getFileNameString());
            }
        }
    }

    private void makeDirectory(File destination, String fileName) {
        String[] dirs = fileName.split("\\\\");
        if (dirs == null) {
            return;
        }
        String path = "";
        for (String dir : dirs) {
            path = path + separator + dir;
            new File(destination, path).mkdir();
        }
    }


    @Test
    public void myZipTest() {
        unZip("C:\\Users\\ii\\Desktop\\三层.zip", "C:\\Users\\ii\\Desktop\\" + System.currentTimeMillis());
    }

    public static final int KB = 2048;


    /**
     * 23.     * 解压Zip文件
     * 24.     * @param path 文件目录
     * 25.
     */
    public static boolean unZip(String path, String destDirPath) {
        int count = -1;
        String savepath = "";
        File file = null;
        InputStream is = null;
        FileOutputStream fos = null;
        BufferedOutputStream bos = null;
//        savepath = path.substring(0, path.lastIndexOf(".")) + File.separator; //保存解压文件目录
        savepath = destDirPath + File.separator; //保存解压文件目录
//        new File(savepath).mkdir(); //创建保存目录
        new File(savepath).mkdirs(); //创建保存目录
        ZipFile zipFile = null;
        try {
            zipFile = new ZipFile(path, "gbk"); //解决中文乱码问题
            Enumeration<?> entries = zipFile.getEntries();
            while (entries.hasMoreElements()) {
                byte buf[] = new byte[KB];
                ZipEntry entry = (ZipEntry) entries.nextElement();
                String filename = entry.getName();
                boolean ismkdir = false;
                if (filename.lastIndexOf("/") != -1) { //检查此文件是否带有文件夹
                    ismkdir = true;
                }
                filename = savepath + filename;
                if (entry.isDirectory()) { //如果是文件夹先创建
                    file = new File(filename);
                    file.mkdirs();
                    continue;
                }
                file = new File(filename);
                if (!file.exists()) { //如果是目录先创建
                    if (ismkdir) {
                        new File(filename.substring(0, filename.lastIndexOf("/"))).mkdirs(); //目录先创建
                    }
                }
                file.createNewFile(); //创建文件
                is = zipFile.getInputStream(entry);
                fos = new FileOutputStream(file);
                bos = new BufferedOutputStream(fos, KB);
                while ((count = is.read(buf)) > -1) {
                    bos.write(buf, 0, count);
                }
                bos.flush();
                bos.close();
                fos.close();
                is.close();
            }
            zipFile.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
            return false;
        } finally {
            try {
                if (bos != null) {
                    bos.close();
                }
                if (fos != null) {
                    fos.close();
                }
                if (is != null) {
                    is.close();
                }
                if (zipFile != null) {
                    zipFile.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }


    public static boolean createOrExistsDir(File file) {
        // 如果存在，是目录则返回true，是文件则返回false，不存在则返回是否创建成功
        return file != null && (file.exists() ? file.isDirectory() : file.mkdirs());
    }

    @Test
    public void delete() {
        File file = new File("C:\\Users\\ii\\Desktop\\新建文件夹");
        boolean s = delFileUnRoot(file);
    }

    /***
     * 删除文件或者文件夹
     *
     * @param f
     * @return 成功 返回true, 失败返回false
     */
    static private boolean delFileUnRoot(File f) {
        boolean ret = true;
        if (null == f || !f.exists()) {
            return ret;
        }
        Stack<File> tmpFileStack = new Stack<File>();
        tmpFileStack.push(f);
        try {
            while (!tmpFileStack.isEmpty()) {
                File curFile = tmpFileStack.pop();
                if (null == curFile) {
                    continue;
                }
                if (curFile.isFile()) {
                    if (!curFile.delete()) {
                        ret = false;
                    }
                } else {
                    File[] tmpSubFileList = curFile.listFiles();
                    if (null == tmpSubFileList || 0 == tmpSubFileList.length) {    //空文件夹直接删
                        if (!curFile.delete()) {
                            ret = false;
                        }
                    } else {
                        tmpFileStack.push(curFile); // !!!
                        for (File item : tmpSubFileList) {
                            tmpFileStack.push(item);
                        }
                    }
                }
            }
        } catch (Exception e) {
            ret = false;
        }
        return ret;
    }

    /*
     * 采用了新的办法获取APK图标，之前的失败是因为android中存在的一个BUG,通过
     * appInfo.publicSourceDir = apkPath;来修正这个问题，详情参见:
     * http://code.google.com/p/android/issues/detail?id=9151
     */
    private static Drawable getApkIcon(Context context, String apkPath) {
        PackageManager pm = context.getPackageManager();
        PackageInfo info = pm.getPackageArchiveInfo(apkPath,
                PackageManager.GET_ACTIVITIES);
        if (info != null) {
            ApplicationInfo appInfo = info.applicationInfo;
            appInfo.sourceDir = apkPath;
            appInfo.publicSourceDir = apkPath;
            try {
                return appInfo.loadIcon(pm);
            } catch (OutOfMemoryError e) {
            }
        }
        return null;
    }

    @Test
    public void ty() {
        String mimeType =
                MimeTypeMap.getSingleton()
                        .getMimeTypeFromExtension("pdf");
        System.out.print(mimeType);
    }

    @Test
    public void rxTest() {
        rx.Observable.OnSubscribe onSubscribe = new rx.Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                String s = new Random(100).toString();
                subscriber.onNext(s);
                subscriber.onCompleted();
            }

        };
        Observable.create(onSubscribe).subscribe(new Observer() {


            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Object o) {

            }
        });
    }

    @Test
    public void sdt() {
        List<Integer> s = new ArrayList();
        s.add(1);
        s.add(2);
        s.add(3);
        s.add(4);
        int a=s.get(0);
        int b=s.get(1);
        int c=s.get(2);
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
    }

    @Test
    public void test0() {
        String s="adc";
        String news=s.substring(3);
        assertEquals("adc", news);
    }


}