package com.bingcoo.fileexplorer.selection;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.storage.StorageVolume;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bingcoo.fileexplorer.R;
import com.bingcoo.fileexplorer.base.BaseActivity;
import com.bingcoo.fileexplorer.dialog.fragment.ProgressDialogFragment;
import com.bingcoo.fileexplorer.home.FileInfoAdapter;
import com.bingcoo.fileexplorer.receiver.MountReceiver;
import com.bingcoo.fileexplorer.service.FileManagerService;
import com.bingcoo.fileexplorer.service.HeavyOperationListener;
import com.bingcoo.fileexplorer.service.ProgressInfo;
import com.bingcoo.fileexplorer.ui.SlowHorizontalScrollView;
import com.bingcoo.fileexplorer.util.FileInfo;
import com.bingcoo.fileexplorer.util.FileUtils;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.MountPointManager;
import com.bingcoo.fileexplorer.util.PermissionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bingcoo.fileexplorer.home.HomeActivity.MY_PERMISSIONS_REQUEST_TO_READ_EXTERNAL_STORAGE;

/**
 * 类名称：SelectFileActivity
 * 作者：zhshh
 * 内容摘要：电子邮件添加附件跳转
 * 创建日期：2017/4/14
 * 修改者， 修改日期， 修改内容
 */
public class SelectFileActivity extends BaseActivity implements MountReceiver.MountListener,
        AdapterView.OnItemClickListener, View.OnClickListener {
    private static final String TAG = "SelectFileActivity";

    static {
        LogUtils.setDebug(TAG, true);
    }

    private TabManager mTabManager = null;
    private List<FileInfo> mFileInfos = null;
    private Map<String, int[]> mPositionMap = new HashMap<>();
    private int mPosition;
    private int[] mPositionTopList = new int[2];
    private boolean mScrollBack;
    protected FileInfo mSelectedFileInfo = null;

    @BindView(R.id.tv_dst_title)
    TextView mTvTitle;
    @BindView(R.id.lst_media)
    ListView mLstMedia;
    @BindView(R.id.sv_navigation_bar)
    SlowHorizontalScrollView mNavigationBar;
    @BindView(R.id.layout_loading)
    View mLayoutLoading;
    @BindView(R.id.empty_view)
    TextView mLayoutEmptyView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);
        ButterKnife.bind(this);
        mSavedInstanceState = savedInstanceState;
        init();
        if (!PermissionUtils.hasPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_TO_READ_EXTERNAL_STORAGE);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
       /* if (!PermissionUtils.hasStorageReadPermission(this)) {
            finish();
        }*/
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (mCurrentPath != null) {
            outState.putString(SAVED_PATH_KEY, mCurrentPath);
        }
        super.onSaveInstanceState(outState);
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            LogUtils.d(TAG, "handleMessage, msg = " + msg.what);
            switch (msg.what) {
                case MSG_DO_MOUNTED:
                    doOnMounted((String) msg.obj);
                    break;
                case MSG_DO_EJECTED:
                    doOnEjected((String) msg.obj);
                    break;
                case MSG_DO_UNMOUNTED:
                    doOnUnMounted((StorageVolume) msg.obj);
                    break;
                case MSG_DO_SDSWAP:
                    doOnSdSwap();
                    break;
                default:
                    break;
            }
        }
    };

    protected void doOnMounted(String mountPoint) {
        LogUtils.i(TAG, "doOnMounted, mountPoint = " + mountPoint);
        doPrepareForMount(mountPoint);
        if (mMountPointManager.isRootPath(mCurrentPath)) {
            LogUtils.d(TAG, "doOnMounted, mCurrentPath is root path: " + mCurrentPath);
            showDirectoryContent(mCurrentPath);
        }
    }

    protected void doOnEjected(String unMountPoint) {
        if ((mCurrentPath + MountPointManager.SEPARATOR).startsWith(unMountPoint + MountPointManager.SEPARATOR)
                || mMountPointManager.isRootPath(mCurrentPath)
                || mMountPointManager.isPrimaryVolume(unMountPoint)) {
            LogUtils.d(TAG, "onEjected, Current Path = " + mCurrentPath);
            if (mService != null && mService.isBusy(this.getClass().getName())) {
                mService.cancel(this.getClass().getName());
            }
        }
    }

    private void doPrepareForMount(String mountPoint) {
        LogUtils.i(TAG, "doPrepareForMount, mountPoint = " + mountPoint);
        if ((mCurrentPath + MountPointManager.SEPARATOR).startsWith(mountPoint + MountPointManager.SEPARATOR)
                || mMountPointManager.isRootPath(mCurrentPath)) {
            LogUtils.d(TAG, "pre-onMounted");
            if (mService != null && mService.isBusy(this.getClass().getName())) {
                mService.cancel(this.getClass().getName());
            }
        }
        mMountPointManager.init(getApplicationContext());
    }

    @TargetApi(24)
    protected void doOnUnMounted(StorageVolume volume) {
        String unMountPoint = volume.getPath();

        if ((mCurrentPath + MountPointManager.SEPARATOR).startsWith(unMountPoint
                + MountPointManager.SEPARATOR)
                || mMountPointManager.isRootPath(mCurrentPath)) {
            LogUtils.d(TAG, "onUnmounted, Current Path = " + mCurrentPath);
            if (mService != null && mService.isBusy(this.getClass().getName())) {
                mService.cancel(this.getClass().getName());
            }
            showToastForUnmount(volume);

            DialogFragment listFragment = (DialogFragment) getFragmentManager()
                    .findFragmentByTag(ListListener.LIST_DIALOG_TAG);
            if (listFragment != null) {
                LogUtils.d(TAG, "onUnmounted, listFragment dismiss. ");
                listFragment.dismissAllowingStateLoss();
            }

            backToRootPath();
        }
    }

    private void backToRootPath() {
        LogUtils.d(TAG, "backToRootPath...");
        if (mMountPointManager != null && mMountPointManager.isRootPath(mCurrentPath)) {
            showDirectoryContent(mCurrentPath);
        } else if (mTabManager != null) {
            mTabManager.updateNavigationBar(0);
        }
        clearNavigationList();
    }

    /**
     * This method clear navigation history list
     */
    protected void clearNavigationList() {
        if (mFileInfoManager != null) {
            mFileInfoManager.clearNavigationList();
        }
    }

    protected void doOnSdSwap() {
        mMountPointManager.init(getApplicationContext());
        backToRootPath();
    }

    @TargetApi(24)
    private void showToastForUnmount(StorageVolume volume) {
        LogUtils.d(TAG, "showToastForUnmount, path = " + volume.getPath());
        if (isResumed()) {
            String unMountPointDescription = volume.getDescription(this);
            LogUtils.d(TAG, "showToastForUnmount, unMountPointDescription:"
                    + unMountPointDescription);
            mToastHelper.showToast(getString(R.string.unmounted, unMountPointDescription));
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        LogUtils.d(TAG, "onItemClick: position=" + position + ", mode=" + mAdapter.getMode());
        if (mService != null && mService.isBusy(this.getClass().getName())) {
            LogUtils.d(TAG, "onItemClick, service is busy,return. ");
            return;
        }

        if (null != mAdapter) {

            if (position >= mAdapter.getCount() || position < 0) {
                LogUtils.e(TAG, "onItemClick, events error, mFileInfoList.size()= "
                        + mAdapter.getCount());
                return;
            }

            FileInfo selectedItem = mAdapter.getItem(position);

            if (null != selectedItem) {
                if (selectedItem.isDirectory()) {
                    int top = view.getTop();
                    mPosition = mAdapter.getPosition(selectedItem);
                    LogUtils.e(TAG, "***********mPosition=:" + mPosition);
                    mPositionTopList[0] = mPosition;
                    mPositionTopList[1] = top;
                    if (mCurrentPath != null) {
                        mPositionMap.put(mCurrentPath, mPositionTopList);
                    }
                    //addToNavigationList(mCurrentPath, selectedItem, top);
                    String path = selectedItem.getFileAbsolutePath();
                    LogUtils.v(TAG, "onItemClick, fromTop = " + top + ", path=" + path);

                    if (!TextUtils.isEmpty(path)) {
                        showDirectoryContent(path);
                    }
                } else {
                    //分享
                    Intent intent = new Intent();
                    Uri uri = selectedItem.getUri();
                    LogUtils.d(TAG, "onItemClick RESULT_OK, uri : " + uri);
                    intent.setData(uri);
                    setResult(RESULT_OK, intent);
                    finish();
                }

            } else {
                LogUtils.e(TAG, "onItemClick: null == selectedItem ");
            }

        }
    }


    @Override
    public void onBackPressed() {

        if (mService != null && mService.isBusy(this.getClass().getName())) {
            LogUtils.d(TAG, "onBackPressed: service is busy");
            return;
        }

        if (!TextUtils.isEmpty(mCurrentPath)
                && !mMountPointManager.isRootPath(mCurrentPath)) {
            mScrollBack = true;
            String path = FileUtils.getFilePath(mCurrentPath);
            LogUtils.d(TAG, "onBackPressed: path=" + path + ", mCurrentPath=" + mCurrentPath);
            if (mMountPointManager.isMountPoint(mCurrentPath)) {
                path = mMountPointManager.getRootPath();
                LogUtils.d(TAG, "onBackPressed: RootPath=" + path);
            }
            showDirectoryContent(path);
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void onMounted(String mountPoint) {
        Message.obtain(mHandler, MSG_DO_MOUNTED, mountPoint).sendToTarget();
    }

    @TargetApi(24)
    @Override
    public void onUnMounted(StorageVolume volume) {
        String unMountPoint = volume.getPath();
        LogUtils.d(TAG, "onUnMounted,unMountPoint :" + unMountPoint);

        //make sure mCurrentPath be not null
        if (mCurrentPath == null) {
            mCurrentPath = initCurrentFileInfo();
        }

        if (mCurrentPath != null
                && (mCurrentPath.startsWith(unMountPoint)
                || (mMountPointManager != null && mMountPointManager.isRootPath(mCurrentPath)))) {
            ProgressDialogFragment pf = (ProgressDialogFragment) getFragmentManager()
                    .findFragmentByTag(HeavyOperationListener.HEAVY_DIALOG_TAG);
            if (pf != null) {
                pf.dismissAllowingStateLoss();
            }
        }

        Message.obtain(mHandler, MSG_DO_UNMOUNTED, volume).sendToTarget();
    }

    @Override
    public void onEjected(String unMountPoint) {
        Message.obtain(mHandler, MSG_DO_EJECTED, unMountPoint).sendToTarget();
    }

    @Override
    public void onSdSwap() {
        Message.obtain(mHandler, MSG_DO_SDSWAP).sendToTarget();
    }

    private void init() {
        mTvTitle.setText(R.string.choose);
        if (null != mLstMedia) {
            //mLstMedia.setEmptyView(findViewById(R.id.empty_view));
            mLstMedia.setOnItemClickListener(this);
            mLstMedia.setOnItemLongClickListener(this);
            mLstMedia.setFastScrollEnabled(false);
            mLstMedia.setVerticalScrollBarEnabled(true);
            View footer = getLayoutInflater().inflate(R.layout.layout_lst_footer, mLstMedia, false);
            mLstMedia.addFooterView(footer);
        }

        if (null != mNavigationBar) {
            mNavigationBar.setVerticalScrollBarEnabled(false);
            mNavigationBar.setHorizontalScrollBarEnabled(false);
            mTabManager = new TabManager(
                    (LinearLayout) mNavigationBar.findViewById(R.id.ll_tabs_holder), mNavigationBar);
        }
        LogUtils.d(TAG, "init: mFileInfos=" + mFileInfos);
        switchLayout(mLstMedia);
    }

    protected void serviceConnected() {
        super.serviceConnected();

        mFileInfoManager = mService.initFileInfoManager(this.getClass().getName());
        mService.setListType(getPrefsShowHiddenFile() ? FileManagerService.FILE_FILTER_TYPE_ALL
                : FileManagerService.FILE_FILTER_TYPE_DEFAULT, this.getClass().getName());
        mAdapter = new FileInfoAdapter(this, mService, mFileInfoManager);
        if (null != mLstMedia) {
            mLstMedia.setAdapter(mAdapter);
            if (null == mSavedInstanceState) {
                mCurrentPath = initCurrentFileInfo();
                if (mCurrentPath != null) {
                    showDirectoryContent(mCurrentPath);
                }
            } else {

                String savePath = mSavedInstanceState.getString(SAVED_PATH_KEY);
                if (savePath != null
                        && mMountPointManager.isMounted(mMountPointManager
                        .getRealMountPointPath(savePath))) {
                    mCurrentPath = savePath;
                } else {
                    mCurrentPath = initCurrentFileInfo();
                }

                if (mCurrentPath != null) {

                    mTabManager.refreshTab(mCurrentPath);
                    reloadContent();

                }
                restoreDialog();
            }
            mAdapter.notifyDataSetChanged();
        }

        // register Receiver when service connected..
        if (null == mMountReceiver) {
            mMountReceiver = MountReceiver.registerMountReceiver(this);
            mMountReceiver.registerMountListener(this);
        }
    }

    protected String initCurrentFileInfo() {
        if (null != mMountPointManager) {
            return mMountPointManager.getRootPath();
        } else {
            return null;
        }
    }


    protected void showDirectoryContent(String path) {
        LogUtils.d(TAG, "showDirectoryContent, path = " + path);

        mCurrentPath = path;
        if (!TextUtils.isEmpty(mCurrentPath)) {
            boolean isRoot = mMountPointManager.isRootPath(mCurrentPath);
            ViewGroup.LayoutParams lp = mNavigationBar.getLayoutParams();
            lp.height = isRoot ? 0 : getResources().getDimensionPixelOffset(R.dimen.navigation_bar_height);
        }

        if (mService != null) {
            mService.listFiles(this.getClass().getName(), mCurrentPath, new DstListListener());
        }
    }


    public void onClick(View v) {
        if (mService.isBusy(this.getClass().getName())) {
            LogUtils.d(TAG, "onClick(), service is busy.");
            return;
        }
        int id = v.getId();
        LogUtils.d(TAG, "onClick() id=" + id);
        mScrollBack = true;
        mTabManager.updateNavigationBar(id);
    }

    protected void onPathChanged() {
        if (null != mTabManager) {
            mTabManager.refreshTab(mCurrentPath);
        }
    }

    private void switchLayout(View view) {
        if (null != view) {
            view.setVisibility(View.VISIBLE);

            if (view != mLstMedia) {
                mLstMedia.setVisibility(View.GONE);
            }

            if (view != mLayoutLoading) {
                mLayoutLoading.setVisibility(View.GONE);
            }

            if (view != mLayoutEmptyView) {
                mLayoutEmptyView.setVisibility(View.GONE);
            }
        }
    }

    protected class DstListListener extends ListListener {
        @Override
        public void onTaskProgress(ProgressInfo progressInfo) {
            super.onTaskProgress(progressInfo);
            if (isResumed()) {
                switchLayout(mLayoutLoading);
            }
        }

        public void onTaskResult(int result) {
//            if (mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
//                mFileInfoManager.loadFileInfoList(mCurrentPath, mSortType, mSelectedFileInfo);
//                mSelectedFileInfo = mAdapter.getFirstCheckedFileInfoItem();
//            } else {
                mFileInfoManager.loadFileInfoList(mCurrentPath, mSortType);
//            }

            mAdapter.notifyDataSetChanged();
            if (mScrollBack && mPositionMap.containsKey(mCurrentPath)) {
                int mPos = mPositionMap.get(mCurrentPath)[0];
                int mTop = mPositionMap.get(mCurrentPath)[1];
                LogUtils.e(TAG, "*******mPos=:" + mPos);
                LogUtils.e(TAG, "*******mTop=:" + mTop);
                mLstMedia.setSelectionFromTop(mPos, mTop);
                mScrollBack = false;
            } else {
                int selectedItemPosition = restoreSelectedPosition();
                if (selectedItemPosition == -1) {
                    mLstMedia.setSelectionAfterHeaderView();
                } else {
                    mLstMedia.setSelection(selectedItemPosition);
                }
            }
            LogUtils.d(TAG, "onTaskResult: count=" + mAdapter.getCount());
            onPathChanged();
            if (0 == mAdapter.getCount()) {
                switchLayout(mLayoutEmptyView);
            } else {
                switchLayout(mLstMedia);
            }
        }
    }

    private int restoreSelectedPosition() {
        if (mSelectedFileInfo == null) {
            return -1;
        } else {
            int curSelectedItemPosition = mAdapter.getPosition(mSelectedFileInfo);
            mSelectedFileInfo = null;
            return curSelectedItemPosition;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        LogUtils.d(TAG, "onRequestPermissionsResult, requestCode:" + requestCode);
        if (null == permissions || permissions.length == 0 ||
                null == grantResults || grantResults.length == 0 ||
                PackageManager.PERMISSION_DENIED == grantResults[0]) {
            LogUtils.e(TAG, "**********onRequestPermissionsResult, Permission or grant res null*******");
            mToastHelper.showToast(R.string.no_permission_finish);
            finish();
            LogUtils.e(TAG, "没有权限");
            return;
        }

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_TO_READ_EXTERNAL_STORAGE:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    showDirectoryContent(mCurrentPath);
                }
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

}
