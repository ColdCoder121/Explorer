package com.bingcoo.fileexplorer.interfaces;

import android.database.Cursor;

/**
 * 类名称：ItemAdapter
 * 作者：David
 * 内容摘要：
 * 创建日期：2016/12/26
 * 修改者， 修改日期， 修改内容
 */
public interface ItemAdapter {
    String getFileName();
    long getFileModifiedTime();
    String getFileSize();
    String getFilePath();
    Cursor getItem(int pos);
}
