package com.bingcoo.fileexplorer.interfaces;

/**
 * 类名称：OnBackPressedListener
 * 作者：David
 * 内容摘要：
 * 创建日期：2016/12/14
 * 修改者， 修改日期， 修改内容
 */
public interface OnBackPressedListener {
    public boolean onBackPressed();
}
