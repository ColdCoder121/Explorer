package com.bingcoo.fileexplorer.category.picture;

import android.content.Context;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bingcoo.fileexplorer.R;
import com.bingcoo.fileexplorer.home.FileInfoAdapter;
import com.bingcoo.fileexplorer.service.FileManagerService;
import com.bingcoo.fileexplorer.ui.MyExceptionImageView;
import com.bingcoo.fileexplorer.util.FileInfo;
import com.bingcoo.fileexplorer.util.FileInfoManager;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bumptech.glide.Glide;

import java.io.File;

/**
 * 类名称：PictureGridInfoAdapter
 * 作者：David
 * 内容摘要：
 * 创建日期：2017/2/4
 * 修改者， 修改日期， 修改内容
 */
public class PictureGridInfoAdapter extends FileInfoAdapter {
    private static final String TAG = "PictureGridInfoAdapter";

    static {
        LogUtils.setDebug(TAG, true);
    }

    /**
     * The constructor to construct a FileInfoAdapter.
     *
     * @param context            the context of FileManagerActivity
     * @param fileManagerService the service binded with FileManagerActivity
     * @param fileInfoManager    a instance of FileInfoManager, which manages all files.
     */
    public PictureGridInfoAdapter(Context context, FileManagerService fileManagerService, FileInfoManager fileInfoManager) {
        super(context, fileManagerService, fileInfoManager);
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        PictureViewHolder viewHolder;
        View view = convertView;

        if (view == null) {
            view = mInflater.inflate(R.layout.adapter_picture_info, null);
//            View picture_top_space = view.findViewById(R.id.picture_top_space);
//            if (pos <= 3) {
//                picture_top_space.setVisibility(View.VISIBLE);
//            } else {
//                picture_top_space.setVisibility(View.GONE);
//            }
            viewHolder = new PictureViewHolder(
                    (MyExceptionImageView) view.findViewById(R.id.iv_picture_grid_photo),
                    (RelativeLayout) view.findViewById(R.id.rl_picture_grid_masking),
                    (CheckBox) view.findViewById(R.id.cb_picture_grid_check));
            view.setTag(viewHolder);

            int dimension = getDimension();
            ViewGroup.LayoutParams lp = viewHolder.mPhoto.getLayoutParams();
            lp.width = lp.height = dimension;
            lp = viewHolder.mMasking.getLayoutParams();
            lp.width = lp.height = dimension;
        } else {
            viewHolder = (PictureViewHolder) view.getTag();
        }

        FileInfo currentItem = mFileInfoList.get(pos);
        boolean isChecked = currentItem.isChecked();
        if (isChecked) {
            viewHolder.mMasking.setVisibility(View.VISIBLE);
            viewHolder.mCheck.setChecked(true);
        } else {
            viewHolder.mMasking.setVisibility(View.GONE);
            viewHolder.mCheck.setChecked(false);
        }

        setImage(viewHolder.mPhoto, currentItem);

        return view;
    }

    private int getDimension() {
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        int dimension = displayMetrics.widthPixels - 3 * mContext.getResources().getDimensionPixelOffset(R.dimen.picture_space);
        dimension = dimension / 4;
        return dimension;
    }

    void setImage(ImageView iv, FileInfo fileInfo) {
        String imagePath = fileInfo.getFileAbsolutePath();
        if (!TextUtils.isEmpty(imagePath)) {
            LogUtils.d(TAG, "setImage: ");
            Glide.with(mContext)
                    .load(new File(imagePath))
                    .dontAnimate()
                    .placeholder(iv.getDrawable())
                    .centerCrop()
                    .into(iv);
        }
    }

    protected static class PictureViewHolder {
        ImageView mPhoto;
        ViewGroup mMasking;
        CheckBox mCheck;

        PictureViewHolder(ImageView photo, ViewGroup masking, CheckBox check) {
            mPhoto = photo;
            mMasking = masking;
            mCheck = check;
        }
    }

}
