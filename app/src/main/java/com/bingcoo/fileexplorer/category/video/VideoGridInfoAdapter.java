package com.bingcoo.fileexplorer.category.video;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bingcoo.fileexplorer.R;
import com.bingcoo.fileexplorer.home.FileInfoAdapter;
import com.bingcoo.fileexplorer.service.FileManagerService;
import com.bingcoo.fileexplorer.ui.MyExceptionImageView;
import com.bingcoo.fileexplorer.util.FileInfo;
import com.bingcoo.fileexplorer.util.FileInfoManager;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bumptech.glide.Glide;

import java.io.File;


/**
 * 类名称：VideoGridInfoAdapter
 * 作者：David
 * 内容摘要：
 * 创建日期：2017/2/9
 * 修改者， 修改日期， 修改内容
 */
public class VideoGridInfoAdapter extends FileInfoAdapter {
    private static final String TAG = "VideoGridInfoAdapter";

    static {
        LogUtils.setDebug(TAG, true);
    }

    public VideoGridInfoAdapter(Context context, FileManagerService fileManagerService, FileInfoManager fileInfoManager) {
        super(context, fileManagerService, fileInfoManager);
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        VideoViewHolder viewHolder;
        View view = convertView;
        if (null == view) {
            view = mInflater.inflate(R.layout.adapter_video_info, null);
//            View video_top_space = view.findViewById(R.id.video_top_space);
//            if (pos <= 1) {
//                video_top_space.setVisibility(View.VISIBLE);
//            } else {
//                video_top_space.setVisibility(View.GONE);
//            }
            viewHolder = new VideoViewHolder((MyExceptionImageView) view.findViewById(R.id.iv_photo),
                    (RelativeLayout) view.findViewById(R.id.rl_grid_masking),
                    (CheckBox) view.findViewById(R.id.cb_grid_check),
                    (TextView) view.findViewById(R.id.tv_name),
                    (TextView) view.findViewById(R.id.tv_desc));
            view.setTag(viewHolder);

            int width = getWidth();
            int height = getHeight();
            LogUtils.d(TAG, "getView: width=" + width + ", height=" + height);
            ViewGroup.LayoutParams lp = view.findViewById(R.id.fl_photo_container).getLayoutParams();
            lp.width = width;
            lp.height = height;
        } else {
            viewHolder = (VideoViewHolder) view.getTag();
        }

        FileInfo currentItem = mFileInfoList.get(pos);
        boolean isChecked = currentItem.isChecked();
        if (isChecked) {
            viewHolder.mMasking.setVisibility(View.VISIBLE);
            viewHolder.mCheck.setChecked(true);
        } else {
            viewHolder.mMasking.setVisibility(View.GONE);
            viewHolder.mCheck.setChecked(false);
        }

        viewHolder.mName.setText(currentItem.getShowName());
        String desc = currentItem.getDuration() + "  " + currentItem.getFileSizeStr();
        viewHolder.mDesc.setText(desc);
        setImage(viewHolder.mPhoto, currentItem);

        return view;
    }

    void setImage(final ImageView iv, FileInfo fileInfo) {
        String imagePath = fileInfo.getFileAbsolutePath();
        if (!TextUtils.isEmpty(imagePath)) {

                Glide.with(mContext)
                        .load(new File(imagePath))
                        .dontAnimate()
                        .placeholder(iv.getDrawable())
                        .centerCrop()
                        .into(iv);

        }
    }

    int getWidth() {
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        LogUtils.d(TAG, "getWidth: widthPixels=" + displayMetrics.widthPixels);
        int dimension = displayMetrics.widthPixels - 3 * mContext.getResources().getDimensionPixelOffset(R.dimen.video_space);
        dimension = dimension / 2;
        return dimension;
    }

    int getHeight() {
        int width = getWidth();
        Resources res = mContext.getResources();
        int height = Math.round(width * res.getDimensionPixelOffset(R.dimen.video_height) / res.getDimensionPixelOffset(R.dimen.video_width));
        return height;
    }

    private static class VideoViewHolder {
        ImageView mPhoto;
        ViewGroup mMasking;
        CheckBox mCheck;
        TextView mName;
        TextView mDesc;

        public VideoViewHolder(ImageView photo, ViewGroup masking, CheckBox check,
                               TextView name, TextView desc) {
            mPhoto = photo;
            mMasking = masking;
            mCheck = check;
            mName = name;
            mDesc = desc;
        }
    }

}
