package com.bingcoo.fileexplorer.category;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.media.MediaScannerConnection;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.storage.StorageVolume;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bingcoo.fileexplorer.R;
import com.bingcoo.fileexplorer.base.BaseActivity;
import com.bingcoo.fileexplorer.category.zip.OpenZipsActivity;
import com.bingcoo.fileexplorer.destination.DstActivity;
import com.bingcoo.fileexplorer.dialog.fragment.AlertDialogFragment;
import com.bingcoo.fileexplorer.dialog.fragment.ProgressDialog;
import com.bingcoo.fileexplorer.dialog.fragment.ProgressDialogFragment;
import com.bingcoo.fileexplorer.home.FileInfoAdapter;
import com.bingcoo.fileexplorer.receiver.MountReceiver;
import com.bingcoo.fileexplorer.service.FileManagerService;
import com.bingcoo.fileexplorer.service.HeavyOperationListener;
import com.bingcoo.fileexplorer.service.ProgressInfo;
import com.bingcoo.fileexplorer.util.DrmManager;
import com.bingcoo.fileexplorer.util.FileCategoryHelper;
import com.bingcoo.fileexplorer.util.FileInfo;
import com.bingcoo.fileexplorer.util.FileInfoManager;
import com.bingcoo.fileexplorer.util.FileUtils;
import com.bingcoo.fileexplorer.util.IntentUtils;
import com.bingcoo.fileexplorer.util.KeyboardUtils;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.MediaUtils;
import com.bingcoo.fileexplorer.util.MountPointManager;
import com.bingcoo.fileexplorer.util.PermissionUtils;
import com.bingcoo.fileexplorer.util.RingtoneUtils;
import com.bingcoo.fileexplorer.util.SystemUtils;
import com.jakewharton.rxbinding.widget.RxTextView;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import bingo.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.functions.Action1;

import static com.bingcoo.fileexplorer.category.picture.PictureGridActivity.SAVED_SHOW_MORE_DIALOG;
import static com.bingcoo.fileexplorer.category.picture.PictureGridActivity.SAVED_SHOW_OPTION_DIALOG;
import static com.bingcoo.fileexplorer.category.video.VideoGridActivity.SAVED_ADAPTER_MODE;
import static com.bingcoo.fileexplorer.category.video.VideoGridActivity.SAVED_CHECKED_LIST;
import static com.bingcoo.fileexplorer.category.video.VideoGridActivity.SAVED_FILE_LIST;
import static com.bingcoo.fileexplorer.category.video.VideoGridActivity.SAVED_LIST_CHANGED;
import static com.bingcoo.fileexplorer.category.video.VideoGridActivity.SAVED_SHOW_LIST;
import static com.bingcoo.fileexplorer.category.zip.OpenZipsActivity.FileType.RAR;
import static com.bingcoo.fileexplorer.category.zip.OpenZipsActivity.FileType.ZIP;
import static com.bingcoo.fileexplorer.home.HomeActivity.MY_PERMISSIONS_REQUEST_TO_READ_EXTERNAL_STORAGE;
import static com.bingcoo.fileexplorer.util.FileCategoryHelper.FileCategory.Apk;
import static com.bingcoo.fileexplorer.util.FileCategoryHelper.FileCategory.Bluetooth;
import static com.bingcoo.fileexplorer.util.FileCategoryHelper.FileCategory.Doc;
import static com.bingcoo.fileexplorer.util.FileCategoryHelper.FileCategory.Download;
import static com.bingcoo.fileexplorer.util.FileCategoryHelper.FileCategory.Music;
import static com.bingcoo.fileexplorer.util.FileCategoryHelper.FileCategory.Zip;

/**
 * 类名称：ListActivity
 * 作者：David
 * 内容摘要：
 * 创建日期：2017/1/12
 * 修改者， 修改日期， 修改内容
 */
public class ListActivity extends BaseActivity {
    private static final String TAG = "ListActivity";

    static {
        LogUtils.setDebug(TAG, true);
    }

    public static final String EXTRA_TITLE = "extra_title";
    public static final String EXTRA_ZIPS_PATH = "extra_zips_path";
    public static final String EXTRA_ZIPS_NAME = "extra_zips_name";
    public static final String EXTRA_ZIPS_TYPE = "extra_zips_type";
    public static final String EXTRA_ZIPS_PARENT = "extra_zips_parent";

    public static final String DELETE_DIALOG_TAG = "delete_dialog_fragment_tag";
    public static final String RENAME_DIALOG_TAG = "rename_dialog_fragment_tag";
    public static final String AUDIO_SETUP_DIALOG_TAG = "audio_setup_dialog_fragment_tag";

    private static final String SAVED_SELECTED_PATH_KEY = "saved_selected_path";
    public static final String SAVED_SEARCH_TEXT = "search_text";
    public static final String SAVED_SEARCH_TOTAL = "search_total";
    public static final String SAVED_HAS_RESULT = "has_result";

    public static final String EXTRA_LOCAL_DIRECTORY = "extra_local_directory";
    private static final String NEW_FILE_PATH_KEY = "new_file_path_key";

    private static final int REQUEST_CODE_PASTE = 0x100;
    private static final int REQUEST_CODE_BROWSE = 0x101;

    public static final int MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_DELETE = 5;
    public static final int MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_CUT = 6;
    public static final int MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_RENAME = 7;
    public static final int MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_COPY = 8;
    public static final int MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_ZIP = 9;
    private int mFileClickPos;

    protected FileInfo mSelectedFileInfo = null;
    protected int mSelectedTop = -1;
    protected int mTop = -1;
    private String mTitle = null;
    private FileCategoryHelper.FileCategory mFileCategory = null;
    protected boolean mIsAlertDialogShowing = false;
    private AlertDialog mOptionsDialog;
    private AlertDialog mMoreDialog;
    private String mSearchText;
    private long mSearchTotal = 0;
    private long mSearchTotalChanged;//搜索成功不重置
    private int mRingType = RingtoneManager.TYPE_RINGTONE;
    private ProgressDialog mRefreshingProgressDialog;
    private String mRenamedName;
    private FileInfo mRenamedItem;
    private List<FileInfo> mShowFileList = new ArrayList<>();//正常状态列表，搜索状态切换回来后如果没有改动不刷新
    private List<FileInfo> mFileListWhenOut = new ArrayList<>();//退出时列表
    private boolean mListChanged;//搜索状态是否有改动
    private boolean mEnterSearch;//是否进入搜索状态
    private float mScreenWidth;
    private boolean hasResult;//是否startactivtiyforresult

    @BindView(R.id.img_dst_back)
    ImageView mImgBack;
    @BindView(R.id.tv_dst_title)
    TextView mTvTitle;
    @BindView(R.id.lst_media)
    ListView mLstMedia;
    @BindView(R.id.layout_loading)
    View mLayoutLoading;
    @BindView(R.id.empty_view)
    View mLayoutEmptyView;

    @BindView(R.id.layout_edit)
    View mBottomEdit;
    @BindView(R.id.layout_menu)
    View mBottomMenu;
    @BindView(R.id.ll_menu)
    View mViewMenu;

    @BindView(R.id.layout_action_mode_search)
    View mLayoutActionModeSearch;
    @BindView(R.id.layout_action_mode_title)
    View mLayoutActionModeTitle;
    @BindView(R.id.layout_action_mode_select)
    View mLayoutActionModeSelect;
    @BindView(R.id.search_container)
    View mSearchContainer;//搜索框外面的framelayout

    @BindView(R.id.img_search_back)
    ImageView mImgSearchBack;
    @BindView(R.id.et_search_input)
    EditText mEtSearchInput;
    @BindView(R.id.img_search)
    ImageView mImgSearch;
    @BindView(R.id.layout_local_search_result)
    View mLayoutSearchResult;
    @BindView(R.id.tv_search_result)
    TextView mTvSearchResult;

    @BindView(R.id.layout_list)
    View mLayoutList;
    @BindView(R.id.layout_searching)
    View mLayoutSearching;
    @BindView(R.id.layout_no_matching_search)
    View mLayoutNoMatchingSearch;

    @BindView(R.id.tv_select_all)
    TextView mTvSelectAll;
    @BindView(R.id.tv_select_chosen)
    TextView mTvSelectChosen;
    @BindView(R.id.tv_select_cancel)
    TextView mTvSelectCancel;

    @BindView(R.id.tv_share)
    TextView mTvShare;
    @BindView(R.id.tv_delete)
    TextView mTvDelete;
    @BindView(R.id.tv_cut)
    TextView mTvCut;
    @BindView(R.id.tv_more)
    TextView mTvMore;

    private HeavyOperationListener.HeavyOperationListenerCallback mHeavyOperationListenerCallback =
            new HeavyOperationListener.HeavyOperationListenerCallback() {

                @Override
                public void onTaskResult(int errorType) {
                    mFileInfoManager.updateFileInfoList(mSortType);
                    mAdapter.notifyDataSetChanged();
                    mSearchTotalChanged = mAdapter.getCount();
                    mTvSearchResult.setText(getResources().getString(
                            R.string.search_result, mSearchTotalChanged));
                    if (isEmpty()) {
                        switchLayout(mLayoutEmptyView);
                    }
                    if (mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
                        onBackPressed();
                    }
                }

                @Override
                public void onClick(View v) {
                    if (mService != null) {
                        LogUtils.i(TAG, "onClick cancel");
                        mService.cancel(ListActivity.this.getClass().getName());
                    }
                }
            };

    /**
     * 操作文件后判断列表是否为空
     *
     * @return
     */
    private boolean isEmpty() {
        if (mAdapter != null) {
            int cnt = mAdapter.getCount();
            if (cnt == 0) {
                return true;
            } else {
                for (int i = 0; i < cnt; i++) {
                    if (mAdapter.getItem(i).getFileName().startsWith(".")) {
                        continue;
                    } else {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * service connected在onresume后执行
     * onactivityresult在onstart后执行
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        LogUtils.e(TAG, "list onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        ButterKnife.bind(this);
        mSavedInstanceState = savedInstanceState;
        init();
    }

    @Override
    protected void onStart() {
        LogUtils.e(TAG, "list onStart");
        super.onStart();
    }

    @Override
    protected void onResume() {
        LogUtils.e(TAG, "list onResume");
        super.onResume();
        if (!PermissionUtils.hasStorageReadPermission(this)) {
            /*PermissionUtils.requestPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    MY_PERMISSIONS_REQUEST_TO_READ_EXTERNAL_STORAGE);*/
//            ActivityManager.getInstance().exit();
            finish();
        }
        mScreenWidth = SystemUtils.getScreenWidth(this);
        LogUtils.e(TAG, "screenWidth=:" + mScreenWidth);
        setSearchInputWidth();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LogUtils.e(TAG, "onConfigurationChanged");
        float newScreenWidth = SystemUtils.getScreenWidth(this);
        if (newScreenWidth != mScreenWidth) {
            mScreenWidth = newScreenWidth;
            setSearchInputWidth();
        }
    }

    private void setSearchInputWidth() {
        int mTargetDensity = Resources.getSystem().getDisplayMetrics().densityDpi;
        float etWidth = SystemUtils.dp2px(290);
        LogUtils.e(TAG, "etWidth=:" + etWidth);
        int mBackIconWidth = mTargetDensity * 120 / 480;
        LogUtils.e(TAG, "mBackIconWidth=:" + mBackIconWidth);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mSearchContainer.getLayoutParams();
        if (etWidth + mBackIconWidth + SystemUtils.dp2px(26) < mScreenWidth) {
            params.width = (int) etWidth;
        }
        mSearchContainer.setLayoutParams(params);
    }

    @Override
    public void onDialogDismiss() {
        LogUtils.d(TAG, "dialog dismissed...");
        mIsAlertDialogShowing = false;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        LogUtils.e(TAG, "list onSaveInstanceState");
        if (mService!=null&&(mService.isSearchTask(this.getClass().getName()) ||
                mService.isListTask(this.getClass().getName()))) {
            mService.cancel(this.getClass().getName());
        }
        if (mAdapter != null && mAdapter.getCheckedItemsCount() == 1) {
            FileInfo selectFileInfo = mAdapter.getCheckedFileInfoItemsList().get(0);
            if (selectFileInfo != null) {
                outState.putString(SAVED_SELECTED_PATH_KEY, selectFileInfo.getFileAbsolutePath());
            }
        }

//        if (View.VISIBLE == mLayoutActionModeSearch.getVisibility()) {
        outState.putString(SAVED_SEARCH_TEXT, mSearchText);
        outState.putLong(SAVED_SEARCH_TOTAL, mSearchTotalChanged);
//        }
        if (mFileInfoManager != null) {
            mFileListWhenOut.clear();
            mFileListWhenOut.addAll(mFileInfoManager.getShowFileList());
            outState.putParcelableArrayList(SAVED_FILE_LIST, (ArrayList<? extends Parcelable>) mFileListWhenOut);

        }
        if (mAdapter != null) {
            outState.putInt(SAVED_ADAPTER_MODE, mAdapter.getMode());
            outState.putParcelableArrayList(SAVED_CHECKED_LIST, (ArrayList<? extends Parcelable>) mAdapter.getCheckedFileInfoItemsList());
        }
        outState.putParcelableArrayList(SAVED_SHOW_LIST, (ArrayList<? extends Parcelable>) mShowFileList);
        outState.putBoolean(SAVED_LIST_CHANGED, mListChanged);
        outState.putBoolean(SAVED_HAS_RESULT, hasResult);
        if (mOptionsDialog != null && mOptionsDialog.isShowing()) {
            outState.putBoolean(SAVED_SHOW_OPTION_DIALOG, true);
        } else {
            outState.putBoolean(SAVED_SHOW_OPTION_DIALOG, false);
        }
        if (mMoreDialog != null && mMoreDialog.isShowing()) {
            outState.putBoolean(SAVED_SHOW_MORE_DIALOG, true);
        } else {
            outState.putBoolean(SAVED_SHOW_MORE_DIALOG, false);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        mFileClickPos = position;
        if (!PermissionUtils.hasStorageWritePermission(ListActivity.this)) {
            PermissionUtils.requestPermission(ListActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_ZIP);
            return;
        }
        onItemClick(position);
    }

    private void onItemClick(int position) {
        LogUtils.d(TAG, "onItemClick: position=" + position + ", mode=" + mAdapter.getMode());
        if (mService != null && mService.isBusy(this.getClass().getName())) {
            LogUtils.d(TAG, "onItemClick, service is busy,return. ");
            return;
        }

        if (null != mAdapter) {
            if (position >= mAdapter.getCount() || position < 0) {
                LogUtils.e(TAG, "onItemClick, events error, mFileInfoList.size()= "
                        + mAdapter.getCount());
                return;
            }
            if (mAdapter.isMode(FileInfoAdapter.MODE_NORMAL)
                    || mAdapter.isMode(FileInfoAdapter.MODE_SEARCH)) {

               /* if (View.VISIBLE == mLayoutActionModeSearch.getVisibility()) {
                    switchTitle(mLayoutActionModeTitle);
                    mAdapter.changeMode(FileInfoAdapter.MODE_NORMAL);
                    mLayoutSearchResult.setVisibility(View.GONE);
                    showContent();
                }*/

                FileInfo selectedItem = mAdapter.getItem(position);
                if (null != selectedItem) {
                    if (!selectedItem.isDirectory()) {
                        String path = selectedItem.getFileAbsolutePath();
                        String parentPath = selectedItem.getFileParentPath();
                        String fileName = selectedItem.getFileName();
                        if (!TextUtils.isEmpty(path)) {
                            if (path.endsWith(".zip")) {
                                openZipFile(path, parentPath, fileName.substring(0, fileName.lastIndexOf(".")), ZIP);
                            } else if (path.endsWith(".rar")) {
                                openZipFile(path, parentPath, fileName.substring(0, fileName.lastIndexOf(".")), RAR);
                            } else {
                                openFile(selectedItem);
                            }
                        }
                    }
                }

            } else if (mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
                boolean state = mAdapter.getItem(position).isChecked();
                LogUtils.d(TAG, "onItemClick, edit view . position=" + position + ", state=" + state);
                mAdapter.setChecked(position, !state);
                mAdapter.notifyDataSetChanged();
                mTvSelectChosen.setText(getResources().getString(
                        R.string.select_chosen, mAdapter.getCheckedItemsCount()));
                boolean isSelectedAll = mAdapter.getCheckedItemsCount() == mAdapter.getCount();
                mTvSelectAll.setText(isSelectedAll ? R.string.select_not_all : R.string.select_all);
                handleEditView();
            }
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        LogUtils.d(TAG, "onItemLongClick: position=" + position);
        if (mAdapter.isMode(FileInfoAdapter.MODE_NORMAL)
                || mAdapter.isMode(FileInfoAdapter.MODE_SEARCH)) {
            if (!mService.isBusy(this.getClass().getName())) {
                int top = view.getTop();
                switchToEditView(position, top);
                return true;
            }
        }
        return false;
    }

    private void searchAndEditToNormal(){
        mAdapter.changeMode(FileInfoAdapter.MODE_NORMAL);
        switchTitle(mLayoutActionModeTitle);
        switchContent(mLayoutList);
        mLayoutSearchResult.setVisibility(View.GONE);
        mSearchText = null;
        mEnterSearch = false;
    }

    @Override
    public void onBackPressed() {
        if (mAdapter != null && mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
            mAdapter.changeMode(FileInfoAdapter.MODE_NORMAL);
            if (View.VISIBLE == mLayoutSearchResult.getVisibility()) {
                switchTitle(mLayoutActionModeSearch);
            } else {
                switchTitle(mLayoutActionModeTitle);
            }
            return;
        }

        if (View.VISIBLE == mLayoutActionModeSearch.getVisibility()) {
            switchTitle(mLayoutActionModeTitle);
            switchContent(mLayoutList);
            mAdapter.changeMode(FileInfoAdapter.MODE_NORMAL);
            mLayoutSearchResult.setVisibility(View.GONE);
            if (mListChanged) {
                showContent();
                mListChanged = false;
            } else {
                if (mShowFileList.size() > 0) {
                    mFileInfoManager.getShowFileList().clear();
                    mFileInfoManager.getShowFileList().addAll(mShowFileList);
                    mAdapter.notifyDataSetChanged();
                } else {
                    showContent();
                }
            }
            mSearchText = null;
            mEnterSearch = false;
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        LogUtils.e(TAG, "onActivityResult: requestCode=" + requestCode + ";resultCode=" + resultCode);
        /*if (RESULT_OK == resultCode) {
            switch (requestCode) {
                case REQUEST_CODE_PASTE:
                    if (mEnterSearch) {
                        mListChanged = true;
                    } else {
                        showContent();
                    }
                    exitEditMode();
                    break;

                default:
                    break;
            }
        }*/
        if (REQUEST_CODE_PASTE == requestCode) {
            hasResult = false;
            exitEditMode();
            if (RESULT_OK == resultCode) {
                if (mEnterSearch) {
//                    mListChanged = true;
                    searchAndEditToNormal();
                }/* else {*/
                    showContent();
//                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void exitEditMode() {
        if (mAdapter != null && mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
            mAdapter.changeMode(FileInfoAdapter.MODE_NORMAL);
            if (View.VISIBLE == mLayoutSearchResult.getVisibility()) {
                switchTitle(mLayoutActionModeSearch);
            } else {
                switchTitle(mLayoutActionModeTitle);
            }
        }
    }

    private void init() {
        Intent intent = getIntent();
        int id = intent.getIntExtra(EXTRA_TITLE, 0);
        if (0 < id) {
            mTitle = getString(id);
            mTvTitle.setText(mTitle);
            mFileCategory = getFileCategory();
        }

        //标题栏返回
        setViewClickListener(mImgBack,
                new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                });

        //底部菜单
        setViewClickListener(mViewMenu,
                new Runnable() {
                    @Override
                    public void run() {
                        // TODO: 2017/6/21 test
                        showOptionDialog();
                    }
                });

        //搜索返回
        setViewClickListener(mImgSearchBack,
                new Runnable() {
                    @Override
                    public void run() {
                        LogUtils.d(TAG, "ImgSearchBack run: ");
                        KeyboardUtils.hideSoftInput(ListActivity.this);
                        onBackPressed();
                    }
                });

        //键盘搜索
        RxTextView.editorActions(mEtSearchInput)
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer actionId) {
                        LogUtils.d(TAG, "searchInput call: actionId=" + actionId
                                + ", IME_ACTION_SEARCH=" + EditorInfo.IME_ACTION_SEARCH);
                        if (EditorInfo.IME_ACTION_SEARCH == actionId) {
                            KeyboardUtils.hideSoftInput(ListActivity.this);
                            requestSearch(mEtSearchInput.getText().toString());
                        }
                    }
                });

        //放大镜搜索
        setViewClickListener(mImgSearch,
                new Runnable() {
                    @Override
                    public void run() {
                        LogUtils.d(TAG, "ImgSearch run: ");
                        requestSearch(mEtSearchInput.getText().toString());
                    }
                });

        //全选
        setViewClickListener(mTvSelectAll,
                new Runnable() {
                    @Override
                    public void run() {
                        if (null != mAdapter) {
                            boolean isSelectedAll = mAdapter.getCheckedItemsCount() == mAdapter.getCount();
                            mAdapter.setAllItemChecked(!isSelectedAll);

                            mTvSelectChosen.setText(
                                    getResources().getString(R.string.select_chosen, mAdapter.getCheckedItemsCount()));
                            mTvSelectAll.setText(isSelectedAll ? R.string.select_all : R.string.select_not_all);
                            handleEditView();

                            handleEditView();
                        }
                    }
                });

        //取消选择
        setViewClickListener(mTvSelectCancel,
                new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                    }
                });

        if (null != mLstMedia) {

            mLstMedia.setOnItemClickListener(this);
            mLstMedia.setOnItemLongClickListener(this);
            mLstMedia.setFastScrollEnabled(false);
            mLstMedia.setVerticalScrollBarEnabled(true);
            View footer = getLayoutInflater().inflate(R.layout.layout_lst_footer, mLstMedia, false);
            mLstMedia.addFooterView(footer);
        }

        initEditView();
        switchLayout(mLstMedia);
        switchTitle(mLayoutActionModeTitle);
        switchContent(mLayoutList);
    }

    private void initEditView() {
        //分享
        setViewClickListener(mTvShare,
                new Runnable() {
                    @Override
                    public void run() {
                        if (null != mAdapter) {
                            FileInfo fileInfo = mAdapter.getFirstCheckedFileInfoItem();
                            if (null != fileInfo) {
                                boolean isSharingIntentStarted =
                                        IntentUtils.share(ListActivity.this, fileInfo.getFileAbsolutePath());
                                if (!isSharingIntentStarted) {
                                    mToastHelper.showToast(R.string.no_method);
                                }
                            }
                        }
                    }
                });

        //删除
        setViewClickListener(mTvDelete,
                new Runnable() {
                    @Override
                    public void run() {
                        if (!PermissionUtils.hasStorageWritePermission(ListActivity.this)) {
                            PermissionUtils.requestPermission(ListActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_DELETE);
                            return;
                        }
                        showDeleteDialog();
                    }
                });

        //剪切
        setViewClickListener(mTvCut,
                new Runnable() {
                    @Override
                    public void run() {
                        if (!PermissionUtils.hasStorageWritePermission(ListActivity.this)) {
                            PermissionUtils.requestPermission(ListActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_CUT);
                            return;
                        }
                        Intent intent = new Intent(getApplicationContext(), DstActivity.class);
                        intent.putExtra(DstActivity.EXTRA_TYPE, FileInfoManager.PASTE_MODE_CUT);
                        intent.putExtra(DstActivity.EXTRA_LIST, (Serializable) mAdapter.getCheckedFileInfoItemsList());
                        startActivityForResult(intent, REQUEST_CODE_PASTE);
                        hasResult = true;
                    }
                });

        //更多
        setViewClickListener(mTvMore,
                new Runnable() {
                    @Override
                    public void run() {
                        showMoreDialog();
                    }
                });
    }

    private void switchTitle(View title) {
        if (null != title) {
            title.setVisibility(View.VISIBLE);

            if (title != mLayoutActionModeSearch) {
                mLayoutActionModeSearch.setVisibility(View.GONE);
            } else {
                switchBottomBar(null);
            }

            if (title != mLayoutActionModeTitle) {
                mLayoutActionModeTitle.setVisibility(View.GONE);
            } else {
                switchBottomBar(mBottomMenu);
            }

            if (title != mLayoutActionModeSelect) {
                mLayoutActionModeSelect.setVisibility(View.GONE);
            } else {
                switchBottomBar(mBottomEdit);
            }
        }
    }

    private void switchContent(View content) {
        if (null != content) {
            content.setVisibility(View.VISIBLE);

            if (content != mLayoutList) {
                mLayoutList.setVisibility(View.GONE);
            }

            if (content != mLayoutSearching) {
                mLayoutSearching.setVisibility(View.GONE);
            }

            if (content != mLayoutNoMatchingSearch) {
                mLayoutNoMatchingSearch.setVisibility(View.GONE);
            }
        }
    }

    private void switchLayout(View layout) {
        if (null != layout) {
            layout.setVisibility(View.VISIBLE);

            if (layout != mLstMedia) {
                mLstMedia.setVisibility(View.GONE);
            }

            if (layout != mLayoutLoading) {
                mLayoutLoading.setVisibility(View.GONE);
            }

            if (layout != mLayoutEmptyView) {
                mLayoutEmptyView.setVisibility(View.GONE);
            }
        }
    }

    private void switchBottomBar(View bottomBar) {
        if (null != bottomBar) {
            bottomBar.setVisibility(View.VISIBLE);

            if (bottomBar != mBottomEdit) {
                mBottomEdit.setVisibility(View.GONE);
            }

            if (bottomBar != mBottomMenu) {
                mBottomMenu.setVisibility(View.GONE);
            }
        } else {
            mBottomEdit.setVisibility(View.GONE);
            mBottomMenu.setVisibility(View.GONE);
        }
    }

    protected void serviceConnected() {
        LogUtils.e(TAG, "list serviceConnected");
        super.serviceConnected();

        mFileInfoManager.getShowFileList().clear();
        mAdapter = new FileInfoAdapter(this, mService, mFileInfoManager);
        mAdapter.setType(FileInfoAdapter.TYPE_CATEGORY);
        mEtSearchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mLayoutActionModeSearch != null && mLayoutActionModeSearch.getVisibility() == View.VISIBLE) {
                    requestSearch(s.toString());
                }
            }
        });
        if (null != mLstMedia) {
            mLstMedia.setAdapter(mAdapter);
            if (null == mSavedInstanceState) {
                showContent();
            } else {
                hasResult = mSavedInstanceState.getBoolean(SAVED_HAS_RESULT);
                mSearchText = mSavedInstanceState.getString(SAVED_SEARCH_TEXT);
                mFileListWhenOut = mSavedInstanceState.getParcelableArrayList(SAVED_FILE_LIST);
                mShowFileList = mSavedInstanceState.getParcelableArrayList(SAVED_SHOW_LIST);
                mListChanged = mSavedInstanceState.getBoolean(SAVED_LIST_CHANGED);
                int adapterMode = mSavedInstanceState.getInt(SAVED_ADAPTER_MODE);
                mAdapter.changeMode(adapterMode);
                if (!TextUtils.isEmpty(mSearchText)) {
                    mSearchTotalChanged = mSavedInstanceState.getLong(SAVED_SEARCH_TOTAL);
                    mLayoutSearchResult.setVisibility(View.VISIBLE);
                    mTvSearchResult.setText(getResources().getString(
                            R.string.search_result, mSearchTotalChanged));
                    mEtSearchInput.setText(mSearchText);
                    mEtSearchInput.setSelection(mSearchText.length());
                    if (mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
                        //搜索下的编辑模式
                        List<FileInfo> checkedList = mSavedInstanceState.getParcelableArrayList(SAVED_CHECKED_LIST);
                        if (mFileListWhenOut.size() > 0 && checkedList != null) {
                            mFileInfoManager.getShowFileList().addAll(mFileListWhenOut);
                            for (FileInfo info : checkedList) {
                                info.setChecked(true);
                            }
                            mAdapter.notifyDataSetChanged();
                            switchTitle(mLayoutActionModeSelect);
                            mTvSelectChosen.setText(getResources().getString(
                                    R.string.select_chosen, mAdapter.getCheckedItemsCount()));
                            boolean isSelectedAll = mAdapter.getCheckedItemsCount() == mAdapter.getCount();
                            mTvSelectAll.setText(isSelectedAll ? R.string.select_not_all : R.string.select_all);
                            handleEditView();
                            if (hasResult) {
                                //搜索模式下复制成功
                                hasResult = false;
                                exitEditMode();
//                                mListChanged = true;
                                searchAndEditToNormal();
                                showContent();
                            }
                        } else {
                            searchAndEditToNormal();
                            showContent();
                        }
                    } else {
                        //正常搜索模式
                        switchTitle(mLayoutActionModeSearch);
                        if (mFileListWhenOut.size() > 0) {
                            mFileInfoManager.getShowFileList().addAll(mFileListWhenOut);
                            mAdapter.notifyDataSetChanged();
                            if (0 == mSearchTotalChanged) {
                                switchContent(mLayoutNoMatchingSearch);
                            } else {
                                switchContent(mLayoutList);
                            }
                        } else {
                            requestSearch(mSearchText);
                        }
                    }
                } else {
                    if (mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
                        //非搜索下的编辑模式
                        List<FileInfo> checkedList = mSavedInstanceState.getParcelableArrayList(SAVED_CHECKED_LIST);
                        if (mFileListWhenOut.size() > 0 && checkedList != null) {
                            mFileInfoManager.getShowFileList().addAll(mFileListWhenOut);
                            for (FileInfo info : checkedList) {
                                info.setChecked(true);
                            }
                            mAdapter.notifyDataSetChanged();
                            switchTitle(mLayoutActionModeSelect);
                            mTvSelectChosen.setText(getResources().getString(
                                    R.string.select_chosen, mAdapter.getCheckedItemsCount()));
                            boolean isSelectedAll = mAdapter.getCheckedItemsCount() == mAdapter.getCount();
                            mTvSelectAll.setText(isSelectedAll ? R.string.select_not_all : R.string.select_all);
                            handleEditView();
                            if (hasResult) {
                                //正常模式下复制成功
                                hasResult = false;
                                exitEditMode();
                                showContent();
                            }
                        } else {
                            exitEditMode();
                            showContent();
                        }
                    } else {
                        if (mFileListWhenOut.size() > 0) {
                            mFileInfoManager.getShowFileList().addAll(mFileListWhenOut);
                            mAdapter.notifyDataSetChanged();
                            if (0 == mAdapter.getCount()) {
                                switchLayout(mLayoutEmptyView);
                            } else {
                                switchLayout(mLstMedia);
                            }
                        } else {
                            showContent();
                        }
                    }
                }
                restoreDialog();
            }

            mAdapter.notifyDataSetChanged();
        }

        // register Receiver when service connected..
        if (null == mMountReceiver) {
            mMountReceiver = MountReceiver.registerMountReceiver(this);
            mMountReceiver.registerMountListener(this);
        }
    }

    private void showContent() {
        if (isFinishing()) {
            LogUtils.i(TAG, "showContent, isFinishing: true, do not loading again");
            return;
        }

        if ((null != mService) && (null != mFileCategory)) {
            mService.listFiles(this.getClass().getName(), mFileCategory, mSortType, new LstListListener());
        }
    }

    private int restoreSelectedPosition() {
        if (mSelectedFileInfo == null) {
            return -1;
        } else {
            int curSelectedItemPosition = mAdapter.getPosition(mSelectedFileInfo);
            mSelectedFileInfo = null;
            return curSelectedItemPosition;
        }
    }

    private FileCategoryHelper.FileCategory getFileCategory() {
        if (mTitle.equals(getString(R.string.category_document))) {
            return Doc;
        } else if (mTitle.equals(getString(R.string.category_music))) {
            return Music;
        } else if (mTitle.equals(getString(R.string.category_bluetooth))) {
            return Bluetooth;
        } else if (mTitle.equals(getString(R.string.category_download))) {
            return Download;
        } else if (mTitle.equals(getString(R.string.category_apk))) {
            return Apk;
        } else if (mTitle.equals(getString(R.string.category_zip))) {
            return Zip;
        }
        return null;
    }

    /**
     * 打开zip或rar列表
     *
     * @param path
     * @param fileName
     */
    private void openZipFile(String path, String parentPath, String fileName, OpenZipsActivity.FileType type) {

        if (path != null) {
            Intent intent = new Intent(ListActivity.this, OpenZipsActivity.class);
            intent.putExtra(EXTRA_ZIPS_PATH, path);
            intent.putExtra(EXTRA_ZIPS_NAME, fileName);
            intent.putExtra(EXTRA_ZIPS_TYPE, type);
            intent.putExtra(EXTRA_ZIPS_PARENT, parentPath);
            startActivity(intent);
        }

    }

    private void openFile(FileInfo selectedItem) {
        if (null == selectedItem) {
            return;
        }
        boolean canOpen = true;
        String mimeType = selectedItem.getFileMimeType(mService);
        LogUtils.e(TAG, "mimeType=:" + mimeType);
        if (selectedItem.isDrmFile()) {
            mimeType = DrmManager.getInstance().getOriginalMimeType(
                    selectedItem.getFileAbsolutePath());

            if (TextUtils.isEmpty(mimeType)) {
                canOpen = false;
                mToastHelper.showToast(R.string.msg_unable_open_file);
            }
        }
        if (canOpen) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = selectedItem.getUri();
            LogUtils.d(TAG, "openFile, Open uri file: " + uri);
            LogUtils.e(TAG, "mFileCategory=:" + mFileCategory);
            if (mFileCategory == Music) {
                intent.setDataAndType(uri, "audio/mp3");
            } else {
                intent.setDataAndType(uri, mimeType);
            }
            try {
                startActivityForResult(intent, REQUEST_CODE_BROWSE);
            } catch (android.content.ActivityNotFoundException e) {
                //mToastHelper.showToast(R.string.msg_unable_open_file);
                showOpenMethodDialog(selectedItem);
                LogUtils.w(TAG, "openFile, Cannot open file: "
                        + selectedItem.getFileAbsolutePath());
            }
        }
    }

    protected void restoreDialog() {
        // Restore the heavy_dialog : pasting deleting
        ProgressDialogFragment pf = (ProgressDialogFragment) getFragmentManager()
                .findFragmentByTag(HeavyOperationListener.HEAVY_DIALOG_TAG);
        if (pf != null) {
            if (mService.isBusy(this.getClass().getName())
                    && mService.isHeavyOperationTask(this.getClass().getName())) {
                HeavyOperationListener listener = new HeavyOperationListener(
                        AlertDialogFragment.INVIND_RES_ID, this, mHeavyOperationListenerCallback);
                mService.reconnected(this.getClass().getName(), listener);
                pf.setCancelListener(listener);
            } else {
                pf.dismissAllowingStateLoss();
            }
        }

        // list dialog
        DialogFragment listFramgent = (DialogFragment) getFragmentManager().findFragmentByTag(
                ListListener.LIST_DIALOG_TAG);
        if (listFramgent != null) {
            LogUtils.i(TAG, "listFramgent != null");
            if (mService.isBusy(this.getClass().getName())) {
                LogUtils.i(TAG, "list reconnected mService");
                mService.reconnected(this.getClass().getName(), new LstListListener());
            } else {
                LogUtils.i(TAG, "the list is complete dismissAllowingStateLoss");
                listFramgent.dismissAllowingStateLoss();
            }
        }

        // sort dialog
        AlertDialogFragment.ChoiceDialogFragment sortDialogFragment = (AlertDialogFragment.ChoiceDialogFragment) getFragmentManager()
                .findFragmentByTag(AlertDialogFragment.ChoiceDialogFragment.CHOICE_DIALOG_TAG);
        if (sortDialogFragment != null) {
            sortDialogFragment.setItemClickListener(new SortClickListener());
        }

        String saveSelectedPath = mSavedInstanceState.getString(SAVED_SELECTED_PATH_KEY);
        FileInfo saveSelectedFile = null;
        if (saveSelectedPath != null) {
            saveSelectedFile = new FileInfo(saveSelectedPath);
        }

        // audio setup dialog
        AlertDialogFragment.ChoiceDialogFragment audioSetupDialogFragment = (AlertDialogFragment.ChoiceDialogFragment) getFragmentManager()
                .findFragmentByTag(AUDIO_SETUP_DIALOG_TAG);
        if (sortDialogFragment != null) {
            AudioSetupClickListener setupClickListener = new AudioSetupClickListener();
            setupClickListener.setFile(new File(saveSelectedPath));
            audioSetupDialogFragment.setItemClickListener(setupClickListener);
        }

        // rename dialog
        AlertDialogFragment.EditTextDialogFragment renameDialogFragment = (AlertDialogFragment.EditTextDialogFragment) getFragmentManager()
                .findFragmentByTag(RENAME_DIALOG_TAG);
        if (renameDialogFragment != null && saveSelectedFile != null) {
            renameDialogFragment
                    .setOnEditTextDoneListener(new RenameDoneListener(saveSelectedFile));
        }

        AlertDialogFragment af;
        // delete dialog
        af = (AlertDialogFragment) getFragmentManager().findFragmentByTag(DELETE_DIALOG_TAG);
        if (af != null) {
            af.setOnDoneListener(new DeleteListener());
        }
        if (mSavedInstanceState != null && mSavedInstanceState.getBoolean(SAVED_SHOW_MORE_DIALOG)) {
            showMoreDialog();
        }
        if (mSavedInstanceState != null && mSavedInstanceState.getBoolean(SAVED_SHOW_OPTION_DIALOG)) {
            showOptionDialog();
        }
        /*// Restore the detail_dialog
        af = (AlertDialogFragment) getFragmentManager().findFragmentByTag(
                DetailInfoListener.DETAIL_DIALOG_TAG);
        if (af != null && saveSelectedFile != null && mService != null) {
            DetailInfoListener listener = new DetailInfoListener(saveSelectedFile);
            af.setDismissListener(listener);
            if (mService.isBusy(this.getClass().getName()) && mService.isDetailTask(this.getClass().getName())) {
                mService.reconnected(this.getClass().getName(), listener);
            } else if (!mService.isBusy(this.getClass().getName())) {
                af.dismissAllowingStateLoss();
                mService.getDetailInfo(this.getClass().getName(), saveSelectedFile, listener);
            } else {
                af.dismissAllowingStateLoss();
            }
        } else if (af != null && saveSelectedFile == null) {
            af.dismissAllowingStateLoss();
            mIsAlertDialogShowing = false;
        }*/
    }

    private void showOpenMethodDialog(FileInfo fileInfo) {
        LogUtils.d(TAG, "show Open method Dialog...");
        if (mIsAlertDialogShowing) {
            LogUtils.d(TAG, "Another Dialog is exist, return!~~");
            return;
        }

        if (isResumed()) {
            mIsAlertDialogShowing = true;
            AlertDialogFragment.ChoiceDialogFragmentBuilder builder =
                    new AlertDialogFragment.ChoiceDialogFragmentBuilder();
            builder.setDefault(R.array.open_method, OpenMethodClickListener.OPEN_METHOD_TEXT)
                    .setTitle(R.string.unknown_format)
                    .setCancelTitle(R.string.cancel)
                    .setDoneTitle(R.string.ok);
            AlertDialogFragment.ChoiceDialogFragment openDialogFragment = builder.create();
            OpenMethodClickListener clickListener = new OpenMethodClickListener(fileInfo);
            openDialogFragment.setItemClickListener(clickListener);
            openDialogFragment.setOnDialogDismissListener(this);
            openDialogFragment.show(getFragmentManager(),
                    AlertDialogFragment.ChoiceDialogFragment.CHOICE_DIALOG_TAG);
            boolean ret = getFragmentManager().executePendingTransactions();
            LogUtils.d(TAG, "executing pending transactions result: " + ret);
        }
    }

    private void showOptionDialog() {
        /*if (null == mOptionsDialog) {
            View view = getLayoutInflater().inflate(R.layout.dialog_local_options, null);
            setViewClickListener(view.findViewById(R.id.tv_option_searching),
                    new Runnable() {
                        @Override
                        public void run() {
                            mOptionsDialog.cancel();
                            if (mFileInfoManager != null) {
                                mShowFileList.clear();
                                mShowFileList.addAll(mFileInfoManager.getShowFileList());
                            }
                            mEtSearchInput.setText(null);
                            mEtSearchInput.setSelection(0);
                            switchTitle(mLayoutActionModeSearch);
                            mEtSearchInput.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    KeyboardUtils.showSoftInput(ListActivity.this, mEtSearchInput);
                                }
                            }, 200);
                        }
                    });

            setViewClickListener(view.findViewById(R.id.tv_option_refresh),
                    new Runnable() {
                        @Override
                        public void run() {
                            handleRefresh();
                            mOptionsDialog.cancel();
                        }
                    });

            setViewClickListener(view.findViewById(R.id.tv_option_sorting),
                    new Runnable() {
                        @Override
                        public void run() {
                            showSortDialog();
                            mOptionsDialog.cancel();
                        }
                    });
            view.findViewById(R.id.tv_option_sorting).setBackgroundResource(R.drawable.bottom_corner_bg);
            View viewNewFolder = view.findViewById(R.id.tv_option_create_new_folder);
            View viewNewFolderLine = view.findViewById(R.id.new_folder_line);
            if (null != viewNewFolder) {
                viewNewFolder.setVisibility(View.GONE);
            }
            if (null != viewNewFolderLine) {
                viewNewFolderLine.setVisibility(View.GONE);
            }

            mOptionsDialog = new AlertDialog.Builder(this)
                    .setView(view)
                    .create();
            mOptionsDialog.setCanceledOnTouchOutside(true);
        }

        if (!mOptionsDialog.isShowing()) {
            mOptionsDialog.show();
        }*/
        mOptionsDialog = new AlertDialog.Builder(this)
                .setItems(new String[]{"afaf","hlakfa","ahlkf","ahlkf","ahlkf","ahlkf","ahlkf","ahlkf"}, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }
                )
                .create();
        mOptionsDialog.show();
    }

    protected void handleRefresh() {
        LogUtils.d(TAG, "handleRefresh: ");

        mRefreshingProgressDialog = new ProgressDialog(this);
        mRefreshingProgressDialog.setMessage(getString(R.string.refreshing));
        mRefreshingProgressDialog.setIndeterminate(true);
        mRefreshingProgressDialog.setCancelable(false);
        mRefreshingProgressDialog.show();

        //int id = ResourceUtils.systemId(this, "progress", ID);
        ProgressBar progressBar = (ProgressBar) mRefreshingProgressDialog.findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressBar.setIndeterminateTintList(
                    ColorStateList.valueOf(ContextCompat.getColor(this, R.color.progress_indeterminate_color)));
            progressBar.setIndeterminateTintMode(PorterDuff.Mode.SRC_ATOP);
        } else {
            progressBar.setIndeterminateDrawable(ContextCompat.getDrawable(this, R.drawable.loading_progress));
        }
        //String[] paths = new String[] {Environment.getExternalStorageDirectory().getAbsolutePath()};
        String[] paths = new String[mMountPointManager.getMountCount()];
        mMountPointManager.getMountPointPaths().toArray(paths);
        final int pathsSize = paths.length;
        MediaScannerConnection.OnScanCompletedListener callback =
                new MediaScannerConnection.OnScanCompletedListener() {
                    private int cnt = 0;

                    @Override
                    public void onScanCompleted(String path, Uri uri) {
                        LogUtils.d(TAG, "handleRefresh onScanCompleted: path=" + path + ", uri=" + uri);
                        cnt++;
                        if (cnt == pathsSize) {
                            mRefreshingProgressDialog.cancel();
                            showContent();
                        }
                    }
                };
        MediaUtils.scanFiles(this, paths, null, callback);
    }

    protected void showSortDialog() {
        LogUtils.d(TAG, "show SortDialog...");
        if (mIsAlertDialogShowing) {
            LogUtils.d(TAG, "Another Dialog is exist, return!~~");
            return;
        }

        if (isResumed()) {
            mIsAlertDialogShowing = true;
            AlertDialogFragment.ChoiceDialogFragmentBuilder builder = new AlertDialogFragment.ChoiceDialogFragmentBuilder();
            builder.setDefault(R.array.sort_order, mSortType).setTitle(R.string.sort_order);
            AlertDialogFragment.ChoiceDialogFragment sortDialogFragment = builder.create();
            sortDialogFragment.setItemClickListener(new SortClickListener());
            sortDialogFragment.setOnDialogDismissListener(this);
            sortDialogFragment.show(getFragmentManager(), AlertDialogFragment.ChoiceDialogFragment.CHOICE_DIALOG_TAG);
            boolean ret = getFragmentManager().executePendingTransactions();
            LogUtils.d(TAG, "executing pending transactions result: " + ret);
        }
    }

    protected void showDeleteDialog() {
        LogUtils.d(TAG, "show DeleteDialog...");
        if (mIsAlertDialogShowing) {
            LogUtils.d(TAG, "Another Dialog is exist, return!~~");
            return;
        }

        if (isResumed()) {
            mIsAlertDialogShowing = true;
            AlertDialogFragment.AlertDialogFragmentBuilder builder = new AlertDialogFragment.AlertDialogFragmentBuilder();
            AlertDialogFragment deleteDialogFragment =
                    builder.setMessage(R.string.delete_msg)
                            .setDoneTitle(R.string.ok)
                            .setCancelTitle(R.string.cancel)
                            .setTitle(R.string.delete)
                            .create();
            deleteDialogFragment.setOnDoneListener(new DeleteListener());
            deleteDialogFragment.setOnDialogDismissListener(this);
            deleteDialogFragment.show(getFragmentManager(), DELETE_DIALOG_TAG);
            boolean ret = getFragmentManager().executePendingTransactions();
            LogUtils.d(TAG, "executing pending transactions result: " + ret);

            View titleDivider = deleteDialogFragment.getDialog().findViewById(R.id.titleDivider);
            if (null != titleDivider) {
                LogUtils.d(TAG, "showDeleteDialog: null != titleDivider");
                titleDivider.setVisibility(View.GONE);
            }

            TextView msg = (TextView) deleteDialogFragment.getDialog().findViewById(R.id.message);
            if (null != msg) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    msg.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                }
            }
        }
    }

    private void showMoreDialog() {
        if (null == mMoreDialog) {
            View view;
            if (null != mAdapter) {
                int checkedItemCnt = mAdapter.getCheckedItemsCount();
                if (1 == checkedItemCnt) {
                    FileInfo fileInfo = mAdapter.getFirstCheckedFileInfoItem();
                    if (fileInfo.isDirectory()) {
                        view = getLayoutInflater().inflate(R.layout.dialog_more_single_folder, null);
                    } else {
                        if (Music == mFileCategory) {
                            view = getLayoutInflater().inflate(R.layout.dialog_more_single_audio_file, null);
                        } else {
                            view = getLayoutInflater().inflate(R.layout.dialog_more_single_file, null);
                        }
                    }
                } else {
                    view = getLayoutInflater().inflate(R.layout.dialog_more_multi, null);
                }

                if (null != view) {
                    if (Apk == mFileCategory) {
                        View openMode = view.findViewById(R.id.tv_more_open_mode);
                        View openModeLine = view.findViewById(R.id.open_mode_line);
                        if (openMode != null) {
                            openMode.setVisibility(View.GONE);
                        }
                        if (openModeLine != null) {
                            openModeLine.setVisibility(View.GONE);
                        }
                        if (1 == checkedItemCnt) {
                            view.findViewById(R.id.tv_more_copy).setBackgroundResource(R.drawable.top_corner_bg);
                        }
                    } else {
                        setViewClickListener(view.findViewById(R.id.tv_more_open_mode), new Runnable() {

                            @Override
                            public void run() {
                                if (null != mAdapter) {
                                    openFile(mAdapter.getFirstCheckedFileInfoItem());
                                    mMoreDialog.cancel();
                                    exitEditMode();
                                }
                            }
                        });
                    }

                    setViewClickListener(view.findViewById(R.id.tv_more_audio_setup),
                            new Runnable() {

                                @Override
                                public void run() {
                                    if (null != mAdapter) {
                                        showSetupAudioDialog();
                                        mMoreDialog.cancel();
                                        exitEditMode();
                                    }
                                }
                            });

                    setViewClickListener(view.findViewById(R.id.tv_more_copy),
                            new Runnable() {
                                @Override
                                public void run() {
                                    mMoreDialog.cancel();
                                    if (!PermissionUtils.hasStorageWritePermission(ListActivity.this)) {
                                        PermissionUtils.requestPermission(ListActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                                MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_COPY);
                                        return;
                                    }
                                    Intent intent = new Intent(ListActivity.this, DstActivity.class);
                                    intent.putExtra(DstActivity.EXTRA_TYPE, FileInfoManager.PASTE_MODE_COPY);
                                    intent.putExtra(DstActivity.EXTRA_LIST, (Serializable) mAdapter.getCheckedFileInfoItemsList());
                                    startActivityForResult(intent, REQUEST_CODE_PASTE);
                                    hasResult=true;

                                }
                            });

                    setViewClickListener(view.findViewById(R.id.tv_more_rename),
                            new Runnable() {
                                @Override
                                public void run() {
                                    mMoreDialog.cancel();
                                    if (!PermissionUtils.hasStorageWritePermission(ListActivity.this)) {
                                        PermissionUtils.requestPermission(ListActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                                MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_RENAME);
                                        return;
                                    }
                                    showRenameDialog();
                                    exitEditMode();
                                }
                            });

                    setViewClickListener(view.findViewById(R.id.tv_more_details),
                            new Runnable() {
                                @Override
                                public void run() {
                                    mService.getDetailInfo(ListActivity.this.getClass().getName(),
                                            mAdapter.getCheckedFileInfoItemsList().get(0),
                                            new DetailInfoListener(mAdapter.getCheckedFileInfoItemsList().get(0)));
                                    mMoreDialog.cancel();
                                    exitEditMode();
                                }
                            });

                    mMoreDialog = new AlertDialog.Builder(this)
                            .setView(view)
                            .create();
                    mMoreDialog.setCanceledOnTouchOutside(true);
                    mMoreDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            mMoreDialog = null;
                        }
                    });
                }

            }
        }

        if (!mMoreDialog.isShowing()) {
            mMoreDialog.show();
        }

    }

    protected void showSetupAudioDialog() {
        LogUtils.d(TAG, "show SetupAudioDialog...");
        if (mIsAlertDialogShowing) {
            LogUtils.d(TAG, "Another Dialog is exist, return!~~");
            return;
        }

        if (isResumed()) {
            mIsAlertDialogShowing = true;
            AlertDialogFragment.ChoiceDialogFragmentBuilder builder = new AlertDialogFragment.ChoiceDialogFragmentBuilder();
            int defaultChoice = (mRingType == RingtoneManager.TYPE_RINGTONE) ?
                    0 : (mRingType == RingtoneManager.TYPE_NOTIFICATION ? 1 : 2);
            builder.setDefault(R.array.audio_setup, defaultChoice)
                    .setTitle(R.string.more_audio_setup)
                    .setCancelTitle(R.string.cancel)
                    .setDoneTitle(R.string.ok);
            AlertDialogFragment.ChoiceDialogFragment setupDialogFragment = builder.create();
            FileInfo fileInfo = mAdapter.getFirstCheckedFileInfoItem();
            AudioSetupClickListener setupClickListener = new AudioSetupClickListener();
            setupClickListener.setFile(new File(fileInfo.getFileAbsolutePath()));
            setupDialogFragment.setItemClickListener(setupClickListener);
            setupDialogFragment.setOnDialogDismissListener(this);
            setupDialogFragment.show(getFragmentManager(), AUDIO_SETUP_DIALOG_TAG);
            boolean ret = getFragmentManager().executePendingTransactions();
            LogUtils.d(TAG, "executing pending transactions result: " + ret);
        }
    }

    protected void showRenameDialog() {
        LogUtils.d(TAG, "show RenameDialog...");
        if (mIsAlertDialogShowing) {
            LogUtils.d(TAG, "Another Dialog showing, return!~~");
            return;
        }
        FileInfo fileInfo = mAdapter.getFirstCheckedFileInfoItem();
        mRenamedItem = fileInfo;
        int selection = 0;
        if (fileInfo != null) {
            String name = fileInfo.getFileName();
            String fileExtension = FileUtils.getFileExtension(name);
            selection = name.length();
            if (!fileInfo.isDirectory() && fileExtension != null) {
                selection = selection - fileExtension.length() - 1;
            }
            if (isResumed()) {
                mIsAlertDialogShowing = true;
                AlertDialogFragment.EditDialogFragmentBuilder builder = new AlertDialogFragment.EditDialogFragmentBuilder();
                builder.setDefault(name, selection).setDoneTitle(R.string.done).setCancelTitle(
                        R.string.cancel).setTitle(R.string.rename);
                AlertDialogFragment.EditTextDialogFragment renameDialogFragment = builder.create();
                renameDialogFragment.setOnEditTextDoneListener(new RenameDoneListener(fileInfo));
                renameDialogFragment.setOnDialogDismissListener(this);
                renameDialogFragment.show(getFragmentManager(), RENAME_DIALOG_TAG);
                boolean ret = getFragmentManager().executePendingTransactions();
                LogUtils.d(TAG, "executing pending transactions result: " + ret);

                View titleDivider = renameDialogFragment.getDialog().findViewById(R.id.titleDivider);
                if (null != titleDivider) {
                    LogUtils.d(TAG, "showCreateFolderDialog: null != titleDivider");
                    titleDivider.setVisibility(View.GONE);
                }
            }
        }
    }

    protected void showRenameDialogAgain() {
        LogUtils.d(TAG, "show RenameDialog...");
        if (mIsAlertDialogShowing) {
            LogUtils.d(TAG, "Another Dialog showing, return!~~");
            return;
        }
        FileInfo fileInfo = mRenamedItem;
        int selection = 0;
        if (fileInfo != null) {
            String name = fileInfo.getFileName();
            String fileExtension = FileUtils.getFileExtension(name);
            selection = name.length();
            if (!fileInfo.isDirectory() && fileExtension != null) {
                selection = selection - fileExtension.length() - 1;
            }
            if (isResumed()) {
                mIsAlertDialogShowing = true;
                AlertDialogFragment.EditDialogFragmentBuilder builder = new AlertDialogFragment.EditDialogFragmentBuilder();
                builder.setDefault(name, selection).setDoneTitle(R.string.done).setCancelTitle(
                        R.string.cancel).setTitle(R.string.rename);
                AlertDialogFragment.EditTextDialogFragment renameDialogFragment = builder.create();
                renameDialogFragment.setOnEditTextDoneListener(new RenameDoneListener(fileInfo));
                renameDialogFragment.setOnDialogDismissListener(this);
                renameDialogFragment.show(getFragmentManager(), RENAME_DIALOG_TAG);
                boolean ret = getFragmentManager().executePendingTransactions();
                LogUtils.d(TAG, "executing pending transactions result: " + ret);

                View titleDivider = renameDialogFragment.getDialog().findViewById(R.id.titleDivider);
                if (null != titleDivider) {
                    LogUtils.d(TAG, "showCreateFolderDialog: null != titleDivider");
                    titleDivider.setVisibility(View.GONE);
                }
            }
        }
    }

    private void sortFileInfoList() {
        LogUtils.d(TAG, "Start sortFileInfoList()");

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.sorting_in));
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();

        //int id = ResourceUtils.systemId(this, "progress", ID);
        ProgressBar progressBar = (ProgressBar) progressDialog.findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressBar.setIndeterminateTintList(
                    ColorStateList.valueOf(ContextCompat.getColor(this, R.color.bingo_theme_color)));
            progressBar.setIndeterminateTintMode(PorterDuff.Mode.SRC_ATOP);
        } else {
            progressBar.setIndeterminateDrawable(ContextCompat.getDrawable(this, R.drawable.loading_progress));
        }
        progressDialog.getWindow().getDecorView().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        int selection = mLstMedia.getFirstVisiblePosition(); // save current
                        // refresh only when paste or delete operation is performed
                        mFileInfoManager.sort(mSortType);
                        mAdapter.notifyDataSetChanged();
                        mLstMedia.setSelection(selection);
                        // restore the selection in the navigation view

                        LogUtils.d(TAG, "End sortFileInfoList()");

                        progressDialog.cancel();
                    }
                }, 1000);


    }

    private void requestSearch(String query) {
        LogUtils.d(TAG, "requestSearch: query=" + query);
        mSearchTotalChanged = 0;
        switchTitle(mLayoutActionModeSearch);
        switchContent(mLayoutSearching);
        if (mAdapter != null) {
            mAdapter.changeMode(FileInfoAdapter.MODE_SEARCH);
        }
        if (null != mService) {
            mService.search(this.getClass().getName(), query, mFileCategory, mSortType, new SearchListener(query));
        }
    }

    private void switchToEditView(int position, int top) {
        LogUtils.d(TAG, "switchToEditView: position=" + position + ", top=" + top);
        switchTitle(mLayoutActionModeSelect);
        mAdapter.setChecked(position, true);
        mLstMedia.setSelectionFromTop(position, top);
        switchToEditView();
    }

    private void switchToEditView() {
        LogUtils.d(TAG, "Switch to edit view");
        mLstMedia.setFastScrollEnabled(false);
        mAdapter.changeMode(FileInfoAdapter.MODE_EDIT);
        mTvSelectChosen.setText(getResources().getString(
                R.string.select_chosen, mAdapter.getCheckedItemsCount()));
        boolean isSelectedAll = mAdapter.getCheckedItemsCount() == mAdapter.getCount();
        mTvSelectAll.setText(isSelectedAll ? R.string.select_not_all : R.string.select_all);
        handleEditView();
    }

    private void handleEditView() {
        boolean showShare = false;
        if (null != mAdapter) {
            showShare = mAdapter.getCheckedItemsCount() == 1;
            if (showShare) {
                FileInfo fileInfo = mAdapter.getFirstCheckedFileInfoItem();
                showShare = !fileInfo.isDirectory();
            }
        }
        mTvShare.setEnabled(showShare);

        boolean hasCheckedItem = (null != mAdapter) ? mAdapter.getCheckedItemsCount() > 0 : false;
        mTvDelete.setEnabled(hasCheckedItem);
        mTvCut.setEnabled(hasCheckedItem);
        mTvMore.setEnabled(hasCheckedItem);
    }

    @Override
    protected void doOnMounted(String mountPoint) {
        super.doOnMounted(mountPoint);
        LogUtils.i(TAG, "doOnMounted, mountPoint = " + mountPoint);
        doPrepareForMount(mountPoint);
        showContent();
    }

    @Override
    protected void doOnEjected(String unMountPoint) {
        LogUtils.e(TAG, "***doOnEjected*********");
        super.doOnEjected(unMountPoint);
        if (mService != null && mService.isBusy(this.getClass().getName())) {
            mService.cancel(this.getClass().getName());
        }
        showContent();
    }

    @TargetApi(24)
    @Override
    protected void doOnUnMounted(StorageVolume volume) {
        LogUtils.e(TAG, "***doOnUnMounted*********");
        super.doOnUnMounted(volume);
        String unMountPoint = volume.getPath();
        if (mFileInfoManager != null) {
            int pasteCnt = mFileInfoManager.getPasteCount();
            LogUtils.i(TAG, "doOnUnmounted, unMountPoint: " + unMountPoint + ",pasteCnt = "
                    + pasteCnt);

            if (pasteCnt > 0) {
                FileInfo fileInfo = mFileInfoManager.getPasteList().get(0);
                if (fileInfo.getFileAbsolutePath().startsWith(
                        unMountPoint + MountPointManager.SEPARATOR)) {
                    LogUtils.i(TAG, "doOnUnmounted, clear paste list. ");
                    mFileInfoManager.clearPasteList();
                }
            }
        }

        if (mService != null && mService.isBusy(this.getClass().getName())) {
            mService.cancel(this.getClass().getName());
        }
        showToastForUnmount(volume);

        DialogFragment listFragment = (DialogFragment) getFragmentManager()
                .findFragmentByTag(ListListener.LIST_DIALOG_TAG);
        if (listFragment != null) {
            LogUtils.d(TAG, "onUnmounted, listFragment dismiss. ");
            listFragment.dismissAllowingStateLoss();
        }
        showContent();
    }

    @Override
    protected void doOnSdSwap() {
        super.doOnSdSwap();
        mMountPointManager.init(getApplicationContext());
        showContent();
    }

    private void doPrepareForMount(String mountPoint) {
        LogUtils.i(TAG, "doPrepareForMount, mountPoint = " + mountPoint);
        if (mService != null && mService.isBusy(this.getClass().getName())) {
            mService.cancel(this.getClass().getName());
        }
        mMountPointManager.init(getApplicationContext());
    }

    @TargetApi(24)
    private void showToastForUnmount(StorageVolume volume) {
        LogUtils.d(TAG, "showToastForUnmount, path = " + volume.getPath());
        if (isResumed()) {
            String unMountPointDescription = volume.getDescription(this);
            LogUtils.d(TAG, "showToastForUnmount, unMountPointDescription:"
                    + unMountPointDescription);
            mToastHelper.showToast(getString(R.string.unmounted, unMountPointDescription));
        }
    }

    class LstListListener extends ListListener {

        @Override
        public void onTaskProgress(ProgressInfo progressInfo) {
            super.onTaskProgress(progressInfo);
            if (isResumed()) {
                switchLayout(mLayoutLoading);
            }
        }

        @Override
        public void onTaskResult(int result) {
            LogUtils.d(TAG, "LstListListener onTaskResult: ");
            super.onTaskResult(result);
//            if (mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
//                LogUtils.e(TAG,"mCurrentPath=:"+mCurrentPath);
//                mFileInfoManager.loadFileInfoList(mCurrentPath, mSortType, mSelectedFileInfo);
//                mSelectedFileInfo = mAdapter.getFirstCheckedFileInfoItem();
//            } else {
                mFileInfoManager.loadFileInfoList(mSortType);
//            }

            mAdapter.notifyDataSetChanged();
            int selectedItemPosition = restoreSelectedPosition();
            if (selectedItemPosition == -1) {
                mLstMedia.setSelectionAfterHeaderView();
            } else if (selectedItemPosition >= 0 && selectedItemPosition < mAdapter.getCount()) {
                if (mSelectedTop != -1) {
                    mLstMedia.setSelectionFromTop(selectedItemPosition, mSelectedTop);
                    mSelectedTop = -1;
                } else if (mTop != -1) {
                    mLstMedia.setSelectionFromTop(selectedItemPosition, mTop);
                    mTop = -1;
                } else {
                    mLstMedia.setSelection(selectedItemPosition);
                }
            }

            LogUtils.d(TAG, "onTaskResult: cnt=" + mLstMedia.getCount() + ",adapterCnt=" + mAdapter.getCount());
            if (0 == mAdapter.getCount()) {
                switchLayout(mLayoutEmptyView);
            } else {
                switchLayout(mLstMedia);
            }
        }
    }

    private class OpenMethodClickListener implements DialogInterface.OnClickListener {
        static final int OPEN_METHOD_TEXT = 0;
        static final int OPEN_METHOD_AUDIO = 1;
        static final int OPEN_METHOD_VIDEO = 2;
        static final int OPEN_METHOD_IMAGE = 3;

        private FileInfo mSelectedItem = null;
        private Intent mOpenIntent = null;

        OpenMethodClickListener(FileInfo selectedItem) {
            mSelectedItem = selectedItem;
            mOpenIntent = new Intent(Intent.ACTION_VIEW);
            open(".txt");
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            LogUtils.d(TAG, "OpenMethodClickListener onClick: which=" + which);
            switch (which) {
                case OPEN_METHOD_TEXT:
                    open(".txt");
                    break;

                case OPEN_METHOD_AUDIO:
                    open(".mp3");
                    break;

                case OPEN_METHOD_VIDEO:
                    open(".3gp");
                    break;

                case OPEN_METHOD_IMAGE:
                    open(".png");
                    break;

                case DialogInterface.BUTTON_POSITIVE:
                    try {
                        startActivity(getOpenIntent());
                        dialog.dismiss();
                    } catch (android.content.ActivityNotFoundException e) {
                        mToastHelper.showToast(R.string.msg_unable_open_file);
                        LogUtils.w(TAG, "OpenMethodClickListener, Cannot open file: ");
                    }
                    break;

                default:
                    break;
            }
        }

        public Intent getOpenIntent() {
            return mOpenIntent;
        }

        private void open(String extension) {
            if ((null != mSelectedItem) && !TextUtils.isEmpty(extension)) {
                Uri uri = mSelectedItem.getUri();
                LogUtils.d(TAG, "openFile, Open uri file: " + uri);
                String mimeType = FileUtils.getMimeTypeBySpecialExtension(mSelectedItem.getFileAbsolutePath(), extension);
                mOpenIntent.setDataAndType(uri, mimeType);
                mOpenIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
        }
    }

    private class AudioSetupClickListener implements DialogInterface.OnClickListener {

        static final int TYPE_TELEPHONE = 0;
        static final int TYPE_SMS = 1;
        static final int TYPE_ALARM = 2;

        private File mFile = null;

        public void setFile(File file) {
            mFile = file;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            LogUtils.d(TAG, "AudioSetupClickListener onClick: which=" + which);

            switch (which) {
                case TYPE_TELEPHONE:
                    mRingType = RingtoneManager.TYPE_RINGTONE;
                    break;

                case TYPE_SMS:
                    mRingType = RingtoneManager.TYPE_NOTIFICATION;
                    break;

                case TYPE_ALARM:
                    mRingType = RingtoneManager.TYPE_ALARM;
                    break;

                case DialogInterface.BUTTON_POSITIVE:
                    if ((0 < mRingType) && (null != mFile)) {
                        RingtoneUtils.setRingtone(getApplicationContext(), mRingType, mFile);
                    }
                    dialog.cancel();
                    break;
            }

        }
    }

    private class SortClickListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialog, int id) {
            LogUtils.d(TAG, "SortClickListener onClick: id=" + id);
            if (id != mSortType) {
                setPrefsSortBy(id);
                sortFileInfoList();
            }
            dialog.dismiss();
        }
    }

    protected class SearchListener implements FileManagerService.OperationEventListener {
        private static final int FIRST_UPDATE_COUNT = 20;
        private static final int NEED_UPDATE_LIST = 6;
        private boolean mIsResultSet = false;
        private int mCount = 0;

        /**
         * Constructor of SearchListener.
         *
         * @param text the search target(String), which will be shown on searchResult TextView..
         */
        public SearchListener(String text) {
            if (text == null) {
                throw new IllegalArgumentException();
            }
            mSearchText = text;
        }

        @Override
        public void onTaskResult(int result) {
            LogUtils.d(TAG, "SearchListener onTaskResult: ");
            mEnterSearch = true;
            if (result == ERROR_CODE_USER_CANCEL) {
                mFileInfoManager.getAddedFileList().clear();
                mSearchTotal = 0;
                mLayoutSearchResult.setVisibility(View.GONE);
                if (TextUtils.isEmpty(mSearchText)) {
                    switchContent(mLayoutNoMatchingSearch);
                }
                return;
            }
            if (mTvSearchResult != null && !mIsResultSet) {
                mLayoutSearchResult.setVisibility(View.VISIBLE);
                mTvSearchResult.setText(getResources().getString(
                        R.string.search_result, mSearchTotal));
                mIsResultSet = true;
            }
            if (0 == mSearchTotal) {
                switchContent(mLayoutNoMatchingSearch);
            } else {
                switchContent(mLayoutList);
//                mLayoutSearchResult.setVisibility(View.VISIBLE);
//                mTvSearchResult.setText(getResources().getString(
//                        R.string.search_result, mSearchTotal));
//                mIsResultSet = true;
                mFileInfoManager.updateSearchList();
                mAdapter.notifyDataSetChanged();
            }
            mSearchTotalChanged = mSearchTotal;
            mSearchTotal = 0;
        }

        @Override
        public void onTaskPrepare() {
            LogUtils.d(TAG, "SearchListener onTaskPrepare: ");
            mAdapter.changeMode(FileInfoAdapter.MODE_SEARCH);
        }

        @Override
        public void onTaskProgress(ProgressInfo progressInfo) {
            if (!progressInfo.isFailInfo()) {
//                if (mTvSearchResult != null && !mIsResultSet) {
//                    mSearchTotal = progressInfo.getTotal();
//                    LogUtils.d(TAG, "onTaskProgress: mSearchTotal=" + mSearchTotal);
//                    mLayoutSearchResult.setVisibility(View.VISIBLE);
//                    mTvSearchResult.setText(getResources().getString(
//                            R.string.search_result, mSearchTotal));
//                    mIsResultSet = true;
//                }
                mSearchTotal = progressInfo.getTotal();
//                mSearchTotal++;
                LogUtils.d(TAG, "SearchListener onTaskProgress: fileName=" + progressInfo.getFileInfo().getShowName());
                if (progressInfo.getFileInfo() != null) {
                    mFileInfoManager.addItem(progressInfo.getFileInfo());
                }
                mCount++;
                if (mCount > FIRST_UPDATE_COUNT) {
                    if (mLstMedia.getLastVisiblePosition() + NEED_UPDATE_LIST > mAdapter.getCount()) {
                        mFileInfoManager.updateSearchList();
                        mAdapter.notifyDataSetChanged();
                        mCount = 0;
                    }
                }
            } else {
                mSearchTotal = 0;
            }
        }
    }

    private class DeleteListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialog, int id) {
            LogUtils.d(TAG, "onClick() method for alertDeleteDialog, OK button");
            final AlertDialogFragment deleteFragment = (AlertDialogFragment) getFragmentManager().
                    findFragmentByTag(DELETE_DIALOG_TAG);
            if (null != deleteFragment) {
                deleteFragment.dismissAllowingStateLoss();
            }
            if (mEnterSearch) {
                mListChanged = true;
            }
            if (mService != null) {
                HeavyOperationListener listener = new HeavyOperationListener(R.string.deleting, ListActivity.this,
                        mHeavyOperationListenerCallback);
                mService.deleteFiles(ListActivity.this.getClass().getName(),
                        mAdapter.getCheckedFileInfoItemsList(), listener);
            }
        }
    }

    protected class RenameDoneListener implements AlertDialogFragment.EditTextDialogFragment.EditTextDoneListener {
        FileInfo mSrcfileInfo;

        public RenameDoneListener(FileInfo srcFile) {
            mSrcfileInfo = srcFile;
        }

        @Override
        public void onClick(String text) {

            AlertDialogFragment.EditTextDialogFragment rename = (AlertDialogFragment.EditTextDialogFragment) getFragmentManager().findFragmentByTag(RENAME_DIALOG_TAG);
            if (rename != null) {
                rename.dismiss();
            }
            String newFilePath = mSrcfileInfo.getFileParentPath() + MountPointManager.SEPARATOR + text;
            mRenamedName = text;
            LogUtils.d(TAG, "RenameDoneListener onClick: newFilePath=" + newFilePath);
            if (null == mSrcfileInfo) {
                LogUtils.w(TAG, "mSrcfileInfo is null.");
                return;
            }
            if (FileUtils.isExtensionChange(newFilePath, mSrcfileInfo.getFileAbsolutePath())) {
                showRenameExtensionDialog(mSrcfileInfo, newFilePath);
            } else {
                if (mEnterSearch) {
                    mListChanged = true;
                }
                if (mService != null) {
                    mService.rename(ListActivity.this.getClass().getName(),
                            mSrcfileInfo, new FileInfo(newFilePath), new LightOperationListener(
                                    FileUtils.getFileName(newFilePath)));
                }
            }

        }
    }

    private void showRenameExtensionDialog(final FileInfo srcFile, final String newFilePath) {
        LogUtils.d(TAG, "show Open method Dialog...");
//        if (mIsAlertDialogShowing) {
//            LogUtils.d(TAG, "Another Dialog is exist, return!~~");
//            return;
//        }

        if (isResumed()) {
            mIsAlertDialogShowing = true;
            AlertDialogFragment.AlertDialogFragmentBuilder builder =
                    new AlertDialogFragment.AlertDialogFragmentBuilder();
            builder.setCancelTitle(R.string.cancel)
                    .setDoneTitle(R.string.ok)
                    .setTitle(R.string.confirm_rename)
                    .setMessage(R.string.rename_extension);
            AlertDialogFragment renameDialogFragment = builder.create();
            renameDialogFragment.setOnDialogDismissListener(this);
            renameDialogFragment.getArguments().putString(NEW_FILE_PATH_KEY, newFilePath);
            renameDialogFragment.setOnDoneListener(new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (mEnterSearch) {
                        mListChanged = true;
                    }
                    if (mService != null) {
                        mService.rename(ListActivity.this.getClass().getName(),
                                srcFile, new FileInfo(newFilePath), new ListActivity.LightOperationListener(
                                        FileUtils.getFileName(newFilePath)));
                    }
                }
            });
            renameDialogFragment.show(getFragmentManager(),
                    AlertDialogFragment.TAG);
            boolean ret = getFragmentManager().executePendingTransactions();
            LogUtils.d(TAG, "executing pending transactions result: " + ret);
        }
    }

    protected class LightOperationListener implements FileManagerService.OperationEventListener {

        String mDstName = null;

        LightOperationListener(String dstName) {
            mDstName = dstName;
        }

        @Override
        public void onTaskResult(int errorType) {
            LogUtils.i(TAG, "LightOperationListener,TaskResult result = " + errorType);
            switch (errorType) {
                case ERROR_CODE_SUCCESS:
                    //重命名成功
                    if ((mAdapter != null && mAdapter.getCount() == 0) ||
                            (mAdapter != null && mAdapter.getCount() == 1 &&
                                    mRenamedName != null && mRenamedName.startsWith("."))) {
                        switchLayout(mLayoutEmptyView);
                    }
                    if (mRenamedName != null && mRenamedName.startsWith(".") && mSearchTotalChanged > 0) {
                        mSearchTotalChanged--;
                        mTvSearchResult.setText(getResources().getString(
                                R.string.search_result, mSearchTotalChanged));
                    }
                    mRenamedName = null;
                    if ((null != mService) && (null != mFileCategory)) {
                        mService.listFiles(ListActivity.this.getClass().getName(), mFileCategory, mSortType, new LstListListener());
                    }
                case ERROR_CODE_USER_CANCEL:
                    FileInfo fileInfo = mFileInfoManager.updateOneFileInfoList(mSortType);
                    mAdapter.notifyDataSetChanged();
                    if (fileInfo != null) {
                        int position = mAdapter.getPosition(fileInfo);
                        LogUtils.d(TAG, "LightOperation position = " + position);
                        mLstMedia.setSelection(position);
                    }

                    break;
                case ERROR_CODE_FILE_EXIST:
                    if (mDstName != null) {
                        mToastHelper.showToast(R.string.rename_hint);
                    }
                    showRenameDialogAgain();
                    break;
                case ERROR_CODE_NAME_EMPTY:
                    mToastHelper.showToast(R.string.invalid_empty_name);
                    break;
                case ERROR_CODE_NAME_TOO_LONG:
                    mToastHelper.showToast(R.string.file_name_too_long);
                    break;
                case ERROR_CODE_NOT_ENOUGH_SPACE:
                    mToastHelper.showToast(R.string.insufficient_memory);
                    break;
                case ERROR_CODE_UNSUCCESS:
                    mToastHelper.showToast(R.string.operation_fail);
                    break;
                default:
                    LogUtils.e(TAG, "wrong errorType for LightOperationListener");
                    break;
            }
        }

        @Override
        public void onTaskPrepare() {
            return;
        }

        @Override
        public void onTaskProgress(ProgressInfo progressInfo) {
            return;
        }
    }

    protected class DetailInfoListener implements FileManagerService.OperationEventListener,
            DialogInterface.OnDismissListener {
        public static final String DETAIL_DIALOG_TAG = "detaildialogtag";
        private FileInfo mFileInfo;

        public DetailInfoListener(FileInfo fileInfo) {
            mFileInfo = fileInfo;
        }

        @Override
        public void onTaskPrepare() {
            if (isResumed()) {
                AlertDialogFragment.AlertDialogFragmentBuilder builder = new AlertDialogFragment.AlertDialogFragmentBuilder();
                AlertDialogFragment detailFragment = builder.setCancelTitle(R.string.ok).setLayout(
                        R.layout.dialog_details).setTitle(R.string.details).create();

                detailFragment.setDismissListener(this);
                detailFragment.show(getFragmentManager(), DETAIL_DIALOG_TAG);
                boolean ret = getFragmentManager().executePendingTransactions();
                LogUtils.d(TAG, "executing pending transactions result: " + ret);
                Dialog dialog = detailFragment.getDialog();
                if (dialog != null) {
                    setViewContent(dialog.findViewById(R.id.tv_detail_name), mFileInfo.getShowName());
                    String mimeType = mFileInfo.getFileMimeType(mService);
                    if (FileInfo.MIMETYPE_EXTENSION_UNKONW.equals(mimeType)
                            || FileInfo.MIMETYPE_EXTENSION_NULL.equals(mimeType)) {
                        mimeType = getString(R.string.unknown_mime);
                    }
                    setViewContent(dialog.findViewById(R.id.tv_detail_type),
                            mFileInfo.isDirectory() ? getString(R.string.folder) : /*getString(R.string.file)*/ mimeType);
                    setViewContent(dialog.findViewById(R.id.tv_detail_size), mFileInfo.getFileSizeStr());

                    if (mFileInfo.isDirectory()) {
                        File dirFile = mFileInfo.getFile();
                        int dirs = 0;
                        int files = 0;
                        for (File file : dirFile.listFiles()) {
                            if (file.isDirectory()) {
                                dirs++;
                            } else {
                                files++;
                            }
                        }
                        StringBuilder sb = new StringBuilder();
                        sb.append(getString(R.string.file)).append(":");
                        sb.append(files).append(", ");
                        sb.append(getString(R.string.folder)).append(":");
                        sb.append(dirs);

                        setViewContent(dialog.findViewById(R.id.tv_detail_include), sb.toString());
                        View view = dialog.findViewById(R.id.rl_include);
                        view.setVisibility(View.VISIBLE);
                    } else {
                        View view = dialog.findViewById(R.id.rl_include);
                        view.setVisibility(View.GONE);
                    }

                    setViewContent(dialog.findViewById(R.id.tv_detail_time),
                            FileUtils.formatModifiedTime(getApplicationContext(), mFileInfo.getFileLastModifiedTime()));
                    setViewContent(dialog.findViewById(R.id.tv_detail_route), mFileInfo.getShowParentPath());
                }
            } else {
                LogUtils.e(TAG, "onTaskPrepare activity is not resumed");
            }
        }

        private void setViewContent(View view, String content) {
            if (view instanceof TextView) {
                TextView tv = (TextView) view;
                tv.setText(content);
            }
        }

        @Override
        public void onTaskProgress(ProgressInfo progressInfo) {

        }

        @Override
        public void onTaskResult(int result) {
            LogUtils.d(TAG, "DetailInfoListener onTaskResult.");
            AlertDialogFragment detailFragment = (AlertDialogFragment) getFragmentManager().findFragmentByTag(DETAIL_DIALOG_TAG);
            if (detailFragment != null) {
                //detailFragment.getArguments().putString(DETAIL_INFO_KEY, mStringBuilder.toString());
                //detailFragment.getArguments().putLong(DETAIL_INFO_SIZE_KEY, mFileLength);
            } else {
                // this case may happen in case of this operation already canceled.
                LogUtils.d(TAG, "get detail fragment is null...");
            }
            return;
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            if (mService != null) {
                LogUtils.d(this.getClass().getName(), "onDismiss");
                mService.cancel(ListActivity.this.getClass().getName());
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        LogUtils.d(TAG, "onRequestPermissionsResult, requestCode:" + requestCode);
        if (null == permissions || permissions.length == 0 ||
                null == grantResults || grantResults.length == 0 ||
                PackageManager.PERMISSION_DENIED == grantResults[0]) {
            LogUtils.e(TAG, "**********onRequestPermissionsResult, Permission or grant res null*******");
            return;
        }

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_DELETE:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    showDeleteDialog();
                }
                break;
            case MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_RENAME:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    showRenameDialog();
                    exitEditMode();
                }
                break;
            case MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_COPY:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    Intent intent = new Intent(ListActivity.this, DstActivity.class);
                    intent.putExtra(DstActivity.EXTRA_TYPE, FileInfoManager.PASTE_MODE_COPY);
                    intent.putExtra(DstActivity.EXTRA_LIST, (Serializable) mAdapter.getCheckedFileInfoItemsList());
                    startActivityForResult(intent, REQUEST_CODE_PASTE);
                    hasResult=true;
                }
                break;
            case MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_CUT:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    Intent intent = new Intent(getApplicationContext(), DstActivity.class);
                    intent.putExtra(DstActivity.EXTRA_TYPE, FileInfoManager.PASTE_MODE_CUT);
                    intent.putExtra(DstActivity.EXTRA_LIST, (Serializable) mAdapter.getCheckedFileInfoItemsList());
                    startActivityForResult(intent, REQUEST_CODE_PASTE);
                    hasResult=true;
                }
                break;
            case MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_ZIP:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    onItemClick(mFileClickPos);
                }
                break;
            case MY_PERMISSIONS_REQUEST_TO_READ_EXTERNAL_STORAGE:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    showContent();
                }
                break;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

}
