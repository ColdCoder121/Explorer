package com.bingcoo.fileexplorer.category.zip;

/**
 * 作者：zhshh
 * 内容摘要：压缩文件列表对象
 * 创建日期：2017/2/24
 * 修改者， 修改日期， 修改内容
 */

public class ZipInfo {
    private String mName;
    private long mSize;
    private boolean mIsDirectory;
    private String mPath;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public long getSize() {
        return mSize;
    }

    public void setSize(long size) {
        this.mSize = size;
    }

    public String getPath() {
        return mPath;
    }

    public void setPath(String path) {
        this.mPath = path;
    }

    public boolean isDirectory() {
        return mIsDirectory;
    }

    public void setDirectory(boolean directory) {
        mIsDirectory = directory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ZipInfo zipInfo = (ZipInfo) o;
        if (mSize != zipInfo.mSize) return false;
        if (mIsDirectory != zipInfo.mIsDirectory) return false;
        if (!mName.equals(zipInfo.mName)) return false;
        return mPath.equals(zipInfo.mPath);
    }

}
