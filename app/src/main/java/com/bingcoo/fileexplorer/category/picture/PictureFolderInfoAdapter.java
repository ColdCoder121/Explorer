package com.bingcoo.fileexplorer.category.picture;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bingcoo.fileexplorer.R;
import com.bingcoo.fileexplorer.home.FileInfoAdapter;
import com.bingcoo.fileexplorer.service.FileManagerService;
import com.bingcoo.fileexplorer.ui.MyExceptionImageView;
import com.bingcoo.fileexplorer.util.FileInfo;
import com.bingcoo.fileexplorer.util.FileInfoManager;
import com.bingcoo.fileexplorer.util.FileUtils;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.MediaUtils;
import com.bumptech.glide.Glide;

import java.io.File;

/**
 * 类名称：PictureFolderInfoAdapter
 * 作者：David
 * 内容摘要：
 * 创建日期：2017/1/19
 * 修改者， 修改日期， 修改内容
 */
public class PictureFolderInfoAdapter extends FileInfoAdapter {
    private static final String TAG = "PictureFolderInfoAdapter";

    private int width = 0;
    private int height = 0;

    public PictureFolderInfoAdapter(Context context,
                                    FileManagerService fileManagerService,
                                    FileInfoManager fileInfoManager) {
        super(context, fileManagerService, fileInfoManager);
        width = mResources.getDimensionPixelOffset(R.dimen.photo_frame_width);
        height = mResources.getDimensionPixelOffset(R.dimen.photo_frame_height);
        LogUtils.d(TAG, "PictureFolderInfoAdapter: width=" + width + ", height=" + height);
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        FileViewHolder viewHolder;
        View view = convertView;

        if (view == null) {
            view = mInflater.inflate(R.layout.adapter_picture_folder_info, null);
            viewHolder = new FileViewHolder(
                    (TextView) view.findViewById(R.id.edit_adapter_name),
                    (TextView) view.findViewById(R.id.edit_adapter_desc),
                    (MyExceptionImageView) view.findViewById(R.id.edit_adapter_img),
                    (ImageView)view.findViewById(R.id.edit_adapter_enter),
                    (CheckBox)view.findViewById(R.id.edit_adapter_cb));
            view.setTag(viewHolder);
        } else {
            viewHolder = (FileViewHolder) view.getTag();
        }

        FileInfo currentItem = mFileInfoList.get(pos);
        LogUtils.d(TAG, "getView, pos = " + pos + ", mMode = " + mMode + ", checked=" + currentItem.isChecked());
        viewHolder.mName.setText(currentItem.getShowName());
        viewHolder.mEnter.setVisibility(
                (currentItem.isDirectory() && (MODE_EDIT!=mMode)) ? View.VISIBLE : View.INVISIBLE);
        viewHolder.mCheck.setVisibility(MODE_EDIT==mMode ? View.VISIBLE : View.GONE);

        switch (mMode) {
            case MODE_EDIT:
                viewHolder.mCheck.setChecked(currentItem.isChecked() ? true : false);
                setCntText(viewHolder.mDesc, currentItem);
                break;

            case MODE_NORMAL:
                setCntText(viewHolder.mDesc, currentItem);
                break;

            case MODE_SEARCH:
                setCntText(viewHolder.mDesc, currentItem);
                break;
        }
        setImage(viewHolder.mIcon, currentItem);

        return view;
    }


    void setCntText(TextView textView, FileInfo fileInfo) {
        if (fileInfo.isDirectory()) {
            int pictures = 0;
            File dirFile = fileInfo.getFile();
            if (null != dirFile.listFiles()) {
                for (File file : dirFile.listFiles()) {
                    String fileName = file.getName();
                    if(fileName!=null&&fileName.startsWith(".")){
                        continue;
                    }
                    String mimeType = FileUtils.getFileMimeType(fileName);
                    //LogUtils.d(TAG, "setCntText: fileName=" + fileName + ", mimeType=" + mimeType);

                    if (!TextUtils.isEmpty(mimeType)
                            && MediaUtils.isImage(mimeType)) {
                        pictures++;
                    }
                }
            }

            StringBuilder sb = new StringBuilder();
            sb.append(pictures).append(mResources.getString(R.string.picture_cnt));

            textView.setText(sb.toString());
        }
    }

    void setImage(ImageView iv, FileInfo fileInfo) {
        if (fileInfo.isDirectory()) {
            File dirFile = fileInfo.getFile();
            String imagePath = null;
            if (null != dirFile.listFiles()) {
                for (File file : dirFile.listFiles()) {
                    String fileName = file.getName();
                    String mimeType = FileUtils.getFileMimeType(fileName);
                    //LogUtils.d(TAG, "setCntText: fileName=" + fileName + ", mimeType=" + mimeType);

                    if (!TextUtils.isEmpty(mimeType)
                            && MediaUtils.isImage(mimeType)) {
                        imagePath = file.getAbsolutePath();
                        LogUtils.d(TAG, "setImage: imagePath AbsolutePath=" + imagePath + ", file.getPath()=" + file.getPath());
                        break;
                    }
                }
            }

            if (!TextUtils.isEmpty(imagePath)) {
                LogUtils.d(TAG, "setImage: ");
                Glide.with(mContext)
                        .load(new File(imagePath))
                        .dontAnimate()
                        .placeholder(iv.getDrawable())
                        .override(width, height)
                        .centerCrop()
                        .into(iv);
            }
        }
    }

}
