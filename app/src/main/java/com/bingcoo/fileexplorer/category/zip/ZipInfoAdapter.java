package com.bingcoo.fileexplorer.category.zip;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bingcoo.fileexplorer.R;
import com.bingcoo.fileexplorer.util.FileUtils;
import com.bingcoo.fileexplorer.util.IconManager;
import com.bingcoo.fileexplorer.util.LogUtils;

import java.util.List;

import static com.bingcoo.fileexplorer.util.FileInfo.MIMETYPE_M4A;
import static com.bingcoo.fileexplorer.util.FileInfo.MIMETYPE_MP3;
import static com.bingcoo.fileexplorer.util.FileInfo.MIMETYPE_RAR;

/**
 * 作者：zhshh
 * 内容摘要：
 * 创建日期：2017/2/24
 * 修改者， 修改日期， 修改内容
 */

public class ZipInfoAdapter extends BaseAdapter {

    private static final String TAG = "ZipInfoAdapter";

    static {
        LogUtils.setDebug(TAG, true);
    }

    private List<ZipInfo> mZipLists;
    private Context mContext;
//    private FileIconLoader mFileIconLoader;

    public List<ZipInfo> getZipLists() {
        return mZipLists;
    }

    public void setZipLists(List<ZipInfo> zipLists) {
        this.mZipLists = zipLists;
    }

    public ZipInfoAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mZipLists == null ? 0 : mZipLists.size();
    }

    @Override
    public Object getItem(int position) {
        return mZipLists == null ? null : mZipLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FileViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.adapter_zipinfos, null);
            viewHolder = new FileViewHolder();
            viewHolder.mName = (TextView) convertView.findViewById(R.id.edit_adapter_name);
            viewHolder.mDesc = (TextView) convertView.findViewById(R.id.edit_adapter_desc);
            viewHolder.mIcon = (ImageView) convertView.findViewById(R.id.edit_adapter_img);
            viewHolder.mEnter = (ImageView) convertView.findViewById(R.id.edit_adapter_enter);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (FileViewHolder) convertView.getTag();
        }
        ZipInfo currentItem = mZipLists.get(position);
        viewHolder.mName.setText(currentItem.getName());
        viewHolder.mDesc.setVisibility(currentItem.isDirectory() ? View.GONE : View.VISIBLE);
//        boolean end = position == mZipLists.size() - 1;
        if (!currentItem.isDirectory()) {
            viewHolder.mEnter.setVisibility(View.INVISIBLE);
            viewHolder.mDesc.setText(FileUtils.sizeToString(mZipLists.get(position).getSize()));
            setIcon(viewHolder.mIcon, currentItem);
        } else {
            viewHolder.mEnter.setVisibility(View.VISIBLE);
            viewHolder.mIcon.setImageResource(R.drawable.fm_folder);
        }

        return convertView;
    }

    private void setIcon(ImageView view, ZipInfo currentItem) {

        if (view == null || currentItem == null) {
            return;
        }
        String fileName = currentItem.getName();
        String extension = FileUtils.getFileExtension(fileName);
        if (extension == null) {
            view.setImageResource(R.drawable.fm_unknown);
            return;
        }

        String mimeType;
        if (extension.equalsIgnoreCase("mp3")) {
            mimeType = MIMETYPE_MP3;
        } else if (extension.equalsIgnoreCase("m4a")) {
            mimeType = MIMETYPE_M4A;
        } else if (extension.equalsIgnoreCase("rar")) {
            mimeType = MIMETYPE_RAR;
        } else {
//            mimeType = MediaFile.getMimeTypeForFile(fileName);
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
       /* if (!isSupported(fileName)) {
            view.setImageResource(R.drawable.fm_unknown);
            return;
        }*/
        int id=IconManager.getDrawableId(mContext,mimeType);
        view.setImageResource(id);
    }

    /*private FileCategorys getFileType(String fileName) {
        String extension = FileUtils.getFileExtension(fileName);
        if (extension == null) {
            return FileCategorys.Unknown;
        }
        String mimeType =
                MimeTypeMap.getSingleton()
                        .getMimeTypeFromExtension(extension);
        if (TextUtils.isEmpty(mimeType)) {
            if (extension.equals("rar")) {
                return FileCategorys.Zip;
            }
            return FileCategorys.Unknown;
        } else if (mimeType.startsWith("application/vnd.android.package-archive")) {
            return FileCategorys.Apk;
        } else if (mimeType.startsWith("application/zip") || mimeType.startsWith("application/rar")) {
            return FileCategorys.Zip;
        } else if (mimeType.startsWith("application/ogg")) {
            return FileCategorys.Music;
        } else if (mimeType.startsWith("audio/")) {
            return FileCategorys.Music;
        } else if (mimeType.startsWith("image/")) {
            return FileCategorys.Picture;
        } else if (mimeType.startsWith("text/") || idDoc(mimeType)) {
            return FileCategorys.Doc;
        } else if (mimeType.startsWith("video/")) {
            return FileCategorys.Video;
        } else {
            return FileCategorys.Unknown;
        }
    }*/

    /*static String[] docMimeList = new String[]{

            "application/vnd.ms-excel",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.template",
            "application/vnd.ms-powerpoint",
            "application/vnd.openxmlformats-officedocument.presentationml.presentation",
            "application/vnd.openxmlformats-officedocument.presentationml.template",
            "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
            "application/msword",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.template"
    };*/

    /*private boolean idDoc(String mimeType) {
        boolean isDoc = false;
        if (TextUtils.isEmpty(mimeType)) {
            return false;
        }
        for (int i = 0; i < docMimeList.length; i++) {
            if (docMimeList[i].equals(mimeType)) {
                isDoc = true;
                break;
            }
        }
        return isDoc;
    }*/

    protected static class FileViewHolder {
        public TextView mName;
        public TextView mDesc;
        public ImageView mIcon;
        public ImageView mEnter;
    }

    /*private enum FileCategorys {
        Unknown, Music, Video, Picture, Doc, Zip, Apk
    }*/

    /*private boolean isSupported(String fileName) {
        String extension = FileUtils.getFileExtension(fileName);
        if (extension == null) {
            return false;
        }
        String mimeType;
        if (extension.equals("rar")) {
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension("zip");
        } else {
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        PackageManager packageManager = mContext.getPackageManager();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setType(mimeType);
        ResolveInfo info = packageManager.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return info != null;
    }*/

    /**
     * This method gets index of certain fileInfo(item) in fileInfoList
     *
     * @param zipInfo the fileInfo which wants to be located.
     * @return the index of the item in the listView.
     */
    public int getPosition(ZipInfo zipInfo) {
        return mZipLists.indexOf(zipInfo);
    }

}
