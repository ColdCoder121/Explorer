package com.bingcoo.fileexplorer.category.picture;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.ColorStateList;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.storage.StorageVolume;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bingcoo.fileexplorer.R;
import com.bingcoo.fileexplorer.base.BaseActivity;
import com.bingcoo.fileexplorer.destination.DstActivity;
import com.bingcoo.fileexplorer.dialog.fragment.AlertDialogFragment;
import com.bingcoo.fileexplorer.dialog.fragment.ProgressDialog;
import com.bingcoo.fileexplorer.dialog.fragment.ProgressDialogFragment;
import com.bingcoo.fileexplorer.home.FileInfoAdapter;
import com.bingcoo.fileexplorer.receiver.MountReceiver;
import com.bingcoo.fileexplorer.service.FileManagerService;
import com.bingcoo.fileexplorer.service.HeavyOperationListener;
import com.bingcoo.fileexplorer.service.ProgressInfo;
import com.bingcoo.fileexplorer.util.DrmManager;
import com.bingcoo.fileexplorer.util.FileCategoryHelper;
import com.bingcoo.fileexplorer.util.FileInfo;
import com.bingcoo.fileexplorer.util.FileInfoManager;
import com.bingcoo.fileexplorer.util.FileUtils;
import com.bingcoo.fileexplorer.util.IntentUtils;
import com.bingcoo.fileexplorer.util.KeyboardUtils;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.MediaUtils;
import com.bingcoo.fileexplorer.util.MountPointManager;
import com.bingcoo.fileexplorer.util.PermissionUtils;
import com.jakewharton.rxbinding.widget.RxTextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import bingo.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.functions.Action1;

import static com.bingcoo.fileexplorer.R.string.rename;
import static com.bingcoo.fileexplorer.category.ListActivity.SAVED_HAS_RESULT;
import static com.bingcoo.fileexplorer.category.video.VideoGridActivity.SAVED_ADAPTER_MODE;
import static com.bingcoo.fileexplorer.category.video.VideoGridActivity.SAVED_CHECKED_LIST;
import static com.bingcoo.fileexplorer.category.video.VideoGridActivity.SAVED_FILE_LIST;
import static com.bingcoo.fileexplorer.home.HomeActivity.MY_PERMISSIONS_REQUEST_TO_READ_EXTERNAL_STORAGE;

/**
 * 类名称：PictureGridActivity
 * 作者：David
 * 内容摘要：
 * 创建日期：2017/2/4
 * 修改者， 修改日期， 修改内容
 */
public class PictureGridActivity extends BaseActivity {
    private static final String TAG = "PictureGridActivity";

    static {
        LogUtils.setDebug(TAG, true);
    }

    private static final int REQUEST_CODE_PASTE = 0x100;
    private static final int REQUEST_CODE_BROWSE = 0x101;
    public static final int MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_DELETE = 5;
    public static final int MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_CUT = 6;
    public static final int MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_RENAME = 7;
    public static final int MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_COPY = 8;

    public static final String EXTRA_PICTURE_DIRECTORY = "extra_picture_directory";

    public static final String DELETE_DIALOG_TAG = "delete_dialog_fragment_tag";
    public static final String RENAME_DIALOG_TAG = "rename_dialog_fragment_tag";
    public static final String SETUP_DIALOG_TAG = "audio_setup_dialog_fragment_tag";

    private static final String SAVED_SELECTED_PATH_KEY = "saved_selected_path";
    public static final String SAVED_SEARCH_TEXT = "search_text";
    public static final String SAVED_SEARCH_TOTAL = "search_total";
    public static final String SAVED_SETUP_MAP = "setup_map";
    public static final String SAVED_SHOW_OPTION_DIALOG = "show_option_dialog";
    public static final String SAVED_SHOW_MORE_DIALOG = "show_more_dialog";
    private static final String NEW_FILE_PATH_KEY = "new_file_path_key";

    private FileCategoryHelper.FileCategory mFileCategory = FileCategoryHelper.FileCategory.Picture;
    //    private String mDir = null;
    protected int mSelectedTop = -1;
    protected int mTop = -1;
    protected FileInfo mSelectedFileInfo = null;
    private String mSearchText;
    private AlertDialog mOptionsDialog;
    private AlertDialog mMoreDialog;
    private ProgressDialog mRefreshingProgressDialog;
    protected boolean mIsAlertDialogShowing = false;
    private long mSearchTotal;
    private HashMap<Integer, Intent> mSetupMap;
    private String mRenamedName;
    private FileInfo mRenamedItem;
    private List<FileInfo> mFileListWhenOut = new ArrayList<>();//触发onsaveinstancestate时的列表
    private boolean hasResult;

    @BindView(R.id.img_dst_back)
    ImageView mImgBack;
    @BindView(R.id.tv_dst_title)
    TextView mTvTitle;

    @BindView(R.id.ll_menu)
    View mViewMenu;

    @BindView(R.id.grd_media)
    GridView mGrdMedia;
    @BindView(R.id.layout_loading)
    View mLayoutLoading;
    @BindView(R.id.empty_view)
    View mLayoutEmptyView;

    @BindView(R.id.tv_select_all)
    TextView mTvSelectAll;
    @BindView(R.id.tv_select_chosen)
    TextView mTvSelectChosen;
    @BindView(R.id.tv_select_cancel)
    TextView mTvSelectCancel;

    @BindView(R.id.tv_share)
    TextView mTvShare;
    @BindView(R.id.tv_delete)
    TextView mTvDelete;
    @BindView(R.id.tv_cut)
    TextView mTvCut;
    @BindView(R.id.tv_more)
    TextView mTvMore;

    @BindView(R.id.layout_action_mode_search)
    View mLayoutActionModeSearch;
    @BindView(R.id.layout_action_mode_title)
    View mLayoutActionModeTitle;
    @BindView(R.id.layout_action_mode_select)
    View mLayoutActionModeSelect;

    @BindView(R.id.layout_edit)
    View mBottomEdit;
    @BindView(R.id.layout_menu)
    View mBottomMenu;

    @BindView(R.id.img_search_back)
    ImageView mImgSearchBack;
    @BindView(R.id.et_search_input)
    EditText mEtSearchInput;
    @BindView(R.id.img_search)
    ImageView mImgSearch;
    @BindView(R.id.layout_local_search_result)
    View mLayoutSearchResult;
    @BindView(R.id.tv_search_result)
    TextView mTvSearchResult;

    @BindView(R.id.layout_grid)
    View mLayoutList;
    @BindView(R.id.layout_searching)
    View mLayoutSearching;
    @BindView(R.id.layout_no_matching_search)
    View mLayoutNoMatchingSearch;

    private HeavyOperationListener.HeavyOperationListenerCallback mHeavyOperationListenerCallback =
            new HeavyOperationListener.HeavyOperationListenerCallback() {

                @Override
                public void onTaskResult(int errorType) {
                    mFileInfoManager.updateFileInfoList(mSortType);
                    mAdapter.notifyDataSetChanged();
                    if (isEmpty()) {
                        switchLayout(mLayoutEmptyView);
                    }
                    if (mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
                        onBackPressed();
                    }
                    setResult(RESULT_OK);
                }

                @Override
                public void onClick(View v) {
                    if (mService != null) {
                        LogUtils.i(TAG, "onClick cancel");
                        mService.cancel(PictureGridActivity.this.getClass().getName());
                    }
                }
            };

    /**
     * 操作文件后判断列表是否为空
     *
     * @return
     */
    private boolean isEmpty() {
        if (mAdapter != null) {
            int cnt = mAdapter.getCount();
            if (cnt == 0) {
                return true;
            } else {
                for (int i = 0; i < cnt; i++) {
                    if (mAdapter.getItem(i).getFileName().startsWith(".")) {
                        continue;
                    } else {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_grid);
        ButterKnife.bind(this);
        mSavedInstanceState = savedInstanceState;
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!PermissionUtils.hasStorageReadPermission(this)) {
            /*PermissionUtils.requestPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    MY_PERMISSIONS_REQUEST_TO_READ_EXTERNAL_STORAGE);*/
//            ActivityManager.getInstance().exit();
            finish();
        }
    }

    private void init() {
        Intent intent = getIntent();
        if (null != intent) {
            mCurrentPath = intent.getStringExtra(EXTRA_PICTURE_DIRECTORY);
            LogUtils.d(TAG, "init: mDir=" + mCurrentPath);
        }

        mTvTitle.setText(R.string.picture_title);
        setViewClickListener(mImgBack,
                new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                });

        setViewClickListener(mTvSelectAll,
                new Runnable() {
                    @Override
                    public void run() {
                        if (null != mAdapter) {
                            boolean isSelectedAll = mAdapter.getCheckedItemsCount() == mAdapter.getCount();
                            mAdapter.setAllItemChecked(!isSelectedAll);

                            mTvSelectChosen.setText(
                                    getResources().getString(R.string.select_chosen, mAdapter.getCheckedItemsCount()));
                            mTvSelectAll.setText(isSelectedAll ? R.string.select_all : R.string.select_not_all);

                            handleEditView();
                        }
                    }
                });

        setViewClickListener(mTvSelectCancel,
                new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                    }
                });

        setViewClickListener(mViewMenu,
                new Runnable() {
                    @Override
                    public void run() {
                        showOptionDialog();
                    }
                });

        setViewClickListener(mImgSearchBack,
                new Runnable() {
                    @Override
                    public void run() {
                        LogUtils.d(TAG, "ImgSearchBack run: ");
                        KeyboardUtils.hideSoftInput(PictureGridActivity.this);
                        onBackPressed();
                    }
                });

        RxTextView.editorActions(mEtSearchInput)
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer actionId) {
                        LogUtils.d(TAG, "searchInput call: actionId=" + actionId
                                + ", IME_ACTION_SEARCH=" + EditorInfo.IME_ACTION_SEARCH);
                        if (EditorInfo.IME_ACTION_SEARCH == actionId) {
                            KeyboardUtils.hideSoftInput(PictureGridActivity.this);
                            requestSearch(mEtSearchInput.getText().toString());
                        }
                    }
                });

        setViewClickListener(mImgSearch,
                new Runnable() {
                    @Override
                    public void run() {
                        LogUtils.d(TAG, "ImgSearch run: ");
                        requestSearch(mEtSearchInput.getText().toString());
                    }
                });


        if (null != mGrdMedia) {
            mGrdMedia.setOnItemClickListener(this);
            mGrdMedia.setOnItemLongClickListener(this);
            mGrdMedia.setFastScrollEnabled(false);
            mGrdMedia.setVerticalScrollBarEnabled(true);
        }

        initEditView();
        switchLayout(mGrdMedia);
        switchTitle(mLayoutActionModeTitle);
        switchContent(mLayoutList);
    }

    private void initEditView() {
        setViewClickListener(mTvShare,
                new Runnable() {
                    @Override
                    public void run() {
                        if (null != mAdapter) {
                            FileInfo fileInfo = mAdapter.getFirstCheckedFileInfoItem();
                            if (null != fileInfo) {
                                boolean isSharingIntentStarted =
                                        IntentUtils.share(PictureGridActivity.this, fileInfo.getFileAbsolutePath());
                                if (!isSharingIntentStarted) {
                                    mToastHelper.showToast(R.string.no_method);
                                }
                            }
                        }
                    }
                });

        setViewClickListener(mTvDelete,
                new Runnable() {
                    @Override
                    public void run() {
                        if (!PermissionUtils.hasStorageWritePermission(PictureGridActivity.this)) {
                            PermissionUtils.requestPermission(PictureGridActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_DELETE);
                            return;
                        }
                        showDeleteDialog();
                    }
                });

        setViewClickListener(mTvCut,
                new Runnable() {
                    @Override
                    public void run() {
                        if (!PermissionUtils.hasStorageWritePermission(PictureGridActivity.this)) {
                            PermissionUtils.requestPermission(PictureGridActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_CUT);
                            return;
                        }
                        Intent intent = new Intent(getApplicationContext(), DstActivity.class);
                        intent.putExtra(DstActivity.EXTRA_TYPE, FileInfoManager.PASTE_MODE_CUT);
                        intent.putExtra(DstActivity.EXTRA_LIST, (Serializable) mAdapter.getCheckedFileInfoItemsList());
                        startActivityForResult(intent, REQUEST_CODE_PASTE);
                        hasResult = true;
                    }
                });

        setViewClickListener(mTvMore,
                new Runnable() {
                    @Override
                    public void run() {
                        showMoreDialog();
                    }
                });
    }

    @Override
    protected void serviceConnected() {
        super.serviceConnected();

        mFileInfoManager.getShowFileList().clear();
        mAdapter = new PictureGridInfoAdapter(this, mService, mFileInfoManager);
        if (null != mGrdMedia) {
            mGrdMedia.setAdapter(mAdapter);
            if (null == mSavedInstanceState) {
                showContent();
            } else {
                /*mSearchText = mSavedInstanceState.getString(SAVED_SEARCH_TEXT);
                if (!TextUtils.isEmpty(mSearchText)) {
                    mSearchTotal = mSavedInstanceState.getLong(SAVED_SEARCH_TOTAL);
                    mLayoutSearchResult.setVisibility(View.VISIBLE);
                    mTvSearchResult.setText(getResources().getString(
                            R.string.search_result, mSearchTotal));
                    requestSearch(mSearchText);
                }*/
                hasResult = mSavedInstanceState.getBoolean(SAVED_HAS_RESULT);
                mFileListWhenOut = mSavedInstanceState.getParcelableArrayList(SAVED_FILE_LIST);
//                mShowFileList = mSavedInstanceState.getParcelableArrayList(SAVED_SHOW_LIST);
//                mListChanged = mSavedInstanceState.getBoolean(SAVED_LIST_CHANGED);
                int adapterMode = mSavedInstanceState.getInt(SAVED_ADAPTER_MODE);
                mAdapter.changeMode(adapterMode);
                if (mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
                    //编辑模式
                    List<FileInfo> checkedList = mSavedInstanceState.getParcelableArrayList(SAVED_CHECKED_LIST);
                    if (mFileListWhenOut.size() > 0 && checkedList != null) {
                        mFileInfoManager.getShowFileList().addAll(mFileListWhenOut);
                        for (FileInfo info : checkedList) {
                            info.setChecked(true);
                        }
                        mAdapter.notifyDataSetChanged();
                        switchTitle(mLayoutActionModeSelect);
                        mTvSelectChosen.setText(getResources().getString(
                                R.string.select_chosen, mAdapter.getCheckedItemsCount()));
                        boolean isSelectedAll = mAdapter.getCheckedItemsCount() == mAdapter.getCount();
                        mTvSelectAll.setText(isSelectedAll ? R.string.select_not_all : R.string.select_all);
                        handleEditView();
                        if (hasResult) {
                            hasResult = false;
                            exitEditMode();
                            showContent();
//                            setResult(RESULT_OK);
                        }
                    } else {
                        showContent();
                    }
                } else {
                    if (mFileListWhenOut.size() > 0) {
                        mFileInfoManager.getShowFileList().addAll(mFileListWhenOut);
                        mAdapter.notifyDataSetChanged();
                        if (0 == mAdapter.getCount()) {
                            switchLayout(mLayoutEmptyView);
                        } else {
                            switchLayout(mGrdMedia);
                        }
                    } else {
                        showContent();
                    }
                }
                restoreDialog();
            }
            mAdapter.notifyDataSetChanged();
        }

        // register Receiver when service connected..
        if (null == mMountReceiver) {
            mMountReceiver = MountReceiver.registerMountReceiver(this);
            mMountReceiver.registerMountListener(this);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        LogUtils.d(TAG, "onItemClick: position=" + position + ", mode=" + mAdapter.getMode());
        if (mService != null && mService.isBusy(this.getClass().getName())) {
            LogUtils.d(TAG, "onItemClick, service is busy,return. ");
            return;
        }

        if (null != mAdapter) {
            if (position >= mAdapter.getCount() || position < 0) {
                LogUtils.e(TAG, "onItemClick, events error, mFileInfoList.size()= "
                        + mAdapter.getCount());
                return;
            }
            if (mAdapter.isMode(FileInfoAdapter.MODE_NORMAL)
                    || mAdapter.isMode(FileInfoAdapter.MODE_SEARCH)) {

               /* if (View.VISIBLE == mLayoutActionModeSearch.getVisibility()) {
                    switchTitle(mLayoutActionModeTitle);
                    mAdapter.changeMode(FileInfoAdapter.MODE_NORMAL);
                    mLayoutSearchResult.setVisibility(View.GONE);
                    showContent();
                }*/

                FileInfo selectedItem = mAdapter.getItem(position);
                if (null != selectedItem) {
                    if (!selectedItem.isDirectory()) {
                        String path = selectedItem.getFileAbsolutePath();
                        if (!TextUtils.isEmpty(path)) {
                            openFile(selectedItem);
                        }
                    }
                }

            } else if (mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
                boolean state = mAdapter.getItem(position).isChecked();
                LogUtils.d(TAG, "onItemClick, edit view . position=" + position + ", state=" + state);
                mAdapter.setChecked(position, !state);
                mAdapter.notifyDataSetChanged();
                mTvSelectChosen.setText(getResources().getString(
                        R.string.select_chosen, mAdapter.getCheckedItemsCount()));
                boolean isSelectedAll = mAdapter.getCheckedItemsCount() == mAdapter.getCount();
                mTvSelectAll.setText(isSelectedAll ? R.string.select_not_all : R.string.select_all);
                handleEditView();
            }
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        LogUtils.d(TAG, "onItemLongClick: position=" + position);
        if (mAdapter.isMode(FileInfoAdapter.MODE_NORMAL)
                || mAdapter.isMode(FileInfoAdapter.MODE_SEARCH)) {
            if (!mService.isBusy(this.getClass().getName())) {
                int top = view.getTop();
                switchToEditView(position, top);
                return true;
            }
        }
        return false;
    }

    @Override
    public void onDialogDismiss() {
        LogUtils.d(TAG, "dialog dismissed...");
        mIsAlertDialogShowing = false;
    }

    @Override
    public void onBackPressed() {
        if (mAdapter != null && mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
            mAdapter.changeMode(FileInfoAdapter.MODE_NORMAL);
            if (View.VISIBLE == mLayoutSearchResult.getVisibility()) {
                switchTitle(mLayoutActionModeSearch);
            } else {
                switchTitle(mLayoutActionModeTitle);
            }
            return;
        }

        LogUtils.d(TAG, "onBackPressed: mLayoutActionModeSearch.getVisibility()=" + mLayoutActionModeSearch.getVisibility());
        if (View.VISIBLE == mLayoutActionModeSearch.getVisibility()) {
            switchTitle(mLayoutActionModeTitle);
            switchContent(mLayoutList);
            mAdapter.changeMode(FileInfoAdapter.MODE_NORMAL);
            mLayoutSearchResult.setVisibility(View.GONE);
            showContent();
            mSearchText = null;
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        /*if (RESULT_OK == resultCode) {
            switch (requestCode) {
                case REQUEST_CODE_PASTE:
                    showContent();
                    exitEditMode();
                    setResult(RESULT_OK);
                    break;

                default:
                    break;
            }
        }*/
        if (REQUEST_CODE_PASTE == requestCode) {
            hasResult = false;
            exitEditMode();
            if (RESULT_OK == resultCode) {
//                String dir = data == null ? null : data.getStringExtra(EXTRA_PASTE_DIRECTORY);
//                if (mCurrentPath != null && dir != null &&
//                        dir.equals(mCurrentPath)) {
                    showContent();
//                }
                setResult(RESULT_OK);
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    protected void reloadContent() {
        LogUtils.d(TAG, "****reloadContent******");
        if (mService != null && !mService.isBusy(this.getClass().getName())) {
            if (MountPointManager.getInstance().isRootPath(mCurrentPath)
                    || (mFileInfoManager != null && mFileInfoManager.isPathModified(mCurrentPath))) {
                LogUtils.d(TAG, "**** change need to reloadContent******");
                showContent();
                setResult(RESULT_OK);
            } else if (mFileInfoManager != null && mAdapter != null) {
                LogUtils.d(TAG, "****no change ******");
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mService!=null&&(mService.isSearchTask(this.getClass().getName()) ||
                mService.isListTask(this.getClass().getName()))) {
            mService.cancel(this.getClass().getName());
        }
        if (mAdapter != null && mAdapter.getCheckedItemsCount() == 1) {
            FileInfo selectFileInfo = mAdapter.getCheckedFileInfoItemsList().get(0);
            if (selectFileInfo != null) {
                outState.putString(SAVED_SELECTED_PATH_KEY, selectFileInfo.getFileAbsolutePath());
            }
        }
//        if (View.VISIBLE == mLayoutActionModeSearch.getVisibility()) {
        outState.putString(SAVED_SEARCH_TEXT, mSearchText);
        outState.putLong(SAVED_SEARCH_TOTAL, mSearchTotal);
//        }

        if (null != mSetupMap) {
            outState.putSerializable(SAVED_SETUP_MAP, mSetupMap);
        }
        if (mFileInfoManager != null) {
            mFileListWhenOut.clear();
            mFileListWhenOut.addAll(mFileInfoManager.getShowFileList());
            outState.putParcelableArrayList(SAVED_FILE_LIST, (ArrayList<? extends Parcelable>) mFileListWhenOut);

        }
        if (mAdapter != null) {
            outState.putInt(SAVED_ADAPTER_MODE, mAdapter.getMode());
            outState.putParcelableArrayList(SAVED_CHECKED_LIST, (ArrayList<? extends Parcelable>) mAdapter.getCheckedFileInfoItemsList());
        }
//        outState.putParcelableList(SAVED_SHOW_LIST, mShowFileList);
//        outState.putBoolean(SAVED_LIST_CHANGED, mListChanged);
        if (mOptionsDialog != null && mOptionsDialog.isShowing()) {
            outState.putBoolean(SAVED_SHOW_OPTION_DIALOG, true);
        } else {
            outState.putBoolean(SAVED_SHOW_OPTION_DIALOG, false);
        }
        if (mMoreDialog != null && mMoreDialog.isShowing()) {
            outState.putBoolean(SAVED_SHOW_MORE_DIALOG, true);
        } else {
            outState.putBoolean(SAVED_SHOW_MORE_DIALOG, false);
        }
        outState.putBoolean(SAVED_HAS_RESULT, hasResult);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void doOnMounted(String mountPoint) {
        super.doOnMounted(mountPoint);
        LogUtils.i(TAG, "doOnMounted, mountPoint = " + mountPoint);
        doPrepareForMount(mountPoint);
        showContent();
    }

    @Override
    protected void doOnEjected(String unMountPoint) {
        super.doOnEjected(unMountPoint);
        if (mService != null && mService.isBusy(this.getClass().getName())) {
            mService.cancel(this.getClass().getName());
        }
        showContent();
    }

    @TargetApi(24)
    @Override
    protected void doOnUnMounted(StorageVolume volume) {
        super.doOnUnMounted(volume);
        String unMountPoint = volume.getPath();
        if (mFileInfoManager != null) {
            int pasteCnt = mFileInfoManager.getPasteCount();
            LogUtils.i(TAG, "doOnUnmounted, unMountPoint: " + unMountPoint + ",pasteCnt = "
                    + pasteCnt);

            if (pasteCnt > 0) {
                FileInfo fileInfo = mFileInfoManager.getPasteList().get(0);
                if (fileInfo.getFileAbsolutePath().startsWith(
                        unMountPoint + MountPointManager.SEPARATOR)) {
                    LogUtils.i(TAG, "doOnUnmounted, clear paste list. ");
                    mFileInfoManager.clearPasteList();
                }
            }
        }

        if (mService != null && mService.isBusy(this.getClass().getName())) {
            mService.cancel(this.getClass().getName());
        }
        showToastForUnmount(volume);

        DialogFragment listFragment = (DialogFragment) getFragmentManager()
                .findFragmentByTag(ListListener.LIST_DIALOG_TAG);
        if (listFragment != null) {
            LogUtils.d(TAG, "onUnmounted, listFragment dismiss. ");
            listFragment.dismissAllowingStateLoss();
        }
        showContent();
    }

    @Override
    protected void doOnSdSwap() {
        super.doOnSdSwap();
        mMountPointManager.init(getApplicationContext());
        showContent();
    }

    private void doPrepareForMount(String mountPoint) {
        LogUtils.i(TAG, "doPrepareForMount, mountPoint = " + mountPoint);
        if (mService != null && mService.isBusy(this.getClass().getName())) {
            mService.cancel(this.getClass().getName());
        }
        mMountPointManager.init(getApplicationContext());
    }

    @TargetApi(24)
    private void showToastForUnmount(StorageVolume volume) {
        LogUtils.d(TAG, "showToastForUnmount, path = " + volume.getPath());
        if (isResumed()) {
            String unMountPointDescription = volume.getDescription(this);
            LogUtils.d(TAG, "showToastForUnmount, unMountPointDescription:"
                    + unMountPointDescription);
            mToastHelper.showToast(getString(R.string.unmounted, unMountPointDescription));
        }
    }

    private void switchToEditView(int position, int top) {
        LogUtils.d(TAG, "switchToEditView: position=" + position + ", top=" + top);
        switchTitle(mLayoutActionModeSelect);
        mAdapter.setChecked(position, true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mGrdMedia.setSelectionFromTop(position, top);
        } else {
            mGrdMedia.setSelection(position);
        }
        switchToEditView();
    }

    private void switchToEditView() {
        LogUtils.d(TAG, "Switch to edit view");
        mGrdMedia.setFastScrollEnabled(false);
        mAdapter.changeMode(FileInfoAdapter.MODE_EDIT);
        mTvSelectChosen.setText(getResources().getString(
                R.string.select_chosen, mAdapter.getCheckedItemsCount()));
        boolean isSelectedAll = mAdapter.getCheckedItemsCount() == mAdapter.getCount();
        mTvSelectAll.setText(isSelectedAll ? R.string.select_not_all : R.string.select_all);
        handleEditView();
    }

    private void handleEditView() {
        boolean showShare = false;
        if (null != mAdapter) {
            showShare = mAdapter.getCheckedItemsCount() == 1;
            if (showShare) {
                FileInfo fileInfo = mAdapter.getFirstCheckedFileInfoItem();
                showShare = !fileInfo.isDirectory();
            }
        }
        mTvShare.setEnabled(showShare);

        boolean hasCheckedItem = (null != mAdapter) ? mAdapter.getCheckedItemsCount() > 0 : false;
        mTvDelete.setEnabled(hasCheckedItem);
        mTvCut.setEnabled(hasCheckedItem);
        mTvMore.setEnabled(hasCheckedItem);
    }

    private void switchContent(View content) {
        if (null != content) {
            content.setVisibility(View.VISIBLE);

            if (content != mLayoutList) {
                mLayoutList.setVisibility(View.GONE);
            }

            if (content != mLayoutSearching) {
                mLayoutSearching.setVisibility(View.GONE);
            }

            if (content != mLayoutNoMatchingSearch) {
                mLayoutNoMatchingSearch.setVisibility(View.GONE);
            }
        }
    }

    private void switchTitle(View title) {
        if (null != title) {
            title.setVisibility(View.VISIBLE);

            if (title != mLayoutActionModeSearch) {
                mLayoutActionModeSearch.setVisibility(View.GONE);
            } else {
                switchBottomBar(null);
            }

            if (title != mLayoutActionModeTitle) {
                mLayoutActionModeTitle.setVisibility(View.GONE);
            } else {
                switchBottomBar(mBottomMenu);
            }

            if (title != mLayoutActionModeSelect) {
                mLayoutActionModeSelect.setVisibility(View.GONE);
            } else {
                switchBottomBar(mBottomEdit);
            }
        }
    }

    private void switchBottomBar(View bottomBar) {
        if (null != bottomBar) {
            bottomBar.setVisibility(View.VISIBLE);

            if (bottomBar != mBottomEdit) {
                mBottomEdit.setVisibility(View.GONE);
            }

            if (bottomBar != mBottomMenu) {
                mBottomMenu.setVisibility(View.GONE);
            }
        } else {
            mBottomEdit.setVisibility(View.GONE);
            mBottomMenu.setVisibility(View.GONE);
        }
    }

    private void showContent() {
        if (isFinishing()) {
            LogUtils.i(TAG, "showContent, isFinishing: true, do not loading again");
            return;
        }

        if ((null != mService) && (null != mFileCategory)) {
            mService.listDirs(this.getClass().getName(), mFileCategory, mSortType, mCurrentPath, new PictureListListener());
        }
    }

    private void showDeleteDialog() {
        LogUtils.d(TAG, "show DeleteDialog...");
        if (mIsAlertDialogShowing) {
            LogUtils.d(TAG, "Another Dialog is exist, return!~~");
            return;
        }

        if (isResumed()) {
            mIsAlertDialogShowing = true;
            AlertDialogFragment.AlertDialogFragmentBuilder builder = new AlertDialogFragment.AlertDialogFragmentBuilder();
            AlertDialogFragment deleteDialogFragment =
                    builder.setMessage(R.string.delete_msg)
                            .setDoneTitle(R.string.ok)
                            .setCancelTitle(R.string.cancel)
                            .setTitle(R.string.delete)
                            .create();
            deleteDialogFragment.setOnDoneListener(new DeleteListener());
            deleteDialogFragment.setOnDialogDismissListener(this);
            deleteDialogFragment.show(getFragmentManager(), DELETE_DIALOG_TAG);
            boolean ret = getFragmentManager().executePendingTransactions();
            LogUtils.d(TAG, "executing pending transactions result: " + ret);

            View titleDivider = deleteDialogFragment.getDialog().findViewById(R.id.titleDivider);
            if (null != titleDivider) {
                LogUtils.d(TAG, "showDeleteDialog: null != titleDivider");
                titleDivider.setVisibility(View.GONE);
            }

            TextView msg = (TextView) deleteDialogFragment.getDialog().findViewById(R.id.message);
            if (null != msg) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    msg.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                }
            }
        }
    }

    private void showMoreDialog() {
        if (null == mMoreDialog) {
            View view;
            if (null != mAdapter) {
                int checkedItemCnt = mAdapter.getCheckedItemsCount();
                if (1 == checkedItemCnt) {
                    view = getLayoutInflater().inflate(R.layout.dialog_more_single_audio_file, null);
                } else {
                    view = getLayoutInflater().inflate(R.layout.dialog_more_multi, null);
                }

                if (null != view) {
                    setViewClickListener(view.findViewById(R.id.tv_more_open_mode),
                            new Runnable() {

                                @Override
                                public void run() {
                                    if (null != mAdapter) {
                                        openFile(mAdapter.getFirstCheckedFileInfoItem());
                                        mMoreDialog.cancel();
                                        exitEditMode();
                                    }
                                }
                            });

                    setViewClickListener(view.findViewById(R.id.tv_more_audio_setup),
                            new Runnable() {

                                @Override
                                public void run() {
                                    if (null != mAdapter) {
                                        showSetupDialog();
                                        mMoreDialog.cancel();
                                        exitEditMode();
                                    }
                                }
                            });

                    setViewClickListener(view.findViewById(R.id.tv_more_copy),
                            new Runnable() {
                                @Override
                                public void run() {
                                    if (!PermissionUtils.hasStorageWritePermission(PictureGridActivity.this)) {
                                        PermissionUtils.requestPermission(PictureGridActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                                MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_COPY);
                                        mMoreDialog.cancel();
                                        return;
                                    }
                                    Intent intent = new Intent(PictureGridActivity.this, DstActivity.class);
                                    intent.putExtra(DstActivity.EXTRA_TYPE, FileInfoManager.PASTE_MODE_COPY);
                                    intent.putExtra(DstActivity.EXTRA_LIST, (Serializable) mAdapter.getCheckedFileInfoItemsList());
                                    startActivityForResult(intent, REQUEST_CODE_PASTE);
                                    mMoreDialog.cancel();
                                    hasResult = true;
                                }
                            });

                    setViewClickListener(view.findViewById(R.id.tv_more_rename),
                            new Runnable() {
                                @Override
                                public void run() {
                                    mMoreDialog.cancel();
                                    if (!PermissionUtils.hasStorageWritePermission(PictureGridActivity.this)) {
                                        PermissionUtils.requestPermission(PictureGridActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                                MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_RENAME);
                                        return;
                                    }
                                    showRenameDialog();
                                    exitEditMode();
                                }
                            });

                    setViewClickListener(view.findViewById(R.id.tv_more_details),
                            new Runnable() {
                                @Override
                                public void run() {
                                    mService.getDetailInfo(PictureGridActivity.this.getClass().getName(),
                                            mAdapter.getCheckedFileInfoItemsList().get(0),
                                            new DetailInfoListener(mAdapter.getCheckedFileInfoItemsList().get(0)));
                                    mMoreDialog.cancel();
                                    exitEditMode();
                                }
                            });

                    mMoreDialog = new AlertDialog.Builder(this)
                            .setView(view)
                            .create();
                    mMoreDialog.setCanceledOnTouchOutside(true);
                    mMoreDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            mMoreDialog = null;
                        }
                    });
                }

            }
        }

        if (!mMoreDialog.isShowing()) {
            mMoreDialog.show();
        }
    }

    private void switchLayout(View layout) {
        if (null != layout) {
            layout.setVisibility(View.VISIBLE);

            if (layout != mGrdMedia) {
                mGrdMedia.setVisibility(View.GONE);
            }

            if (layout != mLayoutLoading) {
                mLayoutLoading.setVisibility(View.GONE);
            }

            if (layout != mLayoutEmptyView) {
                mLayoutEmptyView.setVisibility(View.GONE);
            }
        }
    }

    private void showOptionDialog() {
        if (null == mOptionsDialog) {
            View view = getLayoutInflater().inflate(R.layout.dialog_local_options, null);
            View searchView = view.findViewById(R.id.tv_option_searching);
            View searchViewLine = view.findViewById(R.id.search_line);
            if (searchView != null) {
                searchView.setVisibility(View.GONE);
            }
            if (searchViewLine != null) {
                searchViewLine.setVisibility(View.GONE);
            }
            /*setViewClickListener(view.findViewById(R.id.tv_option_searching),
                    new Runnable() {
                        @Override
                        public void run() {
                            mOptionsDialog.cancel();
                            switchTitle(mLayoutActionModeSearch);
                            mEtSearchInput.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    KeyboardUtils.showSoftInput(PictureGridActivity.this, mEtSearchInput);
                                }
                            }, 200);
                        }
                    });*/

            setViewClickListener(view.findViewById(R.id.tv_option_refresh),
                    new Runnable() {
                        @Override
                        public void run() {
                            handleRefresh();
                            mOptionsDialog.cancel();
                        }
                    });
            view.findViewById(R.id.tv_option_refresh).setBackgroundResource(R.drawable.top_corner_bg);
            setViewClickListener(view.findViewById(R.id.tv_option_sorting),
                    new Runnable() {
                        @Override
                        public void run() {
                            showSortDialog();
                            mOptionsDialog.cancel();
                        }
                    });
            view.findViewById(R.id.tv_option_sorting).setBackgroundResource(R.drawable.bottom_corner_bg);
            View viewNewFolder = view.findViewById(R.id.tv_option_create_new_folder);
            View viewNewFolderLine = view.findViewById(R.id.new_folder_line);
            if (null != viewNewFolder) {
                viewNewFolder.setVisibility(View.GONE);
            }
            if (null != viewNewFolderLine) {
                viewNewFolderLine.setVisibility(View.GONE);
            }

            mOptionsDialog = new AlertDialog.Builder(this)
                    .setView(view)
                    .create();
            mOptionsDialog.setCanceledOnTouchOutside(true);
        }

        if (!mOptionsDialog.isShowing()) {
            mOptionsDialog.show();
        }
    }

    protected void handleRefresh() {
        LogUtils.d(TAG, "handleRefresh: ");

        mRefreshingProgressDialog = new ProgressDialog(this);
        mRefreshingProgressDialog.setMessage(getString(R.string.refreshing));
        mRefreshingProgressDialog.setIndeterminate(true);
        mRefreshingProgressDialog.setCancelable(false);
        mRefreshingProgressDialog.show();

        //int id = ResourceUtils.systemId(this, "progress", ID);
        ProgressBar progressBar = (ProgressBar) mRefreshingProgressDialog.findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressBar.setIndeterminateTintList(
                    ColorStateList.valueOf(ContextCompat.getColor(this, R.color.bingo_theme_color)));
            progressBar.setIndeterminateTintMode(PorterDuff.Mode.SRC_ATOP);
        } else {
            progressBar.setIndeterminateDrawable(ContextCompat.getDrawable(this, R.drawable.loading_progress));
        }
        //String[] paths = new String[] {Environment.getExternalStorageDirectory().getAbsolutePath()};
        String[] paths = new String[mMountPointManager.getMountCount()];
        mMountPointManager.getMountPointPaths().toArray(paths);
        final int pathsSize = paths.length;
        MediaScannerConnection.OnScanCompletedListener callback =
                new MediaScannerConnection.OnScanCompletedListener() {
                    private int cnt = 0;

                    @Override
                    public void onScanCompleted(String path, Uri uri) {
                        LogUtils.d(TAG, "handleRefresh onScanCompleted: path=" + path + ", uri=" + uri);
                        cnt++;
                        if (cnt == pathsSize) {
                            mRefreshingProgressDialog.cancel();
                            showContent();
                        }
                    }
                };
        MediaUtils.scanFiles(this, paths, null, callback);
    }

    protected void showSortDialog() {
        LogUtils.d(TAG, "show SortDialog...");
        if (mIsAlertDialogShowing) {
            LogUtils.d(TAG, "Another Dialog is exist, return!~~");
            return;
        }

        if (isResumed()) {
            mIsAlertDialogShowing = true;
            AlertDialogFragment.ChoiceDialogFragmentBuilder builder = new AlertDialogFragment.ChoiceDialogFragmentBuilder();
            builder.setDefault(R.array.sort_order, mSortType).setTitle(R.string.sort_order);
            AlertDialogFragment.ChoiceDialogFragment sortDialogFragment = builder.create();
            sortDialogFragment.setItemClickListener(new SortClickListener());
            sortDialogFragment.setOnDialogDismissListener(this);
            sortDialogFragment.show(getFragmentManager(), AlertDialogFragment.ChoiceDialogFragment.CHOICE_DIALOG_TAG);
            boolean ret = getFragmentManager().executePendingTransactions();
            LogUtils.d(TAG, "executing pending transactions result: " + ret);
        }
    }

    private void sortFileInfoList() {
        LogUtils.d(TAG, "Start sortFileInfoList()");

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.sorting_in));
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();

        //int id = ResourceUtils.systemId(this, "progress", ID);
        ProgressBar progressBar = (ProgressBar) progressDialog.findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressBar.setIndeterminateTintList(
                    ColorStateList.valueOf(ContextCompat.getColor(this, R.color.bingo_theme_color)));
            progressBar.setIndeterminateTintMode(PorterDuff.Mode.SRC_ATOP);
        } else {
            progressBar.setIndeterminateDrawable(ContextCompat.getDrawable(this, R.drawable.loading_progress));
        }
        progressDialog.getWindow().getDecorView().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        int selection = mGrdMedia.getFirstVisiblePosition(); // save current
                        // refresh only when paste or delete operation is performed
                        mFileInfoManager.sort(mSortType);
                        mAdapter.notifyDataSetChanged();
                        mGrdMedia.setSelection(selection);
                        // restore the selection in the navigation view

                        LogUtils.d(TAG, "End sortFileInfoList()");

                        progressDialog.cancel();
                    }
                }, 1000);
    }

    protected void showSetupDialog() {
        if (mIsAlertDialogShowing) {
            LogUtils.d(TAG, "Another Dialog is exist, return!~~");
            return;
        }

        String mime = "image/*";
        Uri uri = mAdapter.getFirstCheckedFileInfoItem().getContentUri();
        LogUtils.d(TAG, "showSetupDialog: uri=" + uri);

        Intent intent = new Intent();
        intent.setDataAndType(uri, mime)
                .setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                .setAction(Intent.ACTION_ATTACH_DATA)
                .putExtra("mimeType", mime);
        PackageManager pm = getPackageManager();
        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        if (null != resolveInfos) {
            CharSequence[] array = new CharSequence[resolveInfos.size()];
            HashMap<Integer, Intent> map = new HashMap();

            for (int i = 0; i < resolveInfos.size(); i++) {
                ResolveInfo resolveInfo = resolveInfos.get(i);
                LogUtils.d(TAG, "showSetupDialog: resolveInfo=" + resolveInfo);

                CharSequence label = resolveInfo.loadLabel(pm);
                array[i] = label;

                Intent intentCopy = new Intent(intent);
                intentCopy.setClassName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
                map.put(i, intentCopy);
            }

            if (isResumed()) {
                mIsAlertDialogShowing = true;
                AlertDialogFragment.ChoiceDialogFragmentBuilder builder = new AlertDialogFragment.ChoiceDialogFragmentBuilder();
                int defaultChoice = 0;
                builder.setDefault(array, defaultChoice)
                        .setTitle(R.string.more_audio_setup)
                        .setCancelTitle(R.string.cancel)
                        .setDoneTitle(R.string.ok);
                AlertDialogFragment.ChoiceDialogFragment setupDialogFragment = builder.create();
                SetupClickListener setupClickListener = new SetupClickListener();
                setupClickListener.setMap(map);
                setupDialogFragment.setItemClickListener(setupClickListener);
                setupDialogFragment.setOnDialogDismissListener(this);
                setupDialogFragment.show(getFragmentManager(), SETUP_DIALOG_TAG);
                boolean ret = getFragmentManager().executePendingTransactions();
                LogUtils.d(TAG, "executing pending transactions result: " + ret);
            }
        }
    }

    protected void showRenameDialog() {
        LogUtils.d(TAG, "show RenameDialog...");
        if (mIsAlertDialogShowing) {
            LogUtils.d(TAG, "Another Dialog showing, return!~~");
            return;
        }
        FileInfo fileInfo = mAdapter.getFirstCheckedFileInfoItem();
        mRenamedItem = fileInfo;
        int selection = 0;
        if (fileInfo != null) {
            String name = fileInfo.getFileName();
            String fileExtension = FileUtils.getFileExtension(name);
            selection = name.length();
            if (!fileInfo.isDirectory() && fileExtension != null) {
                selection = selection - fileExtension.length() - 1;
            }
            if (isResumed()) {
                mIsAlertDialogShowing = true;
                AlertDialogFragment.EditDialogFragmentBuilder builder = new AlertDialogFragment.EditDialogFragmentBuilder();
                builder.setDefault(name, selection).setDoneTitle(R.string.done).setCancelTitle(
                        R.string.cancel).setTitle(rename);
                AlertDialogFragment.EditTextDialogFragment renameDialogFragment = builder.create();
                renameDialogFragment.setOnEditTextDoneListener(new RenameDoneListener(fileInfo));
                renameDialogFragment.setOnDialogDismissListener(this);
                renameDialogFragment.show(getFragmentManager(), RENAME_DIALOG_TAG);
                boolean ret = getFragmentManager().executePendingTransactions();
                LogUtils.d(TAG, "executing pending transactions result: " + ret);

                View titleDivider = renameDialogFragment.getDialog().findViewById(R.id.titleDivider);
                if (null != titleDivider) {
                    LogUtils.d(TAG, "showCreateFolderDialog: null != titleDivider");
                    titleDivider.setVisibility(View.GONE);
                }
            }
        }
    }

    /**
     * 重命名重复后再次显示弹窗
     */
    protected void showRenameDialogAgain() {
        LogUtils.d(TAG, "show RenameDialog...");
        if (mIsAlertDialogShowing) {
            LogUtils.d(TAG, "Another Dialog showing, return!~~");
            return;
        }
        FileInfo fileInfo = mRenamedItem;
        int selection = 0;
        if (fileInfo != null) {
            String name = fileInfo.getFileName();
            String fileExtension = FileUtils.getFileExtension(name);
            selection = name.length();
            if (!fileInfo.isDirectory() && fileExtension != null) {
                selection = selection - fileExtension.length() - 1;
            }
            if (isResumed()) {
                mIsAlertDialogShowing = true;
                AlertDialogFragment.EditDialogFragmentBuilder builder = new AlertDialogFragment.EditDialogFragmentBuilder();
                builder.setDefault(name, selection).setDoneTitle(R.string.done).setCancelTitle(
                        R.string.cancel).setTitle(rename);
                AlertDialogFragment.EditTextDialogFragment renameDialogFragment = builder.create();
                renameDialogFragment.setOnEditTextDoneListener(new RenameDoneListener(fileInfo));
                renameDialogFragment.setOnDialogDismissListener(this);
                renameDialogFragment.show(getFragmentManager(), RENAME_DIALOG_TAG);
                boolean ret = getFragmentManager().executePendingTransactions();
                LogUtils.d(TAG, "executing pending transactions result: " + ret);

                View titleDivider = renameDialogFragment.getDialog().findViewById(R.id.titleDivider);
                if (null != titleDivider) {
                    LogUtils.d(TAG, "showCreateFolderDialog: null != titleDivider");
                    titleDivider.setVisibility(View.GONE);
                }
            }
        }
    }

    private void showOpenMethodDialog(FileInfo fileInfo) {
        LogUtils.d(TAG, "show Open method Dialog...");
        if (mIsAlertDialogShowing) {
            LogUtils.d(TAG, "Another Dialog is exist, return!~~");
            return;
        }

        if (isResumed()) {
            mIsAlertDialogShowing = true;
            AlertDialogFragment.ChoiceDialogFragmentBuilder builder =
                    new AlertDialogFragment.ChoiceDialogFragmentBuilder();
            builder.setDefault(R.array.open_method, OpenMethodClickListener.OPEN_METHOD_TEXT)
                    .setTitle(R.string.unknown_format)
                    .setCancelTitle(R.string.cancel)
                    .setDoneTitle(R.string.ok);
            AlertDialogFragment.ChoiceDialogFragment openDialogFragment = builder.create();
            OpenMethodClickListener clickListener = new OpenMethodClickListener(fileInfo);
            openDialogFragment.setItemClickListener(clickListener);
            openDialogFragment.setOnDialogDismissListener(this);
            openDialogFragment.show(getFragmentManager(),
                    AlertDialogFragment.ChoiceDialogFragment.CHOICE_DIALOG_TAG);
            boolean ret = getFragmentManager().executePendingTransactions();
            LogUtils.d(TAG, "executing pending transactions result: " + ret);
        }
    }

    private void requestSearch(String query) {
        LogUtils.d(TAG, "requestSearch: query=" + query);
        if (!TextUtils.isEmpty(query)) {
            switchTitle(mLayoutActionModeSearch);
            switchContent(mLayoutSearching);

            if (null != mService) {
                mService.search(this.getClass().getName(), query, mCurrentPath, mFileCategory, mSortType, new SearchListener(query));
            }
        }
    }

    private void exitEditMode() {
        if (mAdapter != null && mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
            mAdapter.changeMode(FileInfoAdapter.MODE_NORMAL);
            if (View.VISIBLE == mLayoutSearchResult.getVisibility()) {
                switchTitle(mLayoutActionModeSearch);
            } else {
                switchTitle(mLayoutActionModeTitle);
            }
        }
    }

    private int restoreSelectedPosition() {
        if (mSelectedFileInfo == null) {
            return -1;
        } else {
            int curSelectedItemPosition = mAdapter.getPosition(mSelectedFileInfo);
            mSelectedFileInfo = null;
            return curSelectedItemPosition;
        }
    }

    private void openFile(FileInfo selectedItem) {
        if (null == selectedItem) {
            return;
        }

        boolean canOpen = true;
        String mimeType = selectedItem.getFileMimeType(mService);

        if (selectedItem.isDrmFile()) {
            mimeType = DrmManager.getInstance().getOriginalMimeType(
                    selectedItem.getFileAbsolutePath());

            if (TextUtils.isEmpty(mimeType)) {
                canOpen = false;
                mToastHelper.showToast(R.string.msg_unable_open_file);
            }
        }

        if (canOpen) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = selectedItem.getUri();
            LogUtils.d(TAG, "openFile, Open uri file: " + uri);
            intent.setDataAndType(uri, mimeType);

            try {
                startActivityForResult(intent, REQUEST_CODE_BROWSE);
            } catch (android.content.ActivityNotFoundException e) {
                //mToastHelper.showToast(R.string.msg_unable_open_file);
                showOpenMethodDialog(selectedItem);
                LogUtils.w(TAG, "openFile, Cannot open file: "
                        + selectedItem.getFileAbsolutePath());
            }
        }
    }

    protected void restoreDialog() {
        // Restore the heavy_dialog : pasting deleting
        ProgressDialogFragment pf = (ProgressDialogFragment) getFragmentManager()
                .findFragmentByTag(HeavyOperationListener.HEAVY_DIALOG_TAG);
        if (pf != null) {
            if (mService.isBusy(this.getClass().getName())
                    && mService.isHeavyOperationTask(this.getClass().getName())) {
                HeavyOperationListener listener = new HeavyOperationListener(
                        AlertDialogFragment.INVIND_RES_ID, this, mHeavyOperationListenerCallback);
                mService.reconnected(this.getClass().getName(), listener);
                pf.setCancelListener(listener);
            } else {
                pf.dismissAllowingStateLoss();
            }
        }

        // list dialog
        DialogFragment listFramgent = (DialogFragment) getFragmentManager().findFragmentByTag(
                ListListener.LIST_DIALOG_TAG);
        if (listFramgent != null) {
            LogUtils.i(TAG, "listFramgent != null");
            if (mService.isBusy(this.getClass().getName())) {
                LogUtils.i(TAG, "list reconnected mService");
                mService.reconnected(this.getClass().getName(), new PictureListListener());
            } else {
                LogUtils.i(TAG, "the list is complete dismissAllowingStateLoss");
                listFramgent.dismissAllowingStateLoss();
            }
        }

        // sort dialog
        AlertDialogFragment.ChoiceDialogFragment sortDialogFragment = (AlertDialogFragment.ChoiceDialogFragment) getFragmentManager()
                .findFragmentByTag(AlertDialogFragment.ChoiceDialogFragment.CHOICE_DIALOG_TAG);
        if (sortDialogFragment != null) {
            sortDialogFragment.setItemClickListener(new SortClickListener());
        }

        String saveSelectedPath = mSavedInstanceState.getString(SAVED_SELECTED_PATH_KEY);
        FileInfo saveSelectedFile = null;
        if (saveSelectedPath != null) {
            saveSelectedFile = new FileInfo(saveSelectedPath);
        }
        HashMap<Integer, Intent> map = (HashMap<Integer, Intent>) mSavedInstanceState.getSerializable(SAVED_SETUP_MAP);

        // setup dialog
        AlertDialogFragment.ChoiceDialogFragment audioSetupDialogFragment = (AlertDialogFragment.ChoiceDialogFragment) getFragmentManager()
                .findFragmentByTag(SETUP_DIALOG_TAG);
        if (sortDialogFragment != null && map != null) {
            SetupClickListener setupClickListener = new SetupClickListener();
            setupClickListener.setMap(map);
            audioSetupDialogFragment.setItemClickListener(setupClickListener);
        }

        // rename dialog
        AlertDialogFragment.EditTextDialogFragment renameDialogFragment = (AlertDialogFragment.EditTextDialogFragment) getFragmentManager()
                .findFragmentByTag(RENAME_DIALOG_TAG);
        if (renameDialogFragment != null && saveSelectedFile != null) {
            renameDialogFragment
                    .setOnEditTextDoneListener(new RenameDoneListener(saveSelectedFile));
        }

        AlertDialogFragment af;
        // delete dialog
        af = (AlertDialogFragment) getFragmentManager().findFragmentByTag(DELETE_DIALOG_TAG);
        if (af != null) {
            af.setOnDoneListener(new DeleteListener());
        }
        if (mSavedInstanceState != null && mSavedInstanceState.getBoolean(SAVED_SHOW_MORE_DIALOG)) {
            showMoreDialog();
        }
        if (mSavedInstanceState != null && mSavedInstanceState.getBoolean(SAVED_SHOW_OPTION_DIALOG)) {
            showOptionDialog();
        }

       /* //详情弹窗显示的时候已经退出编辑模式了，所以原来的saveSelectedFile永远为空
        String saveDetailPath = mSavedInstanceState.getString(SAVED_DETAIL_PATH_KEY);
        FileInfo saveDetailFile = null;
        if (saveDetailPath != null) {
            saveDetailFile = new FileInfo(saveDetailPath);
        }
        // Restore the detail_dialog
        af = (AlertDialogFragment) getFragmentManager().findFragmentByTag(
                DetailInfoListener.DETAIL_DIALOG_TAG);
        if (af != null && saveDetailFile != null && mService != null) {
            DetailInfoListener listener = new DetailInfoListener(saveDetailFile);
            af.setDismissListener(listener);
            if (mService.isBusy(this.getClass().getName()) && mService.isDetailTask(this.getClass().getName())) {
                mService.reconnected(this.getClass().getName(), listener);
            } *//*else if (!mService.isBusy(this.getClass().getName())) {
                af.dismissAllowingStateLoss();
                mService.getDetailInfo(this.getClass().getName(), saveDetailFile, listener);
            } else {
                af.dismissAllowingStateLoss();
            }*//*
        } else if (af != null && saveDetailFile == null) {
            af.dismissAllowingStateLoss();
            mIsAlertDialogShowing = false;
        }*/
    }

    class PictureListListener extends ListListener {
        @Override
        public void onTaskProgress(ProgressInfo progressInfo) {
            super.onTaskProgress(progressInfo);
            if (isResumed()) {
                switchLayout(mLayoutLoading);
            }
        }

        @Override
        public void onTaskResult(int result) {
            LogUtils.d(TAG, "LstListListener onTaskResult: ");
            super.onTaskResult(result);
//            if (mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
//                mFileInfoManager.loadFileInfoList(mCurrentPath, mSortType, mSelectedFileInfo);
//                mSelectedFileInfo = mAdapter.getFirstCheckedFileInfoItem();
//            } else {
                mFileInfoManager.loadFileInfoList(mSortType);
//            }

            mAdapter.notifyDataSetChanged();
            int selectedItemPosition = restoreSelectedPosition();
            if (selectedItemPosition == -1) {

            } else if (selectedItemPosition >= 0 && selectedItemPosition < mAdapter.getCount()) {
                if (mSelectedTop != -1) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        mGrdMedia.setSelectionFromTop(selectedItemPosition, mSelectedTop);
                    } else {
                        mGrdMedia.setSelection(selectedItemPosition);
                    }
                    mSelectedTop = -1;
                } else if (mTop != -1) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        mGrdMedia.setSelectionFromTop(selectedItemPosition, mTop);
                    } else {
                        mGrdMedia.setSelection(selectedItemPosition);
                    }
                    mTop = -1;
                } else {
                    mGrdMedia.setSelection(selectedItemPosition);
                }
            }

            LogUtils.d(TAG, "onTaskResult: cnt=" + mGrdMedia.getCount() + ",adapterCnt=" + mAdapter.getCount());
            if (0 == mAdapter.getCount()) {
                switchLayout(mLayoutEmptyView);
            } else {
                switchLayout(mGrdMedia);
            }
        }
    }

    private class SortClickListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialog, int id) {
            LogUtils.d(TAG, "SortClickListener onClick: id=" + id);
            if (id != mSortType) {
                setPrefsSortBy(id);
                sortFileInfoList();
            }
            dialog.dismiss();
        }
    }

    protected class SearchListener implements FileManagerService.OperationEventListener {
        private static final int FIRST_UPDATE_COUNT = 20;
        private static final int NEED_UPDATE_LIST = 6;
        private boolean mIsResultSet = false;
        private int mCount = 0;

        /**
         * Constructor of SearchListener.
         *
         * @param text the search target(String), which will be shown on searchResult TextView..
         */
        public SearchListener(String text) {
            if (text == null) {
                throw new IllegalArgumentException();
            }
            mSearchText = text;
        }

        @Override
        public void onTaskResult(int result) {
            LogUtils.d(TAG, "SearchListener onTaskResult: ");
            if (mTvSearchResult != null && !mIsResultSet) {
                LogUtils.d(TAG, "onTaskProgress: mSearchTotal=" + mSearchTotal);
                mLayoutSearchResult.setVisibility(View.VISIBLE);
                mTvSearchResult.setText(getResources().getString(
                        R.string.search_result, mSearchTotal));
                mIsResultSet = true;
            }
            if (0 == mSearchTotal) {
                switchContent(mLayoutNoMatchingSearch);
            } else {
                switchContent(mLayoutList);
                mFileInfoManager.updateSearchList();
                mAdapter.notifyDataSetChanged();
            }
            mSearchTotal = 0;
        }

        @Override
        public void onTaskPrepare() {
            LogUtils.d(TAG, "SearchListener onTaskPrepare: ");
            mAdapter.changeMode(FileInfoAdapter.MODE_SEARCH);
        }

        @Override
        public void onTaskProgress(ProgressInfo progressInfo) {
            if (!progressInfo.isFailInfo()) {
                mSearchTotal = progressInfo.getTotal();
                /*if (progressInfo.getProgeress() == progressInfo.getTotalNumber() &&
                        mTvSearchResult != null && !mIsResultSet) {
                    LogUtils.d(TAG, "onTaskProgress: mSearchTotal=" + mSearchTotal);
                    mLayoutSearchResult.setVisibility(View.VISIBLE);
                    mTvSearchResult.setText(getResources().getString(
                            R.string.search_result, mSearchTotal));
                    mIsResultSet = true;
                }*/

                if (progressInfo.getFileInfo() != null) {
                    mFileInfoManager.addItem(progressInfo.getFileInfo());
                }
                mCount++;
                if (mCount > FIRST_UPDATE_COUNT) {
                    if (mGrdMedia.getLastVisiblePosition() + NEED_UPDATE_LIST > mAdapter.getCount()) {
                        mFileInfoManager.updateSearchList();
                        mAdapter.notifyDataSetChanged();
                        mCount = 0;
                    }
                }
            }
        }
    }

    private class DeleteListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialog, int id) {
            LogUtils.d(TAG, "onClick() method for alertDeleteDialog, OK button");
            final AlertDialogFragment deleteFragment = (AlertDialogFragment) getFragmentManager().
                    findFragmentByTag(DELETE_DIALOG_TAG);
            if (null != deleteFragment) {
                deleteFragment.dismissAllowingStateLoss();
            }
            if (mService != null) {
                HeavyOperationListener listener = new HeavyOperationListener(R.string.deleting, PictureGridActivity.this,
                        mHeavyOperationListenerCallback);
                mService.deleteFiles(PictureGridActivity.this.getClass().getName(),
                        mAdapter.getCheckedFileInfoItemsList(), listener);
            }
        }
    }

    protected class DetailInfoListener implements FileManagerService.OperationEventListener,
            DialogInterface.OnDismissListener {
        public static final String DETAIL_DIALOG_TAG = "detaildialogtag";
        private FileInfo mFileInfo;

        public DetailInfoListener(FileInfo fileInfo) {
            mFileInfo = fileInfo;
        }

        @Override
        public void onTaskPrepare() {
            if (isResumed()) {
                AlertDialogFragment.AlertDialogFragmentBuilder builder = new AlertDialogFragment.AlertDialogFragmentBuilder();
                AlertDialogFragment detailFragment = builder.setCancelTitle(R.string.ok).setLayout(
                        R.layout.dialog_picture_details).setTitle(R.string.details).create();

                detailFragment.setDismissListener(this);
                detailFragment.show(getFragmentManager(), DETAIL_DIALOG_TAG);
                boolean ret = getFragmentManager().executePendingTransactions();
                LogUtils.d(TAG, "executing pending transactions result: " + ret);
                Dialog dialog = detailFragment.getDialog();
                if (dialog != null) {
                    setViewContent(dialog.findViewById(R.id.tv_detail_name), mFileInfo.getShowName());
                    setViewContent(dialog.findViewById(R.id.tv_detail_type),
                            mFileInfo.getFileMimeType(mService));
                    setViewContent(dialog.findViewById(R.id.tv_detail_size), mFileInfo.getFileSizeStr());
                    setViewContent(dialog.findViewById(R.id.tv_detail_resolution), getResolution(mFileInfo.getFileAbsolutePath()));
                    setViewContent(dialog.findViewById(R.id.tv_detail_time),
                            FileUtils.formatModifiedTime(getApplicationContext(), mFileInfo.getFileLastModifiedTime()));
                    setViewContent(dialog.findViewById(R.id.tv_detail_route), mFileInfo.getShowParentPath());
                }
            } else {
                LogUtils.e(TAG, "onTaskPrepare activity is not resumed");
            }
        }

        private void setViewContent(View view, String content) {
            if (view instanceof TextView) {
                TextView tv = (TextView) view;
                tv.setText(content);
            }
        }

        @Override
        public void onTaskProgress(ProgressInfo progressInfo) {

        }

        @Override
        public void onTaskResult(int result) {
            LogUtils.d(TAG, "DetailInfoListener onTaskResult.");
            AlertDialogFragment detailFragment = (AlertDialogFragment) getFragmentManager().findFragmentByTag(DETAIL_DIALOG_TAG);
            if (detailFragment != null) {
                //detailFragment.getArguments().putString(DETAIL_INFO_KEY, mStringBuilder.toString());
                //detailFragment.getArguments().putLong(DETAIL_INFO_SIZE_KEY, mFileLength);
            } else {
                // this case may happen in case of this operation already canceled.
                LogUtils.d(TAG, "get detail fragment is null...");
            }
            return;
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            if (mService != null) {
                LogUtils.d(this.getClass().getName(), "onDismiss");
                mService.cancel(PictureGridActivity.this.getClass().getName());
            }
        }

        private String getResolution(String filePath) {
            String resolution = "";
            /*ExifInterface exif = new ExifInterface();
            try {
                exif.readExif(filePath);
            } catch (FileNotFoundException e) {
                LogUtils.w(TAG, "Could not find file to read exif: " + filePath, e);
            } catch (IOException e) {
                LogUtils.w(TAG, "Could not read exif from file: " + filePath, e);
            }

            String width = getExifData(exif.getTag(ExifInterface.TAG_IMAGE_WIDTH));
            String height = getExifData(exif.getTag(ExifInterface.TAG_IMAGE_LENGTH));
            resolution = width + " * " + height;*/
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(filePath, options);
            resolution = options.outWidth + " * " + options.outHeight;
            return resolution;
        }

    }

    protected class RenameDoneListener implements AlertDialogFragment.EditTextDialogFragment.EditTextDoneListener {
        FileInfo mSrcfileInfo;

        public RenameDoneListener(FileInfo srcFile) {
            mSrcfileInfo = srcFile;
        }

        @Override
        public void onClick(String text) {

            AlertDialogFragment.EditTextDialogFragment rename = (AlertDialogFragment.EditTextDialogFragment) getFragmentManager().findFragmentByTag(RENAME_DIALOG_TAG);
            if (rename != null) {
                rename.dismiss();
            }
            String newFilePath = mSrcfileInfo.getFileParentPath() + MountPointManager.SEPARATOR + text;
            mRenamedName = text;
            LogUtils.d(TAG, "RenameDoneListener onClick: newFilePath=" + newFilePath);
            if (null == mSrcfileInfo) {
                LogUtils.w(TAG, "mSrcfileInfo is null.");
                return;
            }
            if (FileUtils.isExtensionChange(newFilePath, mSrcfileInfo.getFileAbsolutePath())) {
                showRenameExtensionDialog(mSrcfileInfo, newFilePath);
            } else {
                if (mService != null) {
                    mService.rename(PictureGridActivity.this.getClass().getName(),
                            mSrcfileInfo, new FileInfo(newFilePath), new LightOperationListener(
                                    FileUtils.getFileName(newFilePath)));
                }
            }
        }
    }

    private void showRenameExtensionDialog(final FileInfo srcFile, final String newFilePath) {
        LogUtils.d(TAG, "show Open method Dialog...");
//        if (mIsAlertDialogShowing) {
//            LogUtils.d(TAG, "Another Dialog is exist, return!~~");
//            return;
//        }

        if (isResumed()) {
            mIsAlertDialogShowing = true;
            AlertDialogFragment.AlertDialogFragmentBuilder builder =
                    new AlertDialogFragment.AlertDialogFragmentBuilder();
            builder.setCancelTitle(R.string.cancel)
                    .setDoneTitle(R.string.ok)
                    .setTitle(R.string.confirm_rename)
                    .setMessage(R.string.rename_extension);
            AlertDialogFragment renameDialogFragment = builder.create();
            renameDialogFragment.setOnDialogDismissListener(this);
            renameDialogFragment.getArguments().putString(NEW_FILE_PATH_KEY, newFilePath);
            renameDialogFragment.setOnDoneListener(new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (mService != null) {
                        mService.rename(PictureGridActivity.this.getClass().getName(),
                                srcFile, new FileInfo(newFilePath), new PictureGridActivity.LightOperationListener(
                                        FileUtils.getFileName(newFilePath)));
                    }
                }
            });
            renameDialogFragment.show(getFragmentManager(),
                    AlertDialogFragment.TAG);
            boolean ret = getFragmentManager().executePendingTransactions();
            LogUtils.d(TAG, "executing pending transactions result: " + ret);
        }
    }

    protected class LightOperationListener implements FileManagerService.OperationEventListener {

        String mDstName = null;

        LightOperationListener(String dstName) {
            mDstName = dstName;
        }

        @Override
        public void onTaskResult(int errorType) {
            LogUtils.i(TAG, "LightOperationListener,TaskResult result = " + errorType);
            switch (errorType) {
                case ERROR_CODE_SUCCESS:
                    //重命名成功
                    Log.e(TAG, "rename task is success");
                    if ((mAdapter != null && mAdapter.getCount() == 0) ||
                            (mAdapter != null && mAdapter.getCount() == 1 &&
                                    mRenamedName != null && mRenamedName.startsWith("."))) {
                        switchLayout(mLayoutEmptyView);
                        mRenamedName = null;
                    }
                    setResult(RESULT_OK);
                case ERROR_CODE_USER_CANCEL:
                    FileInfo fileInfo = mFileInfoManager.updateOneFileInfoList(mSortType);
                    mAdapter.notifyDataSetChanged();
                    if (fileInfo != null) {
                        int position = mAdapter.getPosition(fileInfo);
                        LogUtils.d(TAG, "LightOperation position = " + position);
                        mGrdMedia.setSelection(position);
                    }

                    break;
                case ERROR_CODE_FILE_EXIST:
                    Log.e(TAG, "rename is duplicate");
                    if (mDstName != null) {
                        mToastHelper.showToast(R.string.rename_hint);
                    }
                    showRenameDialogAgain();
                    break;
                case ERROR_CODE_NAME_EMPTY:
                    mToastHelper.showToast(R.string.invalid_empty_name);
                    break;
                case ERROR_CODE_NAME_TOO_LONG:
                    mToastHelper.showToast(R.string.file_name_too_long);
                    break;
                case ERROR_CODE_NOT_ENOUGH_SPACE:
                    mToastHelper.showToast(R.string.insufficient_memory);
                    break;
                case ERROR_CODE_UNSUCCESS:
                    mToastHelper.showToast(R.string.operation_fail);
                    break;
                default:
                    LogUtils.e(TAG, "wrong errorType for LightOperationListener");
                    break;
            }
        }

        @Override
        public void onTaskPrepare() {
            return;
        }

        @Override
        public void onTaskProgress(ProgressInfo progressInfo) {
            return;
        }
    }

    private class OpenMethodClickListener implements DialogInterface.OnClickListener {
        static final int OPEN_METHOD_TEXT = 0;
        static final int OPEN_METHOD_AUDIO = 1;
        static final int OPEN_METHOD_VIDEO = 2;
        static final int OPEN_METHOD_IMAGE = 3;

        private FileInfo mSelectedItem = null;
        private Intent mOpenIntent = null;

        OpenMethodClickListener(FileInfo selectedItem) {
            mSelectedItem = selectedItem;
            mOpenIntent = new Intent(Intent.ACTION_VIEW);
            open(".txt");
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            LogUtils.d(TAG, "OpenMethodClickListener onClick: which=" + which);
            switch (which) {
                case OPEN_METHOD_TEXT:
                    open(".txt");
                    break;

                case OPEN_METHOD_AUDIO:
                    open(".mp3");
                    break;

                case OPEN_METHOD_VIDEO:
                    open(".3gp");
                    break;

                case OPEN_METHOD_IMAGE:
                    open(".png");
                    break;

                case DialogInterface.BUTTON_POSITIVE:
                    try {
                        startActivity(getOpenIntent());
                        dialog.dismiss();
                    } catch (android.content.ActivityNotFoundException e) {
                        mToastHelper.showToast(R.string.msg_unable_open_file);
                        LogUtils.w(TAG, "OpenMethodClickListener, Cannot open file: ");
                    }
                    break;

                default:
                    break;
            }
        }

        public Intent getOpenIntent() {
            return mOpenIntent;
        }

        private void open(String extension) {
            if ((null != mSelectedItem) && !TextUtils.isEmpty(extension)) {
                Uri uri = mSelectedItem.getUri();
                LogUtils.d(TAG, "openFile, Open uri file: " + uri);
                String mimeType = FileUtils.getMimeTypeBySpecialExtension(mSelectedItem.getFileAbsolutePath(), extension);
                mOpenIntent.setDataAndType(uri, mimeType);
                mOpenIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
        }
    }

    private class SetupClickListener implements DialogInterface.OnClickListener {
        private HashMap<Integer, Intent> mMap;
        private int mChoice;

        public void setMap(HashMap<Integer, Intent> map) {
            mMap = map;
            mSetupMap = mMap;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            if (DialogInterface.BUTTON_POSITIVE == which) {
                Intent intent = mMap.get(mChoice);
                startActivity(intent);
                dialog.cancel();
            } else if (0 <= which) {
                mChoice = which;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        LogUtils.d(TAG, "onRequestPermissionsResult, requestCode:" + requestCode);
        if (null == permissions || permissions.length == 0 ||
                null == grantResults || grantResults.length == 0 ||
                PackageManager.PERMISSION_DENIED == grantResults[0]) {
            LogUtils.e(TAG, "**********onRequestPermissionsResult, Permission or grant res null*******");
            return;
        }

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_DELETE:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    showDeleteDialog();
                }
                break;
            case MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_RENAME:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    showRenameDialog();
                    exitEditMode();
                }
                break;
            case MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_COPY:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    Intent intent = new Intent(PictureGridActivity.this, DstActivity.class);
                    intent.putExtra(DstActivity.EXTRA_TYPE, FileInfoManager.PASTE_MODE_COPY);
                    intent.putExtra(DstActivity.EXTRA_LIST, (Serializable) mAdapter.getCheckedFileInfoItemsList());
                    startActivityForResult(intent, REQUEST_CODE_PASTE);
                    hasResult = true;
                }
                break;
            case MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_CUT:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    Intent intent = new Intent(getApplicationContext(), DstActivity.class);
                    intent.putExtra(DstActivity.EXTRA_TYPE, FileInfoManager.PASTE_MODE_CUT);
                    intent.putExtra(DstActivity.EXTRA_LIST, (Serializable) mAdapter.getCheckedFileInfoItemsList());
                    startActivityForResult(intent, REQUEST_CODE_PASTE);
                    hasResult = true;
                }
                break;
            case MY_PERMISSIONS_REQUEST_TO_READ_EXTERNAL_STORAGE:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    showContent();
                }
                break;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
