package com.bingcoo.fileexplorer.category.zip;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.storage.StorageVolume;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bingcoo.fileexplorer.R;
import com.bingcoo.fileexplorer.dialog.fragment.AlertDialogFragment;
import com.bingcoo.fileexplorer.receiver.MountReceiver;
import com.bingcoo.fileexplorer.service.GetZipListTask;
import com.bingcoo.fileexplorer.service.UnzipFileTask;
import com.bingcoo.fileexplorer.util.ConstUtils;
import com.bingcoo.fileexplorer.util.DrmManager;
import com.bingcoo.fileexplorer.util.FileInfo;
import com.bingcoo.fileexplorer.util.FileInfoComparator;
import com.bingcoo.fileexplorer.util.FileInfoManager;
import com.bingcoo.fileexplorer.util.FileUtils;
import com.bingcoo.fileexplorer.util.IconManager;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.PermissionUtils;
import com.bingcoo.fileexplorer.util.SharedPrefUtils;
import com.bingcoo.fileexplorer.util.ToastHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bingcoo.fileexplorer.home.HomeActivity.MY_PERMISSIONS_REQUEST_TO_READ_EXTERNAL_STORAGE;

/**
 * 类名称：OpenZipsActivity
 * 作者：zhshh
 * 内容摘要：
 * 创建日期：2017/2/24
 * 修改者， 修改日期， 修改内容
 */
public class OpenZipsActivity extends Activity implements MountReceiver.MountListener,
        AdapterView.OnItemClickListener,
        AlertDialogFragment.OnDialogDismissListener {

    private static final String TAG = "OpenZipsActivity";
    public static final String EXTRA_ZIPS_PATH = "extra_zips_path";
    public static final String EXTRA_ZIPS_TYPE = "extra_zips_type";
    public static final String EXTRA_ZIPS_NAME = "extra_zips_name";
    public static final String EXTRA_ZIPS_PARENT = "extra_zips_parent";
    public static final int MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_ZIP = 9;
    private int mFileClickPos;
    private View mClickView;

    //    protected int mSortType = 0;
    protected FileInfoManager mFileInfoManager = null;
    protected ToastHelper mToastHelper = null;
    protected boolean mIsAlertDialogShowing = false;
    private String mCurrentTitle;
    private List<String> mTitleLists = new ArrayList<>();
    private List<String> mPathLists = new ArrayList<>();
    private String mSeparator;
    protected MountReceiver mMountReceiver = null;
    private ZipInfoAdapter mZipAdapter;
    private int mIndex = 0;//第几层目录
    private FileType mFileType;
    private String mZipPath;//当前压缩文件绝对路径  parent/name.zip
    private String mParentPath;//当前压缩文件父目录 parent
    private String mZipFileName;//当前压缩文件名字无后缀  name
    private String mDestDirPath;//文件解压到的路径  parent/.name$bingcoo_$/        parent/.name$bingcoo_$/M205/SignedApk/*.apk
    private String mSelectedItemName;
    private String mSelectedItemPath;
    private String mSelectedItemParentPath;
    private UnzipFileTask mUnzipTask;
    private GetZipListTask mGetListTask;
    private Map<Integer, List<ZipInfo>> mFileMap = new HashMap<>();//每层目录对应的文件列表
    private int mPosition;
    private int[] mPositionTopList = new int[2];
    private Map<String, int[]> mPositionMap = new HashMap<>();
    boolean scrollBack;

    @BindView(R.id.img_dst_back)
    ImageView mImgBack;
    @BindView(R.id.tv_dst_title)
    TextView mTvTitle;
    @BindView(R.id.lst_media)
    ListView mLstMedia;
    @BindView(R.id.layout_loading)
    View mLayoutLoading;
    @BindView(R.id.empty_view)
    View mLayoutEmptyView;
    @BindView(R.id.fl_bottombar)
    View mBottombar;

    static {
        LogUtils.setDebug(TAG, true);
    }

    public enum FileType {
        ZIP,
        RAR
    }

    private int getPrefsSortBy() {
        return SharedPrefUtils.getInstance(ConstUtils.FILE_EXPLORER_PREF)
                .getInt(ConstUtils.PREF_SORT_BY, FileInfoComparator.SORT_BY_TYPE);
    }


    private void switchLayout(View layout) {
        if (null != layout) {
            layout.setVisibility(View.VISIBLE);

            if (layout != mLstMedia) {
                mLstMedia.setVisibility(View.GONE);
            }

            if (layout != mLayoutLoading) {
                mLayoutLoading.setVisibility(View.GONE);
            }

            if (layout != mLayoutEmptyView) {
                mLayoutEmptyView.setVisibility(View.GONE);
            }
        }
    }


    @Override
    public void onDialogDismiss() {
        mIsAlertDialogShowing = false;
    }

    @Override
    public void onMounted(String mountPoint) {
        //sd卡装载
    }

    @TargetApi(24)
    @Override
    public void onUnMounted(StorageVolume volume) {
        //sd卡卸载
        String unMountPath = volume.getPath() + File.separator;
        if (mZipPath != null && mZipPath.startsWith(unMountPath)) {
            if (mUnzipTask != null && mUnzipTask.isBusy()) {
                mUnzipTask.cancel(true);
            }
            if (mGetListTask != null && mGetListTask.isBusy()) {
                mGetListTask.cancel(true);
            }
            showToastForUnmount(volume);
            finish();
            LogUtils.i(TAG, "onUnMounted方法执行");
        }
    }

    @Override
    public void onEjected(String unMountPoint) {
        //sd卡弹出
        //接收到 ACTION_MEDIA_EJECT 广播之后，sd 卡还是可以读写的，
        if (mZipPath != null && mZipPath.startsWith(unMountPoint + File.separator)) {
            if (mUnzipTask != null && mUnzipTask.isBusy()) {
                mUnzipTask.cancel(true);
            }
            if (mGetListTask != null && mGetListTask.isBusy()) {
                mGetListTask.cancel(true);
            }
        }
        LogUtils.i(TAG, "onEjected方法执行");
    }

    @Override
    public void onSdSwap() {
        //切换
    }

    @TargetApi(24)
    private void showToastForUnmount(StorageVolume volume) {
        LogUtils.d(TAG, "showToastForUnmount, path = " + volume.getPath());
        if (isResumed()) {
            String unMountPointDescription = volume.getDescription(this);
            LogUtils.d(TAG, "showToastForUnmount, unMountPointDescription:"
                    + unMountPointDescription);
            mToastHelper.showToast(getString(R.string.unmounted, unMountPointDescription));
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zips);
        ButterKnife.bind(this);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!PermissionUtils.hasStorageReadPermission(this)) {
            /*PermissionUtils.requestPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    MY_PERMISSIONS_REQUEST_TO_READ_EXTERNAL_STORAGE);*/
            finish();
        }
    }

    private void init() {

        mBottombar.setVisibility(View.GONE);
        mToastHelper = new ToastHelper(this);
//        mSortType = getPrefsSortBy();
        Context appCxt = getApplicationContext();
        DrmManager.getInstance().init(appCxt);
        // register unmount/mount Receiver
        if (null == mMountReceiver) {
            mMountReceiver = MountReceiver.registerMountReceiver(this);
            mMountReceiver.registerMountListener(this);
        }
        mZipAdapter = new ZipInfoAdapter(this);
        getZipExtra();
        if (null != mLstMedia) {
            mLstMedia.setOnItemClickListener(this);
            mLstMedia.setFastScrollEnabled(false);
            mLstMedia.setVerticalScrollBarEnabled(true);
            LayoutInflater mInflater = LayoutInflater.from(this);
            View mHeader = mInflater.inflate(R.layout.layout_lst_header, mLstMedia, false);
            ImageView mHeaderImg= (ImageView) mHeader.findViewById(R.id.zip_header_img);
            if(mFileType==FileType.RAR){
                mHeaderImg.setImageResource(R.drawable.fm_rar);
            }else{
                mHeaderImg.setImageResource(R.drawable.fm_zip);
            }
            mLstMedia.addHeaderView(mHeader);
            View mFooter = mInflater.inflate(R.layout.layout_lst_footer, mLstMedia, false);
            mLstMedia.addFooterView(mFooter);
            mLstMedia.setAdapter(mZipAdapter);
        }

        initTitleListener();
        switchLayout(mLayoutLoading);
        showContent();
    }

    private void initTitleListener() {
        //标题栏返回
        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIndex <= 0) {
                    finish();
                } else {
                    scrollBack = true;
                    mIndex = mIndex - 1;
                    if (mTitleLists != null && mTitleLists.size() >= 2) {
                        mCurrentTitle = mTitleLists.get(mTitleLists.size() - 2);
                        mTvTitle.setText(mCurrentTitle);
                        mTitleLists.remove(mTitleLists.size() - 1);
                    }
                    List<ZipInfo> mList;
                    if (mPathLists != null && mPathLists.size() >= 2) {
                        mPathLists.remove(mPathLists.size() - 1);
                        mList = getCurrentPathList(mPathLists.get(mPathLists.size() - 1), mFileMap.get(mIndex));
                    } else {
                        mPathLists.remove(mPathLists.size() - 1);
                        mList = mFileMap.get(mIndex);
                    }
                    notifyDataSetChanged(mList);
                }
            }
        });

    }

    @Override
    public void onBackPressed() {

        if (mIndex >= 1) {
            scrollBack = true;
            mIndex = mIndex - 1;
            if (mTitleLists != null && mTitleLists.size() >= 2) {
                mCurrentTitle = mTitleLists.get(mTitleLists.size() - 2);
                mTitleLists.remove(mTitleLists.size() - 1);
                mTvTitle.setText(mCurrentTitle);
            }
            List<ZipInfo> mList;
            if (mPathLists != null && mPathLists.size() >= 2) {
                mPathLists.remove(mPathLists.size() - 1);
                mList = getCurrentPathList(mPathLists.get(mPathLists.size() - 1), mFileMap.get(mIndex));
            } else {
                mList = mFileMap.get(mIndex);
                mPathLists.remove(mPathLists.size() - 1);
            }
            notifyDataSetChanged(mList);
            return;
        }
        super.onBackPressed();
    }

    private void getZipExtra() {

        Intent intent = getIntent();
        mZipPath = intent.getStringExtra(EXTRA_ZIPS_PATH);
        mZipFileName = intent.getStringExtra(EXTRA_ZIPS_NAME);
        mParentPath = intent.getStringExtra(EXTRA_ZIPS_PARENT);
        mFileType = (FileType) intent.getSerializableExtra(EXTRA_ZIPS_TYPE);
        mDestDirPath = (mParentPath + File.separator + "." + mZipFileName + "$bingcoo_$");
        if (mFileType == FileType.RAR) {
            mSeparator = "\\";
        } else {
            mSeparator = "/";
        }
        mCurrentTitle = mZipFileName;
        mTvTitle.setText(mCurrentTitle);
        mTitleLists.add(mCurrentTitle);

    }

    private void showContent() {
        mGetListTask = new GetZipListTask(mCallback, mFileType, mZipPath);
//        getListTask.setmSortType(mSortType);
        mGetListTask.execute();
    }

    private void notifyDataSetChanged(List<ZipInfo> zipList) {
        if (zipList != null && zipList.size() >= 1) {
            mZipAdapter.setZipLists(zipList);
            switchLayout(mLstMedia);
            mZipAdapter.notifyDataSetChanged();
            File mCurrentFile = new File(((ZipInfo) mZipAdapter.getItem(0)).getPath());
            String mCurrentPath = mCurrentFile.getParent();
            LogUtils.e(TAG, "notifyDataSetChanged*****mCurrentPath=:" + mCurrentPath);
            if (scrollBack && mCurrentPath != null && mPositionMap.containsKey(mCurrentPath)) {
                int mPos = mPositionMap.get(mCurrentPath)[0];
                int mTop = mPositionMap.get(mCurrentPath)[1];
                LogUtils.e(TAG, "*******mPos=:" + mPos);
                LogUtils.e(TAG, "*******mTop=:" + mTop);
                mLstMedia.setSelectionFromTop(mPos, mTop);
                scrollBack = false;
            }
        } else {
            switchLayout(mLayoutEmptyView);
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        LogUtils.d(TAG, "onItemClick: position=" + position);
        mFileClickPos = position;
        mClickView = view;
        //返回上一级
        if (position == 0) {
            if (mIndex == 0) {
                finish();
            } else {
                scrollBack = true;
                mIndex = mIndex - 1;
                if (mTitleLists != null && mTitleLists.size() >= 2) {
                    mCurrentTitle = mTitleLists.get(mTitleLists.size() - 2);
                    mTvTitle.setText(mCurrentTitle);
                    mTitleLists.remove(mTitleLists.size() - 1);
                }
                List<ZipInfo> mList;
                if (mPathLists != null && mPathLists.size() >= 2) {
                    mPathLists.remove(mPathLists.size() - 1);
                    mList = getCurrentPathList(mPathLists.get(mPathLists.size() - 1), mFileMap.get(mIndex));
                } else {
                    mPathLists.remove(mPathLists.size() - 1);
                    mList = mFileMap.get(mIndex);
                }
                notifyDataSetChanged(mList);
            }
            return;
        }
        if (!PermissionUtils.hasStorageWritePermission(OpenZipsActivity.this)) {
            PermissionUtils.requestPermission(OpenZipsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_ZIP);
            return;
        }
        MyZipClick(view, position);
    }

    private void MyZipClick(View view, int position) {
        if (null != mZipAdapter) {
            //有个header
            if (position >= mZipAdapter.getCount() + 1 || position <= 0) {
                return;
            }
            //有个header，所以实际position-1
            ZipInfo selectedItem = (ZipInfo) mZipAdapter.getItem(position - 1);
            if (null != selectedItem) {
                if (!selectedItem.isDirectory()) {
                    //不是文件夹
                    mSelectedItemName = selectedItem.getName();
                    if (!TextUtils.isEmpty(mSelectedItemName)) {
//                        mDestDirPath = (mParentPath + File.separator + "." + System.currentTimeMillis());
                        mSelectedItemPath = (mDestDirPath + File.separator + selectedItem.getPath()).replace("\\", "/");
                        mSelectedItemParentPath = mSelectedItemPath.substring(0, mSelectedItemPath.lastIndexOf("/"));
//                        mNeedDeleteLists.add(mDestDirPath);
                        mUnzipTask = new UnzipFileTask(mZipPath, mDestDirPath, selectedItem.getPath(), unzipCallback);
                        mUnzipTask.setType(mFileType);
                        if (selectedItem.getSize() >= 5 * 1024 * 1024) {
                            switchLayout(mLayoutLoading);
                        }
                        mUnzipTask.execute();
                    }
                } else {
                    //是文件夹
                    mIndex = mIndex + 1;
                    mCurrentTitle = selectedItem.getName();
                    mTvTitle.setText(mCurrentTitle);
                    mTitleLists.add(mCurrentTitle);
                    String currentPath = selectedItem.getPath();
                    File selectedFile = new File(currentPath);
                    int top = view.getTop();
                    mPosition = mZipAdapter.getPosition(selectedItem) + 1;
                    LogUtils.e(TAG, "***********mPosition=:" + mPosition);
                    mPositionTopList[0] = mPosition;
                    mPositionTopList[1] = top;
                    String mSavePath = selectedFile.getParent();
                    LogUtils.e(TAG, "onitemclick*********mSavePath=:" + mSavePath);
                    if (mSavePath != null) {
                        mPositionMap.put(mSavePath, mPositionTopList);
                    }
                    mPathLists.add(currentPath);
                    List<ZipInfo> mList = getCurrentPathList(currentPath, mFileMap.get(mIndex));
                    notifyDataSetChanged(mList);
                }
            }
        }
    }

    UnzipFileTask.UnzipCallback unzipCallback = new UnzipFileTask.UnzipCallback() {
        @Override
        public void successCallback() {
            if (mSelectedItemName.endsWith(".zip")) {
                openZipFile(mSelectedItemPath, mSelectedItemParentPath, mSelectedItemName.substring(0, mSelectedItemName.length() - 4), FileType.ZIP);
            } else if (mSelectedItemName.endsWith(".rar")) {
                openZipFile(mSelectedItemPath, mSelectedItemParentPath, mSelectedItemName.substring(0, mSelectedItemName.length() - 4), FileType.RAR);
            } else {
                File file = new File(mSelectedItemPath);
                if (file != null) {
                    openFile(file);
                }
            }
            if (mLayoutLoading.getVisibility() == View.VISIBLE) {
                switchLayout(mLstMedia);
            }
        }

        @Override
        public void failCallback() {

            //解压失败
            showParsingErrorDialog();
            if (mLayoutLoading.getVisibility() == View.VISIBLE) {
                switchLayout(mLstMedia);
            }
//            switchLayout(mLayoutEmptyView);
        }
    };

    /**
     * 得到当前目录下的文件列表
     *
     * @param path
     * @param mList
     * @return
     */
    private List<ZipInfo> getCurrentPathList(String path, List<ZipInfo> mList) {

        List<ZipInfo> currentPathList = new ArrayList<>();
        if (mList == null) {
            return new ArrayList<>();
        } else {
            if (TextUtils.isEmpty(path)) {
                return mList;
            } else {
                String mPath = path.endsWith(mSeparator) ? path : path + mSeparator;
                for (int i = 0; i < mList.size(); i++) {
                    if (mList.get(i).getPath().startsWith(mPath)) {
                        currentPathList.add(mList.get(i));
                    }
                }
                return currentPathList;
            }
        }
    }

    /**
     * 打开zip或rar列表
     *
     * @param path
     * @param fileName
     */
    private void openZipFile(String path, String parentPath, String fileName, OpenZipsActivity.FileType type) {

        if (path != null) {
            Intent intent = new Intent(this, OpenZipsActivity.class);
            intent.putExtra(EXTRA_ZIPS_PATH, path);
            intent.putExtra(EXTRA_ZIPS_PARENT, parentPath);
            intent.putExtra(EXTRA_ZIPS_NAME, fileName);
            intent.putExtra(EXTRA_ZIPS_TYPE, type);
            startActivity(intent);
        }
    }

    private void openFile(File file) {
        FileInfo selectedItem = new FileInfo(file);
        if (null == selectedItem) {
            return;
        }
        boolean canOpen = true;
        String mimeType = selectedItem.getFileMimeType(null);
        if (selectedItem.isDrmFile()) {
            mimeType = DrmManager.getInstance().getOriginalMimeType(
                    selectedItem.getFileAbsolutePath());

            if (TextUtils.isEmpty(mimeType)) {
                canOpen = false;
                mToastHelper.showToast(R.string.msg_unable_open_file);
            }
        }
        if (canOpen) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = selectedItem.getUri();
            LogUtils.d(TAG, "openFile, Open uri file: " + uri);
            intent.setDataAndType(uri, mimeType);
            try {
                startActivity(intent);
            } catch (android.content.ActivityNotFoundException e) {
                //mToastHelper.showToast(R.string.msg_unable_open_file);
                showOpenMethodDialog(selectedItem);
                LogUtils.w(TAG, "openFile, Cannot open file: "
                        + selectedItem.getFileAbsolutePath());
            }
        }
    }

    private void showOpenMethodDialog(FileInfo fileInfo) {
        LogUtils.d(TAG, "show Open method Dialog...");
        if (mIsAlertDialogShowing) {
            LogUtils.d(TAG, "Another Dialog is exist, return!~~");
            return;
        }

        if (isResumed()) {
            mIsAlertDialogShowing = true;
            AlertDialogFragment.ChoiceDialogFragmentBuilder builder =
                    new AlertDialogFragment.ChoiceDialogFragmentBuilder();
            builder.setDefault(R.array.open_method, OpenMethodClickListener.OPEN_METHOD_TEXT)
                    .setTitle(R.string.unknown_format)
                    .setCancelTitle(R.string.cancel)
                    .setDoneTitle(R.string.ok);
            AlertDialogFragment.ChoiceDialogFragment openDialogFragment = builder.create();
            OpenMethodClickListener clickListener = new OpenMethodClickListener(fileInfo);
            openDialogFragment.setItemClickListener(clickListener);
            openDialogFragment.setOnDialogDismissListener(this);
            openDialogFragment.show(getFragmentManager(),
                    AlertDialogFragment.ChoiceDialogFragment.CHOICE_DIALOG_TAG);
            boolean ret = getFragmentManager().executePendingTransactions();
            LogUtils.d(TAG, "executing pending transactions result: " + ret);
        }
    }

    private class OpenMethodClickListener implements DialogInterface.OnClickListener {
        static final int OPEN_METHOD_TEXT = 0;
        static final int OPEN_METHOD_AUDIO = 1;
        static final int OPEN_METHOD_VIDEO = 2;
        static final int OPEN_METHOD_IMAGE = 3;

        private FileInfo mSelectedItem = null;
        private Intent mOpenIntent = null;

        OpenMethodClickListener(FileInfo selectedItem) {
            mSelectedItem = selectedItem;
            mOpenIntent = new Intent(Intent.ACTION_VIEW);
            open(".txt");
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            LogUtils.d(TAG, "OpenMethodClickListener onClick: which=" + which);
            switch (which) {
                case OPEN_METHOD_TEXT:
                    open(".txt");
                    break;

                case OPEN_METHOD_AUDIO:
                    open(".mp3");
                    break;

                case OPEN_METHOD_VIDEO:
                    open(".3gp");
                    break;

                case OPEN_METHOD_IMAGE:
                    open(".png");
                    break;

                case DialogInterface.BUTTON_POSITIVE:
                    try {
                        startActivity(getOpenIntent());
                        dialog.dismiss();
                    } catch (android.content.ActivityNotFoundException e) {
                        mToastHelper.showToast(R.string.msg_unable_open_file);
                        LogUtils.w(TAG, "OpenMethodClickListener, Cannot open file: ");
                    }
                    break;

                default:
                    break;
            }


        }

        public Intent getOpenIntent() {
            return mOpenIntent;
        }

        private void open(String extension) {
            if ((null != mSelectedItem) && !TextUtils.isEmpty(extension)) {
                Uri uri = mSelectedItem.getUri();
                LogUtils.d(TAG, "openFile, Open uri file: " + uri);
                String mimeType = FileUtils.getMimeTypeBySpecialExtension(mSelectedItem.getFileAbsolutePath(), extension);
                mOpenIntent.setDataAndType(uri, mimeType);
                mOpenIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        DrmManager.getInstance().init(getApplicationContext());
        IconManager.updateCustomDrawableMap(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDestDirPath != null) {
            File desFile = new File(mDestDirPath);
            delFileUnRoot(desFile);
            mDestDirPath = null;
        }
        if (null != mMountReceiver) {
            mMountReceiver.unregisterMountListener(this);
            unregisterReceiver(mMountReceiver);
            mMountReceiver = null;
        }
        DrmManager.getInstance().release();
        super.onDestroy();
    }

    /**
     * 获取压缩文件列表回调
     */
    GetZipListTask.getZipListCallback mCallback = new GetZipListTask.getZipListCallback() {
        @Override
        public void successCallback(Map<Integer, List<ZipInfo>> fileMap) {
            mFileMap = fileMap;
            notifyDataSetChanged(mFileMap.get(mIndex));
        }

        @Override
        public void failCallback() {
            switchLayout(mLayoutEmptyView);
            showParsingErrorDialog();
        }
    };

    protected void showParsingErrorDialog() {

        if (mIsAlertDialogShowing) {
            LogUtils.d(TAG, "Another Dialog is exist, return!~~");
            return;
        }
        if (isResumed()) {
            mIsAlertDialogShowing = true;
            AlertDialogFragment.AlertDialogFragmentBuilder builder = new AlertDialogFragment.AlertDialogFragmentBuilder();
            AlertDialogFragment parseErrorDialogFragment =
                    builder.setMessage(R.string.File_parsing_error_detail)
                            .setDoneTitle(R.string.ok)
//                            .setCancelTitle(R.string.cancel)
                            .setTitle(R.string.File_parsing_error)
                            .create();
            parseErrorDialogFragment.setOnDoneListener(new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            parseErrorDialogFragment.setOnDialogDismissListener(this);
            parseErrorDialogFragment.show(getFragmentManager(), "");
            boolean ret = getFragmentManager().executePendingTransactions();
            LogUtils.d(TAG, "executing pending transactions result: " + ret);

            View titleDivider = parseErrorDialogFragment.getDialog().findViewById(R.id.titleDivider);
            if (null != titleDivider) {
                LogUtils.d(TAG, "showDeleteDialog: null != titleDivider");
                titleDivider.setVisibility(View.GONE);
            }

            TextView msg = (TextView) parseErrorDialogFragment.getDialog().findViewById(R.id.message);
            if (null != msg) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    msg.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                }
            }
        }
    }

    /***
     * 删除文件或者文件夹
     *
     * @param f
     * @return 成功 返回true, 失败返回false
     */
    static private boolean delFileUnRoot(File f) {
        boolean ret = true;
        if (null == f || !f.exists()) {
            return ret;
        }
        Stack<File> tmpFileStack = new Stack<File>();
        tmpFileStack.push(f);
        try {
            while (!tmpFileStack.isEmpty()) {
                File curFile = tmpFileStack.pop();
                if (null == curFile) {
                    continue;
                }
                if (curFile.isFile()) {
                    if (!curFile.delete()) {
                        ret = false;
                    }
                } else {
                    File[] tmpSubFileList = curFile.listFiles();
                    if (null == tmpSubFileList || 0 == tmpSubFileList.length) {    //空文件夹直接删
                        if (!curFile.delete()) {
                            ret = false;
                        }
                    } else {
                        tmpFileStack.push(curFile); // !!!
                        for (File item : tmpSubFileList) {
                            tmpFileStack.push(item);
                        }
                    }
                }
            }
        } catch (Exception e) {
            ret = false;
        }
        return ret;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        LogUtils.d(TAG, "onRequestPermissionsResult, requestCode:" + requestCode);
        if (null == permissions || permissions.length == 0 ||
                null == grantResults || grantResults.length == 0 ||
                PackageManager.PERMISSION_DENIED == grantResults[0]) {
            LogUtils.e(TAG, "**********onRequestPermissionsResult, Permission or grant res null*******");
            return;
        }

        switch (requestCode) {

            case MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_ZIP:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    MyZipClick(mClickView, mFileClickPos);
                }
                break;
            case MY_PERMISSIONS_REQUEST_TO_READ_EXTERNAL_STORAGE:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    showContent();
                }
                break;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

}
