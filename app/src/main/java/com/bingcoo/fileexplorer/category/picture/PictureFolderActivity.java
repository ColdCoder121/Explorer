package com.bingcoo.fileexplorer.category.picture;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.storage.StorageVolume;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bingcoo.fileexplorer.R;
import com.bingcoo.fileexplorer.base.BaseActivity;
import com.bingcoo.fileexplorer.dialog.fragment.AlertDialogFragment;
import com.bingcoo.fileexplorer.dialog.fragment.ProgressDialog;
import com.bingcoo.fileexplorer.dialog.fragment.ProgressDialogFragment;
import com.bingcoo.fileexplorer.home.FileInfoAdapter;
import com.bingcoo.fileexplorer.receiver.MountReceiver;
import com.bingcoo.fileexplorer.service.FileManagerService;
import com.bingcoo.fileexplorer.service.HeavyOperationListener;
import com.bingcoo.fileexplorer.service.ProgressInfo;
import com.bingcoo.fileexplorer.util.ConstUtils;
import com.bingcoo.fileexplorer.util.FileCategoryHelper;
import com.bingcoo.fileexplorer.util.FileInfo;
import com.bingcoo.fileexplorer.util.FileUtils;
import com.bingcoo.fileexplorer.util.KeyboardUtils;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.MediaUtils;
import com.bingcoo.fileexplorer.util.MountPointManager;
import com.bingcoo.fileexplorer.util.PermissionUtils;
import com.jakewharton.rxbinding.view.RxView;
import com.jakewharton.rxbinding.widget.RxTextView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import bingo.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.functions.Action1;

import static com.bingcoo.fileexplorer.category.picture.PictureGridActivity.SAVED_SHOW_OPTION_DIALOG;
import static com.bingcoo.fileexplorer.category.video.VideoGridActivity.SAVED_ADAPTER_MODE;
import static com.bingcoo.fileexplorer.category.video.VideoGridActivity.SAVED_CHECKED_LIST;
import static com.bingcoo.fileexplorer.category.video.VideoGridActivity.SAVED_FILE_LIST;
import static com.bingcoo.fileexplorer.home.HomeActivity.MY_PERMISSIONS_REQUEST_TO_READ_EXTERNAL_STORAGE;

public class PictureFolderActivity extends BaseActivity {
    private static final String TAG = "PictureFolderActivity";

    static {
        LogUtils.setDebug(TAG, true);
    }

    public static final String DELETE_DIALOG_TAG = "delete_dialog_fragment_tag";

    private static final String SAVED_SELECTED_PATH_KEY = "saved_selected_path";
    public static final String SAVED_SEARCH_TEXT = "search_text";
    public static final String SAVED_SEARCH_TOTAL = "search_total";
    public static final int MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_DELETE = 5;
    public static final int VIEW_PICTURE = 0;

    private FileCategoryHelper.FileCategory mFileCategory = FileCategoryHelper.FileCategory.Picture;
    protected FileInfo mSelectedFileInfo = null;
    protected int mSelectedTop = -1;
    protected int mTop = -1;
    private AlertDialog mOptionsDialog;
    private ProgressDialog mRefreshingProgressDialog;
    private String mSearchText;
    private long mSearchTotal;
    protected boolean mIsAlertDialogShowing = false;
    private int mPosition;
    private int[] mPositionTopList = new int[2];
    private Map<String, int[]> mPositionMap = new HashMap<>();
    private boolean mScrollBack;
    //    private List<FileInfo> mShowFileList = new ArrayList<>();//正常状态列表，搜索状态切换回来后如果没有改动不刷新
    private List<FileInfo> mFileListWhenOut = new ArrayList<>();//触发onsaveinstancestate时的列表

    @BindView(R.id.img_dst_back)
    ImageView mImgBack;
    @BindView(R.id.tv_dst_title)
    TextView mTvTitle;

    @BindView(R.id.lst_media)
    ListView mLstMedia;
    @BindView(R.id.layout_loading)
    View mLayoutLoading;
    @BindView(R.id.empty_view)
    View mLayoutEmptyView;

    @BindView(R.id.layout_edit)
    View mBottomEdit;
    @BindView(R.id.layout_menu)
    View mBottomMenu;

    @BindView(R.id.ll_menu)
    View mViewMenu;

    @BindView(R.id.layout_action_mode_search)
    View mLayoutActionModeSearch;
    @BindView(R.id.layout_action_mode_title)
    View mLayoutActionModeTitle;
    @BindView(R.id.layout_action_mode_select)
    View mLayoutActionModeSelect;

    @BindView(R.id.img_search_back)
    ImageView mImgSearchBack;
    @BindView(R.id.et_search_input)
    EditText mEtSearchInput;
    @BindView(R.id.img_search)
    ImageView mImgSearch;
    @BindView(R.id.layout_local_search_result)
    View mLayoutSearchResult;
    @BindView(R.id.tv_search_result)
    TextView mTvSearchResult;

    @BindView(R.id.layout_picture_folder_list)
    View mLayoutList;
    @BindView(R.id.layout_searching)
    View mLayoutSearching;
    @BindView(R.id.layout_no_matching_search)
    View mLayoutNoMatchingSearch;

    @BindView(R.id.tv_select_all)
    TextView mTvSelectAll;
    @BindView(R.id.tv_select_chosen)
    TextView mTvSelectChosen;
    @BindView(R.id.tv_select_cancel)
    TextView mTvSelectCancel;

    @BindView(R.id.tv_delete)
    TextView mTvDelete;
    @BindView(R.id.tv_details)
    TextView mTvDetails;

    private HeavyOperationListener.HeavyOperationListenerCallback mHeavyOperationListenerCallback =
            new HeavyOperationListener.HeavyOperationListenerCallback() {

                @Override
                public void onTaskResult(int errorType) {
                    mFileInfoManager.updateFileInfoList(mSortType);
                    mFileInfoManager.getShowFileList().removeAll(mAdapter.getCheckedFileInfoItemsList());
                    mAdapter.notifyDataSetChanged();
                    if (isEmpty()) {
                        switchLayout(mLayoutEmptyView);
                    }
                    if (mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
                        onBackPressed();
                    }
                }

                @Override
                public void onClick(View v) {
                    if (mService != null) {
                        LogUtils.i(TAG, "onClick cancel");
                        mService.cancel(PictureFolderActivity.this.getClass().getName());
                    }
                }
            };

    /**
     * 操作文件后判断列表是否为空
     *
     * @return
     */
    private boolean isEmpty() {
        if (mAdapter != null) {
            int cnt = mAdapter.getCount();
            if (cnt == 0) {
                return true;
            } else {
                for (int i = 0; i < cnt; i++) {
                    if (mAdapter.getItem(i).getFileName().startsWith(".")) {
                        continue;
                    } else {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_folder);
        ButterKnife.bind(this);
        mSavedInstanceState = savedInstanceState;
        init();
        showContent();
    }

    private void init() {

        mTvTitle.setText(R.string.category_picture);

        setViewClickListener(mImgBack,
                new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                });

        setViewClickListener(mViewMenu,
                new Runnable() {
                    @Override
                    public void run() {
                        showOptionDialog();
                    }
                });

        setViewClickListener(mImgSearchBack,
                new Runnable() {
                    @Override
                    public void run() {
                        LogUtils.d(TAG, "ImgSearchBack run: ");
                        KeyboardUtils.hideSoftInput(PictureFolderActivity.this);
                        onBackPressed();
                    }
                });

        RxTextView.editorActions(mEtSearchInput)
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer actionId) {
                        LogUtils.d(TAG, "searchInput call: actionId=" + actionId
                                + ", IME_ACTION_SEARCH=" + EditorInfo.IME_ACTION_SEARCH);
                        if (EditorInfo.IME_ACTION_SEARCH == actionId) {
                            KeyboardUtils.hideSoftInput(PictureFolderActivity.this);
                            requestSearch(mEtSearchInput.getText().toString());
                        }
                    }
                });

        setViewClickListener(mImgSearch,
                new Runnable() {
                    @Override
                    public void run() {
                        LogUtils.d(TAG, "ImgSearch run: ");
                        requestSearch(mEtSearchInput.getText().toString());
                    }
                });

        setViewClickListener(mTvSelectAll,
                new Runnable() {
                    @Override
                    public void run() {
                        if (null != mAdapter) {
                            boolean isSelectedAll = mAdapter.getCheckedItemsCount() == mAdapter.getCount();
                            mAdapter.setAllItemChecked(!isSelectedAll);

                            mTvSelectChosen.setText(
                                    getResources().getString(R.string.select_chosen, mAdapter.getCheckedItemsCount()));
                            mTvSelectAll.setText(isSelectedAll ? R.string.select_all : R.string.select_not_all);

                            handleEditView();
                        }
                    }
                });

        setViewClickListener(mTvSelectCancel,
                new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                    }
                });

        if (null != mLstMedia) {
            mLstMedia.setOnItemClickListener(this);
            mLstMedia.setOnItemLongClickListener(this);
            mLstMedia.setFastScrollEnabled(false);
            mLstMedia.setVerticalScrollBarEnabled(true);
            View footer = getLayoutInflater().inflate(R.layout.layout_lst_footer, mLstMedia, false);
            mLstMedia.addFooterView(footer);
        }

        initEditView();
        switchLayout(mLstMedia);
        switchTitle(mLayoutActionModeTitle);
        switchContent(mLayoutList);
    }

    private void initEditView() {
        setViewClickListener(mTvDelete,
                new Runnable() {
                    @Override
                    public void run() {
                        if (!PermissionUtils.hasStorageWritePermission(PictureFolderActivity.this)) {
                            PermissionUtils.requestPermission(PictureFolderActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_DELETE);
                            return;
                        }
                        showDeleteDialog();
                    }
                });

        setViewClickListener(mTvDetails,
                new Runnable() {
                    @Override
                    public void run() {
                        showDetailsDialog();
                    }
                });
    }

    @Override
    protected void serviceConnected() {
        LogUtils.d(TAG, "serviceConnected: ");
        super.serviceConnected();

        mFileInfoManager.getShowFileList().clear();
        mAdapter = new PictureFolderInfoAdapter(this, mService, mFileInfoManager);
        mAdapter.setType(FileInfoAdapter.TYPE_CATEGORY);
        if (null != mLstMedia) {
            mLstMedia.setAdapter(mAdapter);

            if (null == mSavedInstanceState) {
                showContent();

            } else {
                /*mSearchText = mSavedInstanceState.getString(SAVED_SEARCH_TEXT);
                if (!TextUtils.isEmpty(mSearchText)) {
                    mSearchTotal = mSavedInstanceState.getLong(SAVED_SEARCH_TOTAL);
                    mLayoutSearchResult.setVisibility(View.VISIBLE);
                    mTvSearchResult.setText(getResources().getString(
                            R.string.search_result, mSearchTotal));
                    requestSearch(mSearchText);
                }*/
                mFileListWhenOut = mSavedInstanceState.getParcelableArrayList(SAVED_FILE_LIST);
//                mShowFileList = mSavedInstanceState.getParcelableArrayList(SAVED_SHOW_LIST);
//                mListChanged = mSavedInstanceState.getBoolean(SAVED_LIST_CHANGED);
                int adapterMode = mSavedInstanceState.getInt(SAVED_ADAPTER_MODE);
                mAdapter.changeMode(adapterMode);
                if (mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
                    //编辑模式
                    List<FileInfo> checkedList = mSavedInstanceState.getParcelableArrayList(SAVED_CHECKED_LIST);
                    if (mFileListWhenOut.size() > 0 && checkedList != null) {
                        mFileInfoManager.getShowFileList().addAll(mFileListWhenOut);
                        for (FileInfo info : checkedList) {
                            info.setChecked(true);
                        }
                        mAdapter.notifyDataSetChanged();
                        switchTitle(mLayoutActionModeSelect);
                        mTvSelectChosen.setText(getResources().getString(
                                R.string.select_chosen, mAdapter.getCheckedItemsCount()));
                        boolean isSelectedAll = mAdapter.getCheckedItemsCount() == mAdapter.getCount();
                        mTvSelectAll.setText(isSelectedAll ? R.string.select_not_all : R.string.select_all);
                        handleEditView();
                    } else {
                        showContent();
                    }
                } else {
                    //正常模式
                    if (mFileListWhenOut.size() > 0) {
                        mFileInfoManager.getShowFileList().addAll(mFileListWhenOut);
                        mAdapter.notifyDataSetChanged();
                        if (0 == mAdapter.getCount()) {
                            switchLayout(mLayoutEmptyView);
                        } else {
                            switchLayout(mLstMedia);
                        }
                    } else {
                        showContent();
                    }
                }
                restoreDialog();
            }
            mAdapter.notifyDataSetChanged();
        }

        // register Receiver when service connected..
        if (null == mMountReceiver) {
            mMountReceiver = MountReceiver.registerMountReceiver(this);
            mMountReceiver.registerMountListener(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (PermissionUtils.hasStorageReadPermission(this)) {
//            showContent();
        } else {
            /*PermissionUtils.requestPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    MY_PERMISSIONS_REQUEST_TO_READ_EXTERNAL_STORAGE);*/
//            ActivityManager.getInstance().exit();
            finish();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        LogUtils.d(TAG, "onItemClick: position=" + position + ", mode=" + mAdapter.getMode());
        if (mService != null && mService.isBusy(this.getClass().getName())) {
            LogUtils.d(TAG, "onItemClick, service is busy,return. ");
            return;
        }

        if (null != mAdapter) {
            if (position >= mAdapter.getCount() || position < 0) {
                LogUtils.e(TAG, "onItemClick, events error, mFileInfoList.size()= "
                        + mAdapter.getCount());
                return;
            }
            if (mAdapter.isMode(FileInfoAdapter.MODE_NORMAL)
                    || mAdapter.isMode(FileInfoAdapter.MODE_SEARCH)) {

               /* if (View.VISIBLE == mLayoutActionModeSearch.getVisibility()) {
                    switchTitle(mLayoutActionModeTitle);
                    mAdapter.changeMode(FileInfoAdapter.MODE_NORMAL);
                    mLayoutSearchResult.setVisibility(View.GONE);
                    showContent();
                }*/

                FileInfo selectedItem = mAdapter.getItem(position);
                if (null != selectedItem) {
                    if (selectedItem.isDirectory()) {
                        mCurrentPath = selectedItem.getFileAbsolutePath();
                        mPosition = mAdapter.getPosition(selectedItem);
                        int top = view.getTop();
                        mPositionTopList[0] = mPosition;
                        mPositionTopList[1] = top;
                        if (mCurrentPath != null) {
                            mPositionMap.put(mCurrentPath, mPositionTopList);
                        }
//                        String path = selectedItem.getFileAbsolutePath();
                        LogUtils.d(TAG, "onItemClick: path=" + mCurrentPath + ", getShowParentPath=" + selectedItem.getShowParentPath());
                        if (!TextUtils.isEmpty(mCurrentPath)) {
                            Intent intent = new Intent(this, PictureGridActivity.class);
                            intent.putExtra(PictureGridActivity.EXTRA_PICTURE_DIRECTORY, mCurrentPath);
                            startActivityForResult(intent, VIEW_PICTURE);
                        }
                    }
                }

            } else if (mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
                boolean state = mAdapter.getItem(position).isChecked();
                LogUtils.d(TAG, "onItemClick, edit view . position=" + position + ", state=" + state);
                mAdapter.setChecked(position, !state);
                mAdapter.notifyDataSetChanged();
                mTvSelectChosen.setText(getResources().getString(
                        R.string.select_chosen, mAdapter.getCheckedItemsCount()));
                boolean isSelectedAll = mAdapter.getCheckedItemsCount() == mAdapter.getCount();
                mTvSelectAll.setText(isSelectedAll ? R.string.select_not_all : R.string.select_all);
                handleEditView();
            }
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        LogUtils.d(TAG, "onItemLongClick: position=" + position);
        if (mAdapter.isMode(FileInfoAdapter.MODE_NORMAL)
                || mAdapter.isMode(FileInfoAdapter.MODE_SEARCH)) {
            if (!mService.isBusy(this.getClass().getName())) {
                int top = view.getTop();
                switchToEditView(position, top);
                return true;
            }
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (mAdapter != null && mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
            mAdapter.changeMode(FileInfoAdapter.MODE_NORMAL);
            if (View.VISIBLE == mLayoutSearchResult.getVisibility()) {
                switchTitle(mLayoutActionModeSearch);
            } else {
                switchTitle(mLayoutActionModeTitle);
            }
            return;
        }

        if (View.VISIBLE == mLayoutActionModeSearch.getVisibility()) {
            switchTitle(mLayoutActionModeTitle);
            switchContent(mLayoutList);
            mAdapter.changeMode(FileInfoAdapter.MODE_NORMAL);
            mLayoutSearchResult.setVisibility(View.GONE);
            showContent();
            mSearchText = null;
            return;
        }
        mScrollBack = true;
        super.onBackPressed();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mService!=null&&(mService.isSearchTask(this.getClass().getName()) ||
                mService.isListTask(this.getClass().getName()))) {
            mService.cancel(this.getClass().getName());
        }
        if (mAdapter != null && mAdapter.getCheckedItemsCount() == 1) {
            FileInfo selectFileInfo = mAdapter.getCheckedFileInfoItemsList().get(0);
            if (selectFileInfo != null) {
                outState.putString(SAVED_SELECTED_PATH_KEY, selectFileInfo.getFileAbsolutePath());
            }
        }

//        if (View.VISIBLE == mLayoutActionModeSearch.getVisibility()) {
        outState.putString(SAVED_SEARCH_TEXT, mSearchText);
        outState.putLong(SAVED_SEARCH_TOTAL, mSearchTotal);
//        }
        if (mFileInfoManager != null) {
            mFileListWhenOut.clear();
            mFileListWhenOut.addAll(mFileInfoManager.getShowFileList());
            outState.putParcelableArrayList(SAVED_FILE_LIST, (ArrayList<? extends Parcelable>) mFileListWhenOut);

        }
        if (mAdapter != null) {
            outState.putInt(SAVED_ADAPTER_MODE, mAdapter.getMode());
            outState.putParcelableArrayList(SAVED_CHECKED_LIST, (ArrayList<? extends Parcelable>) mAdapter.getCheckedFileInfoItemsList());
        }
//        outState.putParcelableList(SAVED_SHOW_LIST, mShowFileList);
//        outState.putBoolean(SAVED_LIST_CHANGED, mListChanged);
        if (mOptionsDialog != null && mOptionsDialog.isShowing()) {
            outState.putBoolean(SAVED_SHOW_OPTION_DIALOG, true);
        } else {
            outState.putBoolean(SAVED_SHOW_OPTION_DIALOG, false);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDialogDismiss() {
        LogUtils.d(TAG, "dialog dismissed...");
        mIsAlertDialogShowing = false;
    }

    @Override
    protected void doOnMounted(String mountPoint) {
        super.doOnMounted(mountPoint);
        LogUtils.i(TAG, "doOnMounted, mountPoint = " + mountPoint);
        doPrepareForMount(mountPoint);
        showContent();
    }

    @Override
    protected void doOnEjected(String unMountPoint) {
        super.doOnEjected(unMountPoint);
        if (mService != null && mService.isBusy(this.getClass().getName())) {
            mService.cancel(this.getClass().getName());
        }
        showContent();
    }

    @TargetApi(24)
    @Override
    protected void doOnUnMounted(StorageVolume volume) {
        super.doOnUnMounted(volume);
        String unMountPoint = volume.getPath();
        if (mFileInfoManager != null) {
            int pasteCnt = mFileInfoManager.getPasteCount();
            LogUtils.i(TAG, "doOnUnmounted, unMountPoint: " + unMountPoint + ",pasteCnt = "
                    + pasteCnt);

            if (pasteCnt > 0) {
                FileInfo fileInfo = mFileInfoManager.getPasteList().get(0);
                if (fileInfo.getFileAbsolutePath().startsWith(
                        unMountPoint + MountPointManager.SEPARATOR)) {
                    LogUtils.i(TAG, "doOnUnmounted, clear paste list. ");
                    mFileInfoManager.clearPasteList();
                }
            }
        }

        if (mService != null && mService.isBusy(this.getClass().getName())) {
            mService.cancel(this.getClass().getName());
        }
        showToastForUnmount(volume);

        DialogFragment listFragment = (DialogFragment) getFragmentManager()
                .findFragmentByTag(ListListener.LIST_DIALOG_TAG);
        if (listFragment != null) {
            LogUtils.d(TAG, "onUnmounted, listFragment dismiss. ");
            listFragment.dismissAllowingStateLoss();
        }
        showContent();
    }

    @Override
    protected void doOnSdSwap() {
        super.doOnSdSwap();
        mMountPointManager.init(getApplicationContext());
        showContent();
    }

    private void doPrepareForMount(String mountPoint) {
        LogUtils.i(TAG, "doPrepareForMount, mountPoint = " + mountPoint);
        if (mService != null && mService.isBusy(this.getClass().getName())) {
            mService.cancel(this.getClass().getName());
        }
        mMountPointManager.init(getApplicationContext());
    }

    @TargetApi(24)
    private void showToastForUnmount(StorageVolume volume) {
        LogUtils.d(TAG, "showToastForUnmount, path = " + volume.getPath());
        if (isResumed()) {
            String unMountPointDescription = volume.getDescription(this);
            LogUtils.d(TAG, "showToastForUnmount, unMountPointDescription:"
                    + unMountPointDescription);
            mToastHelper.showToast(getString(R.string.unmounted, unMountPointDescription));
        }
    }

    protected void restoreDialog() {
        // Restore the heavy_dialog : pasting deleting
        ProgressDialogFragment pf = (ProgressDialogFragment) getFragmentManager()
                .findFragmentByTag(HeavyOperationListener.HEAVY_DIALOG_TAG);
        if (pf != null) {
            if (mService.isBusy(this.getClass().getName())
                    && mService.isHeavyOperationTask(this.getClass().getName())) {
                HeavyOperationListener listener = new HeavyOperationListener(
                        AlertDialogFragment.INVIND_RES_ID, this, mHeavyOperationListenerCallback);
                mService.reconnected(this.getClass().getName(), listener);
                pf.setCancelListener(listener);
            } else {
                pf.dismissAllowingStateLoss();
            }
        }

        // list dialog
        DialogFragment listFramgent = (DialogFragment) getFragmentManager().findFragmentByTag(
                ListListener.LIST_DIALOG_TAG);
        if (listFramgent != null) {
            LogUtils.i(TAG, "listFramgent != null");
            if (mService.isBusy(this.getClass().getName())) {
                LogUtils.i(TAG, "list reconnected mService");
                mService.reconnected(this.getClass().getName(), new FolderListListener());
            } else {
                LogUtils.i(TAG, "the list is complete dismissAllowingStateLoss");
                listFramgent.dismissAllowingStateLoss();
            }
        }

        // sort dialog
        AlertDialogFragment.ChoiceDialogFragment sortDialogFragment = (AlertDialogFragment.ChoiceDialogFragment) getFragmentManager()
                .findFragmentByTag(AlertDialogFragment.ChoiceDialogFragment.CHOICE_DIALOG_TAG);
        if (sortDialogFragment != null) {
            sortDialogFragment.setItemClickListener(new SortClickListener());
        }

        AlertDialogFragment af;
        // delete dialog
        af = (AlertDialogFragment) getFragmentManager().findFragmentByTag(DELETE_DIALOG_TAG);
        if (af != null) {
            af.setOnDoneListener(new DeleteListener());
        }
        if (mSavedInstanceState != null && mSavedInstanceState.getBoolean(SAVED_SHOW_OPTION_DIALOG)) {
            showOptionDialog();
        }
        /*List<FileInfo> saveSelectedFiles = (List<FileInfo>) mSavedInstanceState.getSerializable(SAVED_SELECTED_PATH_KEY);
        // Restore the detail_dialog
        af = (AlertDialogFragment) getFragmentManager().findFragmentByTag(
                DetailInfoListener.DETAIL_DIALOG_TAG);
        if (af != null && saveSelectedFiles != null && mService != null) {
            DetailInfoListener listener = new DetailInfoListener(saveSelectedFiles);
            af.setDismissListener(listener);
            if (mService.isBusy(this.getClass().getName()) && mService.isDetailTask(this.getClass().getName())) {
                mService.reconnected(this.getClass().getName(), listener);
            } *//*else if (!mService.isBusy(this.getClass().getName())) {
                af.dismissAllowingStateLoss();
                mService.getDetailInfo(this.getClass().getName(), saveSelectedFiles, listener);
            } else {
                af.dismissAllowingStateLoss();
            }*//*
        } else if (af != null && saveSelectedFiles == null) {
            af.dismissAllowingStateLoss();
            mIsAlertDialogShowing = false;
        }*/
    }

    private void switchToEditView(int position, int top) {
        LogUtils.d(TAG, "switchToEditView: position=" + position + ", top=" + top);
        switchTitle(mLayoutActionModeSelect);
        mAdapter.setChecked(position, true);
        mLstMedia.setSelectionFromTop(position, top);
        switchToEditView();
    }

    private void switchToEditView() {
        LogUtils.d(TAG, "Switch to edit view");
        mLstMedia.setFastScrollEnabled(false);
        mAdapter.changeMode(FileInfoAdapter.MODE_EDIT);
        mTvSelectChosen.setText(getResources().getString(
                R.string.select_chosen, mAdapter.getCheckedItemsCount()));
        boolean isSelectedAll = mAdapter.getCheckedItemsCount() == mAdapter.getCount();
        mTvSelectAll.setText(isSelectedAll ? R.string.select_not_all : R.string.select_all);
        handleEditView();
    }

    private void handleEditView() {
        boolean hasCheckedItem = (null != mAdapter) ? mAdapter.getCheckedItemsCount() > 0 : false;
        mTvDelete.setEnabled(hasCheckedItem);
        mTvDetails.setEnabled(hasCheckedItem);
    }

    protected void showDetailsDialog() {
        mService.getDetailInfo(this.getClass().getName(),
                mAdapter.getCheckedFileInfoItemsList(),
                new DetailInfoListener(mAdapter.getCheckedFileInfoItemsList()));
        onBackPressed();
    }

    protected void showDeleteDialog() {

        LogUtils.d(TAG, "show DeleteDialog...");
        if (mIsAlertDialogShowing) {
            LogUtils.d(TAG, "Another Dialog is exist, return!~~");
            return;
        }

        if (isResumed()) {
            mIsAlertDialogShowing = true;
            AlertDialogFragment.AlertDialogFragmentBuilder builder = new AlertDialogFragment.AlertDialogFragmentBuilder();
            AlertDialogFragment deleteDialogFragment =
                    builder.setMessage(R.string.delete_msg)
                            .setDoneTitle(R.string.ok)
                            .setCancelTitle(R.string.cancel)
                            .setTitle(R.string.delete)
                            .create();
            deleteDialogFragment.setOnDoneListener(new DeleteListener());
            deleteDialogFragment.setOnDialogDismissListener(this);
            deleteDialogFragment.show(getFragmentManager(), DELETE_DIALOG_TAG);
            boolean ret = getFragmentManager().executePendingTransactions();
            LogUtils.d(TAG, "executing pending transactions result: " + ret);

            View titleDivider = deleteDialogFragment.getDialog().findViewById(R.id.titleDivider);
            if (null != titleDivider) {
                LogUtils.d(TAG, "showDeleteDialog: null != titleDivider");
                titleDivider.setVisibility(View.GONE);
            }

            TextView msg = (TextView) deleteDialogFragment.getDialog().findViewById(R.id.message);
            if (null != msg) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    msg.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                }
            }
        }
    }

    private void showOptionDialog() {
        if (null == mOptionsDialog) {
            View view = getLayoutInflater().inflate(R.layout.dialog_local_options, null);
            View searchView = view.findViewById(R.id.tv_option_searching);
            View searchViewLine = view.findViewById(R.id.search_line);
            if (searchView != null) {
                searchView.setVisibility(View.GONE);
            }
            if (searchViewLine != null) {
                searchViewLine.setVisibility(View.GONE);
            }
            /*setViewClickListener(view.findViewById(R.id.tv_option_searching),
                    new Runnable() {
                        @Override
                        public void run() {
                            mOptionsDialog.cancel();
                            switchTitle(mLayoutActionModeSearch);
                            mEtSearchInput.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    KeyboardUtils.showSoftInput(PictureFolderActivity.this, mEtSearchInput);
                                }
                            }, 200);
                        }
                    });*/

            setViewClickListener(view.findViewById(R.id.tv_option_refresh),
                    new Runnable() {
                        @Override
                        public void run() {
                            handleRefresh();
                            mOptionsDialog.cancel();
                        }
                    });
            view.findViewById(R.id.tv_option_refresh).setBackgroundResource(R.drawable.top_corner_bg);
            setViewClickListener(view.findViewById(R.id.tv_option_sorting),
                    new Runnable() {
                        @Override
                        public void run() {
                            showSortDialog();
                            mOptionsDialog.cancel();
                        }
                    });
            view.findViewById(R.id.tv_option_sorting).setBackgroundResource(R.drawable.bottom_corner_bg);
            View viewNewFolder = view.findViewById(R.id.tv_option_create_new_folder);
            View viewNewFolderLine = view.findViewById(R.id.new_folder_line);
            if (null != viewNewFolder) {
                viewNewFolder.setVisibility(View.GONE);
            }
            if (null != viewNewFolderLine) {
                viewNewFolderLine.setVisibility(View.GONE);
            }

            mOptionsDialog = new AlertDialog.Builder(this)
                    .setView(view)
                    .create();
            mOptionsDialog.setCanceledOnTouchOutside(true);
        }

        if (!mOptionsDialog.isShowing()) {
            mOptionsDialog.show();
        }
    }

    protected void showSortDialog() {
        LogUtils.d(TAG, "show SortDialog...");
        if (mIsAlertDialogShowing) {
            LogUtils.d(TAG, "Another Dialog is exist, return!~~");
            return;
        }

        if (isResumed()) {
            mIsAlertDialogShowing = true;
            AlertDialogFragment.ChoiceDialogFragmentBuilder builder = new AlertDialogFragment.ChoiceDialogFragmentBuilder();
            builder.setDefault(R.array.sort_order, mSortType).setTitle(R.string.sort_order);
            AlertDialogFragment.ChoiceDialogFragment sortDialogFragment = builder.create();
            sortDialogFragment.setItemClickListener(new SortClickListener());
            sortDialogFragment.setOnDialogDismissListener(this);
            sortDialogFragment.show(getFragmentManager(), AlertDialogFragment.ChoiceDialogFragment.CHOICE_DIALOG_TAG);
            boolean ret = getFragmentManager().executePendingTransactions();
            LogUtils.d(TAG, "executing pending transactions result: " + ret);
        }
    }

    private void showContent() {
        if (isFinishing()) {
            LogUtils.i(TAG, "showContent, isFinishing: true, do not loading again");
            return;
        }

        if (mAdapter != null && mAdapter.isMode(FileInfoAdapter.MODE_NORMAL) &&
                (null != mService) && (null != mFileCategory)) {
            mService.listDirs(this.getClass().getName(), mFileCategory, mSortType, new FolderListListener());
        }
    }

    private void switchLayout(View layout) {
        if (null != layout) {
            layout.setVisibility(View.VISIBLE);

            if (layout != mLstMedia) {
                mLstMedia.setVisibility(View.GONE);
            }

            if (layout != mLayoutLoading) {
                mLayoutLoading.setVisibility(View.GONE);
            }

            if (layout != mLayoutEmptyView) {
                mLayoutEmptyView.setVisibility(View.GONE);
            }
        }
    }

    private void switchContent(View content) {
        if (null != content) {
            content.setVisibility(View.VISIBLE);

            if (content != mLayoutList) {
                mLayoutList.setVisibility(View.GONE);
            }

            if (content != mLayoutSearching) {
                mLayoutSearching.setVisibility(View.GONE);
            }

            if (content != mLayoutNoMatchingSearch) {
                mLayoutNoMatchingSearch.setVisibility(View.GONE);
            }
        }
    }

    private void switchTitle(View title) {
        if (null != title) {
            title.setVisibility(View.VISIBLE);

            if (title != mLayoutActionModeSearch) {
                mLayoutActionModeSearch.setVisibility(View.GONE);
            } else {
                switchBottomBar(null);
            }

            if (title != mLayoutActionModeTitle) {
                mLayoutActionModeTitle.setVisibility(View.GONE);
            } else {
                switchBottomBar(mBottomMenu);
            }

            if (title != mLayoutActionModeSelect) {
                mLayoutActionModeSelect.setVisibility(View.GONE);
            } else {
                switchBottomBar(mBottomEdit);
            }
        }
    }

    private void switchBottomBar(View bottomBar) {
        if (null != bottomBar) {
            bottomBar.setVisibility(View.VISIBLE);

            if (bottomBar != mBottomEdit) {
                mBottomEdit.setVisibility(View.GONE);
            }

            if (bottomBar != mBottomMenu) {
                mBottomMenu.setVisibility(View.GONE);
            }
        } else {
            mBottomEdit.setVisibility(View.GONE);
            mBottomMenu.setVisibility(View.GONE);
        }
    }

    public void setViewClickListener(View view, final Runnable runnable) {
        if ((null != view) && (null != runnable)) {
            RxView.clicks(view)
                    .throttleFirst(ConstUtils.THROTTLE_TIME, TimeUnit.MILLISECONDS)
                    .subscribe(new Action1<Void>() {
                        @Override
                        public void call(Void aVoid) {
                            runnable.run();
                        }
                    });
        }
    }

    protected void handleRefresh() {
        LogUtils.d(TAG, "handleRefresh: ");

        mRefreshingProgressDialog = new ProgressDialog(this);
        mRefreshingProgressDialog.setMessage(getString(R.string.refreshing));
        mRefreshingProgressDialog.setIndeterminate(true);
        mRefreshingProgressDialog.setCancelable(false);
        mRefreshingProgressDialog.show();

        //int id = ResourceUtils.systemId(this, "progress", ID);
        ProgressBar progressBar = (ProgressBar) mRefreshingProgressDialog.findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressBar.setIndeterminateTintList(
                    ColorStateList.valueOf(ContextCompat.getColor(this, R.color.bingo_theme_color)));
            progressBar.setIndeterminateTintMode(PorterDuff.Mode.SRC_ATOP);
        }else{
            progressBar.setIndeterminateDrawable(ContextCompat.getDrawable(this,R.drawable.loading_progress));
        }
        String[] paths = new String[mMountPointManager.getMountCount()];
        mMountPointManager.getMountPointPaths().toArray(paths);
        final int pathsSize = paths.length;
        MediaScannerConnection.OnScanCompletedListener callback =
                new MediaScannerConnection.OnScanCompletedListener() {
                    private int cnt = 0;

                    @Override
                    public void onScanCompleted(String path, Uri uri) {
                        LogUtils.d(TAG, "handleRefresh onScanCompleted: path=" + path + ", uri=" + uri);
                        cnt++;
                        if (cnt == pathsSize) {
                            mRefreshingProgressDialog.cancel();
                            showContent();
                        }
                    }
                };
        MediaUtils.scanFiles(this, paths, null, callback);
    }

    private int restoreSelectedPosition() {
        if (mSelectedFileInfo == null) {
            return -1;
        } else {
            int curSelectedItemPosition = mAdapter.getPosition(mSelectedFileInfo);
            mSelectedFileInfo = null;
            return curSelectedItemPosition;
        }
    }

    private void requestSearch(String query) {
        LogUtils.d(TAG, "requestSearch: query=" + query);
        if (!TextUtils.isEmpty(query)) {
            switchTitle(mLayoutActionModeSearch);
            switchContent(mLayoutSearching);

            if (null != mService) {
                mService.searchDir(this.getClass().getName(), query, mFileCategory, mSortType, new SearchListener(query));
            }
        }
    }

    private void sortFileInfoList() {
        LogUtils.d(TAG, "Start sortFileInfoList()");

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.sorting_in));
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();

        //int id = ResourceUtils.systemId(this, "progress", ID);
        ProgressBar progressBar = (ProgressBar) progressDialog.findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressBar.setIndeterminateTintList(
                    ColorStateList.valueOf(ContextCompat.getColor(this, R.color.bingo_theme_color)));
            progressBar.setIndeterminateTintMode(PorterDuff.Mode.SRC_ATOP);
        }else{
            progressBar.setIndeterminateDrawable(ContextCompat.getDrawable(this,R.drawable.loading_progress));
        }
        progressDialog.getWindow().getDecorView().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        int selection = mLstMedia.getFirstVisiblePosition(); // save current
                        // refresh only when paste or delete operation is performed
                        mFileInfoManager.sort(mSortType);
                        mAdapter.notifyDataSetChanged();
                        mLstMedia.setSelection(selection);
                        // restore the selection in the navigation view

                        LogUtils.d(TAG, "End sortFileInfoList()");

                        progressDialog.cancel();
                    }
                }, 1000);


    }

    class FolderListListener extends ListListener {
        @Override
        public void onTaskProgress(ProgressInfo progressInfo) {
            super.onTaskProgress(progressInfo);
            if (isResumed()) {
                switchLayout(mLayoutLoading);
            }
        }

        @Override
        public void onTaskResult(int result) {
            LogUtils.d(TAG, "LstListListener onTaskResult: ");
            super.onTaskResult(result);
//            if (mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
//                mFileInfoManager.loadFileInfoList(mCurrentPath, mSortType, mSelectedFileInfo);
//                mSelectedFileInfo = mAdapter.getFirstCheckedFileInfoItem();
//            } else {
                mFileInfoManager.loadFileInfoList(mSortType);
//            }

            mAdapter.notifyDataSetChanged();
            if (mScrollBack && mPositionMap.containsKey(mCurrentPath)) {
                int mPos = mPositionMap.get(mCurrentPath)[0];
                int mTop = mPositionMap.get(mCurrentPath)[1];
                LogUtils.e(TAG, "*******mPos=:" + mPos);
                LogUtils.e(TAG, "*******mTop=:" + mTop);
                mLstMedia.setSelectionFromTop(mPos, mTop);
                mScrollBack = false;
            } else {
                int selectedItemPosition = restoreSelectedPosition();
                if (selectedItemPosition == -1) {
                    mLstMedia.setSelectionAfterHeaderView();
                } else {
                    mLstMedia.setSelection(selectedItemPosition);
                }
            }
            /*int selectedItemPosition = restoreSelectedPosition();
            if (selectedItemPosition == -1) {
                mLstMedia.setSelectionAfterHeaderView();
            } else if (selectedItemPosition >= 0 && selectedItemPosition < mAdapter.getCount()) {
                if (mSelectedTop != -1) {
                    mLstMedia.setSelectionFromTop(selectedItemPosition, mSelectedTop);
                    mSelectedTop = -1;
                } else if (mTop != -1) {
                    mLstMedia.setSelectionFromTop(selectedItemPosition, mTop);
                    mTop = -1;
                } else {
                    mLstMedia.setSelection(selectedItemPosition);
                }
            }*/

            LogUtils.d(TAG, "onTaskResult: cnt=" + mLstMedia.getCount() + ",adapterCnt=" + mAdapter.getCount());
            if (0 == mAdapter.getCount()) {
                switchLayout(mLayoutEmptyView);
            } else {
                switchLayout(mLstMedia);
            }
        }
    }

    protected class SearchListener implements FileManagerService.OperationEventListener {
        private static final int FIRST_UPDATE_COUNT = 20;
        private static final int NEED_UPDATE_LIST = 6;
        private boolean mIsResultSet = false;
        private int mCount = 0;

        /**
         * Constructor of SearchListener.
         *
         * @param text the search target(String), which will be shown on searchResult TextView..
         */
        public SearchListener(String text) {
            if (text == null) {
                throw new IllegalArgumentException();
            }
            mSearchText = text;
        }

        @Override
        public void onTaskResult(int result) {
            LogUtils.d(TAG, "SearchListener onTaskResult: ");
            if ((mTvSearchResult != null) && !mIsResultSet) {
                LogUtils.d(TAG, "onTaskProgress: mSearchTotal=" + mSearchTotal);
                mLayoutSearchResult.setVisibility(View.VISIBLE);
                mTvSearchResult.setText(getResources().getString(
                        R.string.search_result, mSearchTotal));
                mIsResultSet = true;
            }
            if (0 == mSearchTotal) {
                switchContent(mLayoutNoMatchingSearch);
            } else {
                switchContent(mLayoutList);
                mFileInfoManager.updateSearchList();
                mAdapter.notifyDataSetChanged();
            }
            mSearchTotal = 0;
        }

        @Override
        public void onTaskPrepare() {
            LogUtils.d(TAG, "SearchListener onTaskPrepare: ");
            mAdapter.changeMode(FileInfoAdapter.MODE_SEARCH);
        }

        @Override
        public void onTaskProgress(ProgressInfo progressInfo) {
            if (!progressInfo.isFailInfo()) {
                mSearchTotal = progressInfo.getTotal();
                /*if ((mTvSearchResult != null) && (0 < total)) {
                    mSearchTotal = progressInfo.getTotal();
                    LogUtils.d(TAG, "onTaskProgress: mSearchTotal=" + mSearchTotal);
                    mLayoutSearchResult.setVisibility(View.VISIBLE);
                    mTvSearchResult.setText(getResources().getString(
                            R.string.search_result, mSearchTotal));
                    //mIsResultSet = true;
                }*/
                //LogUtils.d(TAG, "SearchListener onTaskProgress: fileName=" + progressInfo.getFileInfo().getShowName());
                if (progressInfo.getFileInfo() != null) {
                    mFileInfoManager.addItem(progressInfo.getFileInfo());
                }
                mCount++;
                if (mCount > FIRST_UPDATE_COUNT) {
                    if (mLstMedia.getLastVisiblePosition() + NEED_UPDATE_LIST > mAdapter.getCount()) {
                        mFileInfoManager.updateSearchList();
                        mAdapter.notifyDataSetChanged();
                        mCount = 0;
                    }
                }
            }
        }
    }

    private class DeleteListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialog, int id) {
            LogUtils.d(TAG, "onClick() method for alertDeleteDialog, OK button");
            final AlertDialogFragment deleteFragment = (AlertDialogFragment) getFragmentManager().
                    findFragmentByTag(DELETE_DIALOG_TAG);
            if (null != deleteFragment) {
                deleteFragment.dismissAllowingStateLoss();
            }
            if (mService != null) {
                HeavyOperationListener listener = new HeavyOperationListener(R.string.deleting, PictureFolderActivity.this,
                        mHeavyOperationListenerCallback);
                mService.deleteFiles(PictureFolderActivity.this.getClass().getName(),
                        mAdapter.getCheckedFileInfoItemsList(), listener);
            }
        }
    }

    private class SortClickListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialog, int id) {
            LogUtils.d(TAG, "SortClickListener onClick: id=" + id);
            if (id != mSortType) {
                setPrefsSortBy(id);
                sortFileInfoList();
            }
            dialog.dismiss();
        }
    }

    protected class DetailInfoListener implements FileManagerService.OperationEventListener,
            DialogInterface.OnDismissListener {
        public static final String DETAIL_DIALOG_TAG = "detaildialogtag";
        private List<FileInfo> mFileInfos;
        private TextView mTvDetailSize;

        public DetailInfoListener(List<FileInfo> fileInfo) {
            mFileInfos = fileInfo;
        }

        @Override
        public void onTaskPrepare() {
            if (isResumed()) {
                AlertDialogFragment.AlertDialogFragmentBuilder builder = new AlertDialogFragment.AlertDialogFragmentBuilder();
                int cnt = mFileInfos.size();
                if (1 == cnt) {
                    AlertDialogFragment detailFragment = builder.setCancelTitle(R.string.ok).setLayout(
                            R.layout.dialog_details).setTitle(R.string.details).create();

                    detailFragment.setDismissListener(this);
                    detailFragment.show(getFragmentManager(), DETAIL_DIALOG_TAG);
                    boolean ret = getFragmentManager().executePendingTransactions();
                    LogUtils.d(TAG, "executing pending transactions result: " + ret);
                    Dialog dialog = detailFragment.getDialog();
                    if (dialog != null) {
                        FileInfo fileInfo = mFileInfos.get(0);
                        setViewContent(dialog.findViewById(R.id.tv_detail_name), fileInfo.getShowName());
                        setViewContent(dialog.findViewById(R.id.tv_detail_type),
                                fileInfo.isDirectory() ? getString(R.string.folder) : getString(R.string.file));
                        mTvDetailSize = (TextView) dialog.findViewById(R.id.tv_detail_size);

                        if (fileInfo.isDirectory()) {
                            File dirFile = fileInfo.getFile();
                            int dirs = 0;
                            int files = 0;
                            for (File file : dirFile.listFiles()) {
                                if (file.isDirectory()) {
                                    dirs++;
                                } else {
                                    files++;
                                }
                            }
                            StringBuilder sb = new StringBuilder();
                            sb.append(getString(R.string.file)).append(":");
                            sb.append(files).append(", ");
                            sb.append(getString(R.string.folder)).append(":");
                            sb.append(dirs);

                            setViewContent(dialog.findViewById(R.id.tv_detail_include), sb.toString());
                            View view = dialog.findViewById(R.id.rl_include);
                            view.setVisibility(View.VISIBLE);
                        } else {
                            View view = dialog.findViewById(R.id.rl_include);
                            view.setVisibility(View.GONE);
                        }

                        setViewContent(dialog.findViewById(R.id.tv_detail_time),
                                FileUtils.formatModifiedTime(getApplicationContext(), fileInfo.getFileLastModifiedTime()));
                        setViewContent(dialog.findViewById(R.id.tv_detail_route), fileInfo.getShowParentPath());
                    }
                } else if (1 < cnt) {
                    AlertDialogFragment detailFragment = builder.setCancelTitle(R.string.ok).setLayout(
                            R.layout.dialog_dirs_details).setTitle(R.string.details).create();

                    detailFragment.setDismissListener(this);
                    detailFragment.show(getFragmentManager(), DETAIL_DIALOG_TAG);
                    boolean ret = getFragmentManager().executePendingTransactions();
                    LogUtils.d(TAG, "executing pending transactions result: " + ret);
                    Dialog dialog = detailFragment.getDialog();
                    if (dialog != null) {
                        setViewContent(dialog.findViewById(R.id.tv_detail_cnt), String.valueOf(cnt));
                        mTvDetailSize = (TextView) dialog.findViewById(R.id.tv_detail_size);
                    }
                }
            } else {
                LogUtils.e(TAG, "onTaskPrepare activity is not resumed");
            }
        }

        private void setViewContent(View view, String content) {
            if (view instanceof TextView) {
                TextView tv = (TextView) view;
                tv.setText(content);
            }
        }

        @Override
        public void onTaskProgress(ProgressInfo progressInfo) {
            long size = progressInfo.getTotal();
            if (null != mTvDetailSize) {
                mTvDetailSize.setText(FileUtils.sizeToString(size));
            }
        }

        @Override
        public void onTaskResult(int result) {
            LogUtils.d(TAG, "DetailInfoListener onTaskResult.");
            AlertDialogFragment detailFragment = (AlertDialogFragment) getFragmentManager().findFragmentByTag(DETAIL_DIALOG_TAG);
            if (detailFragment != null) {
                //detailFragment.getArguments().putString(DETAIL_INFO_KEY, mStringBuilder.toString());
                //detailFragment.getArguments().putLong(DETAIL_INFO_SIZE_KEY, mFileLength);
            } else {
                // this case may happen in case of this operation already canceled.
                LogUtils.d(TAG, "get detail fragment is null...");
            }
            return;
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            if (mService != null) {
                LogUtils.d(this.getClass().getName(), "onDismiss");
                mService.cancel(PictureFolderActivity.this.getClass().getName());
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        LogUtils.d(TAG, "onRequestPermissionsResult, requestCode:" + requestCode);
        if (null == permissions || permissions.length == 0 ||
                null == grantResults || grantResults.length == 0 ||
                PackageManager.PERMISSION_DENIED == grantResults[0]) {
            LogUtils.e(TAG, "**********onRequestPermissionsResult, Permission or grant res null*******");
            return;
        }

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_DELETE:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    showDeleteDialog();
                    LogUtils.d(TAG, "onRequestPermissionsResult: MY_PERMISSIONS_REQUEST_TO_READ_EXTERNAL_STORAGE PERMISSION_GRANTED");
                }
                break;
            case MY_PERMISSIONS_REQUEST_TO_READ_EXTERNAL_STORAGE:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    showContent();
                }
                break;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == VIEW_PICTURE && resultCode == RESULT_OK) {
            //子页面文件有删改，刷新
            switchLayout(mLayoutLoading);
            showContent();
        }
    }
}
