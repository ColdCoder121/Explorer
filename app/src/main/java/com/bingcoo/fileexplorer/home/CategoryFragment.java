package com.bingcoo.fileexplorer.home;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.os.SystemClock;
import android.os.storage.StorageVolume;
import android.provider.Downloads;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bingcoo.fileexplorer.R;
import com.bingcoo.fileexplorer.category.ListActivity;
import com.bingcoo.fileexplorer.category.picture.PictureFolderActivity;
import com.bingcoo.fileexplorer.category.video.VideoGridActivity;
import com.bingcoo.fileexplorer.interfaces.OnBackPressedListener;
import com.bingcoo.fileexplorer.memory.MemoryActivity;
import com.bingcoo.fileexplorer.receiver.MountReceiver;
import com.bingcoo.fileexplorer.receiver.PermissionReceiver;
import com.bingcoo.fileexplorer.ui.StorageRingView;
import com.bingcoo.fileexplorer.util.BluetoothShare;
import com.bingcoo.fileexplorer.util.ConstUtils;
import com.bingcoo.fileexplorer.util.FileCategoryHelper;
import com.bingcoo.fileexplorer.util.FileCategoryHelper.FileCategory;
import com.bingcoo.fileexplorer.util.FileInfoComparator;
import com.bingcoo.fileexplorer.util.FileUtils;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.MountPointManager;
import com.bingcoo.fileexplorer.util.PermissionUtils;
import com.bingcoo.fileexplorer.util.SystemUtils;
import com.jakewharton.rxbinding.view.RxView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

import static com.bingcoo.fileexplorer.util.FileCategoryHelper.FileCategory.Download;

/**
 * 类名称：CategoryFragment
 * 作者：David
 * 内容摘要：
 * 创建日期：2016/11/21
 * 修改者， 修改日期， 修改内容
 */
public class CategoryFragment extends Fragment implements MountReceiver.MountListener, OnBackPressedListener,
        PermissionReceiver.onPermissionGranted {
    private static final String TAG = "CategoryFragment";

    static {
        LogUtils.setDebug(TAG, true);
    }

//    private HomeActivity mHomeActivity;
    private MountPointManager mMountPointManager;
    protected MountReceiver mMountReceiver = null;
    protected PermissionReceiver mPermissionReceiver = null;
    private int mCategoryWidth = 0;
    private HashMap<String, View> mMountViewMap = new HashMap<>();
    private static final int MY_PERMISSIONS_REQUEST_TO_ACCESS_DOWNLOAD = 3;
    private static final int MY_PERMISSIONS_REQUEST_TO_ACCESS_BLUETOOTH = 4;

    @BindView(R.id.ll_category_picture)
    LinearLayout mCategoryPicture;
    @BindView(R.id.tv_category_picture_count)
    TextView mCategoryPictureCount;
    @BindView(R.id.ll_category_music)
    LinearLayout mCategoryMusic;
    @BindView(R.id.tv_category_music_count)
    TextView mCategoryMusicCount;
    @BindView(R.id.ll_category_video)
    LinearLayout mCategoryVideo;
    @BindView(R.id.tv_category_video_count)
    TextView mCategoryVideoCount;
    @BindView(R.id.ll_category_document)
    LinearLayout mCategoryDocument;
    @BindView(R.id.tv_category_document_count)
    TextView mCategoryDocumentCount;
    @BindView(R.id.ll_category_zip)
    LinearLayout mCategoryZip;
    @BindView(R.id.tv_category_zip_count)
    TextView mCategoryZipCount;
    @BindView(R.id.ll_category_apk)
    LinearLayout mCategoryApk;
    @BindView(R.id.tv_category_apk_count)
    TextView mCategoryApkCount;
    /*@BindView(R.id.ll_category_recent)
    LinearLayout mCategoryRecent;
    @BindView(R.id.tv_category_recent_count)
    TextView mCategoryRecentCount;*/
    @BindView(R.id.ll_category_download)
    LinearLayout mCategoryDownload;
    @BindView(R.id.tv_category_download_count)
    TextView mCategoryDownloadCount;
    @BindView(R.id.ll_category_bluetooth)
    LinearLayout mCategoryBluetooth;
    @BindView(R.id.tv_category_bluetooth_count)
    TextView mCategoryBluetoothCount;
    @BindView(R.id.sr_inner)
    View mSrInner;
    @BindView(R.id.sr_sd1)
    View mSrSd1;
    @BindView(R.id.sr_sd2)
    View mSrSd2;
//    @BindView(R.id.ll_clean)
//    View mCleanView;
//    @BindView(R.id.tv_clean)
//    View mCleanTv;
//    @BindView(R.id.im_clean)
//    View mCleanImg;

    private LinearLayout mRootContainer;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /*View view = inflater.inflate(R.layout.fragment_category, container, true);
        ButterKnife.bind(this, view);
        initContentView();
        return view;*/
        LogUtils.e(TAG, "onCreateView");
        init();
        mRootContainer = new LinearLayout(getActivity());
        mRootContainer.post(new Runnable() {
            @Override
            public void run() {
                View view = inflater.inflate(R.layout.fragment_category, mRootContainer, true);
                ButterKnife.bind(CategoryFragment.this, view);
                initContentView();
            }
        });
        return mRootContainer;
    }

    /**
     * 某些版本不执行此方法，而是执行onAttach(Activity activity)
     * 这个过时的方法，android自身bug，坑
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        LogUtils.e(TAG, "onAttach");
        super.onAttach(context);
//        init();
    }

    private void init() {
//        if (context instanceof HomeActivity) {
//            mHomeActivity = (HomeActivity) context;
            if (null == mMountReceiver) {
                mMountReceiver = MountReceiver.registerMountReceiver(getActivity());
                mMountReceiver.registerMountListener(this);
            }
            if (0 == mCategoryWidth) {
                int screenWidth = SystemUtils.getScreenWidth(getActivity());
                int separator = getActivity().getResources().getDimensionPixelSize(R.dimen.separator_width);
                mCategoryWidth = Math.round(screenWidth / 3.0f - 2 * separator);
                LogUtils.d(TAG, "onAttach: mCategoryWidth=" + mCategoryWidth);
            }
            if (mPermissionReceiver == null) {
                mPermissionReceiver = PermissionReceiver.registerPermissionReceiver(getActivity());
                mPermissionReceiver.registerPermissionListener(this);
            }
//        }
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshCategoryCount();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (null != mMountReceiver) {
            mMountReceiver.unregisterMountListener(this);
            getActivity().unregisterReceiver(mMountReceiver);
            mMountReceiver = null;
            LogUtils.d(TAG, "onDetach: unregister mountListener");
        }
        if (mPermissionReceiver != null) {
            getActivity().unregisterReceiver(mPermissionReceiver);
            mPermissionReceiver = null;
        }
    }

    private void initContentView() {
        LogUtils.d(TAG, "initContentView: ");
//        if (!PermissionUtils.hasPermission(getActivity(), Downloads.Impl.PERMISSION_ACCESS_ALL)) {
        //mCategoryDownload.setVisibility(View.INVISIBLE);
//        }
//        if (!PermissionUtils.hasPermission(getActivity(), BluetoothShare.PERMISSION_ACCESS)) {
        //mCategoryBluetooth.setVisibility(View.INVISIBLE);
//        }

        mMountPointManager = MountPointManager.getInstance();
        //mMountPointManager.init(getActivity().getApplicationContext());

        initClicks();
        refreshCategoryCount();
        refreshMemory();
    }

    private void refreshMemory() {
        if ((null != mMountPointManager) && (0 < mMountPointManager.getMountCount())) {
            mMountPointManager.updateMountPointSpaceInfo();
            CopyOnWriteArrayList<MountPointManager.MountPoint> mountPointLst = mMountPointManager.getMountPointList();
            LogUtils.e(TAG, "******根目录数目：" + mountPointLst.size());
            for (MountPointManager.MountPoint mountPoint : mountPointLst) {
                String path = mountPoint.getPath();
                long mFreeSpace = 0;
                long mTotalSpace = 0;
                if (PermissionUtils.hasStorageReadPermission(getActivity())) {
                    mFreeSpace = mountPoint.getFreeSpace();
                    mTotalSpace = mountPoint.getTotalSpace();
                }
                LogUtils.d(TAG, "refreshMemory: path=" + path + ", FreeMemory=" + mFreeSpace + ", TotalSpace=" + mTotalSpace);
                if (!TextUtils.isEmpty(path)) {
                    if (mMountPointManager.isPrimaryVolume(path)) {
                        refreshStorageRingView(mSrInner, getString(R.string.memory), mTotalSpace, mFreeSpace, path);
                    } else if (((View.GONE == mSrSd1.getVisibility()) || (mSrSd1 == mMountViewMap.get(path))) && mountPoint.isMounted()) {
                        if (View.GONE == mSrSd1.getVisibility()) {
                            mMountViewMap.put(path, mSrSd1);
                        }
                        String sdName = (2 == mMountPointManager.getMountCount()) ?
                                getString(R.string.sd) : getString(R.string.sd1);
                        refreshStorageRingView(mSrSd1, sdName, mTotalSpace, mFreeSpace, path);
                    } else if (((View.GONE == mSrSd2.getVisibility()) || (mSrSd2 == mMountViewMap.get(path))) && mountPoint.isMounted()) {
                        if (View.GONE == mSrSd2.getVisibility()) {
                            mMountViewMap.put(path, mSrSd2);
                        }
                        refreshStorageRingView(mSrSd2, getString(R.string.sd2), mTotalSpace, mFreeSpace, path);
                    }
                }
            }
        }
    }

    private void refreshStorageRingView(View view, final String name,
                                        final long totalSpace, final long freeSpace, final String path) {
        if ((null != view) && (0 <= totalSpace) && (0 <= freeSpace) && (freeSpace <= totalSpace)) {
            try {
                view.setVisibility(View.VISIBLE);
                RxView.clicks(view)
                        .throttleFirst(ConstUtils.THROTTLE_TIME, TimeUnit.MILLISECONDS)
                        .subscribe(new Action1<Void>() {
                            @Override
                            public void call(Void aVoid) {
                                //Log.d(TAG, "call: click mCategoryPicture, cnt=" + query());
                                //Toast.makeText(getActivity(), "initViewClicks", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getActivity(), MemoryActivity.class);
                                intent.putExtra(MemoryActivity.EXTRA_TITLE, name);
                                intent.putExtra(MemoryActivity.EXTRA_PATH, path);
                                intent.putExtra(MemoryActivity.EXTRA_AVAILABLE, freeSpace);
                                intent.putExtra(MemoryActivity.EXTRA_TOTAL, totalSpace);
                                startActivity(intent);
                            }
                        });
                long dirtySpace = totalSpace - freeSpace;
                NumberFormat numberFormat = NumberFormat.getNumberInstance();
                numberFormat.setMaximumFractionDigits(2);
                float percent;
                String description;
                String memoryDescription = getResources().getString(R.string.memory_description);
                if (PermissionUtils.hasStorageReadPermission(getActivity())) {
                    if (totalSpace != 0) {
                        percent = 100f * dirtySpace / totalSpace;
                    } else {
                        percent = 0;
                    }
                    LogUtils.d(TAG, "refreshStorageRingView: percent=" + percent + ", dirtySpace=" + dirtySpace);
                    description = String.format(memoryDescription,
                            FileUtils.sizeToString(dirtySpace).replaceAll("\\s*", ""),
                            FileUtils.sizeToString(totalSpace).replaceAll("\\s*", ""));
                } else {
                    percent = 0;
                    description = String.format(memoryDescription,
                            FileUtils.sizeToString(0).replaceAll("\\s*", ""),
                            FileUtils.sizeToString(0).replaceAll("\\s*", ""));
                }
                StorageRingView srv = (StorageRingView) view.findViewById(R.id.sr_storage);
                srv.setPercent(Math.round(percent));
                TextView tvPercentNumber = (TextView) view.findViewById(R.id.tv_percent_number);
                tvPercentNumber.setText(numberFormat.format(percent));
                TextView tvDescription = (TextView) view.findViewById(R.id.tv_description);
                tvDescription.setText(description);
                TextView tvName = (TextView) view.findViewById(R.id.tv_name);
                tvName.setText(name);
            } catch (Exception e) {
                LogUtils.e(TAG, "refreshStorageRingView: Exception !!", e);
            }
        }
    }

    private void initClicks() {
        initViewClicks(mCategoryPicture, new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getActivity(), PictureFolderActivity.class);
                startActivity(intent);
            }
        });

        initViewClicks(mCategoryMusic, new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getActivity(), ListActivity.class);
                intent.putExtra(ListActivity.EXTRA_TITLE, R.string.category_music);
                startActivity(intent);
            }
        });

        initViewClicks(mCategoryVideo, new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getActivity(), VideoGridActivity.class);
                startActivity(intent);
            }
        });

        initViewClicks(mCategoryDocument, new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getActivity(), ListActivity.class);
                intent.putExtra(ListActivity.EXTRA_TITLE, R.string.category_document);
                startActivity(intent);
            }
        });

        initViewClicks(mCategoryZip, new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getActivity(), ListActivity.class);
                intent.putExtra(ListActivity.EXTRA_TITLE, R.string.category_zip);
                startActivity(intent);
            }
        });

        initViewClicks(mCategoryApk, new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getActivity(), ListActivity.class);
                intent.putExtra(ListActivity.EXTRA_TITLE, R.string.category_apk);
                startActivity(intent);
            }
        });

        //initViewClicks(mCategoryRecent, runnable);
        initViewClicks(mCategoryDownload, new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getActivity(), ListActivity.class);
                intent.putExtra(ListActivity.EXTRA_TITLE, R.string.category_download);
                startActivity(intent);
            }
        });

        initViewClicks(mCategoryBluetooth, new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getActivity(), ListActivity.class);
                intent.putExtra(ListActivity.EXTRA_TITLE, R.string.category_bluetooth);
                startActivity(intent);
            }
        });

//        setCleanViewEnable(false);

    }

    /*private void setCleanViewEnable(boolean enable) {
        mCleanView.setEnabled(enable);
        mCleanTv.setEnabled(enable);
        mCleanImg.setEnabled(enable);
    }*/

    private void initViewClicks(View view, final Runnable runnable) {
        if (view == null) {
            return;
        }
        setCategoryWidth(view);

        RxView.clicks(view)
                .throttleFirst(ConstUtils.THROTTLE_TIME, TimeUnit.MILLISECONDS)
                .subscribe(new Action1<Void>() {
                    @Override
                    public void call(Void aVoid) {
                        //Log.d(TAG, "call: click mCategoryPicture, cnt=" + query());
                        //Toast.makeText(getActivity(), "initViewClicks", Toast.LENGTH_SHORT).show();
                        runnable.run();
                    }
                });
    }

    private void setCategoryWidth(View view) {
        try {
            if ((null != view) && ((view.getLayoutParams() instanceof LinearLayout.LayoutParams))) {
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) view.getLayoutParams();
                if (0 < mCategoryWidth) {
                    lp.width = mCategoryWidth;
                    lp.weight = 0;
                } else {
                    lp.width = 0;
                    lp.weight = 1;
                }

                /*int statusBarHeight = ViewUtils.statusBarHeight(getActivity());
                LogUtils.d(TAG, "setCategoryWidth: statusBarHeight=" + statusBarHeight);
                if (0 < statusBarHeight) {
                    lp.height = getResources().getDimensionPixelOffset(R.dimen.category_height);
                } else {
                    lp.height = getResources().getDimensionPixelOffset(R.dimen.category_height_no_nav);
                }
                LogUtils.d(TAG, "setCategoryWidth: lp=" + lp.debug("setCategoryWidth: "));*/
            }
        } catch (Exception e) {
            LogUtils.e(TAG, "setCategoryWidth: Exception !!", e);
        }
    }

    public void refreshCategoryCount() {
        LogUtils.d(TAG, "refreshCategoryCount: ");
        class CategoryInfo {
            FileCategory category;
            int count;
        }

        Observable.OnSubscribe<List<CategoryInfo>> onSubscribe = new Observable.OnSubscribe<List<CategoryInfo>>() {

            @Override
            public void call(Subscriber<? super List<CategoryInfo>> subscriber) {
                try {
                    LogUtils.d(TAG, "call: in refreshCategoryCount()");
                    if (!subscriber.isUnsubscribed()) {
                        List<CategoryInfo> categoryInfoList = new ArrayList<>();
                        addCategoryInfo(categoryInfoList, new CategoryInfo(), FileCategory.Picture);
                        addCategoryInfo(categoryInfoList, new CategoryInfo(), FileCategory.Music);
                        addCategoryInfo(categoryInfoList, new CategoryInfo(), FileCategory.Video);
                        addCategoryInfo(categoryInfoList, new CategoryInfo(), FileCategory.Doc);
                        addCategoryInfo(categoryInfoList, new CategoryInfo(), FileCategory.Zip);
                        addCategoryInfo(categoryInfoList, new CategoryInfo(), FileCategory.Apk);

                        CategoryInfo categoryInfo = new CategoryInfo();
                        categoryInfo.category = Download;
                        if (PermissionUtils.hasPermission(getActivity(), Downloads.Impl.PERMISSION_ACCESS_ALL)) {
                            categoryInfo.count = queryDownload();
                        } else {
                            categoryInfo.count = 0;
                            PermissionUtils.requestPermission(getActivity(), Downloads.Impl.PERMISSION_ACCESS_ALL, MY_PERMISSIONS_REQUEST_TO_ACCESS_DOWNLOAD);
                        }
                        categoryInfoList.add(categoryInfo);

                        categoryInfo = new CategoryInfo();
                        categoryInfo.category = FileCategory.Bluetooth;
                        if (PermissionUtils.hasPermission(getActivity(), BluetoothShare.PERMISSION_ACCESS)) {
                            categoryInfo.count = queryBluetooth();
                        } else {
                            categoryInfo.count = 0;
                            PermissionUtils.requestPermission(getActivity(), BluetoothShare.PERMISSION_ACCESS, MY_PERMISSIONS_REQUEST_TO_ACCESS_BLUETOOTH);
                        }
                        categoryInfoList.add(categoryInfo);

                        subscriber.onNext(categoryInfoList);
                        subscriber.onCompleted();
                    }
                } catch (Exception e) {
                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onError(e);
                    }
                }
            }

            private void addCategoryInfo(List<CategoryInfo> categoryInfoList, CategoryInfo categoryInfo, FileCategory fileCategory) {
                categoryInfo.category = fileCategory;
                categoryInfo.count = query(fileCategory);
                categoryInfoList.add(categoryInfo);
            }
        };

        Subscriber<List<CategoryInfo>> subscriber = new Subscriber<List<CategoryInfo>>() {

            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                LogUtils.e(TAG, "onError: ", e);
            }

            @Override
            public void onNext(List<CategoryInfo> categoryInfos) {
                try {
                    LogUtils.d(TAG, "onNext: ");
                    if (null != categoryInfos) {
                        for (CategoryInfo info : categoryInfos) {
                            switch (info.category) {
                                case Picture:
                                    setViewText(mCategoryPictureCount, info.count);
                                    break;

                                case Music:
                                    setViewText(mCategoryMusicCount, info.count);
                                    break;

                                case Video:
                                    setViewText(mCategoryVideoCount, info.count);
                                    break;

                                case Doc:
                                    setViewText(mCategoryDocumentCount, info.count);
                                    break;

                                case Zip:
                                    setViewText(mCategoryZipCount, info.count);
                                    break;

                                case Apk:
                                    setViewText(mCategoryApkCount, info.count);
                                    break;

                                case Download:
                                    setViewText(mCategoryDownloadCount, info.count);
                                    break;

                                case Bluetooth:
                                    setViewText(mCategoryBluetoothCount, info.count);
                                    break;

                                default:
                                    LogUtils.w(TAG, "onNext: default !!!");
                                    break;
                            }
                        }
                    }
                } catch (Exception e) {
                    LogUtils.e(TAG, "onNext: ", e);
                }
            }

            private void setViewText(TextView tv, int count) {
                if (null != tv) {
                    tv.setText(String.valueOf(count));
                }
            }
        };
            Observable.create(onSubscribe)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .compose(((HomeActivity)getActivity()).<List<CategoryInfo>>bindToLifecycle())
                    .subscribe(subscriber);
    }

    private int query(FileCategory fc) {
        long mills = SystemClock.currentThreadTimeMillis();
        int cnt = 0;
        FileCategoryHelper helper = new FileCategoryHelper(getActivity());
        //FileSortHelper sortHelper = new FileSortHelper();
        //sortHelper.setSortMethod(FileSortHelper.SortMethod.NAME);
        Cursor cursor = helper.query(fc, FileInfoComparator.SORT_BY_NAME);

        if (null != cursor) {
            cnt = cursor.getCount();
            while (cursor.moveToNext()) {
                String path = cursor.getString(cursor.getColumnIndex(MediaStore.Files.FileColumns.DATA));
                if (path != null && FileUtils.getFileName(path).startsWith(".")) {
                    cnt--;
                }
            }
           /* if (FileCategory.Picture == fc) {
                MediaItemAdapter itemAdapter = new MediaItemAdapter(cursor);
                for (int i = 0; i < cnt; i++) {
                    itemAdapter.getItem(i);
                    LogUtils.d(TAG, "query: name=" + itemAdapter.getFileName()
                            + ", time=" + FileUtils.formatModifiedTime(mHomeActivity, itemAdapter.getFileModifiedTime())
                            + ", size=" + itemAdapter.getFileSize());
                }
            }*/
            cursor.close();
        }
        long diff = SystemClock.currentThreadTimeMillis() - mills;
        LogUtils.d(TAG, "query " + fc + ": diff=" + diff);
        return cnt;
    }

    private int queryDownload() {

        long mills = SystemClock.currentThreadTimeMillis();
        int cnt = 0;
        Cursor cursor = SystemUtils.getDownloadCursor(getActivity());
        if (null != cursor) {
            cnt = cursor.getCount();
            while (cursor.moveToNext()) {
                int pathColumnId = cursor.getColumnIndex(Downloads.Impl._DATA);
                String path = cursor.getString(pathColumnId);
                LogUtils.e(TAG, "******Download名字=" + path + "*****");
                if (path != null && FileUtils.getFileName(path).startsWith(".")) {
                    cnt--;
                }
            }
            /*DownloadItemAdapter itemAdapter = new DownloadItemAdapter(cursor);
            for (int i=0; i<cnt; i++) {
                itemAdapter.getItem(i);
                LogUtils.d(TAG, "queryDownload: name=" + itemAdapter.getFileName()
                        + ", time=" + FileUtils.formatModifiedTime(mHomeActivity, itemAdapter.getFileModifiedTime())
                        + ", size=" + itemAdapter.getFileSize()
                        + ", path=" + itemAdapter.getFilePath());
            }*/
            cursor.close();
        }
        long diff = SystemClock.currentThreadTimeMillis() - mills;
        LogUtils.d(TAG, "queryDownload: diff=" + diff + ", cnt=" + cnt);
        return cnt;
    }

    private int queryBluetooth() {
        long mills = SystemClock.currentThreadTimeMillis();
        int cnt = 0;
        Cursor cursor = SystemUtils.getBluetoothCursor(getActivity());
        if (null != cursor) {
            cnt = cursor.getCount();
            while (cursor.moveToNext()) {
                String path = cursor.getString(cursor.getColumnIndex(BluetoothShare._DATA));
                LogUtils.e(TAG, "******Bluetooth路径=" + path + "*****");
                if (path != null && FileUtils.getFileName(path).startsWith(".")) {
                    cnt--;
                }
            }
            /*BluetoothItemAdapter itemAdapter = new BluetoothItemAdapter(cursor);
            for (int i=0; i<cnt; i++) {
                itemAdapter.getItem(i);
                LogUtils.d(TAG, "queryBluetooth: name=" + itemAdapter.getFileName()
                        + ", time=" + FileUtils.formatModifiedTime(mHomeActivity, itemAdapter.getFileModifiedTime())
                        + ", size=" + itemAdapter.getFileSize()
                        + ", path=" + itemAdapter.getFilePath());
            }*/
            cursor.close();
        }
        long diff = SystemClock.currentThreadTimeMillis() - mills;
        LogUtils.d(TAG, "queryBluetooth: diff=" + diff + ", cnt=" + cnt);
        return cnt;
    }


    //重新装载sd卡后要更新存储和分类数量
    @Override
    public void onMounted(String mountPoint) {
        LogUtils.e(TAG, "*****onMounted: mountPoint=" + mountPoint);
        mMountPointManager.init(getActivity().getApplicationContext());
        refreshMemory();
        refreshCategoryCount();
    }

    @TargetApi(24)
    @Override
    public void onUnMounted(StorageVolume volume) {
        LogUtils.e(TAG, "****onUnMounted: volume=" + volume);
        LogUtils.e(TAG, "****onUnMounted: volume.getPath=" + volume.getPath());
        String unMountPoint = volume.getPath();
        onEjected(unMountPoint);
    }

    @Override
    public void onEjected(String unMountPoint) {
        try {
            LogUtils.e(TAG, "*****onEjected: unMountPoint=" + unMountPoint);
            if (View.VISIBLE == mSrSd1.getVisibility()) {
                mSrSd1.setVisibility(View.GONE);
            }
            if (View.VISIBLE == mSrSd2.getVisibility()) {
                mSrSd2.setVisibility(View.GONE);
            }
            mMountPointManager.init(getActivity().getApplicationContext());
            refreshMemory();
        } catch (Exception e) {
            LogUtils.e(TAG, "onEjected: Exception !!", e);
        }
    }

    @Override
    public void onSdSwap() {

    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        LogUtils.d(TAG, "setUserVisibleHint: isVisibleToUser=" + isVisibleToUser);
        if (isVisibleToUser) {
            refreshCategoryCount();
            refreshMemory();
        }
    }

    @Override
    public void doPermissionGranted() {
        refreshMemory();
    }

    /*public class MyPermissionBroadcast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            LogUtils.e(TAG, "******action=:" + action);
            if (action != null && action.equals(HomeActivity.MY_REQUEST_TO_READ_EXTERNAL_STORAGE_PERMISSIONS_GRANTED)) {

            }
        }
    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        LogUtils.d(TAG, "onRequestPermissionsResult, requestCode:" + requestCode);
        if (null == permissions || permissions.length == 0 ||
                null == grantResults || grantResults.length == 0 ||
                PackageManager.PERMISSION_DENIED == grantResults[0]) {
            LogUtils.e(TAG, "**********onRequestPermissionsResult, Permission or grant res null*******");
            return;
        }

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_TO_ACCESS_BLUETOOTH:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    int bluetoothCnt = queryBluetooth();
                    mCategoryBluetoothCount.setText(String.valueOf(bluetoothCnt));
                }
                break;
            case MY_PERMISSIONS_REQUEST_TO_ACCESS_DOWNLOAD:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    int downloadCnt = queryDownload();
                    mCategoryDownloadCount.setText(String.valueOf(downloadCnt));
                }
                break;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
