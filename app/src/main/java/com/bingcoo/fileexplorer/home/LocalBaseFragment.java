package com.bingcoo.fileexplorer.home;

import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.storage.StorageVolume;
import android.util.Log;

import com.bingcoo.fileexplorer.receiver.MountReceiver;
import com.bingcoo.fileexplorer.service.FileManagerService;
import com.bingcoo.fileexplorer.util.DrmManager;
import com.bingcoo.fileexplorer.util.FileInfoManager;
import com.bingcoo.fileexplorer.util.MountPointManager;

import static android.content.Context.BIND_AUTO_CREATE;
import static android.content.Context.MODE_PRIVATE;

/**
 * 类名称：LocalBaseFragment
 * 作者：David
 * 内容摘要：
 * 创建日期：2016/11/25
 * 修改者， 修改日期， 修改内容
 */
public class LocalBaseFragment extends Fragment implements MountReceiver.MountListener {
    private static final String TAG = "LocalBaseFragment";
    private static final String PREF_SHOW_HIDEN_FILE = "pref_show_hiden_file";
    public static final String SAVED_PATH_KEY = "saved_path";

    public static final int MSG_DO_MOUNTED = 0;
    public static final int MSG_DO_EJECTED = 1;
    public static final int MSG_DO_UNMOUNTED = 2;
    public static final int MSG_DO_SDSWAP = 3;

    protected MountPointManager mMountPointManager = null;
    protected String mCurrentPath = null;
    protected FileManagerService mService = null;
    protected boolean mServiceBinded = false;
    protected FileInfoManager mFileInfoManager = null;
    protected MountReceiver mMountReceiver = null;

    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService.disconnected(this.getClass().getName());
            mServiceBinded = false;
            Log.w(TAG, "onServiceDisconnected");
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "onServiceConnected");
            mService = ((FileManagerService.ServiceBinder) service).getServiceInstance();
            mServiceBinded = true;
            serviceConnected();
        }
    };

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            Log.d(TAG, "handleMessage, msg = " + msg.what);
            switch (msg.what) {
                case MSG_DO_MOUNTED:
                    doOnMounted((String) msg.obj);
                    break;
                case MSG_DO_EJECTED:
                    doOnEjected((String) msg.obj);
                    break;
                case MSG_DO_UNMOUNTED:
                    doOnUnMounted((StorageVolume) msg.obj);
                    break;
                case MSG_DO_SDSWAP:
                    doOnSdSwap();
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    public void onMounted(String mountPoint) {

    }

    @Override
    public void onUnMounted(StorageVolume volume) {

    }

    @Override
    public void onEjected(String unMountPoint) {

    }

    @Override
    public void onSdSwap() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach: context=" + context);
        if (context instanceof HomeActivity) {
            Context appCxt = context.getApplicationContext();
            // register unmount/mount Receiver
            if (mMountReceiver == null) {
                mMountReceiver = MountReceiver.registerMountReceiver(context);
                mMountReceiver.registerMountListener(this);
            }

            mMountPointManager = MountPointManager.getInstance();
            mMountPointManager.init(appCxt);
            DrmManager.getInstance().init(appCxt);

            appCxt.bindService(new Intent(appCxt, FileManagerService.class),
                        mServiceConnection, BIND_AUTO_CREATE);
        }
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        if (null != savedInstanceState) {
            String savePath = savedInstanceState.getString(SAVED_PATH_KEY);
            if (savePath != null
                && mMountPointManager.isMounted(mMountPointManager
                                                        .getRealMountPointPath(savePath))) {
                mCurrentPath = savePath;
            } else {
                //mCurrentPath = initCurrentFileInfo();
            }
        }
    }

    private void doOnMounted(String mountPoint) {
        Log.i(TAG, "doOnMounted,mountPoint = " + mountPoint);
        doPrepareForMount(mountPoint);
        if (mMountPointManager.isRootPath(mCurrentPath)) {
            Log.d(TAG, "doOnMounted,mCurrentPath is root path: " + mCurrentPath);
            showDirectoryContent(mCurrentPath);
        }
    }

    private void doOnSdSwap() {

    }

    private void doOnEjected(String unMountPoint) {

    }

    private void doOnUnMounted(StorageVolume volume) {

    }

    private void doPrepareForMount(String mountPoint) {
        Log.i(TAG, "doPrepareForMount,mountPoint = " + mountPoint);
        if ((mCurrentPath + MountPointManager.SEPARATOR).startsWith(mountPoint
                                                                    + MountPointManager.SEPARATOR)
            || mMountPointManager.isRootPath(mCurrentPath)) {
            Log.d(TAG, "pre-onMounted");
            if (mService != null && mService.isBusy(this.getClass().getName())) {
                mService.cancel(this.getClass().getName());
            }
        }

        mMountPointManager.init(getActivity().getApplicationContext());
    }

    protected void showDirectoryContent(String path) {
        Log.d(TAG, "showDirectoryContent,path = " + path);
        if (getActivity().isFinishing()) {
            Log.i(TAG, "showDirectoryContent,isFinishing: true, do not loading again");
            return;
        }
        mCurrentPath = path;
        if (mService != null) {
            // TODO
            //mService.listFiles(this.getClass().getName(), mCurrentPath, new ListListener());
        }
    }

    protected void serviceConnected() {
        Log.i(TAG, "serviceConnected");

        mFileInfoManager = mService.initFileInfoManager(this.getClass().getName());
        mService.setListType(getPrefsShowHidenFile() ? FileManagerService.FILE_FILTER_TYPE_ALL
                                     : FileManagerService.FILE_FILTER_TYPE_DEFAULT, this.getClass().getName());

        /*mAdapter = new FileInfoAdapter(AbsBaseActivity.this, mService, mFileInfoManager);
        if (mListView != null) {
            mListView.setAdapter(mAdapter);

            if (mSavedInstanceState == null) {
                mCurrentPath = initCurrentFileInfo();
                if (mCurrentPath != null) {
                    showDirectoryContent(mCurrentPath);
                }
            } else {
                String savePath = mSavedInstanceState.getString(SAVED_PATH_KEY);
                if (savePath != null
                    && mMountPointManager.isMounted(mMountPointManager
                                                            .getRealMountPointPath(savePath))) {
                    mCurrentPath = savePath;
                } else {
                    mCurrentPath = initCurrentFileInfo();
                }

                if (mCurrentPath != null) {
                    mTabManager.refreshTab(mCurrentPath);
                    reloadContent();
                }
                restoreDialog();

            }
            mAdapter.notifyDataSetChanged();
        }*/
        // register Receiver when service connected..
        if (mMountReceiver == null) {
            mMountReceiver = MountReceiver.registerMountReceiver(getActivity());
            mMountReceiver.registerMountListener(this);
        }
    }

    protected boolean getPrefsShowHidenFile() {
        SharedPreferences prefs = getActivity().getPreferences(MODE_PRIVATE);
        return prefs.getBoolean(PREF_SHOW_HIDEN_FILE, false);
    }

}
