package com.bingcoo.fileexplorer.home;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.bingcoo.fileexplorer.interfaces.OnBackPressedListener;
import com.bingcoo.fileexplorer.util.LogUtils;


/**
 * 类名称：HomePagerAdapter
 * 作者：David
 * 内容摘要：首页adapter
 * 创建日期：2016/11/21
 * 修改者， 修改日期， 修改内容
 */
class HomePagerAdapter extends PagerAdapter {
    private static final String TAG = "HomePagerAdapter";
    static {
        LogUtils.setDebug(TAG, true);
    }

    private static final String TAG_LOCAL_FRAGMENT = "local";
    private static final String TAG_CATEGORY_FRAGMENT = "category";
    public static final int TAB_INDEX_LOCAL = 1;
    public static final int TAB_INDEX_CATEGORY = 0;

    private FragmentTransaction mCurTransaction = null;
    private Fragment mCurrentPrimaryItem = null;
    private int mCount = 0;
    private FragmentManager mFragmentManager = null;
    private final String[] mTabTitles;

    HomePagerAdapter(FragmentManager fragmentManager, int count, String[] tabTitles) {
        mFragmentManager = fragmentManager;
        mCount = count;
        mTabTitles = tabTitles;
    }

    @Override
    public int getCount() {
        return mCount;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return !((null == view) || (null == object)) && ((Fragment) object).getView() == view;
    }

    @SuppressLint("CommitTransaction")
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LogUtils.d(TAG, "instantiateItem: position=" + position);
        if (null == mCurTransaction) {
            mCurTransaction = mFragmentManager.beginTransaction();
        }
        Fragment f = getFragment(position);
        if (f.isAdded()) {
            mCurTransaction.show(f);
        } else {
            mCurTransaction.add(container.getId(), f, getFragmentTag(position));
        }

        f.setUserVisibleHint(f == mCurrentPrimaryItem);
        return f;
    }

    @SuppressLint("CommitTransaction")
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        LogUtils.d(TAG, "destroyItem: position=" + position);
        if (null == mCurTransaction) {
            mCurTransaction = mFragmentManager.beginTransaction();
        }
        mCurTransaction.hide((Fragment) object);
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        LogUtils.d(TAG, "finishUpdate: ");
        if (null != mCurTransaction) {
            mCurTransaction.commitAllowingStateLoss();
            mCurTransaction = null;
            mFragmentManager.executePendingTransactions();
        }
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        LogUtils.d(TAG, "setPrimaryItem: position=" + position);
        Fragment fragment = (Fragment) object;
        if (mCurrentPrimaryItem != fragment) {
            if (null != mCurrentPrimaryItem) {
                mCurrentPrimaryItem.setUserVisibleHint(false);
            }
            if (null != fragment) {
                fragment.setUserVisibleHint(true);
            }
            mCurrentPrimaryItem = fragment;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabTitles[position];
    }

    private Fragment getFragment(int position) {
        LogUtils.d(TAG, "getFragment: position=" + position);
        Fragment fragment = null;
        switch (position) {
            case TAB_INDEX_LOCAL:
                LocalFragment localFragment = (LocalFragment) mFragmentManager.findFragmentByTag(TAG_LOCAL_FRAGMENT);
                if (null == localFragment) {
                    localFragment = new LocalFragment();
                }
                fragment = localFragment;
                break;

            case TAB_INDEX_CATEGORY:
                CategoryFragment categoryFragment = (CategoryFragment) mFragmentManager.findFragmentByTag(TAG_CATEGORY_FRAGMENT);
                if (null == categoryFragment) {
                    categoryFragment = new CategoryFragment();
                }
                fragment = categoryFragment;
                break;

            default:
                LogUtils.w(TAG, "getFragment: invalid position!!! position=" + position);
                break;
        }
        return fragment;
    }

    private static String getFragmentTag(int position) {
        switch (position) {
            case TAB_INDEX_LOCAL:
                return TAG_LOCAL_FRAGMENT;

            case TAB_INDEX_CATEGORY:
                return TAG_CATEGORY_FRAGMENT;

            default:
                LogUtils.w(TAG, "getFragmentTag: invalid position!!! position=" + position);
                break;
        }
        return null;
    }

    public boolean onBackPressed() {
        LogUtils.d(TAG, "onBackPressed: ");
        if (mCurrentPrimaryItem instanceof OnBackPressedListener) {
            return ((OnBackPressedListener) mCurrentPrimaryItem).onBackPressed();
        }
        return false;
    }

}
