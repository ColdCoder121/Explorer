package com.bingcoo.fileexplorer.home;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.util.LayoutDirection;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bingcoo.fileexplorer.R;
import com.bingcoo.fileexplorer.service.FileIconLoader;
import com.bingcoo.fileexplorer.service.FileManagerService;
import com.bingcoo.fileexplorer.util.FileInfo;
import com.bingcoo.fileexplorer.util.FileInfoManager;
import com.bingcoo.fileexplorer.util.FileUtils;
import com.bingcoo.fileexplorer.util.IconManager;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.MountPointManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class FileInfoAdapter extends BaseAdapter {
    private static final String TAG = "FileInfoAdapter";

    static {
        LogUtils.setDebug(TAG, true);
    }

    private static final int DEFAULT_SECONDARY_SIZE_TEXT_COLOR = 0xff414141;
    private static final int DEFAULT_PRIMARY_TEXT_COLOR = Color.BLACK;
    private static final float CUT_ICON_ALPHA = 0.6f;
    private static final float HIDE_ICON_ALPHA = 0.3f;
    private static final float DEFAULT_ICON_ALPHA = 1f;
    public static final int MODE_NORMAL = 0;
    public static final int MODE_EDIT = 1;
    public static final int MODE_SEARCH = 2;

    private static final int TYPE_LOCAL = 100;
    public static final int TYPE_CATEGORY = 101;
    private static final int TYPE_ZIP = 102;
    private int mType = TYPE_LOCAL;

    protected Context mContext;
    protected final Resources mResources;
    protected final LayoutInflater mInflater;
    protected final List<FileInfo> mFileInfoList;
    protected final FileInfoManager mFileInfoManager;
    protected FileIconLoader mFileIconLoader;

    protected int mMode = MODE_NORMAL;
    FileManagerService mService = null;

    /**
     * The constructor to construct a FileInfoAdapter.
     *
     * @param context            the context of FileManagerActivity
     * @param fileManagerService the service binded with FileManagerActivity
     * @param fileInfoManager    a instance of FileInfoManager, which manages all files.
     */
    public FileInfoAdapter(Context context, FileManagerService fileManagerService,
                           FileInfoManager fileInfoManager) {
        mContext = context;
        mResources = context.getResources();
        mInflater = LayoutInflater.from(context);
        mService = fileManagerService;
        mFileInfoManager = fileInfoManager;
        mFileInfoList = fileInfoManager.getShowFileList();
    }

    public void setType(int type) {
        mType = type;
    }

    /**
     * This method gets index of certain fileInfo(item) in fileInfoList
     *
     * @param fileInfo the fileInfo which wants to be located.
     * @return the index of the item in the listView.
     */
    public int getPosition(FileInfo fileInfo) {
        return mFileInfoList.indexOf(fileInfo);
    }

    /**
     * This method sets the item's check boxes
     *
     * @param id      the id of the item
     * @param checked the checked state
     */
    public void setChecked(int id, boolean checked) {
        FileInfo checkInfo = mFileInfoList.get(id);
        if (checkInfo != null) {
            LogUtils.d(TAG, "setChecked: position=" + id + ", checked=" + checked);
            checkInfo.setChecked(checked);
        }
    }

    /**
     * This method sets all items' check boxes
     *
     * @param checked the checked state
     */
    public void setAllItemChecked(boolean checked) {
        for (FileInfo info : mFileInfoList) {
            info.setChecked(checked);
        }
        notifyDataSetChanged();
    }

    /**
     * This method gets the number of the checked items
     *
     * @return the number of the checked items
     */
    public int getCheckedItemsCount() {
        int count = 0;
        for (FileInfo fileInfo : mFileInfoList) {
            if (fileInfo.isChecked()) {
                count++;
            }
        }
        return count;
    }

    /**
     * This method gets the list of the checked items
     *
     * @return the list of the checked items
     */
    public List<FileInfo> getCheckedFileInfoItemsList() {
        List<FileInfo> fileInfoCheckedList = new ArrayList<FileInfo>();
        for (FileInfo fileInfo : mFileInfoList) {
            if (fileInfo.isChecked()) {
                fileInfoCheckedList.add(fileInfo);
            }
        }
        return fileInfoCheckedList;
    }

    /**
     * This method gets the first item in the list of the checked items
     *
     * @return the first item in the list of the checked items
     */
    public FileInfo getFirstCheckedFileInfoItem() {
        for (FileInfo fileInfo : mFileInfoList) {
            if (fileInfo.isChecked()) {
                return fileInfo;
            }
        }
        return null;
    }

    /**
     * This method gets the count of the items in the name list
     *
     * @return the number of the items
     */
    @Override
    public int getCount() {
        return mFileInfoList.size();
    }

    /**
     * This method gets the name of the item at the specified position
     *
     * @param pos the position of item
     * @return the name of the item
     */
    @Override
    public FileInfo getItem(int pos) {
        return mFileInfoList.get(pos);
    }

    /**
     * This method gets the item id at the specified position
     *
     * @param pos the position of item
     * @return the id of the item
     */
    @Override
    public long getItemId(int pos) {
        return pos;
    }

    /**
     * This method change all checked items to be unchecked state
     */
    public void clearChecked() {
        for (FileInfo fileInfo : mFileInfoList) {
            if (fileInfo.isChecked()) {
                fileInfo.setChecked(false);
            }
        }
    }

    /**
     * This method changes the display mode of adapter between MODE_NORMAL, MODE_EDIT, and
     * MODE_SEARCH
     *
     * @param mode the mode which will be changed to be.
     */
    public void changeMode(int mode) {
        LogUtils.d(TAG, "changeMode, mode = " + mode);
        switch (mode) {
            case MODE_NORMAL:
                clearChecked();
                break;
            case MODE_SEARCH:
                mFileInfoList.clear();
                break;
            default:
                break;
        }
        mMode = mode;
        notifyDataSetChanged();
    }

    /**
     * This method gets current display mode of the adapter.
     *
     * @return current display mode of adapter
     */
    public int getMode() {
        return mMode;
    }

    /**
     * This method checks that current mode equals to certain mode, or not.
     *
     * @param mode the display mode of adapter
     * @return true for equal, and false for not equal
     */
    public boolean isMode(int mode) {
        return mMode == mode;
    }

    /**
     * This method gets the view for each item to be displayed in the list view
     *
     * @param pos         the position of the item
     * @param convertView the view to be shown
     * @param parent      the parent view
     * @return the view to be shown
     */
    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        FileViewHolder viewHolder;
        View view = convertView;

        if (view == null) {
            view = mInflater.inflate(R.layout.adapter_fileinfos, null);
            viewHolder = new FileViewHolder(
                    (TextView) view.findViewById(R.id.edit_adapter_name),
                    (TextView) view.findViewById(R.id.edit_adapter_desc),
                    (ImageView) view.findViewById(R.id.edit_adapter_img),
                    (ImageView) view.findViewById(R.id.edit_adapter_enter),
                    (CheckBox) view.findViewById(R.id.edit_adapter_cb));
            view.setTag(viewHolder);
            // navListItemView.setMinimumHeight(mListItemMinHeight);
        } else {
            viewHolder = (FileViewHolder) view.getTag();
        }

        FileInfo currentItem = mFileInfoList.get(pos);
        LogUtils.d(TAG, "getView, pos = " + pos + ", mMode = " + mMode + ", checked=" + currentItem.isChecked());
        viewHolder.mName.setText(currentItem.getShowName());
        viewHolder.mEnter.setVisibility(
                (currentItem.isDirectory() && (MODE_EDIT != mMode)) ? View.VISIBLE : View.INVISIBLE);
        viewHolder.mCheck.setVisibility(MODE_EDIT == mMode ? View.VISIBLE : View.GONE);
        viewHolder.mDesc.setVisibility(TYPE_ZIP == mType && currentItem.isDirectory() ? View.GONE : View.VISIBLE);

        switch (mMode) {
            case MODE_EDIT:
                viewHolder.mCheck.setChecked(currentItem.isChecked() ? true : false);
                setSizeText(viewHolder.mDesc, currentItem);
                break;

            case MODE_NORMAL:
                setSizeText(viewHolder.mDesc, currentItem);
                break;

            case MODE_SEARCH:
                setSizeText(viewHolder.mDesc, currentItem);
                //setSearchSizeText(viewHolder.mDesc, currentItem);
                break;

            default:
                break;
        }
//        boolean end = pos==mFileInfoList.size() - 1;
        int direction = LayoutDirection.LTR;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            direction = parent.getLayoutDirection();
        }
        setIcon(viewHolder, currentItem, direction, pos);
        return view;
    }

    private void setSearchSizeText(TextView textView, FileInfo fileInfo) {
        textView.setText(fileInfo.getShowParentPath());
        textView.setVisibility(View.VISIBLE);
    }

    private void setSizeText(TextView textView, FileInfo fileInfo) {
        if (fileInfo.isDirectory()) {
            if (MountPointManager.getInstance().isMountPoint(fileInfo.getFileAbsolutePath())) {
                StringBuilder sb = new StringBuilder();
                String freeSpaceString = FileUtils.sizeToString(
                        MountPointManager.getInstance()
                                .getMountPointFreeSpace(fileInfo.getFileAbsolutePath()));
                String totalSpaces = FileUtils.sizeToString(
                        MountPointManager.getInstance()
                                .getMountPointTotalSpace(fileInfo.getFileAbsolutePath()));
                LogUtils.d(TAG, "setSizeText, file name = " + fileInfo.getFileName()
                        + ",file path = " + fileInfo.getFileAbsolutePath());
                LogUtils.d(TAG, "setSizeText, freeSpace = "
                        + MountPointManager.getInstance().getMountPointFreeSpace(fileInfo.getFileAbsolutePath())
                        + ",totalSpace = "
                        + MountPointManager.getInstance().getMountPointTotalSpace(fileInfo.getFileAbsolutePath()));

                sb.append(mResources.getString(R.string.total_space)).append(":");
                sb.append(totalSpaces).append(", ");
                sb.append(mResources.getString(R.string.free_space)).append(":");
                sb.append(freeSpaceString);

                textView.setText(sb.toString());
                textView.setVisibility(View.VISIBLE);
            } else {
                // it is a directory
                if (TYPE_ZIP == mType) {
                    //压缩文件列表文件夹不用显示描述
                    return;
                }
                File dirFile = fileInfo.getFile();
                int dirs = 0;
                int files = 0;
                if (null != dirFile.listFiles()) {
                    for (File file : dirFile.listFiles()) {
                        if (file.getName().startsWith(".")) {
                            continue;
                        }

                        if (file.isDirectory()) {
                            dirs++;
                        } else {
                            files++;
                        }
                    }
                }
                LogUtils.d(TAG, "setSizeText: files=" + files + ", dirs=" + dirs);
                StringBuilder sb = new StringBuilder();
                sb.append(mResources.getString(R.string.file)).append(":");
                sb.append(files).append(", ");
                sb.append(mResources.getString(R.string.folder)).append(":");
                sb.append(dirs);

                textView.setText(sb.toString());
                textView.setVisibility(View.VISIBLE);
            }
        } else {
            String desc;
            if (TYPE_LOCAL == mType) {
                desc = FileUtils.formatModifiedTime(textView.getContext(),
                        fileInfo.getFileLastModifiedTime());
            } else if (TYPE_ZIP == mType) {
                desc = fileInfo.getFileSizeStr();
            } else {
                desc = FileUtils.formatModifiedTime(textView.getContext(),
                        fileInfo.getFileLastModifiedTime()) + "  " + fileInfo.getFileSizeStr();
            }
            LogUtils.d(TAG, "setSizeText: desc=" + desc);

            textView.setText(desc);
            textView.setVisibility(View.VISIBLE);
        }
    }

    private void setIcon(FileViewHolder viewHolder, FileInfo fileInfo, int viewDirection, int pos) {
        LogUtils.e(TAG, "getview:" + fileInfo.getFileAbsolutePath());
        if (fileInfo == null) {
            return;
        }
        String extention = FileUtils.getFileExtension(fileInfo.getFileName());
        if (extention != null && extention.equalsIgnoreCase("apk")) {
            if (mFileIconLoader == null) {
                mFileIconLoader = new FileIconLoader(mContext);
            }
            mFileIconLoader.setApkFileImage(viewHolder.mIcon);
            if (!mFileIconLoader.loadIcon(viewHolder.mIcon, fileInfo.getFileAbsolutePath())) {
                viewHolder.mIcon.setImageResource(R.drawable.fm_apk);
            }
        } else {
            if (mFileIconLoader != null) {
                mFileIconLoader.setOtherFileImage(viewHolder.mIcon);
            }
            Bitmap icon = IconManager.getInstance().getIcon(mResources, fileInfo, mService, viewDirection);
            viewHolder.mIcon.setImageBitmap(icon);
        }

        /*viewHolder.mIcon.setAlpha(DEFAULT_ICON_ALPHA);
        if (FileInfoManager.PASTE_MODE_CUT == mFileInfoManager.getPasteType()) {
            if (mFileInfoManager.isPasteItem(fileInfo)) {
                viewHolder.mIcon.setAlpha(CUT_ICON_ALPHA);
            }
        }
        if (fileInfo.isHideFile()) {
            viewHolder.mIcon.setAlpha(HIDE_ICON_ALPHA);
        }*/
    }

    protected static class FileViewHolder {
        public TextView mName;
        public TextView mDesc;
        public ImageView mIcon;
        public ImageView mEnter;
        public CheckBox mCheck;

        /**
         * The constructor to construct an edit view tag
         *
         * @param name the name view of the item
         * @param desc the desc view of the item
         * @param icon the icon view of the item
         */
        public FileViewHolder(TextView name, TextView desc, ImageView icon, ImageView enter, CheckBox check) {
            this.mName = name;
            this.mDesc = desc;
            this.mIcon = icon;
            this.mEnter = enter;
            this.mCheck = check;
        }
    }
}
