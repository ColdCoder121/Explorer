package com.bingcoo.fileexplorer.home;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Parcelable;
import android.os.storage.StorageVolume;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bingcoo.fileexplorer.R;
import com.bingcoo.fileexplorer.category.zip.OpenZipsActivity;
import com.bingcoo.fileexplorer.destination.DstActivity;
import com.bingcoo.fileexplorer.dialog.fragment.AlertDialogFragment;
import com.bingcoo.fileexplorer.dialog.fragment.AlertDialogFragment.AlertDialogFragmentBuilder;
import com.bingcoo.fileexplorer.dialog.fragment.AlertDialogFragment.ChoiceDialogFragment;
import com.bingcoo.fileexplorer.dialog.fragment.AlertDialogFragment.EditDialogFragmentBuilder;
import com.bingcoo.fileexplorer.dialog.fragment.AlertDialogFragment.EditTextDialogFragment;
import com.bingcoo.fileexplorer.dialog.fragment.ProgressDialog;
import com.bingcoo.fileexplorer.dialog.fragment.ProgressDialogFragment;
import com.bingcoo.fileexplorer.interfaces.OnBackPressedListener;
import com.bingcoo.fileexplorer.receiver.MountReceiver;
import com.bingcoo.fileexplorer.receiver.PermissionReceiver;
import com.bingcoo.fileexplorer.service.FileManagerService;
import com.bingcoo.fileexplorer.service.HeavyOperationListener;
import com.bingcoo.fileexplorer.service.ProgressInfo;
import com.bingcoo.fileexplorer.ui.SlowHorizontalScrollView;
import com.bingcoo.fileexplorer.util.ConstUtils;
import com.bingcoo.fileexplorer.util.DrmManager;
import com.bingcoo.fileexplorer.util.FileInfo;
import com.bingcoo.fileexplorer.util.FileInfoComparator;
import com.bingcoo.fileexplorer.util.FileInfoManager;
import com.bingcoo.fileexplorer.util.FileUtils;
import com.bingcoo.fileexplorer.util.IntentUtils;
import com.bingcoo.fileexplorer.util.KeyboardUtils;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.LongStringUtils;
import com.bingcoo.fileexplorer.util.MediaUtils;
import com.bingcoo.fileexplorer.util.MountPointManager;
import com.bingcoo.fileexplorer.util.PermissionUtils;
import com.bingcoo.fileexplorer.util.SharedPrefUtils;
import com.bingcoo.fileexplorer.util.ToastHelper;
import com.jakewharton.rxbinding.view.RxView;
import com.jakewharton.rxbinding.widget.RxTextView;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import bingo.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.functions.Action1;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.BIND_AUTO_CREATE;
import static com.bingcoo.fileexplorer.R.id.lst_media;
import static com.bingcoo.fileexplorer.R.string.rename;
import static com.bingcoo.fileexplorer.category.ListActivity.SAVED_HAS_RESULT;
import static com.bingcoo.fileexplorer.category.picture.PictureGridActivity.SAVED_SHOW_MORE_DIALOG;
import static com.bingcoo.fileexplorer.category.picture.PictureGridActivity.SAVED_SHOW_OPTION_DIALOG;
import static com.bingcoo.fileexplorer.category.video.VideoGridActivity.SAVED_ADAPTER_MODE;
import static com.bingcoo.fileexplorer.category.video.VideoGridActivity.SAVED_CHECKED_LIST;
import static com.bingcoo.fileexplorer.category.video.VideoGridActivity.SAVED_FILE_LIST;
import static com.bingcoo.fileexplorer.category.video.VideoGridActivity.SAVED_LIST_CHANGED;
import static com.bingcoo.fileexplorer.category.video.VideoGridActivity.SAVED_SHOW_LIST;
import static com.bingcoo.fileexplorer.category.zip.OpenZipsActivity.FileType.RAR;
import static com.bingcoo.fileexplorer.category.zip.OpenZipsActivity.FileType.ZIP;
import static com.bingcoo.fileexplorer.home.HomeActivity.MODE_NORMAL;
import static com.bingcoo.fileexplorer.home.HomeActivity.MODE_SEARCH;
import static com.bingcoo.fileexplorer.home.HomeActivity.MODE_SELECT;

//import android.app.AlertDialog;
//import com.github.junrar.rarfile.FileHeader;

/**
 * 类名称：LocalFragment
 * 作者：David
 * 内容摘要：本地页面
 * 创建日期：2016/11/21
 * 修改者， 修改日期， 修改内容
 */
public class LocalFragment extends Fragment implements MountReceiver.MountListener,
        AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener,
        OnBackPressedListener, View.OnClickListener, AlertDialogFragment.OnDialogDismissListener,
        PermissionReceiver.onPermissionGranted {
    private static final String TAG = "LocalFragment";

    static {
        LogUtils.setDebug(TAG, true);
    }

    private static final String PREF_SHOW_HIDDEN_FILE = "pref_show_hidden_file";
    private static final String PREF_SORT_BY = "pref_sort_by";

    public static final String SAVED_PATH_KEY = "saved_path";
    public static final String SAVED_SEARCH_TEXT = "search_text";
    public static final String SAVED_SEARCH_TOTAL = "search_total";
    private static final String SAVED_SELECTED_PATH_KEY = "saved_selected_path";
    private static final String DETAIL_INFO_KEY = "detail_info_key";
    private static final String DETAIL_INFO_SIZE_KEY = "detail_info_size_key";

    public static final String RENAME_EXTENSION_DIALOG_TAG = "rename_extension_dialog_fragment_tag";
    public static final String RENAME_DIALOG_TAG = "rename_dialog_fragment_tag";
    public static final String CREATE_FOLDER_DIALOG_TAG = "CreateFolderDialog";
    private static final String NEW_FILE_PATH_KEY = "new_file_path_key";
    public static final String DELETE_DIALOG_TAG = "delete_dialog_fragment_tag";

    //    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE_FOR_RENAME = 1;
    private static final int REQUEST_CODE_PASTE = 0x100;
    private static final int REQUEST_CODE_BROWSE = 0x101;

    public static final String EXTRA_PASTE_DIRECTORY = "extra_paste_directory";

    public static final int MSG_DO_MOUNTED = 0;
    public static final int MSG_DO_EJECTED = 1;
    public static final int MSG_DO_UNMOUNTED = 2;
    public static final int MSG_DO_SDSWAP = 3;

    public static final String EXTRA_ZIPS_PATH = "extra_zips_path";
    public static final String EXTRA_ZIPS_NAME = "extra_zips_name";
    public static final String EXTRA_ZIPS_TYPE = "extra_zips_type";
    public static final String EXTRA_ZIPS_PARENT = "extra_zips_parent";

    public static final int MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_DELETE = 5;
    public static final int MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_CUT = 6;
    public static final int MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_RENAME = 7;
    public static final int MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_COPY = 8;
    public static final int MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_ZIP = 9;
    public static final int MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_RAR = 10;
    public static final int MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_CREATE = 11;
    private String mItemClickPath;
    private String mItemClickParentPath;
    private String mItemClickFileName;

    protected MountPointManager mMountPointManager = null;
    protected String mCurrentPath = null;
    protected FileManagerService mService = null;
    protected boolean mServiceBinded = false;
    protected FileInfoManager mFileInfoManager = null;
    protected MountReceiver mMountReceiver = null;
    protected PermissionReceiver mPermissionReceiver = null;
    protected Bundle mSavedInstanceState = null;
    protected int mSortType = 0;
    protected FileInfo mSelectedFileInfo = null;
    protected int mSelectedTop = -1;
    protected int mTop = -1;
    private ToastHelper mToastHelper = null;
    private ProgressDialog mRefreshingProgressDialog;

    //    private HomeActivity mHomeActivity;
    private FileInfoAdapter mAdapter;
    private int mPosition;
    private int[] mPositionTopList = new int[2];
    private Map<String, int[]> mPositionMap = new HashMap<>();
    private boolean mScrollBack;
    private boolean mIsRename = false;
    private String mCreatedFolderName = null;
    private String mRenamedName = null;
    private FileInfo mRenamedItem = null;

    private TextView mLocalTitle;
    private View mFooter;
    private LayoutInflater mInflater;
    protected TabManager mTabManager;
    private AlertDialog mOptionsDialog;
    private AlertDialog mMoreDialog;
    private BottomBar mBottomBar;
    protected boolean mIsAlertDialogShowing = false;
    private boolean showRenameOnResume = false;
    private String mSearchText;
    private long mSearchTotal;
    private long mSearchTotalChanged;//搜索成功不重置
    private TextView mTvSelectAll;
    private TextView mTvSelectChosen;
    private TextView mTvSelectCancel;
    private List<FileInfo> mShowFileList = new ArrayList<>();//正常状态列表，搜索状态切换回来后如果没有改动不刷新
    private boolean mListChanged;//搜索状态是否有改动
    private boolean mEnterSearch;//是否进入搜索状态
    private EditText mSearchInput;
    private ArrayList<FileInfo> mFileListWhenOut = new ArrayList<>();//退出时列表
    private boolean hasResult;
    private boolean mOnActivityResult = false;

    @BindView(lst_media)
    ListView mLstMedia;
    @BindView(R.id.sv_navigation_bar)
    SlowHorizontalScrollView mNavigationBar;
    @BindView(R.id.layout_local_search_result)
    View mLayoutLocalSearchResult;
    @BindView(R.id.layout_edit)
    View mBottomBarEdit;
//    @BindView(R.id.layout_clean)
//    View mBottomBarClean;
    @BindView(R.id.layout_menu)
    View mBottomBarMenu;

    @BindView(R.id.ll_menu)
    View mMenuView;
//    @BindView(R.id.ll_clean)
//    View mCleanView;
//    @BindView(R.id.tv_clean)
//    View mCleanTv;
//    @BindView(R.id.im_clean)
//    View mCleanImg;
    @BindView(R.id.layout_local)
    View mLayoutLocal;
    @BindView(R.id.layout_local_search)
    View mLayoutSearch;
    @BindView(R.id.layout_loading)
    View mLayoutLoading;
    @BindView(R.id.layout_no_matching_search)
    View mLayoutNoMatchingSearch;
    @BindView(R.id.tv_search_result)
    TextView mTvSearchResult;
    @BindView(R.id.empty_view)
    TextView mLayoutEmptyView;

    @BindView(R.id.tv_share)
    TextView mTvShare;
    @BindView(R.id.tv_delete)
    TextView mTvDelete;
    @BindView(R.id.tv_cut)
    TextView mTvCut;
    @BindView(R.id.tv_more)
    TextView mTvMore;

    private HeavyOperationListener.HeavyOperationListenerCallback mHeavyOperationListenerCallback =
            new HeavyOperationListener.HeavyOperationListenerCallback() {

                @Override
                public void onTaskResult(int errorType) {
                    mFileInfoManager.updateFileInfoList(mCurrentPath, mSortType);
                    mAdapter.notifyDataSetChanged();
                    mSearchTotalChanged = mAdapter.getCount();
                    mTvSearchResult.setText(getResources().getString(
                            R.string.search_result, mSearchTotalChanged));
                    if (isEmpty()) {
                        switchLayout(mLayoutLocal);
                        mLayoutEmptyView.setVisibility(View.VISIBLE);
                    }
                    if (mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
                        onBackPressed();
                    }
                }

                @Override
                public void onClick(View v) {
                    if (mService != null) {
                        LogUtils.i(TAG, "onClick cancel");
                        mService.cancel(LocalFragment.this.getClass().getName());
                    }
                }
            };

    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService.disconnected(this.getClass().getName());
            mServiceBinded = false;
            LogUtils.w(TAG, "onServiceDisconnected");
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LogUtils.d(TAG, "onServiceConnected");
            mService = ((FileManagerService.ServiceBinder) service).getServiceInstance();
            mServiceBinded = true;
            serviceConnected();
        }
    };

    /**
     * 某些版本不执行此方法，而是执行onAttach(Activity activity)
     * 这个过时的方法，android自身bug，坑
     *
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        LogUtils.d(TAG, "onAttach: context=" + context);
//        init();
    }

    private void init() {
//        if (context instanceof HomeActivity) {
//            mHomeActivity = (HomeActivity) context;
        mToastHelper = new ToastHelper(getActivity());
        Context appCxt = getActivity().getApplicationContext();
        mSortType = getPrefsSortBy();
        // register unmount/mount Receiver
        if (null == mMountReceiver) {
            mMountReceiver = MountReceiver.registerMountReceiver(getActivity());
            mMountReceiver.registerMountListener(this);
        }
        if (mPermissionReceiver == null) {
            mPermissionReceiver = PermissionReceiver.registerPermissionReceiver(getActivity());
            mPermissionReceiver.registerPermissionListener(this);
        }
        mMountPointManager = MountPointManager.getInstance();
        //mMountPointManager.init(appCxt);
        DrmManager.getInstance().init(appCxt);
//        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LogUtils.d(TAG, "onCreateView: ");
        init();
        mInflater = inflater;
        View view = inflater.inflate(R.layout.fragment_local, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSavedInstanceState = savedInstanceState;
        getActivity().bindService(new Intent(getActivity().getApplicationContext(), FileManagerService.class),
                mServiceConnection, BIND_AUTO_CREATE);
        mSearchInput = ((HomeActivity) getActivity()).getEtSearchInput();
        if (null != mLstMedia) {
            //mLstMedia.setEmptyView(mHomeActivity.findViewById(R.id.empty_view));
            mLstMedia.setOnItemClickListener(this);
            mLstMedia.setOnItemLongClickListener(this);
            mLstMedia.setFastScrollEnabled(false);
            mLstMedia.setVerticalScrollBarEnabled(true);
            mFooter = mInflater.inflate(R.layout.layout_lst_footer, mLstMedia, false);
            mLstMedia.addFooterView(mFooter);
        }

        if (null != mNavigationBar) {
            mNavigationBar.setVerticalScrollBarEnabled(false);
            mNavigationBar.setHorizontalScrollBarEnabled(false);
            mTabManager = new TabManager(
                    (LinearLayout) mNavigationBar.findViewById(R.id.ll_tabs_holder));
        }

        mBottomBar = new BottomBar(mBottomBarEdit, mBottomBarMenu);
        mBottomBar.switchBar(null);

        setViewClickListener(mMenuView,
                new Runnable() {
                    @Override
                    public void run() {
                        LogUtils.d(TAG, "mMenuView run: ");
                        showOptionDialog();
                    }
                });

//        setCleanViewEnable(false);
        /*setViewClickListener(mCleanView,
                new Runnable() {
                    @Override
                    public void run() {
                        LogUtils.d(TAG, "mCleanView run: ");
                    }
                });*/

        initSearchView();
        initSelectView();
        initEditView();

        switchLayout(mLayoutLocal);
        switchTopBar(mNavigationBar);
    }

    /*private void setCleanViewEnable(boolean enable) {
        mCleanView.setEnabled(enable);
        mCleanTv.setEnabled(enable);
        mCleanImg.setEnabled(enable);
    }*/

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (mService!=null&&(mService.isSearchTask(this.getClass().getName()) ||
                mService.isListTask(this.getClass().getName()))) {
            mService.cancel(this.getClass().getName());
        }
        if (mCurrentPath != null) {
            outState.putString(SAVED_PATH_KEY, mCurrentPath);
            outState.putString(SAVED_SEARCH_TEXT, mSearchText);
            outState.putLong(SAVED_SEARCH_TOTAL, mSearchTotalChanged);
        }

        if (mAdapter != null && mAdapter.getCheckedItemsCount() == 1) {
            FileInfo selectFileInfo = mAdapter.getCheckedFileInfoItemsList().get(0);
            if (selectFileInfo != null) {
                outState.putString(SAVED_SELECTED_PATH_KEY, selectFileInfo.getFileAbsolutePath());
            }
        }
        if (mFileInfoManager != null) {
            mFileListWhenOut.clear();
            mFileListWhenOut.addAll(mFileInfoManager.getShowFileList());
            outState.putParcelableArrayList(SAVED_FILE_LIST, mFileListWhenOut);
            LogUtils.e(TAG, "保存时mFileListWhenOut size=:" + mFileListWhenOut.size());

        }
        if (mAdapter != null) {
            outState.putInt(SAVED_ADAPTER_MODE, mAdapter.getMode());
            outState.putParcelableArrayList(SAVED_CHECKED_LIST, (ArrayList<? extends Parcelable>) mAdapter.getCheckedFileInfoItemsList());
        }

        outState.putParcelableArrayList(SAVED_SHOW_LIST, (ArrayList<? extends Parcelable>) mShowFileList);
        outState.putBoolean(SAVED_LIST_CHANGED, mListChanged);
        if (mOptionsDialog != null && mOptionsDialog.isShowing()) {
            outState.putBoolean(SAVED_SHOW_OPTION_DIALOG, true);
        } else {
            outState.putBoolean(SAVED_SHOW_OPTION_DIALOG, false);
        }
        if (mMoreDialog != null && mMoreDialog.isShowing()) {
            outState.putBoolean(SAVED_SHOW_MORE_DIALOG, true);
        } else {
            outState.putBoolean(SAVED_SHOW_MORE_DIALOG, false);
        }
        outState.putBoolean(SAVED_HAS_RESULT, hasResult);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStart() {
        LogUtils.e(TAG, "onStart()");
        if (!hasResult && !mOnActivityResult&&mAdapter != null) {
            if (/*mMountPointManager.isMounted(mCurrentPath)&&*/needReload()) {
                if (mAdapter.isMode(FileInfoAdapter.MODE_EDIT)||
                        mAdapter.isMode(FileInfoAdapter.MODE_SEARCH)) {
                    searchAndEditToNormal();
                }
                //有改动
                showDirectoryContent(mCurrentPath);
            }
        }
        mOnActivityResult=false;
        super.onStart();
    }

    @Override
    public void onResume() {
        LogUtils.e(TAG, "onResume: ");
        super.onResume();
        LogUtils.e(TAG, "after super");
//        reloadContent();
        /*if (mAdapter != null && mAdapter.isMode(FileInfoAdapter.MODE_NORMAL) &&
                !TextUtils.isEmpty(mCurrentPath)) {
            showDirectoryContent(mCurrentPath);
        }*/
       /* if (showRenameOnResume) {
            showRenameOnResume = false;
            showRenameDialog();
        }*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //与onresume重复了导致列表重复
        LogUtils.e(TAG, "onActivityResult开始执行");
        /*if (RESULT_OK == resultCode) {
            switch (requestCode) {
                case REQUEST_CODE_PASTE:
                    if (null != data) {
                        String dir = data.getStringExtra(EXTRA_PASTE_DIRECTORY);
                        LogUtils.e(TAG, "onActivityResult: dir=" + dir);
                        if (!TextUtils.isEmpty(dir) &&
                                (dir.equals(mCurrentPath) || new File(dir).getParentFile().getAbsolutePath().equals(mCurrentPath))) {
                            if (mEnterSearch) {
                                mListChanged = true;
                            } else {
                                showDirectoryContent(dir);
                            }
                        }
                    }
                    exitEditMode();
                    break;
                default:
                    break;
            }
        }*/
        if (REQUEST_CODE_PASTE == requestCode) {
            hasResult = false;
            mOnActivityResult = true;
            exitEditMode();
            if (RESULT_OK == resultCode) {
//                String dir = data == null ? null : data.getStringExtra(EXTRA_PASTE_DIRECTORY);
//                LogUtils.e(TAG,"dir=:"+dir);
//                if (!TextUtils.isEmpty(dir) &&
//                        (dir.equals(mCurrentPath) || new File(dir).getParentFile().getAbsolutePath().equals(mCurrentPath))) {
                if (mEnterSearch) {
//                    mListChanged = true;
                    searchAndEditToNormal();
                } /*else {*/
                showDirectoryContent(mCurrentPath);
//                }
//                }
            }
        } else if (REQUEST_CODE_BROWSE == requestCode/*&&mMountPointManager.isMounted(mCurrentPath)*/) {
            if (MountPointManager.getInstance().isRootPath(mCurrentPath)
                    || (mFileInfoManager != null && mFileInfoManager.isPathModified(mCurrentPath))) {
                //有修改
                showDirectoryContent(mCurrentPath);
            }
        }
        LogUtils.e(TAG, "onActivityResult结束");
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void exitEditMode() {
        if (mAdapter != null && mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
            mAdapter.changeMode(FileInfoAdapter.MODE_NORMAL);
            if (View.VISIBLE == mLayoutLocalSearchResult.getVisibility()) {
                mBottomBar.switchBar(null);
                ((HomeActivity) getActivity()).switchMode(MODE_SEARCH);
            } else {
                mBottomBar.switchBar(mBottomBarMenu);
                switchTopBar(mNavigationBar);
                ((HomeActivity) getActivity()).switchMode(MODE_NORMAL);
            }
        }
    }

    @Override
    public void onMounted(String mountPoint) {
        Message.obtain(mHandler, MSG_DO_MOUNTED, mountPoint).sendToTarget();
    }

    @TargetApi(24)
    @Override
    public void onUnMounted(StorageVolume volume) {
        String unMountPoint = volume.getPath();
        LogUtils.d(TAG, "onUnMounted,unMountPoint :" + unMountPoint);

        //make sure mCurrentPath be not null
        if (mCurrentPath == null) {
            mCurrentPath = initCurrentFileInfo();
        }

        if (mCurrentPath != null
                && (mCurrentPath.startsWith(unMountPoint)
                || (mMountPointManager != null && mMountPointManager.isRootPath(mCurrentPath)))) {
            ProgressDialogFragment pf = (ProgressDialogFragment) getFragmentManager()
                    .findFragmentByTag(HeavyOperationListener.HEAVY_DIALOG_TAG);
            if (pf != null) {
                pf.dismissAllowingStateLoss();
            }

            // sort dialog
            ChoiceDialogFragment sortDialogFragment = (ChoiceDialogFragment) getFragmentManager()
                    .findFragmentByTag(ChoiceDialogFragment.CHOICE_DIALOG_TAG);
            if (null != sortDialogFragment) {
                sortDialogFragment.dismissAllowingStateLoss();
            }

            // rename dialog
            EditTextDialogFragment renameDialogFragment = (EditTextDialogFragment) getFragmentManager()
                    .findFragmentByTag(RENAME_DIALOG_TAG);
            if (renameDialogFragment != null) {
                renameDialogFragment.dismissAllowingStateLoss();
            }

            AlertDialogFragment af;

            // restore delete dialog
            af = (AlertDialogFragment) getFragmentManager().findFragmentByTag(DELETE_DIALOG_TAG);
            if (af != null) {
                af.dismissAllowingStateLoss();
            }

            // Restore the detail_dialog
            af = (AlertDialogFragment) getFragmentManager().findFragmentByTag(
                    DetailInfoListener.DETAIL_DIALOG_TAG);
            if (af != null) {
                af.dismissAllowingStateLoss();
            }

        }

        Message.obtain(mHandler, MSG_DO_UNMOUNTED, volume).sendToTarget();
    }

    @Override
    public void onEjected(String unMountPoint) {
        Message.obtain(mHandler, MSG_DO_EJECTED, unMountPoint).sendToTarget();
    }

    @Override
    public void onSdSwap() {
        Message.obtain(mHandler, MSG_DO_SDSWAP).sendToTarget();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        LogUtils.d(TAG, "onItemClick: position=" + position + ", mode=" + mAdapter.getMode());
        if (mService != null && mService.isBusy(this.getClass().getName())) {
            LogUtils.d(TAG, "onItemClick, service is busy,return. ");
            return;
        }
        if (null != mAdapter) {
            if (position >= mAdapter.getCount() || position < 0) {
                LogUtils.e(TAG, "onItemClick, events error, mFileInfoList.size()= "
                        + mAdapter.getCount());
                return;
            }
            if (mAdapter.isMode(FileInfoAdapter.MODE_NORMAL)
                    || mAdapter.isMode(FileInfoAdapter.MODE_SEARCH)) {

               /* if (MODE_SEARCH == mHomeActivity.getMode()) {
                    mBottomBarMenu.setVisibility(View.VISIBLE );
                    mHomeActivity.switchMode(MODE_NORMAL);
                    switchTopBar(mNavigationBar);
                    showDirectoryContent(mCurrentPath);
                }*/

                FileInfo selectedItem = mAdapter.getItem(position);

                if (null != selectedItem) {

                    if (selectedItem.isDirectory()) {
                        switchTopBar(mNavigationBar);
                        ((HomeActivity) getActivity()).switchMode(MODE_NORMAL);
                        int top = view.getTop();
                        mPosition = mAdapter.getPosition(selectedItem);
                        LogUtils.e(TAG, "***********mPosition=:" + mPosition);
                        mPositionTopList[0] = mPosition;
                        mPositionTopList[1] = top;
                        if (mCurrentPath != null) {
                            mPositionMap.put(mCurrentPath, mPositionTopList);
                        }
                        //addToNavigationList(mCurrentPath, selectedItem, top);
                        String path = selectedItem.getFileAbsolutePath();
                        LogUtils.v(TAG, "onItemClick, fromTop = " + top + ", path=" + path);

                        if (!TextUtils.isEmpty(path)) {
                            LogUtils.e(TAG, "start show Directory Content");
                            showDirectoryContent(path);
                        }
                    } else {
                        LogUtils.d(TAG, "onItemClick: ModifiedTime=" + FileUtils.formatModifiedTime(getActivity(), selectedItem.getFileLastModifiedTime()));
                        mItemClickPath = selectedItem.getFileAbsolutePath();
                        mItemClickParentPath = selectedItem.getFileParentPath();
                        mItemClickFileName = selectedItem.getFileName();
                        if (!TextUtils.isEmpty(mItemClickPath)) {
                            if (mItemClickPath.endsWith(".zip")) {
                                if (!PermissionUtils.hasStorageWritePermission(getActivity())) {
                                    PermissionUtils.requestPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                            MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_ZIP);
                                    return;
                                }
                                openZipFile(mItemClickPath, mItemClickParentPath, mItemClickFileName.substring(0, mItemClickFileName.lastIndexOf(".")), ZIP);
                            } else if (mItemClickPath.endsWith(".rar")) {
                                if (!PermissionUtils.hasStorageWritePermission(getActivity())) {
                                    PermissionUtils.requestPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                            MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_RAR);
                                    return;
                                }
                                openZipFile(mItemClickPath, mItemClickParentPath, mItemClickFileName.substring(0, mItemClickFileName.lastIndexOf(".")), RAR);
                            } else {
                                openFile(selectedItem);
                            }
                        }
                        // open file here
//                        openFile(selectedItem);
                    }
                } else {
                    LogUtils.e(TAG, "onItemClick: null == selectedItem ");
                }
            } else if (mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
                boolean state = mAdapter.getItem(position).isChecked();
                LogUtils.d(TAG, "onItemClick, edit view . position=" + position + ", state=" + state);
                mAdapter.setChecked(position, !state);
                mAdapter.notifyDataSetChanged();
                mTvSelectChosen.setText(getResources().getString(
                        R.string.select_chosen, mAdapter.getCheckedItemsCount()));
                boolean isSelectedAll = mAdapter.getCheckedItemsCount() == mAdapter.getCount();
                mTvSelectAll.setText(isSelectedAll ? R.string.select_not_all : R.string.select_all);
                handleEditView();
            }
        }

    }

    /**
     * 打开zip或rar列表
     *
     * @param path
     * @param fileName
     */
    private void openZipFile(String path, String parentPath, String fileName, OpenZipsActivity.FileType type) {

        if (path != null) {
            Intent intent = new Intent(getActivity(), OpenZipsActivity.class);
            intent.putExtra(EXTRA_ZIPS_PATH, path);
            intent.putExtra(EXTRA_ZIPS_NAME, fileName);
            intent.putExtra(EXTRA_ZIPS_TYPE, type);
            intent.putExtra(EXTRA_ZIPS_PARENT, parentPath);
            startActivity(intent);
        }

    }

    private void openFile(FileInfo selectedItem) {
        if (null == selectedItem) {
            return;
        }
        boolean canOpen = true;
        String mimeType = selectedItem.getFileMimeType(mService);
        LogUtils.e(TAG, "mimeType=:" + mimeType);
        if (selectedItem.isDrmFile()) {
            mimeType = DrmManager.getInstance().getOriginalMimeType(
                    selectedItem.getFileAbsolutePath());

            if (TextUtils.isEmpty(mimeType)) {
                canOpen = false;
                mToastHelper.showToast(R.string.msg_unable_open_file);
            }
        }
        /*String fileName = selectedItem.getShowName();
        if (fileName.endsWith(".zip")) {
            zipTest(selectedItem.getFileAbsolutePath());
        } else if (fileName.endsWith(".rar")) {
            rarTest(selectedItem.getFileAbsolutePath());
        }*/

        if (canOpen) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = selectedItem.getUri();
            LogUtils.d(TAG, "openFile, Open uri file: " + uri);
            if (mimeType != null && mimeType.startsWith("audio/")) {
                intent.setDataAndType(uri, "audio/mp3");
            } else {
                intent.setDataAndType(uri, mimeType);
            }

            try {
                startActivityForResult(intent, REQUEST_CODE_BROWSE);
            } catch (android.content.ActivityNotFoundException e) {
                //mToastHelper.showToast(R.string.msg_unable_open_file);
                showOpenMethodDialog(selectedItem);
                LogUtils.w(TAG, "openFile, Cannot open file: "
                        + selectedItem.getFileAbsolutePath());
            }
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        LogUtils.d(TAG, "onItemLongClick: position=" + position);
        if (mAdapter.isMode(FileInfoAdapter.MODE_NORMAL)
                || mAdapter.isMode(FileInfoAdapter.MODE_SEARCH)) {
            if (!mMountPointManager.isRootPath(mCurrentPath)
                    && !mService.isBusy(this.getClass().getName())) {
                int top = view.getTop();
                switchToEditView(position, top);
                return true;
            }
        }
        return false;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        if (null != getActivity()) {
            if (null != mMountReceiver) {
                mMountReceiver.unregisterMountListener(this);
                getActivity().unregisterReceiver(mMountReceiver);
                mMountReceiver = null;
                LogUtils.d(TAG, "onDetach: unregister mountListener");
            }

            if (null != mService) {
                if (mServiceBinded) {
                    getActivity().unbindService(mServiceConnection);
                    mServiceBinded = false;
                    LogUtils.d(TAG, "onDetach: unbind Service");
                }
            }
            if (mPermissionReceiver != null) {
                getActivity().unregisterReceiver(mPermissionReceiver);
                mPermissionReceiver = null;
            }
            DrmManager.getInstance().release();
//            mHomeActivity = null;
        }
    }

    protected void serviceConnected() {
        LogUtils.e(TAG, "serviceConnected: ");

        mFileInfoManager = mService.initFileInfoManager(this.getClass().getName());
        LogUtils.e(TAG, "listsize=:" + mFileInfoManager.getShowFileList().size());
        mService.setListType(getPrefsShowHiddenFile() ? FileManagerService.FILE_FILTER_TYPE_ALL
                : FileManagerService.FILE_FILTER_TYPE_DEFAULT, this.getClass().getName());
        if (null != getActivity()) {
            mAdapter = new FileInfoAdapter(getActivity(), mService, mFileInfoManager);
            if (mSearchInput == null) {
                mSearchInput = ((HomeActivity) getActivity()).getEtSearchInput();
            }
            mSearchInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (getActivity() != null && ((HomeActivity) getActivity()).getMode() == MODE_SEARCH) {
                        requestSearch(s.toString());
                    }
                }
            });
            if (null != mLstMedia) {
                mLstMedia.setAdapter(mAdapter);
                /*Intent intent = mHomeActivity.getIntent();
                String dir = intent.getStringExtra(EXTRA_LOCAL_DIRECTORY);
                intent.putExtra(EXTRA_LOCAL_DIRECTORY, "");
                if (!TextUtils.isEmpty(dir)) {
                    showDirectoryContent(dir);
                } else {*/
                if (null == mSavedInstanceState) {
                    mCurrentPath = initCurrentFileInfo();
                    if (mCurrentPath != null) {
                        showDirectoryContent(mCurrentPath);
                    }
                } else {
                    hasResult = mSavedInstanceState.getBoolean(SAVED_HAS_RESULT);
                    mSearchText = mSavedInstanceState.getString(SAVED_SEARCH_TEXT);
                    mFileListWhenOut = mSavedInstanceState.getParcelableArrayList(SAVED_FILE_LIST);
                    mShowFileList = mSavedInstanceState.getParcelableArrayList(SAVED_SHOW_LIST);
                    mListChanged = mSavedInstanceState.getBoolean(SAVED_LIST_CHANGED);
                    int adapterMode = mSavedInstanceState.getInt(SAVED_ADAPTER_MODE);
                    mAdapter.changeMode(adapterMode);
//                    if (!TextUtils.isEmpty(mSearchText)) {
//                        mSearchTotalChanged = mSavedInstanceState.getLong(SAVED_SEARCH_TOTAL);
//                        switchTopBar(mLayoutLocalSearchResult);
//                        mTvSearchResult.setText(getResources().getString(
//                                R.string.search_result, mSearchTotalChanged));
//                    }
                    String savePath = mSavedInstanceState.getString(SAVED_PATH_KEY);
                    if (savePath != null
                            && mMountPointManager.isMounted(mMountPointManager
                            .getRealMountPointPath(savePath))) {
                        mCurrentPath = savePath;
                    } else {
                        mCurrentPath = initCurrentFileInfo();
                    }

                    if (mCurrentPath != null) {
                        if (!TextUtils.isEmpty(mSearchText)) {
                            mSearchTotalChanged = mSavedInstanceState.getLong(SAVED_SEARCH_TOTAL);
                            mTvSearchResult.setText(getResources().getString(
                                    R.string.search_result, mSearchTotalChanged));
//                            LogUtils.e(TAG, "mHomeActivity mode=:" + mHomeActivity.getMode());
                            mSearchInput.setText(mSearchText);
                            LogUtils.e(TAG, "settext");
                            mSearchInput.setSelection(mSearchText.length());
                            switchTopBar(mLayoutLocalSearchResult);
                            if (mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
                                //搜索下的编辑模式
                                List<FileInfo> checkedList = mSavedInstanceState.getParcelableArrayList(SAVED_CHECKED_LIST);
                                if (mFileListWhenOut.size() > 0 && checkedList != null) {
                                    mFileInfoManager.getShowFileList().clear();
                                    mFileInfoManager.getShowFileList().addAll(mFileListWhenOut);
                                    for (FileInfo info : checkedList) {
                                        info.setChecked(true);
                                    }
                                    mAdapter.notifyDataSetChanged();
                                    mBottomBar.switchBar(mBottomBarEdit);
                                    ((HomeActivity) getActivity()).switchMode(MODE_SELECT);
                                    switchToEditView();
                                    handleEditView();
                                    if (hasResult) {
                                        hasResult = false;
                                        exitEditMode();
                                        searchAndEditToNormal();
                                        showDirectoryContent(mCurrentPath);
//                                        mListChanged = true;
                                    } else if (needReload()) {
                                        searchAndEditToNormal();
                                        showDirectoryContent(mCurrentPath);
                                    }
                                } else {
                                    searchAndEditToNormal();
                                    showDirectoryContent(mCurrentPath);
//                                    reloadContent();
                                }
                            } else {
                                //正常搜索模式
                                ((HomeActivity) getActivity()).switchMode(MODE_SEARCH);
                                mBottomBar.switchBar(null);
                                if (mFileListWhenOut.size() > 0) {
                                    mFileInfoManager.getShowFileList().clear();
                                    mFileInfoManager.getShowFileList().addAll(mFileListWhenOut);
                                    mAdapter.notifyDataSetChanged();
                                    if (0 == mSearchTotalChanged) {
                                        switchLayout(mLayoutNoMatchingSearch);
                                    } else {
                                        switchLayout(mLayoutLocal);
                                    }
                                } else {
                                    requestSearch(mSearchText);
                                }
                                if (needReload()) {
                                    mListChanged = true;
                                }
                            }
                        } else {
                            mTabManager.refreshTab(mCurrentPath);
                            switchTopBar(mNavigationBar);
                            if (mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
                                //正常编辑模式
                                List<FileInfo> checkedList = mSavedInstanceState.getParcelableArrayList(SAVED_CHECKED_LIST);
                                if (mFileListWhenOut.size() > 0 && checkedList != null) {
                                    mFileInfoManager.getShowFileList().clear();
                                    mFileInfoManager.getShowFileList().addAll(mFileListWhenOut);
                                    for (FileInfo info : checkedList) {
                                        info.setChecked(true);
                                    }
                                    mAdapter.notifyDataSetChanged();
                                    mBottomBar.switchBar(mBottomBarEdit);
                                    ((HomeActivity) getActivity()).switchMode(MODE_SELECT);
                                    switchToEditView();
                                    handleEditView();
                                    if (hasResult) {
                                        hasResult = false;
                                        exitEditMode();
                                        showDirectoryContent(mCurrentPath);
                                    } else if (needReload()) {
                                        exitEditMode();
                                        showDirectoryContent(mCurrentPath);
                                    }
                                } else {
                                    exitEditMode();
                                    showDirectoryContent(mCurrentPath);
//                                    reloadContent();
                                }
                            } else {
                                //正常模式
                                reloadContent();
                            }
                        }
                    }
                    restoreDialog();
                }

                mAdapter.notifyDataSetChanged();
            }
            // register Receiver when service connected..
            if (mMountReceiver == null) {
                mMountReceiver = MountReceiver.registerMountReceiver(getActivity());
                mMountReceiver.registerMountListener(this);
            }
        }

    }

    /**
     * 判断本路径是否有修改需要重新请求数据
     *
     * @return
     */
    private boolean needReload() {
        if ((mCurrentPath != null && MountPointManager.getInstance().isRootPath(mCurrentPath))
                || (mFileInfoManager != null && mFileInfoManager.isPathModified(mCurrentPath))) {
            return true;
        }
        return false;
    }

    private void reloadContent() {
        LogUtils.d(TAG, "reloadContent");
        if (mService != null && !mService.isBusy(this.getClass().getName())) {
            if (MountPointManager.getInstance().isRootPath(mCurrentPath)
                    || (mFileInfoManager != null && mFileInfoManager.isPathModified(mCurrentPath))) {
                showDirectoryContent(mCurrentPath);
            } else if (mFileInfoManager != null && mAdapter != null) {
                LogUtils.e(TAG, "没有改动");
                boolean isRoot = mMountPointManager.isRootPath(mCurrentPath);
                mBottomBar.switchBar(isRoot ? null : mBottomBarMenu);
                ViewGroup.LayoutParams lp = mNavigationBar.getLayoutParams();
                lp.height = isRoot ? 0 : getResources().getDimensionPixelOffset(R.dimen.navigation_bar_height);
                mAdapter.notifyDataSetChanged();
                mLayoutEmptyView.setVisibility((0 == mAdapter.getCount()) ? View.VISIBLE : View.GONE);
            }
        }
    }

    protected void restoreDialog() {
        // Restore the heavy_dialog : pasting deleting
        ProgressDialogFragment pf = (ProgressDialogFragment) getFragmentManager()
                .findFragmentByTag(HeavyOperationListener.HEAVY_DIALOG_TAG);
        if (pf != null) {
            LogUtils.e(TAG, "****ProgressDialog show****");
            if (mService.isBusy(this.getClass().getName())
                    && mService.isHeavyOperationTask(this.getClass().getName())) {
                HeavyOperationListener listener = new HeavyOperationListener(
                        AlertDialogFragment.INVIND_RES_ID, getActivity(), mHeavyOperationListenerCallback);
                mService.reconnected(this.getClass().getName(), listener);
                pf.setCancelListener(listener);
            } else {
                pf.dismissAllowingStateLoss();
            }
        }

        // list dialog
        DialogFragment listFramgent = (DialogFragment) getFragmentManager().findFragmentByTag(
                ListListener.LIST_DIALOG_TAG);
        if (listFramgent != null) {
            LogUtils.e(TAG, "listFramgent != null");
            if (mService.isBusy(this.getClass().getName())) {
                LogUtils.i(TAG, "list reconnected mService");
                mService.reconnected(this.getClass().getName(), new ListListener());
            } else {
                LogUtils.i(TAG, "the list is complete dismissAllowingStateLoss");
                listFramgent.dismissAllowingStateLoss();
            }
        }

        // sort dialog
        ChoiceDialogFragment sortDialogFragment = (ChoiceDialogFragment) getFragmentManager()
                .findFragmentByTag(ChoiceDialogFragment.CHOICE_DIALOG_TAG);
        if (sortDialogFragment != null) {
            sortDialogFragment.setItemClickListener(new SortClickListener());
        }

        // create new folder dialog
        EditTextDialogFragment createFolderDialogFragment = (EditTextDialogFragment) getFragmentManager()
                .findFragmentByTag(CREATE_FOLDER_DIALOG_TAG);
        if (createFolderDialogFragment != null) {
            createFolderDialogFragment.setOnEditTextDoneListener(new CreateFolderListener());
            createFolderDialogFragment.restoreOkButtonState();
        }

        String saveSelectedPath = mSavedInstanceState.getString(SAVED_SELECTED_PATH_KEY);
        LogUtils.e(TAG, "***saveSelectedPath***");
        FileInfo saveSelectedFile = null;
        if (saveSelectedPath != null) {
            saveSelectedFile = new FileInfo(saveSelectedPath);
        }

        // rename dialog
        EditTextDialogFragment renameDialogFragment = (EditTextDialogFragment) getFragmentManager()
                .findFragmentByTag(RENAME_DIALOG_TAG);
        if (renameDialogFragment != null && saveSelectedFile != null) {
            renameDialogFragment
                    .setOnEditTextDoneListener(new RenameDoneListener(saveSelectedFile));
        }

        AlertDialogFragment af;
        // delete dialog
        af = (AlertDialogFragment) getFragmentManager().findFragmentByTag(DELETE_DIALOG_TAG);
        if (af != null) {
            af.setOnDoneListener(new DeleteListener());
        }
        if (mSavedInstanceState != null && mSavedInstanceState.getBoolean(SAVED_SHOW_MORE_DIALOG)) {
            showMoreDialog();
        }
        if (mSavedInstanceState != null && mSavedInstanceState.getBoolean(SAVED_SHOW_OPTION_DIALOG)) {
            showOptionDialog();
        }
        /*// Restore the detail_dialog
        af = (AlertDialogFragment) getFragmentManager().findFragmentByTag(
                DetailInfoListener.DETAIL_DIALOG_TAG);
        if (af != null && saveSelectedFile != null && mService != null) {
            LogUtils.e(TAG,"show  detail_dialog ***");
            DetailInfoListener listener = new DetailInfoListener(saveSelectedFile);
            af.setDismissListener(listener);
            if (mService.isBusy(this.getClass().getName()) && mService.isDetailTask(this.getClass().getName())) {
                mService.reconnected(this.getClass().getName(), listener);
            } else if (!mService.isBusy(this.getClass().getName())) {
                af.dismissAllowingStateLoss();
                mService.getDetailInfo(this.getClass().getName(), saveSelectedFile, listener);
            } else {
                af.dismissAllowingStateLoss();
            }
        } else if (af != null && saveSelectedFile == null) {
            af.dismissAllowingStateLoss();
            mIsAlertDialogShowing = false;
        }*/
    }

    protected void showDirectoryContent(String path) {
        LogUtils.d(TAG, "showDirectoryContent, path = " + path);
        if ((null != getActivity()) && getActivity().isFinishing()) {
            LogUtils.i(TAG, "showDirectoryContent, isFinishing: true, do not loading again");
            return;
        }
        mCurrentPath = path;
        if (!TextUtils.isEmpty(mCurrentPath)) {
            boolean isRoot = mMountPointManager.isRootPath(mCurrentPath);
            mBottomBar.switchBar(isRoot ? null : mBottomBarMenu);
            ViewGroup.LayoutParams lp = mNavigationBar.getLayoutParams();
            lp.height = isRoot ? 0 : getResources().getDimensionPixelOffset(R.dimen.navigation_bar_height);
        }

        if (mService != null) {
            mService.listFiles(this.getClass().getName(), mCurrentPath, new ListListener());
        }
    }

    protected boolean getPrefsShowHiddenFile() {
        return SharedPrefUtils.getInstance(ConstUtils.FILE_EXPLORER_PREF)
                .getBoolean(ConstUtils.PREF_SHOW_HIDDEN_FILE, false);
    }

    private int getPrefsSortBy() {
        return SharedPrefUtils.getInstance(ConstUtils.FILE_EXPLORER_PREF)
                .getInt(ConstUtils.PREF_SORT_BY, FileInfoComparator.SORT_BY_TYPE);
    }

    private void setPrefsSortBy(int sort) {
        mSortType = sort;
        SharedPrefUtils.getInstance(ConstUtils.FILE_EXPLORER_PREF)
                .putInt(ConstUtils.PREF_SORT_BY, mSortType);
    }

    protected String initCurrentFileInfo() {
        if (null != mMountPointManager) {
            return mMountPointManager.getRootPath();
        } else {
            return null;
        }
    }

    private int restoreSelectedPosition() {
        if (mSelectedFileInfo == null) {
            return -1;
        } else {
            int curSelectedItemPosition = mAdapter.getPosition(mSelectedFileInfo);
            mSelectedFileInfo = null;
            return curSelectedItemPosition;
        }
    }

    protected void onPathChanged() {
        if ((null != mTabManager) && (null != getActivity())) {
            mTabManager.refreshTab(mCurrentPath);
        }
    }

    /**
     * 从编辑或搜索模式切换到正常模式
     * 不论是否有搜索
     * 数据需要重新获取
     */
    private void searchAndEditToNormal() {
//        if (mAdapter != null && mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
        mAdapter.changeMode(FileInfoAdapter.MODE_NORMAL);
        mBottomBar.switchBar(mBottomBarMenu);
        switchTopBar(mNavigationBar);
        switchLayout(mLayoutLocal);
        ((HomeActivity) getActivity()).switchMode(MODE_NORMAL);
        mSearchText = null;
        mEnterSearch = false;
//        }
    }

    @Override
    public boolean onBackPressed() {

        if (mAdapter != null && mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
            mAdapter.changeMode(FileInfoAdapter.MODE_NORMAL);
            if (View.VISIBLE == mLayoutLocalSearchResult.getVisibility()) {
                mBottomBar.switchBar(null);
                ((HomeActivity) getActivity()).switchMode(MODE_SEARCH);
            } else {
                mBottomBar.switchBar(mBottomBarMenu);
                switchTopBar(mNavigationBar);
                ((HomeActivity) getActivity()).switchMode(MODE_NORMAL);
            }
            return true;
        }

        if (MODE_SEARCH == ((HomeActivity) getActivity()).getMode()) {
            mAdapter.changeMode(FileInfoAdapter.MODE_NORMAL);
            ((HomeActivity) getActivity()).switchMode(MODE_NORMAL);
            switchLayout(mLayoutLocal);
            switchTopBar(mNavigationBar);
            if (mListChanged) {
                showDirectoryContent(mCurrentPath);
                mListChanged = false;
            } else {
                if (mShowFileList.size() > 0) {
                    mFileInfoManager.getShowFileList().clear();
                    mFileInfoManager.getShowFileList().addAll(mShowFileList);
                    mAdapter.notifyDataSetChanged();
                    mTabManager.refreshTab(mCurrentPath);
                } else {
                    showDirectoryContent(mCurrentPath);
                }
            }
            mBottomBarMenu.setVisibility(View.VISIBLE);
            mSearchText = null;
            mEnterSearch = false;
            return true;
        }

        if (mService != null && mService.isBusy(this.getClass().getName())) {
            LogUtils.d(TAG, "onBackPressed: service is busy");
            return true;
        }

        if (!TextUtils.isEmpty(mCurrentPath)
                && !mMountPointManager.isRootPath(mCurrentPath)) {
            mScrollBack = true;
            String path = FileUtils.getFilePath(mCurrentPath);
            LogUtils.d(TAG, "onBackPressed: path=" + path + ", mCurrentPath=" + mCurrentPath);
            if (mMountPointManager.isMountPoint(mCurrentPath)) {
                path = mMountPointManager.getRootPath();
                LogUtils.d(TAG, "onBackPressed: RootPath=" + path);
            }
            showDirectoryContent(path);
            return true;
        }

        return false;
    }

    /**
     * This method add a path into navigation history list
     */
    protected void addToNavigationList(String path, FileInfo selectedFileInfo, int top) {
        mFileInfoManager.addToNavigationList(new FileInfoManager.NavigationRecord(path, selectedFileInfo, top));
    }

    /**
     * This method clear navigation history list
     */
    protected void clearNavigationList() {
        if (mFileInfoManager != null) {
            mFileInfoManager.clearNavigationList();
        }
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            LogUtils.d(TAG, "handleMessage, msg = " + msg.what);
            switch (msg.what) {
                case MSG_DO_MOUNTED:
                    doOnMounted((String) msg.obj);
                    break;
                case MSG_DO_EJECTED:
                    doOnEjected((String) msg.obj);
                    break;
                case MSG_DO_UNMOUNTED:
                    doOnUnMounted((StorageVolume) msg.obj);
                    break;
                case MSG_DO_SDSWAP:
                    doOnSdSwap();
                    break;
                default:
                    break;
            }
        }
    };

    private void doOnMounted(String mountPoint) {
        LogUtils.i(TAG, "doOnMounted, mountPoint = " + mountPoint);
        doPrepareForMount(mountPoint);
        if (mMountPointManager.isRootPath(mCurrentPath)) {
            LogUtils.d(TAG, "doOnMounted, mCurrentPath is root path: " + mCurrentPath);
            showDirectoryContent(mCurrentPath);
        }
    }

    private void doOnEjected(String unMountPoint) {
        if ((mCurrentPath + MountPointManager.SEPARATOR).startsWith(unMountPoint + MountPointManager.SEPARATOR)
                || mMountPointManager.isRootPath(mCurrentPath)
                || mMountPointManager.isPrimaryVolume(unMountPoint)) {
            LogUtils.d(TAG, "onEjected, Current Path = " + mCurrentPath);
            if (mService != null && mService.isBusy(this.getClass().getName())) {
                mService.cancel(this.getClass().getName());
            }
        }
    }

    private void doPrepareForMount(String mountPoint) {
        LogUtils.i(TAG, "doPrepareForMount, mountPoint = " + mountPoint);
        if ((mCurrentPath + MountPointManager.SEPARATOR).startsWith(mountPoint + MountPointManager.SEPARATOR)
                || mMountPointManager.isRootPath(mCurrentPath)) {
            LogUtils.d(TAG, "pre-onMounted");
            if (mService != null && mService.isBusy(this.getClass().getName())) {
                mService.cancel(this.getClass().getName());
            }
        }

        if (null != getActivity()) {
            mMountPointManager.init(getActivity().getApplicationContext());
        }
    }

    @TargetApi(24)
    private void doOnUnMounted(StorageVolume volume) {
        String unMountPoint = volume.getPath();
        if (mFileInfoManager != null) {
            int pasteCnt = mFileInfoManager.getPasteCount();
            LogUtils.i(TAG, "doOnUnmounted, unMountPoint: " + unMountPoint + ",pasteCnt = "
                    + pasteCnt);

            if (pasteCnt > 0) {
                FileInfo fileInfo = mFileInfoManager.getPasteList().get(0);
                if (fileInfo.getFileAbsolutePath().startsWith(
                        unMountPoint + MountPointManager.SEPARATOR)) {
                    LogUtils.i(TAG, "doOnUnmounted, clear paste list. ");
                    mFileInfoManager.clearPasteList();
                }
            }
        }

        if ((mCurrentPath + MountPointManager.SEPARATOR).startsWith(unMountPoint
                + MountPointManager.SEPARATOR)
                || mMountPointManager.isRootPath(mCurrentPath)) {
            LogUtils.d(TAG, "onUnmounted, Current Path = " + mCurrentPath);
            if (mService != null && mService.isBusy(this.getClass().getName())) {
                mService.cancel(this.getClass().getName());
            }
            showToastForUnmount(volume);

            DialogFragment listFragment = (DialogFragment) getFragmentManager()
                    .findFragmentByTag(ListListener.LIST_DIALOG_TAG);
            if (listFragment != null) {
                LogUtils.d(TAG, "onUnmounted, listFragment dismiss. ");
                listFragment.dismissAllowingStateLoss();
            }

            AlertDialogFragment.EditTextDialogFragment createFolderDialogFragment =
                    (AlertDialogFragment.EditTextDialogFragment) getFragmentManager()
                            .findFragmentByTag(CREATE_FOLDER_DIALOG_TAG);
            if (createFolderDialogFragment != null) {
                LogUtils.d(TAG, "onUnmounted, createFolderDialogFragment dismiss. ");
                createFolderDialogFragment.dismissAllowingStateLoss();
            }

            backToRootPath();
        }
    }

    private void doOnSdSwap() {
        if (null != getActivity()) {
            mMountPointManager.init(getActivity().getApplicationContext());
        }
        backToRootPath();
    }

    @TargetApi(24)
    private void showToastForUnmount(StorageVolume volume) {
        LogUtils.d(TAG, "showToastForUnmount, path = " + volume.getPath());
        if (isResumed()) {
            String unMountPointDescription = volume.getDescription(getActivity());
            LogUtils.d(TAG, "showToastForUnmount, unMountPointDescription:"
                    + unMountPointDescription);
            mToastHelper.showToast(getString(R.string.unmounted, unMountPointDescription));
        }
    }

    private void backToRootPath() {
        LogUtils.d(TAG, "backToRootPath...");
        if (mMountPointManager != null && mMountPointManager.isRootPath(mCurrentPath)) {
            showDirectoryContent(mCurrentPath);
        } else if (mTabManager != null) {
            mTabManager.updateNavigationBar(0);
        }
        clearNavigationList();
    }

    private void switchToEditView(int position, int top) {
        LogUtils.d(TAG, "switchToEditView: position=" + position + ", top=" + top);
        mBottomBar.switchBar(mBottomBarEdit);
        ((HomeActivity) getActivity()).switchMode(MODE_SELECT);
//        mAdapter.clearChecked();
        mAdapter.setChecked(position, true);
        mLstMedia.setSelectionFromTop(position, top);
        switchToEditView();
    }

    private void switchToEditView() {
        LogUtils.d(TAG, "Switch to edit view");
        mLstMedia.setFastScrollEnabled(false);
        mAdapter.changeMode(FileInfoAdapter.MODE_EDIT);
        mTvSelectChosen.setText(getResources().getString(
                R.string.select_chosen, mAdapter.getCheckedItemsCount()));
        boolean isSelectedAll = mAdapter.getCheckedItemsCount() == mAdapter.getCount();
        mTvSelectAll.setText(isSelectedAll ? R.string.select_not_all : R.string.select_all);
        handleEditView();
    }

    @Override
    public void onClick(View v) {
        if (mService.isBusy(this.getClass().getName())) {
            LogUtils.d(TAG, "onClick(), service is busy.");
            return;
        }

        int id = v.getId();
        LogUtils.d(TAG, "onClick() id=" + id);
        mScrollBack = true;
        mTabManager.updateNavigationBar(id);
    }

    private void initSearchView() {
        ImageView back = ((HomeActivity) getActivity()).getImgSearchBack();
        setViewClickListener(back,
                new Runnable() {
                    @Override
                    public void run() {
                        LogUtils.d(TAG, "ImgSearchBack run: ");
                        //mHomeActivity.switchMode(MODE_NORMAL);
                        KeyboardUtils.hideSoftInput(getActivity());
                        //switchLayout(mLayoutLocal);
                        onBackPressed();
                    }
                });


        if (mSearchInput == null) {
            mSearchInput = ((HomeActivity) getActivity()).getEtSearchInput();
        }
        RxTextView.editorActions(mSearchInput)
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer actionId) {
                        LogUtils.d(TAG, "searchInput call: actionId=" + actionId
                                + ", IME_ACTION_SEARCH=" + EditorInfo.IME_ACTION_SEARCH);
                        if (EditorInfo.IME_ACTION_SEARCH == actionId) {
                            KeyboardUtils.hideSoftInput(getActivity());
                            Editable search = mSearchInput.getText();
                            if (null != search) {
                                requestSearch(search.toString());
                            }
                        }
                    }
                });

        ImageView search = ((HomeActivity) getActivity()).getImgSearch();

        setViewClickListener(search,
                new Runnable() {
                    @Override
                    public void run() {
                        LogUtils.d(TAG, "ImgSearch run: ");
                        Editable search = mSearchInput.getText();
                        if (null != search) {
                            requestSearch(search.toString());
                        }
                    }
                });
    }

    private void initSelectView() {
        mTvSelectAll = ((HomeActivity) getActivity()).getTvSelectAll();
        mTvSelectChosen = ((HomeActivity) getActivity()).getTvSelectChosen();
        mTvSelectCancel = ((HomeActivity) getActivity()).getTvSelectCancel();

        setViewClickListener(mTvSelectAll,
                new Runnable() {
                    @Override
                    public void run() {
                        if (null != mAdapter) {
                            boolean isSelectedAll = mAdapter.getCheckedItemsCount() == mAdapter.getCount();
                            mAdapter.setAllItemChecked(!isSelectedAll);

                            mTvSelectChosen.setText(
                                    getResources().getString(R.string.select_chosen, mAdapter.getCheckedItemsCount()));
                            mTvSelectAll.setText(isSelectedAll ? R.string.select_all : R.string.select_not_all);

                            handleEditView();
                        }
                    }
                });

        setViewClickListener(mTvSelectCancel,
                new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                    }
                });
    }

    private void initEditView() {
        handleEditView();

        setViewClickListener(mTvShare,
                new Runnable() {
                    @Override
                    public void run() {
                        if (null != mAdapter) {
                            FileInfo fileInfo = mAdapter.getFirstCheckedFileInfoItem();
                            if (null != fileInfo) {
                                boolean isSharingIntentStarted =
                                        IntentUtils.share(getActivity(), fileInfo.getFileAbsolutePath());
                                if (!isSharingIntentStarted) {
                                    mToastHelper.showToast(R.string.no_method);
                                }
                            }
                        }
                    }
                });

        setViewClickListener(mTvDelete,
                new Runnable() {
                    @Override
                    public void run() {
                        if (!PermissionUtils.hasStorageWritePermission(getActivity())) {
                            PermissionUtils.requestPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_DELETE);
                            return;
                        }
                        showDeleteDialog();
                    }
                });

        setViewClickListener(mTvCut,
                new Runnable() {
                    @Override
                    public void run() {
                        if (!PermissionUtils.hasStorageWritePermission(getActivity())) {
                            PermissionUtils.requestPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_CUT);
                            return;
                        }
                        Intent intent = new Intent(getActivity(), DstActivity.class);
                        intent.putExtra(DstActivity.EXTRA_TYPE, FileInfoManager.PASTE_MODE_CUT);
                        intent.putExtra(DstActivity.EXTRA_LIST, (Serializable) mAdapter.getCheckedFileInfoItemsList());
                        startActivityForResult(intent, REQUEST_CODE_PASTE);
                        hasResult = true;
                    }
                });

        setViewClickListener(mTvMore,
                new Runnable() {
                    @Override
                    public void run() {
                        showMoreDialog();
                    }
                });
    }

    private void handleEditView() {
        boolean showShare = false;
        if (null != mAdapter) {
            showShare = mAdapter.getCheckedItemsCount() == 1;
            if (showShare) {
                FileInfo fileInfo = mAdapter.getFirstCheckedFileInfoItem();
                showShare = !fileInfo.isDirectory();
            }
        }
        mTvShare.setEnabled(showShare);

        boolean hasCheckedItem = (null != mAdapter) ? mAdapter.getCheckedItemsCount() > 0 : false;
        mTvDelete.setEnabled(hasCheckedItem);
        mTvCut.setEnabled(hasCheckedItem);
        mTvMore.setEnabled(hasCheckedItem);
    }

    private void showMoreDialog() {
        if (null == mMoreDialog) {
            View view;
            if (null != mAdapter) {
                int checkedItemCnt = mAdapter.getCheckedItemsCount();
                if (1 == checkedItemCnt) {
                    FileInfo fileInfo = mAdapter.getFirstCheckedFileInfoItem();
                    if (fileInfo.isDirectory()) {
                        view = mInflater.inflate(R.layout.dialog_more_single_folder, null);
                    } else {
                        view = mInflater.inflate(R.layout.dialog_more_single_file, null);
                    }
                } else {
                    view = mInflater.inflate(R.layout.dialog_more_multi, null);
                }

                if (null != view) {
                    setViewClickListener(view.findViewById(R.id.tv_more_open_mode),
                            new Runnable() {

                                @Override
                                public void run() {
                                    if (null != mAdapter) {
                                        openFile(mAdapter.getFirstCheckedFileInfoItem());
                                        mMoreDialog.cancel();
                                        exitEditMode();
                                    }
                                }
                            });

                    setViewClickListener(view.findViewById(R.id.tv_more_copy),
                            new Runnable() {
                                @Override
                                public void run() {
                                    mMoreDialog.cancel();
                                    if (!PermissionUtils.hasStorageWritePermission(getActivity())) {
                                        PermissionUtils.requestPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                                MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_COPY);
                                        return;
                                    }
                                    Intent intent = new Intent(getActivity(), DstActivity.class);
                                    intent.putExtra(DstActivity.EXTRA_TYPE, FileInfoManager.PASTE_MODE_COPY);
                                    intent.putExtra(DstActivity.EXTRA_LIST, (Serializable) mAdapter.getCheckedFileInfoItemsList());
                                    startActivityForResult(intent, REQUEST_CODE_PASTE);
                                    hasResult = true;

                                }
                            });

                    setViewClickListener(view.findViewById(R.id.tv_more_rename),
                            new Runnable() {
                                @Override
                                public void run() {
                                    mMoreDialog.cancel();
                                    if (!PermissionUtils.hasStorageWritePermission(getActivity())) {
                                        PermissionUtils.requestPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                                MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_RENAME);
                                        return;
                                    }
                                    showRenameDialog();
                                    exitEditMode();
                                    mIsRename = true;
                                }
                            });

                    setViewClickListener(view.findViewById(R.id.tv_more_details),
                            new Runnable() {
                                @Override
                                public void run() {
                                    mService.getDetailInfo(LocalFragment.this.getClass().getName(),
                                            mAdapter.getCheckedFileInfoItemsList().get(0),
                                            new DetailInfoListener(mAdapter.getCheckedFileInfoItemsList().get(0)));
                                    mMoreDialog.cancel();
                                    exitEditMode();
                                }
                            });

                    mMoreDialog = new AlertDialog.Builder(getActivity())
                            .setView(view)
                            .create();
                    mMoreDialog.setCanceledOnTouchOutside(true);
                    mMoreDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            mMoreDialog = null;
                        }
                    });
                }

            }
        }

        if (!mMoreDialog.isShowing()) {
            mMoreDialog.show();
        }
    }

    private void showOpenMethodDialog(FileInfo fileInfo) {
        LogUtils.d(TAG, "show Open method Dialog...");
        if (mIsAlertDialogShowing) {
            LogUtils.d(TAG, "Another Dialog is exist, return!~~");
            return;
        }

        if (isResumed()) {
            mIsAlertDialogShowing = true;
            AlertDialogFragment.ChoiceDialogFragmentBuilder builder =
                    new AlertDialogFragment.ChoiceDialogFragmentBuilder();
            builder.setDefault(R.array.open_method, OpenMethodClickListener.OPEN_METHOD_TEXT)
                    .setTitle(R.string.unknown_format)
                    .setCancelTitle(R.string.cancel)
                    .setDoneTitle(R.string.ok);
            AlertDialogFragment.ChoiceDialogFragment openDialogFragment = builder.create();
            OpenMethodClickListener clickListener = new OpenMethodClickListener(fileInfo);
            openDialogFragment.setItemClickListener(clickListener);
            openDialogFragment.setOnDialogDismissListener(this);
            openDialogFragment.show(getFragmentManager(),
                    AlertDialogFragment.ChoiceDialogFragment.CHOICE_DIALOG_TAG);
            boolean ret = getFragmentManager().executePendingTransactions();
            LogUtils.d(TAG, "executing pending transactions result: " + ret);
        }
    }

    private void switchLayout(View layout) {
        if (layout == mLayoutEmptyView) {
            LogUtils.d(TAG, "switchLayout: mLayoutEmptyView");
        }
        if (null != layout) {
            layout.setVisibility(View.VISIBLE);

            if (layout != mLayoutLocal) {
                mLayoutLocal.setVisibility(View.GONE);
            }
            if (layout != mLayoutSearch) {
                mLayoutSearch.setVisibility(View.GONE);
            }
            if (layout != mLayoutLoading) {
                mLayoutLoading.setVisibility(View.GONE);
            }
            if (layout != mLayoutNoMatchingSearch) {
                mLayoutNoMatchingSearch.setVisibility(View.GONE);
            }
            if (layout != mLayoutEmptyView) {
                mLayoutEmptyView.setVisibility(View.GONE);
            }
        }
    }

    private void switchTopBar(View topView) {
        if (null != topView) {
            topView.setVisibility(View.VISIBLE);

            if (topView != mLayoutLocalSearchResult) {
                mLayoutLocalSearchResult.setVisibility(View.GONE);
            }
            if (topView != mNavigationBar) {
                mNavigationBar.setVisibility(View.GONE);
            }
        }
    }

    private void setViewClickListener(View view, final Runnable runnable) {
        if ((null != view) && (null != runnable)) {
            RxView.clicks(view)
                    .throttleFirst(ConstUtils.THROTTLE_TIME, TimeUnit.MILLISECONDS)
                    .subscribe(new Action1<Void>() {
                        @Override
                        public void call(Void aVoid) {
                            runnable.run();
                        }
                    });
        }
    }

    private void showOptionDialog() {
        if (null == mOptionsDialog) {
            View view = mInflater.inflate(R.layout.dialog_local_options, null);
            setViewClickListener(view.findViewById(R.id.tv_option_searching),
                    new Runnable() {
                        @Override
                        public void run() {
                            mOptionsDialog.cancel();
                            if (mFileInfoManager != null) {
                                mShowFileList.clear();
                                mShowFileList.addAll(mFileInfoManager.getShowFileList());
                            }
                            if (mSearchInput == null) {
                                mSearchInput = ((HomeActivity) getActivity()).getEtSearchInput();
                            }
                            mSearchInput.setText(null);
                            mSearchInput.setSelection(0);
                            ((HomeActivity) getActivity()).switchMode(MODE_SEARCH);
                            mBottomBar.switchBar(null);
                            mSearchInput.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    KeyboardUtils.showSoftInput(getActivity(), mSearchInput);
                                }
                            }, 200);
                        }
                    });

            setViewClickListener(view.findViewById(R.id.tv_option_refresh),
                    new Runnable() {
                        @Override
                        public void run() {
                            handleRefresh();
                            mOptionsDialog.cancel();
                        }
                    });

            setViewClickListener(view.findViewById(R.id.tv_option_sorting),
                    new Runnable() {
                        @Override
                        public void run() {
                            showSortDialog();
                            mOptionsDialog.cancel();
                        }
                    });

            setViewClickListener(view.findViewById(R.id.tv_option_create_new_folder),
                    new Runnable() {
                        @Override
                        public void run() {
                            mOptionsDialog.cancel();
                            if (!PermissionUtils.hasStorageWritePermission(getActivity())) {
                                PermissionUtils.requestPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                        MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_CREATE);
                                return;
                            }
                            showCreateFolderDialog();
                            mIsRename = false;
                        }
                    });


            mOptionsDialog = new AlertDialog.Builder(getActivity())
                    .setView(view)
                    .create();
            mOptionsDialog.setCanceledOnTouchOutside(true);
        }

        if (!mOptionsDialog.isShowing()) {
            mOptionsDialog.show();
        }
    }

    protected void handleRefresh() {
        LogUtils.d(TAG, "handleRefresh: ");

        mRefreshingProgressDialog = new ProgressDialog(getActivity());
        mRefreshingProgressDialog.setMessage(getString(R.string.refreshing));
        mRefreshingProgressDialog.setIndeterminate(true);
        mRefreshingProgressDialog.setCancelable(false);
        mRefreshingProgressDialog.show();

        //int id = ResourceUtils.systemId(mHomeActivity, "progress", ID);
        ProgressBar progressBar = (ProgressBar) mRefreshingProgressDialog.findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressBar.setIndeterminateTintList(
                    ColorStateList.valueOf(ContextCompat.getColor(getActivity(), R.color.bingo_theme_color)));
            progressBar.setIndeterminateTintMode(PorterDuff.Mode.SRC_ATOP);
        } else {
            progressBar.setIndeterminateDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.loading_progress));
        }
        String[] paths = new String[]{mCurrentPath};
        //String[] paths = new String[mMountPointManager.getMountCount()];
        //mMountPointManager.getMountPointPaths().toArray(paths);
        MediaScannerConnection.OnScanCompletedListener callback =
                new MediaScannerConnection.OnScanCompletedListener() {

                    @Override
                    public void onScanCompleted(String path, Uri uri) {
                        LogUtils.d(TAG, "handleRefresh onScanCompleted: path=" + path + ", uri=" + uri);
                        mRefreshingProgressDialog.cancel();
                        showDirectoryContent(mCurrentPath);
                    }
                };
        MediaUtils.scanFiles(getActivity(), paths, null, callback);
    }

    private void requestSearch(String query) {
        LogUtils.e(TAG, "requestSearch: query=" + query);
        mSearchTotalChanged = 0;
        switchLayout(mLayoutSearch);
        if (mAdapter != null) {
            mAdapter.changeMode(FileInfoAdapter.MODE_SEARCH);
        }
        if (null != mService) {
            String searchPath = mCurrentPath;
            if (!searchPath.endsWith(MountPointManager.SEPARATOR)) {
                searchPath = searchPath + MountPointManager.SEPARATOR;
            }
            LogUtils.d(TAG, "requestSearch: searchPath=" + searchPath + ", query=" + query
                    + ", mCurrentPath=" + mCurrentPath);
            mService.search(this.getClass().getName(), query, searchPath,
                    new SearchListener(query));
        }
    }

    /**
     * This method switches edit view to navigation view
     * whether to refresh the screen after the switch is done
     */
    private void sortFileInfoList() {
        LogUtils.d(TAG, "Start sortFileInfoList()");

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.sorting_in));
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();

        //int id = ResourceUtils.systemId(getActivity(), "progress", ID);
        ProgressBar progressBar = (ProgressBar) progressDialog.findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressBar.setIndeterminateTintList(
                    ColorStateList.valueOf(ContextCompat.getColor(getActivity(), R.color.progress_indeterminate_color)));
            progressBar.setIndeterminateTintMode(PorterDuff.Mode.SRC_ATOP);
        } else {
            progressBar.setIndeterminateDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.loading_progress));
        }
        progressDialog.getWindow().getDecorView().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        int selection = mLstMedia.getFirstVisiblePosition(); // save current
                        // refresh only when paste or delete operation is performed
                        mFileInfoManager.sort(mSortType);
                        mAdapter.notifyDataSetChanged();
                        mLstMedia.setSelection(selection);
                        // restore the selection in the navigation view

                        LogUtils.d(TAG, "End sortFileInfoList()");

                        progressDialog.cancel();
                    }
                }, 500);


    }

    /**
     * The method creates an alert sort dialog
     *
     * @return a dialog
     */
    protected void showSortDialog() {
        LogUtils.d(TAG, "show SortDialog...");
        if (mIsAlertDialogShowing) {
            LogUtils.d(TAG, "Another Dialog is exist, return!~~");
            return;
        }

        if (isResumed()) {
            mIsAlertDialogShowing = true;
            AlertDialogFragment.ChoiceDialogFragmentBuilder builder = new AlertDialogFragment.ChoiceDialogFragmentBuilder();
            builder.setDefault(R.array.sort_order, mSortType).setTitle(R.string.sort_order);
            AlertDialogFragment.ChoiceDialogFragment sortDialogFragment = builder.create();
            sortDialogFragment.setItemClickListener(new SortClickListener());
            sortDialogFragment.setOnDialogDismissListener(this);
            sortDialogFragment.show(getFragmentManager(), AlertDialogFragment.ChoiceDialogFragment.CHOICE_DIALOG_TAG);
            boolean ret = getFragmentManager().executePendingTransactions();
            LogUtils.d(TAG, "executing pending transactions result: " + ret);
        }
    }

    protected void showCreateFolderDialog() {
        LogUtils.d(TAG, "showCreateFolderDialog");
        if (mIsAlertDialogShowing) {
            LogUtils.d(TAG, "Another Dialog showing, return!~~");
            return;
        }
        if (isResumed()) {
            mIsAlertDialogShowing = true;
            EditDialogFragmentBuilder builder = new EditDialogFragmentBuilder();
            builder.setDefault(getString(R.string.new_folder), getString(R.string.new_folder).length())
                    .setTitle(R.string.new_folder)
                    .setDoneTitle(R.string.ok)
                    .setCancelTitle(R.string.cancel);
            EditTextDialogFragment createFolderDialogFragment = builder.create();
            createFolderDialogFragment.setOnEditTextDoneListener(new CreateFolderListener());
            createFolderDialogFragment.setOnDialogDismissListener(this);
            try {
                createFolderDialogFragment.show(getFragmentManager(), CREATE_FOLDER_DIALOG_TAG);
                boolean ret = getFragmentManager().executePendingTransactions();
                LogUtils.d(TAG, "executing pending transactions result: " + ret);

                View titleDivider = createFolderDialogFragment.getDialog().findViewById(R.id.titleDivider);
                if (null != titleDivider) {
                    LogUtils.d(TAG, "showCreateFolderDialog: null != titleDivider");
                    titleDivider.setVisibility(View.GONE);
                }
            } catch (IllegalStateException e) {
                LogUtils.d(TAG, "call show dialog after onSaveInstanceState " + e);
                if (createFolderDialogFragment != null) {
                    createFolderDialogFragment.dismissAllowingStateLoss();
                }
            }
        }
    }

    protected void showRenameDialog() {
        LogUtils.d(TAG, "show RenameDialog...");
        if (mIsAlertDialogShowing) {
            LogUtils.d(TAG, "Another Dialog showing, return!~~");
            return;
        }
        FileInfo fileInfo = mAdapter.getFirstCheckedFileInfoItem();
        mRenamedItem = fileInfo;
        int selection = 0;
        if (fileInfo != null) {
            String name = fileInfo.getFileName();
            String fileExtension = FileUtils.getFileExtension(name);
            selection = name.length();
            if (!fileInfo.isDirectory() && fileExtension != null) {
                selection = selection - fileExtension.length() - 1;
            }
            if (isResumed()) {
                mIsAlertDialogShowing = true;
                EditDialogFragmentBuilder builder = new EditDialogFragmentBuilder();
                builder.setDefault(name, selection).setDoneTitle(R.string.done).setCancelTitle(
                        R.string.cancel).setTitle(rename);
                EditTextDialogFragment renameDialogFragment = builder.create();
                renameDialogFragment.setOnEditTextDoneListener(new RenameDoneListener(fileInfo));
                renameDialogFragment.setOnDialogDismissListener(this);
                renameDialogFragment.show(getFragmentManager(), RENAME_DIALOG_TAG);
                boolean ret = getFragmentManager().executePendingTransactions();
                LogUtils.d(TAG, "executing pending transactions result: " + ret);
                View titleDivider = renameDialogFragment.getDialog().findViewById(R.id.titleDivider);
                if (null != titleDivider) {
                    LogUtils.d(TAG, "showCreateFolderDialog: null != titleDivider");
                    titleDivider.setVisibility(View.GONE);
                }
            }
        }
    }

    protected void showRenameDialogAgain() {
        LogUtils.d(TAG, "show RenameDialog...");
        if (mIsAlertDialogShowing) {
            LogUtils.d(TAG, "Another Dialog showing, return!~~");
            return;
        }
        FileInfo fileInfo = mRenamedItem;
        int selection = 0;
        if (fileInfo != null) {
            String name = fileInfo.getFileName();
            String fileExtension = FileUtils.getFileExtension(name);
            selection = name.length();
            if (!fileInfo.isDirectory() && fileExtension != null) {
                selection = selection - fileExtension.length() - 1;
            }
            if (isResumed()) {
                mIsAlertDialogShowing = true;
                EditDialogFragmentBuilder builder = new EditDialogFragmentBuilder();
                builder.setDefault(name, selection).setDoneTitle(R.string.done).setCancelTitle(
                        R.string.cancel).setTitle(rename);
                EditTextDialogFragment renameDialogFragment = builder.create();
                renameDialogFragment.setOnEditTextDoneListener(new RenameDoneListener(fileInfo));
                renameDialogFragment.setOnDialogDismissListener(this);
                renameDialogFragment.show(getFragmentManager(), RENAME_DIALOG_TAG);
                boolean ret = getFragmentManager().executePendingTransactions();
                LogUtils.d(TAG, "executing pending transactions result: " + ret);
                View titleDivider = renameDialogFragment.getDialog().findViewById(R.id.titleDivider);
                if (null != titleDivider) {
                    LogUtils.d(TAG, "showCreateFolderDialog: null != titleDivider");
                    titleDivider.setVisibility(View.GONE);
                }
            }
        }
    }

    /**
     * The method creates an alert delete dialog
     */
    protected void showRenameExtensionDialog(FileInfo srcfileInfo, final String newFilePath) {
        LogUtils.d(TAG, "show RenameExtensionDialog...");

        AlertDialogFragmentBuilder builder = new AlertDialogFragmentBuilder();
        AlertDialogFragment renameExtensionDialogFragment = builder.setTitle(
                R.string.confirm_rename).setMessage(
                R.string.rename_extension).setCancelTitle(R.string.cancel).setDoneTitle(R.string.ok)
                .create();
        renameExtensionDialogFragment.getArguments().putString(NEW_FILE_PATH_KEY, newFilePath);
        renameExtensionDialogFragment.setOnDoneListener(new RenameExtensionListener(srcfileInfo,
                newFilePath));
        renameExtensionDialogFragment.show(getFragmentManager(), RENAME_EXTENSION_DIALOG_TAG);
        boolean ret = getFragmentManager().executePendingTransactions();
        LogUtils.d(TAG, "executing pending transactions result: " + ret);
    }

    protected void showDeleteDialog() {
        LogUtils.d(TAG, "show DeleteDialog...");
        if (mIsAlertDialogShowing) {
            LogUtils.d(TAG, "Another Dialog is exist, return!~~");
            return;
        }

        if (isResumed()) {
            mIsAlertDialogShowing = true;
            AlertDialogFragmentBuilder builder = new AlertDialogFragmentBuilder();
            AlertDialogFragment deleteDialogFragment =
                    builder.setMessage(R.string.delete_msg)
                            .setDoneTitle(R.string.ok)
                            .setCancelTitle(R.string.cancel)
                            .setTitle(R.string.delete)
                            .create();
            deleteDialogFragment.setOnDoneListener(new DeleteListener());
            deleteDialogFragment.setOnDialogDismissListener(this);
            deleteDialogFragment.show(getFragmentManager(), DELETE_DIALOG_TAG);
            boolean ret = getFragmentManager().executePendingTransactions();
            LogUtils.d(TAG, "executing pending transactions result: " + ret);

            View titleDivider = deleteDialogFragment.getDialog().findViewById(R.id.titleDivider);
            if (null != titleDivider) {
                LogUtils.d(TAG, "showDeleteDialog: null != titleDivider");
                titleDivider.setVisibility(View.GONE);
            }

            TextView msg = (TextView) deleteDialogFragment.getDialog().findViewById(R.id.message);
            if (null != msg) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    msg.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                }
            }
        }
    }

    @Override
    public void onDialogDismiss() {
        LogUtils.d(TAG, "dialog dismissed...");
        mIsAlertDialogShowing = false;
    }

    @Override
    public void doPermissionGranted() {
        showDirectoryContent(mCurrentPath);
    }

    protected class SearchListener implements FileManagerService.OperationEventListener {
        private static final int FIRST_UPDATE_COUNT = 20;
        private static final int NEED_UPDATE_LIST = 6;
        private boolean mIsResultSet = false;
        private int mCount = 0;

        /**
         * Constructor of SearchListener.
         *
         * @param text the search target(String), which will be shown on searchResult TextView..
         */
        public SearchListener(String text) {
            if (text == null) {
                throw new IllegalArgumentException();
            }
            mSearchText = text;
        }

        @Override
        public void onTaskResult(int result) {
            mEnterSearch = true;
            if (result == ERROR_CODE_USER_CANCEL) {
                mFileInfoManager.getAddedFileList().clear();
                mSearchTotal = 0;
//                switchTopBar(mNavigationBar);
                switchLayout(mLayoutNoMatchingSearch);
                return;
            }
            if (mTvSearchResult != null && !mIsResultSet) {
                LogUtils.d(TAG, "onTaskProgress: mSearchTotal=" + mSearchTotal);
                switchTopBar(mLayoutLocalSearchResult);
                mTvSearchResult.setText(getString(
                        R.string.search_result, mSearchTotal));
                mIsResultSet = true;
            }
            if (0 == mSearchTotal) {
                switchLayout(mLayoutNoMatchingSearch);
            } else {
                switchLayout(mLayoutLocal);
                mFileInfoManager.updateSearchList();
                mAdapter.notifyDataSetChanged();
            }
            mSearchTotalChanged = mSearchTotal;
            mSearchTotal = 0;
        }

        @Override
        public void onTaskPrepare() {
            mAdapter.changeMode(FileInfoAdapter.MODE_SEARCH);
        }

        @Override
        public void onTaskProgress(ProgressInfo progressInfo) {
            if (!progressInfo.isFailInfo()) {
                mSearchTotal = progressInfo.getTotal();
                /*if (mTvSearchResult != null && !mIsResultSet) {
                    mSearchTotal = progressInfo.getTotal();
                    LogUtils.d(TAG, "onTaskProgress: mSearchTotal=" + mSearchTotal);
                    switchTopBar(mLayoutLocalSearchResult);
                    mTvSearchResult.setText(getResources().getString(
                            R.string.search_result, mSearchTotal));
                    mIsResultSet = true;
                }*/
                if (progressInfo.getFileInfo() != null) {
                    mFileInfoManager.addItem(progressInfo.getFileInfo());
                }
                mCount++;
                if (mCount > FIRST_UPDATE_COUNT) {
                    if (mLstMedia.getLastVisiblePosition() + NEED_UPDATE_LIST > mAdapter
                            .getCount()) {
                        mFileInfoManager.updateSearchList();
                        mAdapter.notifyDataSetChanged();
                        mCount = 0;
                    }
                }
            }
        }
    }

    private class SortClickListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialog, int id) {
            dialog.dismiss();
            if (id != mSortType) {
                setPrefsSortBy(id);
                sortFileInfoList();
            }
        }
    }

    private class OpenMethodClickListener implements DialogInterface.OnClickListener {
        static final int OPEN_METHOD_TEXT = 0;
        static final int OPEN_METHOD_AUDIO = 1;
        static final int OPEN_METHOD_VIDEO = 2;
        static final int OPEN_METHOD_IMAGE = 3;

        private FileInfo mSelectedItem = null;
        private Intent mOpenIntent = null;

        OpenMethodClickListener(FileInfo selectedItem) {
            mSelectedItem = selectedItem;
            mOpenIntent = new Intent(Intent.ACTION_VIEW);
            open(".txt");
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            LogUtils.d(TAG, "OpenMethodClickListener onClick: which=" + which);
            switch (which) {
                case OPEN_METHOD_TEXT:
                    open(".txt");
                    break;

                case OPEN_METHOD_AUDIO:
                    open(".mp3");
                    break;

                case OPEN_METHOD_VIDEO:
                    open(".3gp");
                    break;

                case OPEN_METHOD_IMAGE:
                    open(".png");
                    break;

                case DialogInterface.BUTTON_POSITIVE:
                    try {
                        startActivity(getOpenIntent());
                        dialog.dismiss();
                    } catch (android.content.ActivityNotFoundException e) {
                        mToastHelper.showToast(R.string.msg_unable_open_file);
                        LogUtils.w(TAG, "OpenMethodClickListener, Cannot open file: ");
                    }
                    break;

                default:
                    break;
            }
        }

        public Intent getOpenIntent() {
            return mOpenIntent;
        }

        private void open(String extension) {
            if ((null != mSelectedItem) && !TextUtils.isEmpty(extension)) {
                Uri uri = mSelectedItem.getUri();
                LogUtils.d(TAG, "openFile, Open uri file: " + uri);
                String mimeType = FileUtils.getMimeTypeBySpecialExtension(mSelectedItem.getFileAbsolutePath(), extension);
                mOpenIntent.setDataAndType(uri, mimeType);
                mOpenIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
        }
    }

    protected final class CreateFolderListener implements EditTextDialogFragment.EditTextDoneListener {
        public void onClick(String text) {

            AlertDialogFragment.EditTextDialogFragment create = (AlertDialogFragment.EditTextDialogFragment) getFragmentManager().findFragmentByTag(CREATE_FOLDER_DIALOG_TAG);
            if (create != null) {
                create.dismiss();
            }
            if (mService != null) {
                mCreatedFolderName = text;
                String dstPath = mCurrentPath + MountPointManager.SEPARATOR + text;
                mService.createFolder(LocalFragment.this.getClass().getName(), dstPath,
                        new LightOperationListener(text));
            }
        }
    }

    protected class RenameDoneListener implements AlertDialogFragment.EditTextDialogFragment.EditTextDoneListener {
        FileInfo mSrcfileInfo;

        public RenameDoneListener(FileInfo srcFile) {
            mSrcfileInfo = srcFile;
        }

        @Override
        public void onClick(String text) {

            AlertDialogFragment.EditTextDialogFragment rename = (AlertDialogFragment.EditTextDialogFragment) getFragmentManager().findFragmentByTag(RENAME_DIALOG_TAG);
            if (rename != null) {
                rename.dismiss();
            }
            String newFilePath = mCurrentPath + MountPointManager.SEPARATOR + text;
            mRenamedName = text;
            if (null == mSrcfileInfo) {
                LogUtils.w(TAG, "mSrcfileInfo is null.");
                return;
            }
            if (FileUtils.isExtensionChange(newFilePath, mSrcfileInfo.getFileAbsolutePath())) {
               /* AlertDialogFragment alertDialogFragment = (AlertDialogFragment) getFragmentManager().findFragmentByTag(RENAME_DIALOG_TAG);
                if (alertDialogFragment != null && alertDialogFragment.isResumed()) {
                    LogUtils.e(TAG,"*****重命名dialog仍可见******");
                    alertDialogFragment.getDialog().hide();
                }*/
                showRenameExtensionDialog(mSrcfileInfo, newFilePath);
            } else {
                if (mEnterSearch) {
                    mListChanged = true;
                }
                if (mService != null) {
                    mService.rename(LocalFragment.this.getClass().getName(),
                            mSrcfileInfo, new FileInfo(newFilePath), new LightOperationListener(
                                    FileUtils.getFileName(newFilePath)));
                }
            }

        }
    }

    private class RenameExtensionListener implements DialogInterface.OnClickListener {
        private final String mNewFilePath;
        private final FileInfo mSrcFile;

        public RenameExtensionListener(FileInfo fileInfo, String newFilePath) {
            mNewFilePath = newFilePath;
            mSrcFile = fileInfo;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            if (mEnterSearch) {
                mListChanged = true;
            }
            if (mService != null) {
                mService.rename(LocalFragment.this.getClass().getName(), mSrcFile,
                        new FileInfo(mNewFilePath), new LightOperationListener(FileUtils
                                .getFileName(mNewFilePath)));
            }
        }

    }

    protected class LightOperationListener implements FileManagerService.OperationEventListener {

        String mDstName = null;

        LightOperationListener(String dstName) {
            mDstName = dstName;
        }

        @Override
        public void onTaskResult(int errorType) {
            LogUtils.i(TAG, "LightOperationListener,TaskResult result = " + errorType);
            switch (errorType) {
                case ERROR_CODE_SUCCESS:
                    if (mIsRename) {
                        //重命名成功
                        if ((mAdapter != null && mAdapter.getCount() == 0) ||
                                (mAdapter != null && mAdapter.getCount() == 1 &&
                                        mRenamedName != null && mRenamedName.startsWith("."))) {
                            mLayoutEmptyView.setVisibility(View.VISIBLE);
                        }
                        if (mRenamedName != null && mRenamedName.startsWith(".") && mSearchTotalChanged > 0) {
                            mSearchTotalChanged--;
                            mTvSearchResult.setText(getResources().getString(
                                    R.string.search_result, mSearchTotalChanged));
                        }
                        mRenamedName = null;
                    } else {
                        //创建文件成功
                        if (mLayoutEmptyView.getVisibility() == View.VISIBLE) {
                            if (mCreatedFolderName != null && !mCreatedFolderName.startsWith(".")) {
                                mLayoutEmptyView.setVisibility(View.GONE);
                                mCreatedFolderName = null;
                            }
                        }
                    }
                case ERROR_CODE_USER_CANCEL:
                    FileInfo fileInfo = mFileInfoManager.updateOneFileInfoList(mCurrentPath, mSortType);
                    mAdapter.notifyDataSetChanged();
                    if (fileInfo != null) {
                        int postion = mAdapter.getPosition(fileInfo);
                        LogUtils.d(TAG, "LightOperation postion = " + postion);
                        mLstMedia.setSelection(postion);
                    }

                    break;
                case ERROR_CODE_FILE_EXIST:
                    if (mDstName != null) {
                        mToastHelper.showToast(R.string.rename_hint);
                    }
                    if (mIsRename) {
                        showRenameDialogAgain();
                    } else {
                        showCreateFolderDialog();
                    }
                    break;
                case ERROR_CODE_NAME_EMPTY:
                    mToastHelper.showToast(R.string.invalid_empty_name);
                    break;
                case ERROR_CODE_NAME_TOO_LONG:
                    mToastHelper.showToast(R.string.file_name_too_long);
                    break;
                case ERROR_CODE_NOT_ENOUGH_SPACE:
                    mToastHelper.showToast(R.string.insufficient_memory);
                    break;
                case ERROR_CODE_UNSUCCESS:
                    mToastHelper.showToast(R.string.operation_fail);
                    break;
                default:
                    LogUtils.e(TAG, "wrong errorType for LightOperationListener");
                    break;
            }
        }

        @Override
        public void onTaskPrepare() {
            return;
        }

        @Override
        public void onTaskProgress(ProgressInfo progressInfo) {
            return;
        }
    }

    /**
     * 操作文件后判断列表是否为空
     *
     * @return
     */
    private boolean isEmpty() {
        if (mAdapter != null) {
            int cnt = mAdapter.getCount();
            if (cnt == 0) {
                return true;
            } else {
                for (int i = 0; i < cnt; i++) {
                    if (mAdapter.getItem(i).getFileName().startsWith(".")) {
                        continue;
                    } else {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    protected class ListListener implements FileManagerService.OperationEventListener {
        public static final String LIST_DIALOG_TAG = "ListDialogFragment";

        protected void dismissDialogFragment() {
            LogUtils.d(TAG, "ListListener dismissDialogFragment");
            switchLayout(mLayoutLocal);
        }

        @Override
        public void onTaskPrepare() {
        }

        @Override
        public void onTaskProgress(ProgressInfo progressInfo) {
            LogUtils.d(TAG, "ListListener onTaskProgress: isResumed=" + isResumed());
            if (isResumed()) {
                switchLayout(mLayoutLoading);
            }
        }

        @Override
        public void onTaskResult(int result) {
            LogUtils.e(TAG, "onTaskResult: result=" + result);
            onPathChanged();
//            if (mAdapter.isMode(FileInfoAdapter.MODE_EDIT)) {
//                mFileInfoManager.loadFileInfoList(mCurrentPath, mSortType, mSelectedFileInfo);
//                mSelectedFileInfo = mAdapter.getFirstCheckedFileInfoItem();
//            } else {
                mFileInfoManager.loadFileInfoList(mCurrentPath, mSortType);
//            }

            mAdapter.notifyDataSetChanged();
            if (mScrollBack && mPositionMap.containsKey(mCurrentPath)) {
                int mPos = mPositionMap.get(mCurrentPath)[0];
                int mTop = mPositionMap.get(mCurrentPath)[1];
                LogUtils.e(TAG, "*******mPos=:" + mPos);
                LogUtils.e(TAG, "*******mTop=:" + mTop);
                mLstMedia.setSelectionFromTop(mPos, mTop);
                mScrollBack = false;
            } else {
                int selectedItemPosition = restoreSelectedPosition();
                if (selectedItemPosition == -1) {
                    mLstMedia.setSelectionAfterHeaderView();
                } /*else if (selectedItemPosition >= 0 && selectedItemPosition < mAdapter.getCount()) {
                if (mSelectedTop != -1) {
                    mLstMedia.setSelectionFromTop(selectedItemPosition, mSelectedTop);
                    mSelectedTop = -1;
                } else if (mTop != -1) {
                    mLstMedia.setSelectionFromTop(selectedItemPosition, mTop);
                    mTop = -1;
                }*/ else {
                    mLstMedia.setSelection(selectedItemPosition);
//                }
                }
            }

            LogUtils.d(TAG, "onTaskResult: count=" + mAdapter.getCount());
            switchLayout(mLayoutLocal);
            mLayoutEmptyView.setVisibility((0 == mAdapter.getCount()) ? View.VISIBLE : View.GONE);

            //dismissDialogFragment();
//            onPathChanged();
        }
    }

    private class DeleteListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialog, int id) {
            LogUtils.d(TAG, "onClick() method for alertDeleteDialog, OK button");
            final AlertDialogFragment deleteFragment = (AlertDialogFragment) getFragmentManager().
                    findFragmentByTag(DELETE_DIALOG_TAG);
            if (null != deleteFragment) {
                deleteFragment.dismissAllowingStateLoss();
            }
            if (mEnterSearch) {
                mListChanged = true;
            }
            if (mService != null) {
                HeavyOperationListener listener = new HeavyOperationListener(R.string.deleting, getActivity(),
                        mHeavyOperationListenerCallback);
                mService.deleteFiles(LocalFragment.this.getClass().getName(),
                        mAdapter.getCheckedFileInfoItemsList(), listener);
            }
        }
    }

    protected class DetailInfoListener implements FileManagerService.OperationEventListener,
            DialogInterface.OnDismissListener {
        public static final String DETAIL_DIALOG_TAG = "detaildialogtag";
        private FileInfo mFileInfo;

        public DetailInfoListener(FileInfo fileInfo) {
            mFileInfo = fileInfo;
        }

        @Override
        public void onTaskPrepare() {
            if (isResumed()) {
                AlertDialogFragmentBuilder builder = new AlertDialogFragmentBuilder();
                AlertDialogFragment detailFragment = builder.setCancelTitle(R.string.ok).setLayout(
                        R.layout.dialog_details).setTitle(R.string.details).create();

                detailFragment.setDismissListener(this);
                detailFragment.show(getFragmentManager(), DETAIL_DIALOG_TAG);
                boolean ret = getFragmentManager().executePendingTransactions();
                LogUtils.d(TAG, "executing pending transactions result: " + ret);
                Dialog dialog = detailFragment.getDialog();
                if (dialog != null) {
                    setViewContent(dialog.findViewById(R.id.tv_detail_name), mFileInfo.getShowName());
                    String mimeType = mFileInfo.getFileMimeType(mService);
                    if (FileInfo.MIMETYPE_EXTENSION_UNKONW.equals(mimeType)
                            || FileInfo.MIMETYPE_EXTENSION_NULL.equals(mimeType)) {
                        mimeType = getString(R.string.unknown_mime);
                    }
                    setViewContent(dialog.findViewById(R.id.tv_detail_type),
                            mFileInfo.isDirectory() ? getString(R.string.folder) : /*getString(R.string.file)*/ mimeType);
                    setViewContent(dialog.findViewById(R.id.tv_detail_size), mFileInfo.getFileSizeStr());

                    if (mFileInfo.isDirectory()) {
                        File dirFile = mFileInfo.getFile();
                        int dirs = 0;
                        int files = 0;
                        for (File file : dirFile.listFiles()) {
                            if (file.isDirectory()) {
                                dirs++;
                            } else {
                                files++;
                            }
                        }
                        StringBuilder sb = new StringBuilder();
                        sb.append(getString(R.string.file)).append(":");
                        sb.append(files).append(", ");
                        sb.append(getString(R.string.folder)).append(":");
                        sb.append(dirs);

                        setViewContent(dialog.findViewById(R.id.tv_detail_include), sb.toString());
                        View view = dialog.findViewById(R.id.rl_include);
                        view.setVisibility(View.VISIBLE);
                    } else {
                        View view = dialog.findViewById(R.id.rl_include);
                        view.setVisibility(View.GONE);
                    }

                    setViewContent(dialog.findViewById(R.id.tv_detail_time),
                            FileUtils.formatModifiedTime(getActivity(), mFileInfo.getFileLastModifiedTime()));
                    setViewContent(dialog.findViewById(R.id.tv_detail_route), mFileInfo.getShowParentPath());
                }
            } else {
                LogUtils.e(TAG, "onTaskPrepare activity is not resumed");
            }
        }

        private void setViewContent(View view, String content) {
            if (view instanceof TextView) {
                TextView tv = (TextView) view;
                tv.setText(content);
            }
        }

        @Override
        public void onTaskProgress(ProgressInfo progressInfo) {

        }

        @Override
        public void onTaskResult(int result) {
            LogUtils.d(TAG, "DetailInfoListener onTaskResult.");
            AlertDialogFragment detailFragment = (AlertDialogFragment) getFragmentManager().findFragmentByTag(DETAIL_DIALOG_TAG);
            if (detailFragment != null) {
                //detailFragment.getArguments().putString(DETAIL_INFO_KEY, mStringBuilder.toString());
                //detailFragment.getArguments().putLong(DETAIL_INFO_SIZE_KEY, mFileLength);
            } else {
                // this case may happen in case of this operation already canceled.
                LogUtils.d(TAG, "get detail fragment is null...");
            }
            return;
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            if (mService != null) {
                LogUtils.d(this.getClass().getName(), "onDismiss");
                mService.cancel(LocalFragment.this.getClass().getName());
            }
        }
    }

    protected int getViewDirection() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return mNavigationBar.getParent().getParent().getLayoutDirection();
        } else {
            return ViewGroup.LAYOUT_DIRECTION_LTR;
        }
    }

    protected class TabManager {
        private final List<String> mTabNameList = new ArrayList<>();
        protected LinearLayout mTabsHolder = null;
        private String mCurFilePath = null;
        private static final int TAB_TET_MAX_LENGTH = 250;

        public TabManager(LinearLayout tabsHolder) {
            mTabsHolder = tabsHolder;
        }

        public void refreshTab(String initFileInfo) {
            LogUtils.e(TAG, "refreshTab,initFileInfo = " + initFileInfo);
            int count = mTabsHolder.getChildCount();
            mTabsHolder.removeViews(0, count);
            mTabNameList.clear();

            mCurFilePath = initFileInfo;
            if (mCurFilePath != null) {
                addTab(getString(R.string.local));
                if (!mMountPointManager.isRootPath(mCurFilePath)) {
                    String path = mMountPointManager.getDescriptionPath(mCurFilePath);
                    String[] result = path.split(MountPointManager.SEPARATOR);
                    for (String string : result) {
                        addTab(string);
                    }

                    if (getViewDirection() == ViewGroup.LAYOUT_DIRECTION_LTR) {
                        // scroll to right with slow-slide animation
                        startActionBarScroll();
                    } else if (getViewDirection() == ViewGroup.LAYOUT_DIRECTION_RTL) {
                        // scroll horizontal view to the right
                        mNavigationBar.startHorizontalScroll(-mNavigationBar.getScrollX(),
                                -mNavigationBar.getRight());
                    }
                }
            }
            updateHomeButton();
            LogUtils.e(TAG, "refreshTab finish");
        }

        private void startActionBarScroll() {
            // scroll to right with slow-slide animation
            // To pass the Launch performance test, avoid the scroll
            // animation when launch.
            int tabHostCount = mTabsHolder.getChildCount();
            int navigationBarCount = mNavigationBar.getChildCount();
            if ((tabHostCount > 2) && (navigationBarCount >= 1)) {
                View view = mNavigationBar.getChildAt(navigationBarCount - 1);
                if (null == view) {
                    LogUtils.d(TAG, "startActionBarScroll, navigationbar child is null");
                    return;
                }
                int width = view.getRight();
                mNavigationBar.startHorizontalScroll(mNavigationBar.getScrollX(), width
                        - mNavigationBar.getScrollX());
            }
        }

        protected void updateHomeButton() {
        }

        /**
         * This method updates the navigation view to the previous view when
         * back button is pressed
         *
         * @param newPath the previous showed directory in the navigation history
         */
        protected void showPrevNavigationView(String newPath) {
            refreshTab(newPath);
            showDirectoryContent(newPath);
        }

        /**
         * This method creates tabs on the navigation bar
         *
         * @param text the name of the tab
         */
        protected void addTab(String text) {
            if (!TextUtils.isEmpty(text)) {
                LogUtils.d(TAG, "addTab: text=[" + text + "]");

                TextView button = new TextView(getActivity());
                button.setClickable(true);
                button.setOnClickListener(LocalFragment.this);
                //button.setBackgroundResource(R.drawable.button_path_bg);
                //button.setTextColor(getResources().getColor(R.color.color999));
                button.setTextColor(getResources().getColorStateList(R.color.nav_tab_text_color));
                button.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                        getResources().getDimensionPixelSize(R.dimen.nav_text_size));
                button.setPadding(getResources().getDimensionPixelOffset(R.dimen.path_padding),
                        0, 0, 0);
                button.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                        R.drawable.path_arrow, 0);
                button.setCompoundDrawablePadding(
                        getResources().getDimensionPixelOffset(R.dimen.path_padding));
                button.setMaxWidth(TAB_TET_MAX_LENGTH);
                LongStringUtils.fadeOutLongString(button);
                button.setText(text.trim());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    button.setOutlineProvider(null);
                }

                LinearLayout.LayoutParams mlp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                mlp.setMargins(0, 0, 0, 0);
                mTabsHolder.addView(button, mlp);

                button.setId(mTabNameList.size());
                mTabNameList.add(text);
            }
        }

        /**
         * The method updates the navigation bar
         *
         * @param id the tab id that was clicked
         */
        protected void updateNavigationBar(int id) {
            LogUtils.d(TAG, "updateNavigationBar, id = " + id + ", listSize=" + mTabNameList.size());

            // click current button do not response
            if (id < mTabNameList.size() - 1) {
                String oldPath = mCurFilePath;
                int count = mTabNameList.size() - id - 1;
                mTabsHolder.removeViews(id + 1, count);

                for (int i = 0; i < count; i++) {
                    // update mTabNameList
                    mTabNameList.remove(mTabNameList.size() - 1);
                }

                if (0 == id) {
                    mCurFilePath = mMountPointManager.getRootPath();
                } else {
                    // get mount point path
                    String mntPointPath = mMountPointManager.getRealMountPointPath(mCurFilePath);
                    LogUtils.d(TAG, "updateNavigationBar, mntPointPath: " + mntPointPath + " for mCurFilepath: " + mCurFilePath);
                    String path = mCurFilePath.substring(mntPointPath.length() + 1);
                    StringBuilder sb = new StringBuilder(mntPointPath);
                    String[] pathParts = path.split(MountPointManager.SEPARATOR);
                    // id=0,1 is for mnt point button, so from id=2 to get other parts of path
                    for (int i = 2; i <= id; i++) {
                        sb.append(MountPointManager.SEPARATOR);
                        sb.append(pathParts[i - 2]);
                    }
                    mCurFilePath = sb.toString();
                }

                if (mAdapter.isMode(FileInfoAdapter.MODE_EDIT)
                        && !mCurrentPath.equals(mCurFilePath)) {
                    exitEditMode();
                }
                showDirectoryContent(mCurFilePath);
                updateHomeButton();
            }
        }

    }

    class BottomBar {
        public final static int BOTTOM_BAR_CLEAN = 1;
        public final static int BOTTOM_BAR_MENU = 2;
        public final static int BOTTOM_BAR_EDIT = 3;

        private View mEdit;
//        private View mClean;
        private View mMenu;

        public BottomBar(View edit, View menu) {
            mEdit = edit;
//            mClean = clean;
            mMenu = menu;
        }

        public void switchBar(View view) {
            if (null != view) {
                view.setVisibility(View.VISIBLE);

                if (view != mEdit) {
                    mEdit.setVisibility(View.GONE);
                }

//                if (view != mClean) {
//                    mClean.setVisibility(View.GONE);
//                }

                if (view != mMenu) {
                    mMenu.setVisibility(View.GONE);
                }
            } else {
                mEdit.setVisibility(View.GONE);
//                mClean.setVisibility(View.GONE);
                mMenu.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (null == permissions || permissions.length == 0 ||
                null == grantResults || grantResults.length == 0 ||
                PackageManager.PERMISSION_DENIED == grantResults[0]) {
            return;
        }
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_DELETE:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    showDeleteDialog();
                }
                break;
            case MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_RENAME:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    showRenameDialog();
                    exitEditMode();
                }
                break;
            case MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_COPY:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    Intent intent = new Intent(getActivity(), DstActivity.class);
                    intent.putExtra(DstActivity.EXTRA_TYPE, FileInfoManager.PASTE_MODE_COPY);
                    intent.putExtra(DstActivity.EXTRA_LIST, (Serializable) mAdapter.getCheckedFileInfoItemsList());
                    startActivityForResult(intent, REQUEST_CODE_PASTE);
                    hasResult = true;
                }
                break;
            case MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_CUT:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    Intent intent = new Intent(getActivity(), DstActivity.class);
                    intent.putExtra(DstActivity.EXTRA_TYPE, FileInfoManager.PASTE_MODE_CUT);
                    intent.putExtra(DstActivity.EXTRA_LIST, (Serializable) mAdapter.getCheckedFileInfoItemsList());
                    startActivityForResult(intent, REQUEST_CODE_PASTE);
                    hasResult = true;
                }
                break;
            case MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_CREATE:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    showCreateFolderDialog();
                }
                break;
            case MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_ZIP:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    openZipFile(mItemClickPath, mItemClickParentPath, mItemClickFileName.substring(0, mItemClickFileName.lastIndexOf(".")), ZIP);
                }
                break;
            case MY_PERMISSIONS_REQUEST_TO_WRITE_EXTERNAL_STORAGE_RAR:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    openZipFile(mItemClickPath, mItemClickParentPath, mItemClickFileName.substring(0, mItemClickFileName.lastIndexOf(".")), RAR);
                }
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

}
