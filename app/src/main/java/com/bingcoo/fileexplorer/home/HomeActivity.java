package com.bingcoo.fileexplorer.home;

import android.Manifest;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bingcoo.fileexplorer.R;
import com.bingcoo.fileexplorer.leakcanary.LeakCanaryHelper;
import com.bingcoo.fileexplorer.ui.TouchlessViewPager;
import com.bingcoo.fileexplorer.ui.ViewPagerTabs;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.PermissionUtils;
import com.bingcoo.fileexplorer.util.SystemUtils;
import com.bingcoo.fileexplorer.util.ToastHelper;
import com.ibingo.umsdk.OperateStatisticalData;
import com.trello.rxlifecycle.components.support.RxFragmentActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * 类名称：HomeActivity
 * 作者：David
 * 内容摘要：首页页面，显示“本地”页面和“分页”页面。
 * 创建日期：2016/11/21
 * 修改者， 修改日期， 修改内容
 */
public class HomeActivity extends RxFragmentActivity implements ViewPager.OnPageChangeListener {
    private static final String TAG = "HomeActivity";

    static {
        LogUtils.setDebug(TAG, true);
    }

    static final int TAB_COUNT_DEFAULT = 2;

    public static final int MODE_NORMAL = 0;
    public static final int MODE_SELECT = 1;
    public static final int MODE_SEARCH = 2;
    public static final int MODE_LOCAL = 3;

    public static final int MY_PERMISSIONS_REQUEST_TO_READ_EXTERNAL_STORAGE = 4;
    public static final String MY_REQUEST_TO_READ_EXTERNAL_STORAGE_PERMISSIONS_GRANTED = "my_request_to_read_external_storage_permissions_granted";

    private FragmentManager mFragmentManager;
    private ViewPagerTabs mViewPagerTabs;
    private ArrayList<ViewPager.OnPageChangeListener> mOnPageChangeListeners =
            new ArrayList<>();
    private int mCurrentMode = MODE_NORMAL;
    private HomePagerAdapter mHomePagerAdapter;
    private ToastHelper mToastHelper;

    @BindView(R.id.vp_home)
    TouchlessViewPager mViewPager;
    @BindView(R.id.layout_action_mode_select)
    View mLayoutActionModeSelect;
    @BindView(R.id.layout_action_mode_local)
    View mLayoutActionModeLocal;
    @BindView(R.id.layout_action_mode_search)
    View mLayoutActionModeSearch;
    @BindView(R.id.img_local_back)
    ImageView mImgLocalBack;
    @BindView(R.id.tv_local_title)
    TextView mLocalTitle;
    @BindView(R.id.img_local_options)
    ImageView mImgLocalOptions;
    @BindView(R.id.img_search_back)
    ImageView mImgSearchBack;
    @BindView(R.id.et_search_input)
    EditText mEtSearchInput;
    @BindView(R.id.img_search)
    ImageView mImgSearch;
    @BindView(R.id.search_container)
    View mSearchContainer;//搜索框外面的framelayout

    @BindView(R.id.tv_select_all)
    TextView mTvSelectAll;
    @BindView(R.id.tv_select_chosen)
    TextView mTvSelectChosen;
    @BindView(R.id.tv_select_cancel)
    TextView mTvSelectCancel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
//        ActivityManager.getInstance().addActivity(this);
        OperateStatisticalData.init(this);
        LogUtils.d(TAG, "onCreate: savedInstanceState=" + savedInstanceState);
        if (null != savedInstanceState) {
            String FRAGMENTS_TAG = "android:support:fragments";
            savedInstanceState.remove(FRAGMENTS_TAG);
        }
        mToastHelper = new ToastHelper(this);
        mFragmentManager = getFragmentManager();
        initContentView();
        LeakCanaryHelper.fixFocusedViewLeak(getApplication());
        if (!PermissionUtils.hasStorageReadPermission(getApplicationContext())) {
            PermissionUtils.requestPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    MY_PERMISSIONS_REQUEST_TO_READ_EXTERNAL_STORAGE);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        OperateStatisticalData.onResume(this);
       /* if (!PermissionUtils.hasStorageReadPermission(getApplicationContext())) {
            PermissionUtils.requestPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    MY_PERMISSIONS_REQUEST_TO_READ_EXTERNAL_STORAGE);
        }*/
        /*Intent intent = getIntent();
        String dir = intent.getStringExtra(EXTRA_LOCAL_DIRECTORY);
        LogUtils.d(TAG, "onResume: dir=" + dir);
        if (!TextUtils.isEmpty(dir) && (null != mViewPager)) {
            mViewPager.setCurrentItem(TAB_INDEX_LOCAL, true);
        }*/
        int mTargetDensity = Resources.getSystem().getDisplayMetrics().densityDpi;
        LogUtils.e(TAG,"mTargetDensity=:"+mTargetDensity);
        float etWidth = SystemUtils.dp2px(290);
        LogUtils.e(TAG, "etWidth=:" + etWidth);
        int mBackIconWidth = mTargetDensity * 120 / 480;
        LogUtils.e(TAG, "mBackIconWidth=:" + mBackIconWidth);
        float screenWidth = SystemUtils.getScreenWidth(this);
        LogUtils.e(TAG, "screenWidth=:" + screenWidth);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mSearchContainer.getLayoutParams();
        if (etWidth + mBackIconWidth + SystemUtils.dp2px(26) < screenWidth) {
            params.width = (int) etWidth;
        }
        mSearchContainer.setLayoutParams(params);
    }

    @Override
    protected void onPause() {
        super.onPause();
        OperateStatisticalData.onPause(this);
    }

    private void initContentView() {
        initViewPager();
        registerViewPager(mViewPager);
        initActionBar();
        mEtSearchInput.clearFocus();
    }

    private void initViewPager() {
        //mViewPager = (TouchlessViewPager)findViewById(R.id.vp_home);
        String[] tabTitles = new String[]{getString(R.string.tab_category), getString(R.string.tab_local)};
        mHomePagerAdapter = new HomePagerAdapter(mFragmentManager, TAB_COUNT_DEFAULT, tabTitles);
        mViewPager.setAdapter(mHomePagerAdapter);
        //mViewPager.setOffscreenPageLimit(TAB_COUNT_DEFAULT);
    }

    private void initActionBar() {
        //getActionBar().setCustomView(R.layout.actionbar_home_normal);
        //getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        //mViewPagerTabs = (ViewPagerTabs) getActionBar().getCustomView().findViewById(R.id.tab_pager_home);
        mViewPagerTabs = (ViewPagerTabs) findViewById(R.id.tab_pager_home);
        mViewPagerTabs.setViewPager(mViewPager);
        addOnPageChangeListener(mViewPagerTabs);
    }

    private void addOnPageChangeListener(ViewPager.OnPageChangeListener onPageChangeListener) {
        if (!mOnPageChangeListeners.contains(onPageChangeListener)) {
            mOnPageChangeListeners.add(onPageChangeListener);
        }
    }

    private void removeOnPageChangeListener(ViewPager.OnPageChangeListener onPageChangeListener) {
        if (!mOnPageChangeListeners.contains(onPageChangeListener)) {
            mOnPageChangeListeners.remove(onPageChangeListener);
        }
    }

    private void registerViewPager(@NonNull ViewPager viewPager) {
        viewPager.addOnPageChangeListener(this);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (null != mOnPageChangeListeners) {
            final int count = mOnPageChangeListeners.size();
            for (int i = 0; i < count; i++) {
                mOnPageChangeListeners.get(i).onPageScrolled(position, positionOffset,
                        positionOffsetPixels);
            }
        }
    }

    @Override
    public void onPageSelected(int position) {
        if (null != mOnPageChangeListeners) {
            final int count = mOnPageChangeListeners.size();
            for (int i = 0; i < count; i++) {
                mOnPageChangeListeners.get(i).onPageSelected(position);
            }
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        if (null != mOnPageChangeListeners) {
            final int count = mOnPageChangeListeners.size();
            for (int i = 0; i < count; i++) {
                mOnPageChangeListeners.get(i).onPageScrollStateChanged(state);
            }
        }
    }

    @Override
    public void finish() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            //imm.windowDismissed(getWindow().getDecorView().getWindowToken());
            ((Class.forName("android.view.inputmethod.InputMethodManager"))
                    .getMethod("windowDismissed", IBinder.class))
                    .invoke(imm, getWindow().getDecorView().getWindowToken());
            LogUtils.d(TAG, "finish: ");
        } catch (Throwable throwable) {
            LogUtils.e(TAG, "finish: Exception !!", throwable);
        }
        super.finish();
    }

    @Override
    public void onBackPressed() {
        try {
            if (null != mViewPager) {
                HomePagerAdapter homePagerAdapter = (HomePagerAdapter) mViewPager.getAdapter();
                boolean isHandled = homePagerAdapter.onBackPressed();
                if (!isHandled) {
                    super.onBackPressed();
                }
            } else {
                super.onBackPressed();
            }
        } catch (Exception e) {
            LogUtils.e(TAG, "onBackPressed: Exception !!", e);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        LogUtils.d(TAG, "onRequestPermissionsResult, requestCode:" + requestCode);
        if (permissions[0].equals(Manifest.permission.READ_EXTERNAL_STORAGE)&&
                PackageManager.PERMISSION_DENIED == grantResults[0]) {
            LogUtils.e(TAG, "**********onRequestPermissionsResult, Permission or grant res null*******");
            mToastHelper.showToast(R.string.no_permission_finish);
            finish();
            // TODO: 2017/4/21  
            return;
        }

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_TO_READ_EXTERNAL_STORAGE:
                if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    //通知分类页面刷新存储信息
                    Intent intent = new Intent();
                    intent.setAction(MY_REQUEST_TO_READ_EXTERNAL_STORAGE_PERMISSIONS_GRANTED);
                    sendBroadcast(intent);
                    LogUtils.d(TAG, "onRequestPermissionsResult: MY_PERMISSIONS_REQUEST_TO_READ_EXTERNAL_STORAGE PERMISSION_GRANTED");
                }
                break;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public int getMode() {
        return mCurrentMode;
    }

    public void switchMode(int mode) {
        LogUtils.d(TAG, "switchMode: mode=" + mode);
        mCurrentMode = mode;
        switch (mode) {
            case MODE_NORMAL:
                switchView(mViewPagerTabs);
                break;

            case MODE_SELECT:
                switchView(mLayoutActionModeSelect);
                break;

            case MODE_SEARCH:
                switchView(mLayoutActionModeSearch);
                break;

            case MODE_LOCAL:
                switchView(mLayoutActionModeLocal);
                break;

            default:
                LogUtils.e(TAG, "switchMode: Error!!");
                break;
        }
    }

    private void switchView(View selectedView) {
        if (null != selectedView) {
            selectedView.setVisibility(View.VISIBLE);

            if (selectedView != mLayoutActionModeSelect) {
                mLayoutActionModeSelect.setVisibility(View.GONE);
            }
            if (selectedView != mLayoutActionModeLocal) {
                mLayoutActionModeLocal.setVisibility(View.GONE);
            }
            if (selectedView != mViewPagerTabs) {
                mViewPagerTabs.setVisibility(View.GONE);
            }
            if (selectedView != mLayoutActionModeSearch) {
                mLayoutActionModeSearch.setVisibility(View.GONE);
            }
        }
    }

    public ImageView getImgLocalBack() {
        return mImgLocalBack;
    }

    public TextView getLocalTitle() {
        return mLocalTitle;
    }

    public ImageView getImgLocalOptions() {
        return mImgLocalOptions;
    }

    public ImageView getImgSearchBack() {
        return mImgSearchBack;
    }

    public EditText getEtSearchInput() {
        return mEtSearchInput;
    }

    public ImageView getImgSearch() {
        return mImgSearch;
    }

    public TextView getTvSelectAll() {
        return mTvSelectAll;
    }

    public TextView getTvSelectChosen() {
        return mTvSelectChosen;
    }

    public TextView getTvSelectCancel() {
        return mTvSelectCancel;
    }
}
