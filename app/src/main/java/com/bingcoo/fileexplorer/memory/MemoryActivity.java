package com.bingcoo.fileexplorer.memory;

import android.annotation.TargetApi;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.storage.StorageVolume;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bingcoo.fileexplorer.R;
import com.bingcoo.fileexplorer.adapter.MediaItemAdapter;
import com.bingcoo.fileexplorer.receiver.MountReceiver;
import com.bingcoo.fileexplorer.ui.ChartView;
import com.bingcoo.fileexplorer.util.ConstUtils;
import com.bingcoo.fileexplorer.util.FileCategoryHelper;
import com.bingcoo.fileexplorer.util.FileInfoComparator;
import com.bingcoo.fileexplorer.util.FileUtils;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.MountPointManager;
import com.bingcoo.fileexplorer.util.PermissionUtils;
import com.jakewharton.rxbinding.view.RxView;
import com.trello.rxlifecycle.components.RxActivity;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * 类名称：MemoryActivity
 * 作者：David
 * 内容摘要：
 * 创建日期：2017/2/17
 * 修改者， 修改日期， 修改内容
 */
public class MemoryActivity extends RxActivity implements MountReceiver.MountListener {
    private static final String TAG = "MemoryActivity";

    public static final String EXTRA_TITLE = "extra_title";
    public static final String EXTRA_PATH = "extra_path";
    public static final String EXTRA_AVAILABLE = "extra_available";
    public static final String EXTRA_TOTAL = "extra_total";

    private String mPath;
    private long mOtherSize = 0;
    private long mDirty = 0;
    private MountPointManager mMountPointManager = MountPointManager.getInstance();
    protected MountReceiver mMountReceiver = null;

    @BindView(R.id.img_dst_back)
    ImageView mImgBack;
    @BindView(R.id.tv_dst_title)
    TextView mTvTitle;

    @BindView(R.id.tv_available)
    TextView mTvAvailableSize;
    @BindView(R.id.tv_total)
    TextView mTvTotalSize;

    @BindView(R.id.tv_percent)
    TextView mTvPercent;
    @BindView(R.id.tv_dirty)
    TextView mTvDirty;
    @BindView(R.id.chart)
    ChartView mChart;

    @BindView(R.id.tv_picture_size)
    TextView mTvPictureSize;
    @BindView(R.id.tv_music_size)
    TextView mTvMusicSize;
    @BindView(R.id.tv_video_size)
    TextView mTvVideoSize;
    @BindView(R.id.tv_doc_size)
    TextView mTvDocSize;
    @BindView(R.id.tv_zip_size)
    TextView mTvZipSize;
    @BindView(R.id.tv_apk_size)
    TextView mTvApkSize;
    @BindView(R.id.tv_other_size)
    TextView mTvOtherSize;

    @BindView(R.id.tv_storage_management)
    TextView mTvStorageManagement;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memory);
        ButterKnife.bind(this);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!PermissionUtils.hasStorageReadPermission(this)) {
            finish();
        }
    }

    private void init() {
        // register unmount/mount Receiver
        if (null == mMountReceiver) {
            mMountReceiver = MountReceiver.registerMountReceiver(this);
            mMountReceiver.registerMountListener(this);
        }
        setViewClickListener(mImgBack,
                new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                });

        setViewClickListener(mTvStorageManagement, new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(android.provider.Settings.ACTION_MEMORY_CARD_SETTINGS);
                startActivity(intent);
            }
        });

        refreshCategoryCount();

        Intent intent = getIntent();
        if (null != intent) {
            String title = intent.getStringExtra(EXTRA_TITLE);
            if (!TextUtils.isEmpty(title)) {
                mTvTitle.setText(title);
            }

            mPath = intent.getStringExtra(EXTRA_PATH);
            LogUtils.d(TAG, "init: mPath=" + mPath);

            long available = intent.getLongExtra(EXTRA_AVAILABLE, 0);
            if (0 <= available) {
                String availableSize = getString(R.string.memory_available) + FileUtils.sizeToString(available).replaceAll("\\s*", "");
                mTvAvailableSize.setText(availableSize);
            }

            long total = intent.getLongExtra(EXTRA_TOTAL, 0);
            if (0 <= total) {
                String totalSize = "(" + getString(R.string.memory_total) + FileUtils.sizeToString(total).replaceAll("\\s*", "") + ")";
                mTvTotalSize.setText(totalSize);
            }

            mDirty = total - available;
            mTvDirty.setText(FileUtils.sizeToString(mDirty).replaceAll("\\s*", ""));
            float percent;
            if (total == 0) {
                percent = 0;
            } else {
                percent = 100f * mDirty / total;
            }
            NumberFormat numberFormat = NumberFormat.getNumberInstance();
            numberFormat.setMaximumFractionDigits(2);
            String percentStr = numberFormat.format(percent) + "%";
            mTvPercent.setText(percentStr);

            initCategorySize(mTvPictureSize);
            initCategorySize(mTvMusicSize);
            initCategorySize(mTvVideoSize);
            initCategorySize(mTvDocSize);
            initCategorySize(mTvZipSize);
            initCategorySize(mTvApkSize);
            initCategorySize(mTvOtherSize);
        }
    }

    private void initCategorySize(TextView tv) {
        if (null != tv) {
            tv.setText(FileUtils.sizeToString(0).replaceAll("\\s*", ""));
        }
    }

    public void setViewClickListener(View view, final Runnable runnable) {
        if ((null != view) && (null != runnable)) {
            RxView.clicks(view)
                    .throttleFirst(ConstUtils.THROTTLE_TIME, TimeUnit.MILLISECONDS)
                    .subscribe(new Action1<Void>() {
                        @Override
                        public void call(Void aVoid) {
                            runnable.run();
                        }
                    });
        }
    }

    public void refreshCategoryCount() {
        LogUtils.d(TAG, "refreshCategoryCount: ");
        class CategoryInfo {
            FileCategoryHelper.FileCategory category;
            long count;
        }

        Observable.OnSubscribe<List<CategoryInfo>> onSubscribe = new Observable.OnSubscribe<List<CategoryInfo>>() {

            @Override
            public void call(Subscriber<? super List<CategoryInfo>> subscriber) {
                try {
                    LogUtils.d(TAG, "call: in refreshCategoryCount()");
                    if (!subscriber.isUnsubscribed()) {
                        final List<CategoryInfo> categoryInfoList = new ArrayList<>();
                        addCategoryInfo(categoryInfoList, new CategoryInfo(), FileCategoryHelper.FileCategory.Picture);
                        addCategoryInfo(categoryInfoList, new CategoryInfo(), FileCategoryHelper.FileCategory.Music);
                        addCategoryInfo(categoryInfoList, new CategoryInfo(), FileCategoryHelper.FileCategory.Video);
                        addCategoryInfo(categoryInfoList, new CategoryInfo(), FileCategoryHelper.FileCategory.Doc);
                        addCategoryInfo(categoryInfoList, new CategoryInfo(), FileCategoryHelper.FileCategory.Zip);
                        addCategoryInfo(categoryInfoList, new CategoryInfo(), FileCategoryHelper.FileCategory.Apk);

                        subscriber.onNext(categoryInfoList);
                        subscriber.onCompleted();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                List<Long> list = new ArrayList<>();
                                long size = 0;
                                for (CategoryInfo info : categoryInfoList) {
                                    size += info.count;
                                    list.add(info.count);
                                }
                                long available = mMountPointManager.getMountPointFreeSpace(mPath);
                                long total = mMountPointManager.getMountPointTotalSpace(mPath);
                                String availableSize = getString(R.string.memory_available) + FileUtils.sizeToString(available).replaceAll("\\s*", "");
                                mTvAvailableSize.setText(availableSize);
                                String totalSize = "(" + getString(R.string.memory_total) + FileUtils.sizeToString(total).replaceAll("\\s*", "") + ")";
                                mTvTotalSize.setText(totalSize);
                                mDirty = total - available;
                                mOtherSize = mDirty - size;
                                mTvDirty.setText(FileUtils.sizeToString(mDirty).replaceAll("\\s*", ""));
                                mTvOtherSize.setText(FileUtils.sizeToString(mOtherSize).replaceAll("\\s*", ""));
                                float percent;
                                if (total == 0) {
                                    percent = 0;
                                } else {
                                    percent = 100f * mDirty / total;
                                }
                                NumberFormat numberFormat = NumberFormat.getNumberInstance();
                                numberFormat.setMaximumFractionDigits(2);
                                String percentStr = numberFormat.format(percent) + "%";
                                mTvPercent.setText(percentStr);
                                list.add(mOtherSize);
                                list.add(available);
                                mChart.setList(list);
                            }
                        });
                    }
                } catch (Exception e) {
                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onError(e);
                    }
                }
            }

            private void addCategoryInfo(List<CategoryInfo> categoryInfoList, CategoryInfo categoryInfo, FileCategoryHelper.FileCategory fileCategory) {
                categoryInfo.category = fileCategory;
                categoryInfo.count = query(fileCategory);
                categoryInfoList.add(categoryInfo);
            }
        };

        Subscriber<List<CategoryInfo>> subscriber = new Subscriber<List<CategoryInfo>>() {

            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                LogUtils.e(TAG, "onError: ", e);
            }

            @Override
            public void onNext(List<CategoryInfo> categoryInfos) {
                try {
                    LogUtils.d(TAG, "onNext: ");
                    if (null != categoryInfos) {
                        for (CategoryInfo info : categoryInfos) {
                            switch (info.category) {
                                case Picture:
                                    setViewText(mTvPictureSize, info.count);
                                    break;

                                case Music:
                                    setViewText(mTvMusicSize, info.count);
                                    break;

                                case Video:
                                    setViewText(mTvVideoSize, info.count);
                                    break;

                                case Doc:
                                    setViewText(mTvDocSize, info.count);
                                    break;

                                case Zip:
                                    setViewText(mTvZipSize, info.count);
                                    break;

                                case Apk:
                                    setViewText(mTvApkSize, info.count);
                                    break;

                                default:
                                    LogUtils.w(TAG, "onNext: default !!!");
                                    break;
                            }
                        }
                    }
                } catch (Exception e) {
                    LogUtils.e(TAG, "onNext: ", e);
                }
            }

            private void setViewText(TextView tv, long count) {
                if (null != tv) {
                    tv.setText(FileUtils.sizeToString(count).replaceAll("\\s*", ""));
                }
            }
        };

        Observable.create(onSubscribe)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(this.<List<CategoryInfo>>bindToLifecycle())
                .subscribe(subscriber);

    }

    private long query(FileCategoryHelper.FileCategory fc) {
        int cnt;
        long size = 0;
        FileCategoryHelper helper = new FileCategoryHelper(this);
        Cursor cursor = helper.query(fc, FileInfoComparator.SORT_BY_NAME);
        if (null != cursor) {
            cnt = cursor.getCount();

            MediaItemAdapter itemAdapter = new MediaItemAdapter(cursor);
            for (int i = 0; i < cnt; i++) {
                itemAdapter.getItem(i);
                String mountPointPath = mMountPointManager.getRealMountPointPath(itemAdapter.getFilePath());
                if (mPath.equals(mountPointPath)) {
                    size += cursor.getLong(FileCategoryHelper.COLUMN_SIZE);
                }
            }

            cursor.close();
        }

        return size;
    }

    @Override
    public void onMounted(String mountPoint) {
        if (mPath.equals(mountPoint)) {
            refreshCategoryCount();
        }
    }

    @TargetApi(24)
    @Override
    public void onUnMounted(StorageVolume volume) {
        String unMountPoint = volume.getPath();
        LogUtils.e(TAG, "onUnMounted***********unMountPoint=" + unMountPoint);
        LogUtils.e(TAG, "onUnMounted***********mPath=" + mPath);
        if (mPath.equals(unMountPoint)) {
            finish();
        }
    }

    @Override
    public void onEjected(String unMountPoint) {
        LogUtils.e(TAG, "onEjected***********unMountPoint=" + unMountPoint);
        LogUtils.e(TAG, "onEjected***********mPath=" + mPath);
        if (mPath.equals(unMountPoint)) {
            finish();
        }
    }

    @Override
    public void onSdSwap() {

    }

    @Override
    protected void onDestroy() {
        if (null != mMountReceiver) {
            mMountReceiver.unregisterMountListener(this);
            unregisterReceiver(mMountReceiver);
            mMountReceiver = null;
        }
        super.onDestroy();
    }
}
