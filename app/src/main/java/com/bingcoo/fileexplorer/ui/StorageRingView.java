package com.bingcoo.fileexplorer.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;

import com.bingcoo.fileexplorer.R;
import com.bingcoo.fileexplorer.util.LogUtils;

/**
 * 类名称：StorageView
 * 作者：David
 * 内容摘要：说明主要功能。
 * 创建日期：2016/11/29
 * 修改者， 修改日期， 修改内容
 */
public class StorageRingView extends View {
    private static final String TAG = "StorageRingView";

    static {
        LogUtils.setDebug(TAG, true);
    }

    private int mBgColor = Color.rgb(141, 141, 141);
    private int mFgColor =  Color.rgb(141, 141, 0);
    private int mStartAngle = 180;
    private int mSweepAngle = 180;
    private int mThickness = 10;
    private int mOuterRadius = 20;
    private int mPercent = 0;
    private Paint mPaint = new Paint();
    private Bitmap mBitmapBg = null;
    private RectF mRectFBg = new RectF();

    public StorageRingView(Context context) {
        super(context);
    }

    public StorageRingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttrs(attrs);
    }

    public StorageRingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttrs(attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public StorageRingView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initAttrs(attrs);
    }

    private void initAttrs(AttributeSet attrs) {
        if (null != attrs) {
            TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.StorageRingView);
            if (null != ta) {
                mBgColor = ta.getColor(R.styleable.StorageRingView_bgColor, mBgColor);
                mFgColor =  ta.getColor(R.styleable.StorageRingView_fgColor, mFgColor);
                mStartAngle = ta.getInt(R.styleable.StorageRingView_startAngle, mStartAngle);
                mSweepAngle = ta.getInt(R.styleable.StorageRingView_sweepAngle, mSweepAngle);
                mThickness = ta.getDimensionPixelSize(R.styleable.StorageRingView_thickness, mThickness);
                mOuterRadius = ta.getDimensionPixelSize(R.styleable.StorageRingView_outerRadius, mOuterRadius);
                mPercent = ta.getInt(R.styleable.StorageRingView_percent, mPercent);
                LogUtils.d(TAG, "initAttrs: mStartAngle=" + mStartAngle
                     + ", mSweepAngle=" + mSweepAngle
                     + ", mThickness=" + mThickness
                     + ", mOuterRadius=" + mOuterRadius
                     + ", mPercent=" + mPercent);
            }
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        LogUtils.d(TAG, "onSizeChanged: w=" + w + ", h=" + h + ", oldw=" + oldw + ", oldh=" + oldh);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        setMeasuredDimension(measureDimension(widthMeasureSpec, true),
                             measureDimension(heightMeasureSpec, false));
    }

    private int measureDimension(int measureSpec, boolean isWidth) {
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        int padding = mThickness / 2;
        int value = 0;
        switch (specMode) {
            case MeasureSpec.AT_MOST:
                LogUtils.d(TAG, "measureDimension: MeasureSpec.AT_MOST size=" + specSize
                           + ", mOuterRadius=" + mOuterRadius + ", isWidth=" + isWidth);
                value = isWidth ? Math.min(specSize, mOuterRadius * 2) : Math.min(specSize, mOuterRadius * 1 + padding);
                break;

            case MeasureSpec.EXACTLY:
                LogUtils.d(TAG, "measureDimension: MeasureSpec.EXACTLY size=" + specSize
                           + ", mOuterRadius=" + mOuterRadius + ", isWidth=" + isWidth);
                value = specSize;
                break;
            
            case MeasureSpec.UNSPECIFIED:
                LogUtils.d(TAG, "measureDimension: MeasureSpec.UNSPECIFIED size=" + specSize
                           + ", mOuterRadius=" + mOuterRadius + ", isWidth=" + isWidth);
                value = isWidth ? mOuterRadius * 2 : mOuterRadius * 1 + padding;
                break;

            default:
                LogUtils.e(TAG, "measureDimension: specMode ERROR !!");
                break;
        }
        return value;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float padding = mThickness / 2.0f;
        mRectFBg = new RectF(getMeasuredWidth() / 2 - mOuterRadius + padding
                , getMeasuredWidth() / 2 - mOuterRadius + padding
                , getMeasuredWidth() / 2 + mOuterRadius - padding
                , getMeasuredWidth() / 2 + mOuterRadius - padding);

        canvas.save();
        drawBg(canvas, mPaint);
        drawPercent(canvas, mPaint);
        canvas.restore();
    }

    public int getBgColor() {
        return mBgColor;
    }

    public void setBgColor(int bgColor) {
        mBgColor = bgColor;
        postInvalidate();
    }

    public int getFgColor() {
        return mFgColor;
    }

    public void setFgColor(int fgColor) {
        mFgColor = fgColor;
        postInvalidate();
    }

    public int getStartAngle() {
        return mStartAngle;
    }

    public void setStartAngle(int startAngle) {
        mStartAngle = startAngle;
        postInvalidate();
    }

    public int getSweepAngle() {
        return mSweepAngle;
    }

    public void setSweepAngle(int sweepAngle) {
        mSweepAngle = sweepAngle;
        postInvalidate();
    }

    public int getThickness() {
        return mThickness;
    }

    public void setThickness(int thickness) {
        mThickness = thickness;
        postInvalidate();
    }

    public int getOuterRadius() {
        return mOuterRadius;
    }

    public void setOuterRadius(int outerRadius) {
        mOuterRadius = outerRadius;
        postInvalidate();
    }

    public int getPercent() {
        return mPercent;
    }

    public void setPercent(int percent) {
        mPercent = percent;
        postInvalidate();
    }

    private void drawBg(Canvas canvas, Paint paint) {
        paint.setAntiAlias(true);
        canvas.drawBitmap(getBitmapBg(paint), 0, 0, paint);
    }

    private void drawPercent(Canvas canvas, Paint paint) {
        if ((null!=canvas) && (null!=paint) && (0 < mPercent)) {
            paint.reset();
            paint.setAntiAlias(true);
            paint.setStrokeWidth(mThickness);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeCap(Paint.Cap.ROUND);
            paint.setStrokeJoin(Paint.Join.ROUND);

            Path pathProgress = new Path();
            LogUtils.d(TAG, "drawPercent: mSweepAngle=" + mSweepAngle + ", mPercent=" + mPercent);
            pathProgress.addArc(mRectFBg, mStartAngle, mSweepAngle / 100f * mPercent);
            paint.setColor(mFgColor);
            canvas.drawPath(pathProgress, paint);
        }
    }

    private Bitmap getBitmapBg(Paint paint) {
        if (mBitmapBg == null) {
            mBitmapBg = Bitmap.createBitmap(getMeasuredWidth(), getMeasuredHeight(), Bitmap.Config.ARGB_8888);
            setBitmapBg(paint, mBitmapBg);
        }
        return mBitmapBg;
    }

    private void setBitmapBg(Paint paint, Bitmap bitmap) {
        if ((null!=bitmap) && (null!=paint)) {
            Canvas canvas = new Canvas(bitmap);
            paint.reset();
            paint.setAntiAlias(true);
            paint.setStrokeWidth(mThickness);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeCap(Paint.Cap.ROUND);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setColor(mBgColor);

            Path pathBg = new Path();
            pathBg.addArc(mRectFBg, mStartAngle, mSweepAngle);
            canvas.drawPath(pathBg, paint);

            /*paint.reset();
            paint.setAntiAlias(true);
            paint.setColor(mFgColor);
            paint.setStyle(Paint.Style.STROKE);
            float padding = 0.0f;
            RectF r = new RectF(getMeasuredWidth() / 2 - mOuterRadius + padding
                    , getMeasuredHeight() / 2 - mOuterRadius + padding
                    , getMeasuredWidth() / 2 + mOuterRadius - padding
                    , getMeasuredHeight() / 2 + mOuterRadius - padding);
            canvas.drawRect(r, paint);*/
        }

    }

}
