package com.bingcoo.fileexplorer.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import com.bingcoo.fileexplorer.R;

/**
 * 类名称：OptionDialog
 * 作者：David
 * 内容摘要：
 * 创建日期：2017/1/4
 * 修改者， 修改日期， 修改内容
 */
public class OptionDialog extends Dialog {
    private static final String TAG = "OptionDialog";

    public OptionDialog(Context context) {
        super(context, R.style.dialogStyle);
    }

    public OptionDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    public OptionDialog(Context context, boolean cancelable, Message cancelCallback) {
        super(context, cancelable, cancelCallback);
    }

    public OptionDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Window dialogWindow = getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        lp.x = getContext().getResources().getDimensionPixelSize(R.dimen.option_dialog_x);
        lp.y = getContext().getResources().getDimensionPixelSize(R.dimen.option_dialog_y);
        lp.gravity = Gravity.RIGHT | Gravity.TOP;
        dialogWindow.setAttributes(lp);
    }
}
