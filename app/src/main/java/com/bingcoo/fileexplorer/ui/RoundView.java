package com.bingcoo.fileexplorer.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;

/**
 * 类名称：RoundView
 * 作者：David
 * 内容摘要：说明主要功能。
 * 创建日期：2016/11/24
 * 修改者， 修改日期， 修改内容
 */
public class RoundView extends View {
    private static final String TAG = "RoundView";

    private int mRoundColor = Color.argb(255, 0, 255, 255);
    private int mTextColor = Color.rgb(118, 181, 66);
    private int mWidth;
    private int mHeight;
    private int mPaddingX;
    private Paint mPaint = new Paint();
    private float mPencentTextSize = 65;

    public RoundView(Context context) {
        super(context);
    }

    public RoundView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RoundView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public RoundView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mWidth = getWidth();
        mHeight = getHeight();

        if(mWidth > mHeight){
            mPaddingX = (mWidth-mHeight)/2;
            mWidth = mHeight;
        }

        int halfWidth = (mWidth - mPaddingX*2) / 2;
        mPaint.setAntiAlias(true); // 消除锯齿
        //mPaint.setStyle(Paint.Style.FILL);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeWidth((float) 13.0);              //线宽
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(mRoundColor);
        RectF oval = new RectF(new Rect(mPaddingX, 0, halfWidth * 2 + mPaddingX, halfWidth * 2));
        canvas.drawArc(oval, 0, 60, false, mPaint);

        mPaint.reset();
        mPaint.setTextSize(mPencentTextSize);
        mPaint.setColor(mTextColor);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setTextAlign(Paint.Align.CENTER);
        String number = 70+" %";
        canvas.drawText(number, mWidth/2+mPaddingX, mHeight/2+mPencentTextSize/3, mPaint);
    }
}
