package com.bingcoo.fileexplorer.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import com.bingcoo.fileexplorer.R;
import com.bingcoo.fileexplorer.util.LogUtils;

/**
 * 类名称：TextViewEx
 * 作者：David
 * 内容摘要：
 * 创建日期：2016/12/30
 * 修改者， 修改日期， 修改内容
 */
public class TabTextView extends android.support.v7.widget.AppCompatTextView {
    private static final String TAG = "TabTextView";

    static {
        LogUtils.setDebug(TAG, true);
    }

    public TabTextView(Context context) {
        super(context);
    }

    public TabTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TabTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void drawableStateChanged() {
        LogUtils.e(TAG, "drawableStateChanged: isPressed=" + isPressed()
                + ", isSelected=" + isSelected()
                + ", text=" + getText()
                + ", pressedSize=" + getResources().getDimensionPixelSize(R.dimen.pressed_tab_text_size)
                + ", normalSize=" + getResources().getDimensionPixelSize(R.dimen.tab_text_size));
        setPadding(0, getResources().getDimensionPixelSize(R.dimen.dp_top), 0, 0);
//        if (isPressed()) {
//            setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.pressed_tab_text_size));
//        } else {
//            setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.tab_text_size));
//        }
        Drawable drawable;
//        int color = isSelected() ? ContextCompat.getColor(getContext(), R.color.background) :
//                ContextCompat.getColor(getContext(), R.color.bingo_theme_color);
//        Drawable tintDrawable=TintUtils.tint(drawable, color);
//        setCompoundDrawablesWithIntrinsicBounds(null, null, null, tintDrawable);
        if (isSelected()) {
            drawable = ContextCompat.getDrawable(getContext(),R.drawable.tab_bottom_triangle);
        } else {
            drawable = ContextCompat.getDrawable(getContext(),R.drawable.tab_bottom_triangle_blue);
        }
        setCompoundDrawablesWithIntrinsicBounds(null, null, null, drawable);
        super.drawableStateChanged();
    }
}
