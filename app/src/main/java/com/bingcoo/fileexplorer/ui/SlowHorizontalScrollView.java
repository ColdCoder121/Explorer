package com.bingcoo.fileexplorer.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;
import android.widget.Scroller;

import com.bingcoo.fileexplorer.util.LogUtils;

/**
 * 类名称：SlowHorizontalScrollView
 * 作者：David
 * 内容摘要：
 * 创建日期：2017/1/3
 * 修改者， 修改日期， 修改内容
 */
public class SlowHorizontalScrollView extends HorizontalScrollView {
    private static final String TAG = "SlowHorizontalScrollVie";
    private static final int SCROLL_DURATION = 2000;
    private final Scroller mScroller = new Scroller(getContext());

    public SlowHorizontalScrollView(Context context) {
        super(context);
    }

    public SlowHorizontalScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SlowHorizontalScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SlowHorizontalScrollView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void startHorizontalScroll(int startX, int dx) {
        LogUtils.d(TAG, "start scroll");
        mScroller.startScroll(startX, 0, dx, 0, SCROLL_DURATION);
        invalidate();
    }

    @Override
    public void computeScroll() {
        if (mScroller.computeScrollOffset()) {
            scrollTo(mScroller.getCurrX(), 0);
            postInvalidate();
        }
        super.computeScroll();
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        mScroller.abortAnimation();
        return super.onTouchEvent(ev);
    }

}
