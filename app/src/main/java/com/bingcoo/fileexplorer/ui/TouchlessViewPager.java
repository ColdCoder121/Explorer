package com.bingcoo.fileexplorer.ui;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class TouchlessViewPager extends ViewPager {
    
    private boolean mIsTouchless;

    public TouchlessViewPager(Context context) {
        super(context);
    }

    public TouchlessViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    
    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return !mIsTouchless && super.onInterceptTouchEvent(event);
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return !mIsTouchless && super.onTouchEvent(event);
    }
    
    @Override
    protected boolean canScroll(View arg0, boolean arg1, int arg2, int arg3,
            int arg4) {
        return !mIsTouchless && super.canScroll(arg0, arg1, arg2, arg3, arg4);
    }
    
    @Override
    public boolean canScrollHorizontally(int direction) {
        return !mIsTouchless && super.canScrollHorizontally(direction);
    }
    
    public void setIsTouchlessEnable(boolean touchless) {
        mIsTouchless = touchless;
    }
    
}
