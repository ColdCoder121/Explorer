package com.bingcoo.fileexplorer.ui.ring;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;

import com.bingcoo.fileexplorer.R;

import java.util.ArrayList;
import java.util.List;

public class RingProgress extends View {
    private static final String TAG = "RingProgress";
    private Paint mPaint;
    private Bitmap mBitmapBg;
    private Paint mPaintText;
    private int mStartAngle = 0;
    private int mSweepAngle = 180;
    private int mPadding;
    private int mWidth;
    private int mRingWidth = 0;
    private int mRotateAngle = 270;
    private int mBgShadowColor = Color.argb(100, 0, 0, 0);
    private int mBgColor = Color.rgb(141, 141, 141);
    private int mForeColor= Color.rgb(141, 141, 141);

    private List<Ring> mListRing = new ArrayList<>();
    private RectF mRectFBg = new RectF();
    private boolean mIsCorner = true;
    private boolean mIsDrawBg = true;
    private boolean mIsDrawBgShadow = true;
    private float mRingWidthScale = 0f;
    private boolean mBgChange = false;

    public int getSweepAngle() {
        return mSweepAngle;
    }

    public void setSweepAngle(int sweepAngle) {
        if (sweepAngle < 0) {
            sweepAngle = 0;
        } else if (sweepAngle > 360) {
            sweepAngle = 360;
        }
        mSweepAngle = sweepAngle;
        mBgChange = true;
        invalidate();
    }

    public int getRotateAngle() {
        return mRotateAngle;
    }

    public void setRotateAngle(int rotateAngle) {
        if (rotateAngle < 0)
            rotateAngle = 0;
        else if (rotateAngle > 360) {
            rotateAngle = 360;
        }

        this.mRotateAngle = rotateAngle;
        invalidate();
    }

    public boolean isCorner() {
        return mIsCorner;
    }

    public void setCorner(boolean corner) {
        mIsCorner = corner;
        mBgChange = true;
        invalidate();
    }

    public boolean isDrawBgShadow() {
        return mIsDrawBgShadow;
    }

    public void setDrawBgShadow(boolean drawBgShadow) {
        mIsDrawBgShadow = drawBgShadow;
        mBgChange = true;
        invalidate();
    }

    public void setDrawBgShadow(boolean drawBgShadow, int color) {
        mIsDrawBgShadow = drawBgShadow;
        this.mBgShadowColor = color;
        mBgChange = true;
        invalidate();
    }

    public boolean isDrawBg() {
        return mIsDrawBg;
    }

    public void setDrawBg(boolean drawBg) {
        mIsDrawBg = drawBg;
        mBgChange = true;
        invalidate();
    }

    public void setDrawBg(boolean drawBg, int color) {
        mIsDrawBg = drawBg;
        this.mBgColor = color;
        mBgChange = true;
        invalidate();
    }

    public List<Ring> getmListRing() {
        return mListRing;
    }

    public void setmListRing(List<Ring> mListRing) {
        this.mListRing = mListRing;
    }

    public float getRingWidthScale() {
        return mRingWidthScale;
    }

    public void setRingWidthScale(float ringWidthScale) {
        this.mRingWidthScale = ringWidthScale;
        mBgChange = true;
        invalidate();
    }

    public void setData(List<Ring> mListRing, int time) {
        this.mListRing.clear();
        for (int i = 0; i < mListRing.size(); i++) {
            RectF r = new RectF();
            r.top = mRectFBg.top + mRingWidth * i;
            r.bottom = mRectFBg.bottom - mRingWidth * i;
            r.left = mRectFBg.left + mRingWidth * i;
            r.right = mRectFBg.right - mRingWidth * i;
            mListRing.get(i).setRectFRing(r);
        }

        for (int i = 0; i < mListRing.size(); i++) {
            this.mListRing.add(mListRing.get(i));
        }
        if (time > 0) {
            startAnim(time);
        } else {
            invalidate();
        }

    }


    public RingProgress(Context context) {
        this(context, null);
    }

    public RingProgress(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RingProgress(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }


    private void init(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.RingProgress);
        if (typedArray != null) {
            mIsCorner = typedArray.getBoolean(R.styleable.RingProgress_showRingCorner, false);
            mIsDrawBg = typedArray.getBoolean(R.styleable.RingProgress_showBackground, false);
            mIsDrawBgShadow = typedArray.getBoolean(R.styleable.RingProgress_showBackgroundShadow, false);
            mRotateAngle = typedArray.getInt(R.styleable.RingProgress_rotate, 0);
            mRingWidthScale = typedArray.getFloat(R.styleable.RingProgress_ringWidthScale, 1.0f);
            mBgShadowColor = typedArray.getColor(R.styleable.RingProgress_bgShadowColor, mBgShadowColor);
            //mBgColor = typedArray.getColor(R.styleable.RingProgress_bgColor, mBgColor);
            mForeColor = typedArray.getColor(R.styleable.RingProgress_foregroundColor, mForeColor);
            mStartAngle = typedArray.getInt(R.styleable.RingProgress_ringStartAngle, 0);
            mSweepAngle = typedArray.getInt(R.styleable.RingProgress_ringSweepAngle, 180);
            mRingWidth = typedArray.getDimensionPixelSize(R.styleable.RingProgress_ringWidth, 10);
            Log.d(TAG, "init: mStartAngle=" + mStartAngle + ", mSweepAngle=" + mSweepAngle + ", mRingWidth=" + mRingWidth);
            typedArray.recycle();
        }

        initPaint();
    }


    private void initPaint() {
        mPaint = new Paint();

        mPaintText = new Paint();
        mPaintText.setAntiAlias(true);
        mPaintText.setStyle(Paint.Style.FILL);
        mPaintText.setColor(Color.WHITE);
    }


    private void setBitmapBg(Paint paint, Bitmap mBitmapBg) {
        Canvas canvas = null;
        canvas = new Canvas(mBitmapBg);

        for (int i = 0; i < mListRing.size(); i++) {
            paint.reset();
            paint.setAntiAlias(true);
            paint.setStrokeWidth(mRingWidth);
            paint.setStyle(Paint.Style.STROKE);
            if (mIsCorner) {
                paint.setStrokeCap(Paint.Cap.ROUND);
                paint.setStrokeJoin(Paint.Join.ROUND);
            }

           /* int red = (mBgColor & 0xff0000) >> 16;
            int green = (mBgColor & 0x00ff00) >> 8;
            int blue = (mBgColor & 0x0000ff);
            int colorvaluer = red + (255 - red) / mListRing.size() * i;
            int colorvalueg = green + (255 - green) / mListRing.size() * i;
            int colorvalueb = blue + (255 - blue) / mListRing.size() * i;

            paint.setColor(Color.rgb(colorvaluer, colorvalueg, colorvalueb));*/
            paint.setColor(mBgColor);

            Path pathBg = new Path();
            RectF r = new RectF();
            r.top = mRectFBg.top + mRingWidth * i;
            r.bottom = mRectFBg.bottom - mRingWidth * i;
            r.left = mRectFBg.left + mRingWidth * i;
            r.right = mRectFBg.right - mRingWidth * i;
            mListRing.get(i).setRectFRing(r);

            pathBg.addArc(r, mStartAngle, mSweepAngle);

            if (i == 0 && mIsDrawBgShadow) {
                paint.setShadowLayer(mRingWidth / 3,
                                     0 - mRingWidth / 4,
                                     0, mBgShadowColor);
            }
            if (mIsDrawBg)
                canvas.drawPath(pathBg, paint);

        }
        mBgChange = false;

    }


    private Bitmap getmBitmapBg(Paint paint) {
        if (mBitmapBg == null) {
            mBitmapBg = Bitmap.createBitmap(getMeasuredWidth(), getMeasuredHeight(), Bitmap.Config.ARGB_8888);
            setBitmapBg(paint, mBitmapBg);
        }

        if (mBgChange) {
            mBitmapBg = Bitmap.createBitmap(getMeasuredWidth(), getMeasuredHeight(), Bitmap.Config.ARGB_8888);
            setBitmapBg(paint, mBitmapBg);
        }

        return mBitmapBg;
    }


    private void drawBg(Canvas canvas, Paint paint) {
        paint.setAntiAlias(true);
        canvas.drawBitmap(getmBitmapBg(paint), 0, 0, paint);
    }


    private void drawProgress(Canvas canvas, Paint paint) {
        for (int i = 0; i < mListRing.size(); i++) {
            paint.reset();
            paint.setAntiAlias(true);
            paint.setStrokeWidth(mRingWidth);
            paint.setStyle(Paint.Style.STROKE);
            Path pathProgress = new Path();
            pathProgress.addArc(mListRing.get(i).getRectFRing(), mStartAngle,
                    (int) (mSweepAngle / 100f * mListRing.get(i).getProgress() * mAnimatedValue)
            );

           /* Shader mShader = new LinearGradient(mListRing.get(i).getRectFRing().left, mListRing.get(i).getRectFRing().top,
                                                mListRing.get(i).getRectFRing().left, mListRing.get(i).getRectFRing().bottom,
                                                new int[]{
                            mListRing.get(i).getStartColor(),
                            mListRing.get(i).getEndColor()

                    }, new float[]{0f, 1f},
                                                Shader.TileMode.CLAMP);

            paint.setShader(mShader);*/
            if (mIsCorner) {
                paint.setStrokeCap(Paint.Cap.ROUND);
                paint.setStrokeJoin(Paint.Join.ROUND);
            }
            paint.setColor(mForeColor);
            canvas.drawPath(pathProgress, paint);
            /*paint.setShader(null);

            mPaintText.setTextSize(mPaint.getStrokeWidth() / 2);
            String textvalue = String.valueOf(mListRing.get(i).getValue());
            float arc_length = (float) (Math.PI * mListRing.get(i).getRectFRing().width()
                                        * (mListRing.get(i).getProgress() / 100f)) * (mSweepAngle / 360f);

            float textvalue_length = getFontlength(mPaintText, textvalue);
            if (mAnimatedValue == 1) {
                if (arc_length - textvalue_length * 1.5f <= 0) {
                    float textvalue_length_one = textvalue_length * 1.0f / textvalue.length();
                    int textvalue_size = (int) (arc_length / textvalue_length_one);
                    if (textvalue_size >= textvalue.length()) {
                        canvas.drawTextOnPath(textvalue, pathProgress, 10,
                                getFontHeight(mPaintText) / 3, mPaintText);
                    } else {
                        String text = textvalue.substring(0, 1);
                        for (int j = 0; j < textvalue_size; j++) {
                            text = text + ".";
                        }

                        canvas.drawTextOnPath(text, pathProgress, 10,
                                getFontHeight(mPaintText) / 3, mPaintText);
                    }
                } else {
                    canvas.drawTextOnPath(textvalue, pathProgress,
                                          (float) (arc_length - textvalue_length * 1.5f),
                                            getFontHeight(mPaintText) / 3, mPaintText);
                }
            }

            String text = String.valueOf(mListRing.get(i).getName());
            float textlength = getFontlength(mPaintText, text);
            float textlength_one = textlength * 1.0f / text.length();
            float showtextlength = (float) (arc_length
                    - textvalue_length * 1.8f);
            if (showtextlength < 0)
                showtextlength = 0;

            float textsize = showtextlength / textlength_one;
            if (textsize > text.length()) {
                textsize = text.length();
            } else if (textsize < 1) {
                textsize = 0;
            }

            canvas.drawTextOnPath(text.substring(0, (int) textsize), pathProgress,
                                  10, getFontHeight(mPaintText) / 3, mPaintText);*/
        }

    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        canvas.rotate(mRotateAngle, getMeasuredWidth() / 2f, getMeasuredHeight() / 2f);
        canvas.save();

       /* if (mListRing.size() > 0)
            mRingWidth = (int) (
                    mWidth / 2f / (mListRing.size() + 0.5f) * (1 - mRingWidthScale));

        mRingWidth = 20;*/
        mPadding = mRingWidth;
        mRectFBg = new RectF(getMeasuredWidth() / 2 - mWidth / 2 + mPadding
                , getMeasuredHeight() / 2 - mWidth / 2 + mPadding
                , getMeasuredWidth() / 2 + mWidth / 2 - mPadding
                , getMeasuredHeight() / 2 + mWidth / 2 - mPadding);

        drawBg(canvas, mPaint);
        drawProgress(canvas, mPaint);
        canvas.restore();

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int widthSpecMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSpecSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSpecMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSpecSize = MeasureSpec.getSize(heightMeasureSpec);
        if (widthSpecMode == MeasureSpec.AT_MOST
                && heightSpecMode == MeasureSpec.AT_MOST) {
            setMeasuredDimension(dip2px(30), dip2px(30));
        } else if (widthSpecMode == MeasureSpec.AT_MOST) {
            setMeasuredDimension(heightSpecSize, heightSpecSize);
        } else if (heightSpecMode == MeasureSpec.AT_MOST) {
            setMeasuredDimension(widthSpecSize, widthSpecSize);
        }
        if (getMeasuredWidth() > getHeight())
            mWidth = getMeasuredHeight();
        else
            mWidth = getMeasuredWidth();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (w >h) {
            mWidth = h;
        } else {
            mWidth = w;
        }
    }


    public float getFontlength(Paint paint, String str) {
        return paint.measureText(str);
    }

    public float getFontHeight(Paint paint) {
        Paint.FontMetrics fm = paint.getFontMetrics();
        return fm.descent - fm.ascent;
    }

    public int dip2px(float dpValue) {
        final float scale = getContext().getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }


    public void startAnim(int time) {
        stopAnim();
        startViewAnim(0f, 1f, time);
    }

    private ValueAnimator valueAnimator;
    private float mAnimatedValue = 1f;

    public void stopAnim() {
        if (valueAnimator != null) {
            clearAnimation();
            valueAnimator.setRepeatCount(0);
            valueAnimator.cancel();
            mAnimatedValue = 0f;
            postInvalidate();
        }
    }


    private ValueAnimator startViewAnim(float startF, final float endF, long time) {
        valueAnimator = ValueAnimator.ofFloat(startF, endF);
        valueAnimator.setDuration(time);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.setRepeatCount(0);
        valueAnimator.setRepeatMode(ValueAnimator.RESTART);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                mAnimatedValue = (float) valueAnimator.getAnimatedValue();
                invalidate();
            }
        });

        valueAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
            }

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                super.onAnimationRepeat(animation);
            }
        });

        if (!valueAnimator.isRunning()) {
            valueAnimator.start();
        }

        return valueAnimator;
    }

}
