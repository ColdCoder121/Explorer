package com.bingcoo.fileexplorer.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

/**
 * 作者：zhshh
 * 内容摘要：
 * 创建日期：2017/5/9
 * 修改者， 修改日期， 修改内容
 */

public class MyExceptionImageView extends ImageView{


    public MyExceptionImageView(Context context) {
        super(context);
    }

    public MyExceptionImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyExceptionImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MyExceptionImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        try {
            super.onDraw(canvas);
        }catch (Exception e){
            e.printStackTrace();
            Log.e("Exception",e.toString());
        }
    }
}
