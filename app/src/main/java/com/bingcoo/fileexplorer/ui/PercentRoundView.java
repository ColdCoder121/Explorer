package com.bingcoo.fileexplorer.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * 类名称：PercentRoundView
 * 作者：David
 * 内容摘要：说明主要功能。
 * 创建日期：2016/11/24
 * 修改者， 修改日期， 修改内容
 */
public class PercentRoundView extends View {
    private static final String TAG = "PercentRoundView";

    private int mWidth;
    private int mHeight;
    private int mPaddingX;
    private Paint mPaint = new Paint();
    private float mPencentTextSize = 65;
    private List<Float> mPercentList = new ArrayList<Float>() {{
        add(30f);
    }};
    private int mRoundColor = Color.argb(255, 255, 255, 255);
    private int mTextColor = Color.rgb(118, 181, 66);
    private Float mMax = 100f;

    public PercentRoundView(Context context) {
        super(context);
    }

    public PercentRoundView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PercentRoundView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public PercentRoundView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mWidth = getWidth();
        mHeight = getHeight();

        if(mWidth > mHeight){
            mPaddingX = (mWidth-mHeight)/2;
            mWidth = mHeight;
        }

        int halfWidth = (mWidth - mPaddingX*2) / 2;
        mPaint.setAntiAlias(true); // 消除锯齿
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(mTextColor);
        RectF oval = new RectF(new Rect(mPaddingX, 0, halfWidth * 2 + mPaddingX, halfWidth * 2));
        canvas.drawArc(oval, 0, 360, true, mPaint);

        if (null != mPercentList) {
            mPaint.setColor(Color.rgb(0, 88, 0));
            float startAngle = 0;
            float sweepAngle = 0;
            for (Float i : mPercentList) {
                startAngle += sweepAngle;
                sweepAngle =  i / mMax * 360;
                canvas.drawArc(oval, startAngle, sweepAngle, true, mPaint);
            }
        }

        mPaint.setColor(mRoundColor);
        int diff = 10;
        oval = new RectF(new Rect(mPaddingX+diff, diff, halfWidth * 2 + mPaddingX-diff, halfWidth * 2-diff));
        canvas.drawArc(oval, 0, 360, false, mPaint);
    }

    public void setPencents(List<Float> list) {
        if (null != list) {
            mPercentList = list;
        }
    }

    public void setMax(Float max) {
        mMax = max;
    }

}
