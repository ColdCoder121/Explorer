package com.bingcoo.fileexplorer.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.bingcoo.fileexplorer.R;
import com.bingcoo.fileexplorer.util.LogUtils;

import java.math.BigDecimal;
import java.util.List;

/**
 * 类名称：ChartView
 * 作者：David
 * 内容摘要：
 * 创建日期：2017/2/17
 * 修改者， 修改日期， 修改内容
 */
public class ChartView extends View {
    private static final String TAG = "ChartView";
    static {
        LogUtils.setDebug(TAG, true);
    }

    private List<Long> mList;
    private Long mTotal = new Long(1);
    private int mThickness = 10;
    private int mOuterDiameter;
    private int mInnerDiameter;
    private float   mStartAngle = 270;
    private Paint mPaint      = new Paint();
    private RectF mRectF;


    private static final int INDEX_IMAGE = 0;
    private static final int INDEX_AUDIO = 1;
    private static final int INDEX_VIDEO = 2;
    private static final int INDEX_FILE = 3;
    private static final int INDEX_RAR = 4;
    private static final int INDEX_PACK = 5;
    private static final int INDEX_OTHER = 6;
    private static final int INDEX_LAST = 7;

    public ChartView(Context context) {
        super(context);
    }

    public ChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttrs(attrs);
    }

    public ChartView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttrs(attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ChartView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initAttrs(attrs);
    }

    private void initAttrs(AttributeSet attrs) {
        if (null != attrs) {
            TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.ChartView);
            if (null != ta) {
                mOuterDiameter = ta.getDimensionPixelSize(R.styleable.ChartView_outer_diameter, 0);
                mInnerDiameter = ta.getDimensionPixelSize(R.styleable.ChartView_inner_diameter, 0);
                mThickness = (mOuterDiameter - mInnerDiameter) / 2;
                float padding = mThickness / 2.0f;
                mRectF = new RectF(padding, padding, mOuterDiameter-padding, mOuterDiameter-padding);
            }
        }

        /*mList = new ArrayList<>();
        for (int i=0; i<8; i++) {
            mList.add(i*100);
        }
        setList(mList);*/
    }

    public void setList(List<Long> list) {
        mList = list;
        for (Long value : list) {
            mTotal += value;
        }
        invalidate();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawArc(canvas, mPaint);
    }

    private void drawArc(Canvas canvas, Paint paint) {
        if (null == mList) {
            int color = ContextCompat.getColor(getContext(), R.color.colorE0);
            drawArcSegment(canvas, paint, color, 1, true);
            return;
        }

        float percent;
        int color;
        int length = mList.size();
        for (int i=0; i<length; i++) {
            percent = mList.get(i) * 1.0f / mTotal;
            color = getColor(i);
            drawArcSegment(canvas, paint, color, percent, i == (length - 1));
        }
    }

    private void drawArcSegment(Canvas canvas, Paint paint, int color, float percent, boolean isEnd) {
        if ((null != canvas)
                && (null != paint)
                && (0 <= percent) && (percent <=1)) {
            paint.reset();
            paint.setAntiAlias(true);
            paint.setStrokeWidth(mThickness);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setColor(color);
            Path pathProgress = new Path();
            float sweepAngle = 360 * percent;
            BigDecimal bSweepAngle  =   new BigDecimal(sweepAngle);
            // setScale(2,  BigDecimal.ROUND_HALF_UP)表明四舍五入，保留两位小数
            sweepAngle = bSweepAngle.setScale(2,  BigDecimal.ROUND_HALF_UP).floatValue();
            LogUtils.d(TAG, "drawArcSegment: percent=" + percent
                            + ", mStartAngle=" + mStartAngle
                            + ", sweepAngle=" + sweepAngle);
            pathProgress.addArc(mRectF, mStartAngle, (isEnd ? sweepAngle : sweepAngle+1));
            BigDecimal bStartAngle  =   new BigDecimal(mStartAngle);
            bStartAngle = bStartAngle.add(bSweepAngle);
            mStartAngle = bStartAngle.setScale(2,  BigDecimal.ROUND_HALF_UP).floatValue();
            canvas.drawPath(pathProgress, paint);
        }
    }

    private int getColor(int index) {
        int color = ContextCompat.getColor(getContext(), R.color.colorE0);
        switch (index) {
            case INDEX_IMAGE:
                color = ContextCompat.getColor(getContext(), R.color.image_color);
                break;

            case INDEX_AUDIO:
                color = ContextCompat.getColor(getContext(), R.color.audio_color);
                break;

            case INDEX_VIDEO:
                color = ContextCompat.getColor(getContext(), R.color.video_color);
                break;

            case INDEX_FILE:
                color = ContextCompat.getColor(getContext(), R.color.file_color);
                break;

            case INDEX_RAR:
                color = ContextCompat.getColor(getContext(), R.color.rar_color);
                break;

            case INDEX_PACK:
                color = ContextCompat.getColor(getContext(), R.color.pack_color);
                break;

            case INDEX_OTHER:
                color = ContextCompat.getColor(getContext(), R.color.other_color);
                break;
        }

        return color;
    }


}
