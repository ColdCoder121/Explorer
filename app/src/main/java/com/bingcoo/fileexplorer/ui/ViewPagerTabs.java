package com.bingcoo.fileexplorer.ui;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bingcoo.fileexplorer.util.LogUtils;

import java.util.Locale;

/**
 * 类名称：ViewPagerTabs
 * 作者：David
 * 内容摘要：Lightweight implementation of ViewPager tabs. This looks similar to traditional actionBar tabs,
 * but allows for the view containing the tabs to be placed anywhere on screen. Text-related
 * attributes can also be assigned in XML - these will get propogated to the child TextViews
 * automatically.
 * 创建日期：2016/11/21
 * 修改者， 修改日期， 修改内容
 */
public class ViewPagerTabs extends LinearLayout implements ViewPager.OnPageChangeListener {

    private static final String TAG = "ViewPagerTabs";
    static {
        LogUtils.setDebug(TAG, true);
    }
    ViewPager mPager;
    private LinearLayout mTabStrip;
    private FrameLayout mContainer;

    /**
     * Linearlayout that will contain the TextViews serving as tabs. This is the only child
     * of the parent HorizontalScrollView.
     */
    final int mTextStyle;
    final ColorStateList mTextColor;
    final int mTextSize;
    final boolean mTextAllCaps;
    int mPrevSelected = -1;
    int mSidePadding;

    private int[] mTabIcons;
    private int[] mTabBackground;

    /*private static final ViewOutlineProvider VIEW_BOUNDS_OUTLINE_PROVIDER =
            new ViewOutlineProvider() {
                @Override
                public void getOutline(View view, Outline outline) {
                    outline.setRect(0, 0, view.getWidth(), view.getHeight());
                }
            };
*/
    private static final int TAB_SIDE_PADDING_IN_DPS = 10;

    // TODO: This should use <declare-styleable> in the future
    private static final int[] ATTRS = new int[]{
            android.R.attr.textSize,
            android.R.attr.textStyle,
            android.R.attr.textColor,
            android.R.attr.textAllCaps
    };

    /**
     * Simulates actionbar tab behavior by showing a toast with the tab title when long clicked.
     */
    private class OnTabLongClickListener implements OnLongClickListener {
        final int mPosition;

        public OnTabLongClickListener(int position) {
            mPosition = position;
        }

        @Override
        public boolean onLongClick(View v) {
            final int[] screenPos = new int[2];
            getLocationOnScreen(screenPos);

            final Context context = getContext();
            final int width = getWidth();
            final int height = getHeight();
            final int screenWidth = context.getResources().getDisplayMetrics().widthPixels;

            Toast toast = Toast.makeText(context, mPager.getAdapter().getPageTitle(mPosition),
                    Toast.LENGTH_SHORT);

            // Show the toast under the tab
            toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL,
                    (screenPos[0] + width / 2) - screenWidth / 2, screenPos[1] + height);

            toast.show();
            return true;
        }
    }

    public ViewPagerTabs(Context context) {
        this(context, null);
    }

    public ViewPagerTabs(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ViewPagerTabs(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
//        setFillViewport(true);

        mSidePadding = (int) (getResources().getDisplayMetrics().density * TAB_SIDE_PADDING_IN_DPS);

        final TypedArray a = context.obtainStyledAttributes(attrs, ATTRS);
        mTextSize = a.getDimensionPixelSize(0, 0);
        mTextStyle = a.getInt(1, 0);
        mTextColor = a.getColorStateList(2);
        mTextAllCaps = a.getBoolean(3, false);

        mContainer = new FrameLayout(context);
        mTabStrip = new LinearLayout(context);
        mContainer.addView(mTabStrip,
                new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        addView(mContainer,
                new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        a.recycle();

        // enable shadow casting from view bounds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setOutlineProvider(null/*VIEW_BOUNDS_OUTLINE_PROVIDER*/);
        }
    }

    public void setViewPager(ViewPager viewPager) {
        mPager = viewPager;
        addTabs(mPager.getAdapter());
    }

    public void setTabIcons(int[] tabIcons) {
        mTabIcons = tabIcons;
    }

    public void setTabBackground(int[] background) {
        mTabBackground = background;
    }

    private void addTabs(PagerAdapter adapter) {
        mTabStrip.removeAllViews();

        final int count = adapter.getCount();
        for (int i = 0; i < count; i++) {
            addTab(adapter.getPageTitle(i), i);
        }
    }

    private void addTab(CharSequence tabTitle, final int position) {
        View tabView;
        if (mTabIcons != null && position < mTabIcons.length) {
            ImageView iconView = new ImageView(getContext());

            if (mTabBackground != null) {
                iconView.setBackgroundResource(mTabBackground[position]);
            } else {
                final TypedValue outValue = new TypedValue();
                getContext().getTheme().resolveAttribute(android.R.attr.selectableItemBackgroundBorderless, outValue, true);
                iconView.setBackgroundResource(outValue.resourceId);
            }
            iconView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            iconView.setImageResource(mTabIcons[position]);
            iconView.setContentDescription(tabTitle);

            tabView = iconView;
        } else {
            final TextView textView = new TabTextView(getContext());
            textView.setText(tabTitle);
            textView.setContentDescription(tabTitle);
            if (mTabBackground != null) {
                textView.setBackgroundResource(mTabBackground[position]);
            } else {
//                textView.setBackgroundResource(R.drawable.view_pager_tab_background);
//                textView.setBackgroundResource(R.color.tab_ripple_color);
            }
            // Assign various text appearance related attributes to child views.
            if (mTextStyle > 0) {
                textView.setTypeface(textView.getTypeface(), mTextStyle);
            }
            if (mTextSize > 0) {
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize);
            }
            if (mTextColor != null) {
                textView.setTextColor(mTextColor);
            }
            textView.setAllCaps(mTextAllCaps);
            textView.setGravity(Gravity.CENTER);

            tabView = textView;
        }

        tabView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(getRtlPosition(position));
            }
        });

        //tabView.setOnLongClickListener(new OnTabLongClickListener(position));

//        tabView.setPadding(mSidePadding, 0, mSidePadding, 0);
        mTabStrip.addView(tabView, new LinearLayout.LayoutParams(0/*LayoutParams.WRAP_CONTENT*/,
                LayoutParams.MATCH_PARENT, 1));

        // Default to the first child being selected
        Locale locale = getContext().getResources().getConfiguration().locale;
        String language = locale.getLanguage();
        if (language.endsWith("ar")) {
            if (position == 1) {
                mPrevSelected = 1;
                tabView.setSelected(true);
            }
        } else {
            if (position == 0) {
                mPrevSelected = 0;
                tabView.setSelected(true);
            }
        }

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        position = getRtlPosition(position);
        int tabStripChildCount = mTabStrip.getChildCount();
        if ((tabStripChildCount == 0) || (position < 0) || (position >= tabStripChildCount)) {
            return;
        }
        //mTabStrip.onPageScrolled(position, positionOffset, positionOffsetPixels);
    }

    @Override
    public void onPageSelected(int position) {
        LogUtils.e(TAG,"onPageSelected:position="+position);
        position = getRtlPosition(position);
        LogUtils.e(TAG,"onPageSelected getRtlPosition:position="+position);
        int tabStripChildCount = mTabStrip.getChildCount();
        if ((tabStripChildCount == 0) || (position < 0) || (position >= tabStripChildCount)) {
            return;
        }

        if (mPrevSelected >= 0 && mPrevSelected < tabStripChildCount) {
            mTabStrip.getChildAt(mPrevSelected).setSelected(false);
        }
        /*final*/ View selectedChild = mTabStrip.getChildAt(position);
        selectedChild.setSelected(true);

        // Update scroll position
//        final int scrollPos = selectedChild.getLeft() - (getWidth() - selectedChild.getWidth()) / 2;
//        smoothScrollTo(scrollPos, 0);
        mPrevSelected = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    private int getRtlPosition(int position) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            if (getResources().getConfiguration().getLayoutDirection() == View.LAYOUT_DIRECTION_RTL) {
                return mTabStrip.getChildCount() - 1 - position;
            }
        }
        return position;
    }

    private void setBottomPadding(View view, int resId, int padding) {
        if (view instanceof TextView) {
            TextView tv = (TextView) view;
            tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, resId);
            tv.setCompoundDrawablePadding(padding);
        }
    }
}
