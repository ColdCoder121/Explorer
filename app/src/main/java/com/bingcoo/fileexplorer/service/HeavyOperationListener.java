package com.bingcoo.fileexplorer.service;

import android.app.Activity;
import android.app.ProgressDialog;
import android.view.View;

import com.bingcoo.fileexplorer.R;
import com.bingcoo.fileexplorer.dialog.fragment.AlertDialogFragment;
import com.bingcoo.fileexplorer.dialog.fragment.ProgressDialogFragment;
import com.bingcoo.fileexplorer.service.FileManagerService.OperationEventListener;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.ToastHelper;

/**
 * 类名称：HeavyOperationListener
 * 作者：David
 * 内容摘要：
 * 创建日期：2016/12/14
 * 修改者， 修改日期， 修改内容
 */
public class HeavyOperationListener implements FileManagerService.OperationEventListener,
        View.OnClickListener {
    public static final String HEAVY_DIALOG_TAG = "HeavyDialogFragment";

    private static final String TAG = "HeavyOperationListener";

    //    private boolean operationTypeCut = false;
    private boolean hasError = false;//判断onTaskProgress中是否有错误

    static {
        LogUtils.setDebug(TAG, true);
    }

    int mTitle = R.string.deleting;

    private boolean mPermissionToast = false;
    private boolean mOperationToast = false;
    private Activity mActivity = null;
    private ToastHelper mToastHelper = null;
    private HeavyOperationListenerCallback mCallback = null;

    // test performance
    // private long beforeTime = 0;


  /*  public boolean isOperationTypeCut() {
        return operationTypeCut;
    }*/

  /*  public void setOperationTypeCut(boolean operationTypeCut) {
        this.operationTypeCut = operationTypeCut;
    }*/

    public HeavyOperationListener(int titleID, Activity activity, HeavyOperationListenerCallback callback) {
        mTitle = titleID;
        mActivity = activity;
        mToastHelper = new ToastHelper(activity);
        setCallback(callback);
    }

    @Override
    public void onTaskPrepare() {
        // beforeTime = System.currentTimeMillis();
        if ((null != mActivity) && mActivity.isResumed()) {
            ProgressDialogFragment heavyDialogFragment = ProgressDialogFragment.newInstance(
                    ProgressDialog.STYLE_HORIZONTAL, mTitle, AlertDialogFragment.INVIND_RES_ID, R.string.cancel);
            heavyDialogFragment.setCancelListener(this);
            //heavyDialogFragment.setViewDirection(getViewDirection());
            heavyDialogFragment.show(mActivity.getFragmentManager(), HEAVY_DIALOG_TAG);
            boolean ret = mActivity.getFragmentManager().executePendingTransactions();
            LogUtils.d(TAG, "executing pending transactions result: " + ret);

            View titleDivider = heavyDialogFragment.getDialog().findViewById(R.id.titleDivider);
            if (null != titleDivider) {
                LogUtils.d(TAG, "showDeleteDialog: null != titleDivider");
                titleDivider.setVisibility(View.GONE);
            }
        } else {
            LogUtils.d(TAG, "HeavyOperationListener onTaskResult activity is not resumed.");
        }
    }

    @Override
    public void onTaskProgress(ProgressInfo progressInfo) {
        if (progressInfo.isFailInfo()) {
            switch (progressInfo.getErrorCode()) {
                case OperationEventListener.ERROR_CODE_COPY_NO_PERMISSION://CopyPasteFilesTask
                    hasError = true;
                   /* if (!mPermissionToast) {
                        mToastHelper.showToast(R.string.copy_deny);
                        mPermissionToast = true;
                    }*/
                    break;
                case OperationEventListener.ERROR_CODE_DELETE_NO_PERMISSION:
                    hasError = true;
                    /*if (!mPermissionToast && !operationTypeCut) {
                        mToastHelper.showToast(R.string.delete_deny);
                        mPermissionToast = true;
                    }*/
                    break;
                case OperationEventListener.ERROR_CODE_DELETE_UNSUCCESS:
                    hasError = true;
                    /*if (!mOperationToast && !operationTypeCut) {
                        mToastHelper.showToast(R.string.some_delete_fail);
                        mOperationToast = true;
                    }*/
                    break;
                case OperationEventListener.ERROR_CODE_PASTE_UNSUCCESS:
                    hasError = true;
                    /*if (!mOperationToast) {
                        mToastHelper.showToast(R.string.some_paste_fail);
                        mOperationToast = true;
                    }*/
                    break;
                default:
                    hasError = true;
                    /*if (!mPermissionToast) {
                        mToastHelper.showToast(R.string.operation_fail);
                        mPermissionToast = true;
                    }*/
                    break;
            }

        } else if (null != mActivity) {
            ProgressDialogFragment heavyDialogFragment = (ProgressDialogFragment) mActivity.getFragmentManager()
                    .findFragmentByTag(HEAVY_DIALOG_TAG);
            if (heavyDialogFragment != null) {
                heavyDialogFragment.setProgress(progressInfo);
            }
        }
    }

    @Override
    public void onTaskResult(final int errorType) {
        LogUtils.d(TAG, "HeavyOperationListener,onTaskResult result = " + errorType);
        switch (errorType) {
            case ERROR_CODE_PASTE_TO_SUB:
                mToastHelper.showToast(R.string.paste_sub_folder);
                break;
            case ERROR_CODE_CUT_SAME_PATH:
                mToastHelper.showToast(R.string.paste_same_folder);
                break;
            case ERROR_CODE_NOT_ENOUGH_SPACE:
                mToastHelper.showToast(R.string.insufficient_memory);
                break;
            case ERROR_CODE_DELETE_FAILS:
                mToastHelper.showToast(R.string.delete_fail);
                break;
            case ERROR_CODE_COPY_NO_PERMISSION:
                mToastHelper.showToast(R.string.copy_deny);
                break;
            case ERROR_CODE_COPY_GREATER_4G_TO_FAT32:
                mToastHelper.showToast(R.string.operation_fail);
                break;
            case ERROR_CODE_PASTE_UNSUCCESS:
                mToastHelper.showToast(R.string.operation_fail);
                break;
            default:
                if (hasError || errorType < 0) {
                    mToastHelper.showToast(R.string.operation_fail);
                }
                break;
        }
        hasError = false;
        if (null != mActivity) {
            ProgressDialogFragment heavyDialogFragment = (ProgressDialogFragment) mActivity.getFragmentManager()
                    .findFragmentByTag(HEAVY_DIALOG_TAG);
            if (null != heavyDialogFragment) {
                heavyDialogFragment.dismissAllowingStateLoss();
            }

            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (null != mCallback) {
                        mCallback.onTaskResult(errorType);
                    }
                }
            });
        }

       /* if (mFileInfoManager.getPasteType() == FileInfoManager.PASTE_MODE_CUT) {
            mFileInfoManager.clearPasteList();
            mAdapter.notifyDataSetChanged();
        }*/
        // final long endTime = System.currentTimeMillis();
        // LogUtils.i(TAG,
        // "HeavyOperationListener, onTaskResult,time cost is:" +
        // (endTime-beforeTime)/1000);

        //invalidateOptionsMenu();
    }

    @Override
    public void onClick(View v) {
        /*if (mService != null) {
            LogUtils.i(this.getClass().getName(), "onClick cancel");
            mService.cancel(FileManagerOperationActivity.this.getClass().getName());
        }*/
        if (null != mCallback) {
            mCallback.onClick(v);
        }
    }

    public HeavyOperationListenerCallback getCallback() {
        return mCallback;
    }

    public void setCallback(HeavyOperationListenerCallback callback) {
        mCallback = callback;
    }

    public interface HeavyOperationListenerCallback {
        void onTaskResult(int errorType);

        void onClick(View v);
    }

}
