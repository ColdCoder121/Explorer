package com.bingcoo.fileexplorer.service;

import android.os.AsyncTask;

import com.bingcoo.fileexplorer.category.zip.OpenZipsActivity;
import com.bingcoo.fileexplorer.util.RarUtils;
import com.bingcoo.fileexplorer.util.ZipUtilsForZh;

/**
 * 作者：zhshh
 * 内容摘要：文件解压的异步任务
 * 创建日期：2017/2/20
 * 修改者， 修改日期， 修改内容
 */

public class UnzipFileTask extends AsyncTask<String, Integer, Boolean> {

    private String mZipFilePath;
    private String mDestDirPath;
    private String mChildPath;
    private UnzipCallback mCallback;
    private OpenZipsActivity.FileType mType;
    private boolean isBusy=false;

    public UnzipFileTask(String zipFilePath, String destDirPath, String childPath, UnzipCallback callback) {
        this.mZipFilePath = zipFilePath;
        this.mDestDirPath = destDirPath;
        this.mCallback = callback;
        this.mChildPath = childPath;
    }

    public OpenZipsActivity.FileType getType() {
        return mType;
    }

    public void setType(OpenZipsActivity.FileType type) {
        this.mType = type;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        if (aBoolean) {
            mCallback.successCallback();
        } else {
            mCallback.failCallback();
        }
        isBusy=false;
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        isBusy=false;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        isBusy=true;
    }

    @Override
    protected Boolean doInBackground(String... params) {

        switch (mType) {
            case ZIP:
                return ZipUtilsForZh.unZipSingleFile(mZipFilePath, mChildPath, mDestDirPath);
            case RAR:
                return RarUtils.extractSingleRarFiles(mZipFilePath, mChildPath, mDestDirPath);
            default:

                return false;
        }
    }

    public interface UnzipCallback {
        void successCallback();

        void failCallback();
    }

    public   boolean isBusy(){
        return isBusy||getStatus().equals(Status.RUNNING);
    }
}
