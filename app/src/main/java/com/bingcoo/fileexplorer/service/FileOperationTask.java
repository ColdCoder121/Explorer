package com.bingcoo.fileexplorer.service;

import android.content.Context;
import android.webkit.MimeTypeMap;

import com.bingcoo.fileexplorer.service.MultiMediaStoreHelper.DeleteMediaStoreHelper;
import com.bingcoo.fileexplorer.service.MultiMediaStoreHelper.PasteMediaStoreHelper;
import com.bingcoo.fileexplorer.util.FileInfo;
import com.bingcoo.fileexplorer.util.FileInfoManager;
import com.bingcoo.fileexplorer.util.FileUtils;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.MountPointManager;
import com.bingcoo.fileexplorer.util.PermissionUtils;
import com.bingcoo.fileexplorer.util.SystemUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

abstract class FileOperationTask extends BaseAsyncTask {
    private static final String TAG = "FileOperationTask";

    static {
        LogUtils.setDebug(TAG, true);
    }

    //to increase the copy/paste speed
    protected static final int BUFFER_SIZE = 2048 * 1024;
    //protected static final int BUFFER_SIZE = 512 * 1024;
    protected static final int TOTAL = 100;
    protected Context mContext;
    protected MediaStoreHelper mMediaProviderHelper;

    public FileOperationTask(FileInfoManager fileInfoManager,
                             FileManagerService.OperationEventListener operationEvent, Context context) {
        super(fileInfoManager, operationEvent);
        if (context == null) {
            LogUtils.e(TAG, "construct FileOperationTask exception! ");
            throw new IllegalArgumentException();
        } else {
            mContext = context;
            mMediaProviderHelper = new MediaStoreHelper(context, this);
        }
    }

    protected File getDstFile(HashMap<String, String> pathMap, File file, String defPath) {
        LogUtils.d(TAG, "getDstFile.");
        String curPath = pathMap.get(file.getParent());
        if (curPath == null) {
            curPath = defPath;
        }
        File dstFile = new File(curPath, file.getName());

        return checkFileNameAndRename(dstFile);
    }

    protected boolean deleteFile(File file) {
        if (file == null) {
            publishProgress(new ProgressInfo(FileManagerService.OperationEventListener.ERROR_CODE_DELETE_UNSUCCESS,
                    true));
        } else {
            if (file.canWrite() && file.delete()) {
                // 下载和蓝牙在剪切和删除时要删数据库
                int deleteBluetoothCnt = SystemUtils.getBluetoothCursorDelete(mContext, file);
                int deleteDownloadCnt = SystemUtils.getDownloadCursorDelete(mContext, file);
                LogUtils.e(TAG, "蓝牙删除的条数=" + deleteBluetoothCnt);
                LogUtils.e(TAG, "下载删除的条数=" + deleteDownloadCnt);
                return true;
            } else {
                LogUtils.d(TAG, "deleteFile fail, file name = " + file.getName());
                publishProgress(new ProgressInfo(
                        FileManagerService.OperationEventListener.ERROR_CODE_DELETE_NO_PERMISSION, true));
            }
        }
        return false;
    }

    protected boolean mkdir(HashMap<String, String> pathMap, File srcFile, File dstFile) {
        LogUtils.d(TAG, "mkdir, srcFile = " + srcFile + ", dstFile = " + dstFile);
        if (srcFile.exists() && srcFile.canRead() && dstFile.mkdirs()) {
            pathMap.put(srcFile.getAbsolutePath(), dstFile.getAbsolutePath());
            return true;
        } else {
            publishProgress(new ProgressInfo(FileManagerService.OperationEventListener.ERROR_CODE_PASTE_UNSUCCESS,
                    true));
            return false;
        }
    }

    private long calcNeedSpace(List<File> fileList) {
        long need = 0;
        for (File file : fileList) {
            need += file.length();
        }
        return need;
    }

    protected boolean isGreaterThan4G(UpdateInfo updateInfo) {
        long size = updateInfo.getTotal();
        if (size > (4L * 1024 * 1024 * 1024)) {
            LogUtils.d(TAG, "isGreaterThan4G true.");
            return true;
        }

        LogUtils.d(TAG, "isGreaterThan4G false.");
        return false;
    }

    protected boolean isFat32Disk(String path) {
        MountPointManager mpm = MountPointManager.getInstance();
        return mpm.isFat32Disk(path);
    }

    protected boolean isEnoughSpace(UpdateInfo updateInfo, String dstFolder) {
        LogUtils.d(TAG, "isEnoughSpace,dstFolder = " + dstFolder);
        long needSpace = updateInfo.getTotal();
        File file = new File(dstFolder);
        long freeSpace = file.getFreeSpace();
        if (needSpace > freeSpace) {
            return false;
        }
        return true;
    }

    protected int getAllDeleteFiles(List<FileInfo> fileInfoList, List<File> deleteList) {
        // LogUtils.d(TAG, "getAllDeleteFiles... ");
        int ret = FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS;
        if (DeleteFilesTask.isPicture()) {
            for (FileInfo fileInfo : fileInfoList) {
                ret = getAllDeletePictureFile(fileInfo.getFile(), deleteList);
                if (ret < 0) {
                    break;
                }
            }
        } else {
            for (FileInfo fileInfo : fileInfoList) {
                ret = getAllDeleteFile(fileInfo.getFile(), deleteList);
                if (ret < 0) {
                    break;
                }
            }
        }
        DeleteFilesTask.setIsPicture(false);
        return ret;
    }

    protected int getAllDeletePictureFile(File deleteFile, List<File> deleteList) {
        //LogUtils.d(TAG, "getAllDeleteFile... ");
        if (isCancelled()) {
            LogUtils.i(TAG, "getAllDeleteFile,cancel. ");
            return FileManagerService.OperationEventListener.ERROR_CODE_USER_CANCEL;
        }
        //是图片文件夹
        if (deleteFile.isDirectory() && deleteFile.canWrite()) {
            File[] files = deleteFile.listFiles();
            if (files == null) {
                LogUtils.i(TAG, "getAllDeleteFile,files is null. ");
                return FileManagerService.OperationEventListener.ERROR_CODE_UNSUCCESS;
            }
            for (File file : files) {
                if (file.isDirectory()) {
                    continue;
                }
//                String mimeType = MediaFile.getMimeTypeForFile(file.getAbsolutePath());
                String extension=FileUtils.getFileExtension(file.getAbsolutePath());
                String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
                if (mimeType != null && mimeType.startsWith("image/")) {
                    deleteList.add(0, file);
                }
            }
        }
        return FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS;
    }

    protected int getAllDeleteFile(File deleteFile, List<File> deleteList) {
        //LogUtils.d(TAG, "getAllDeleteFile... ");
        if (isCancelled()) {
            LogUtils.i(TAG, "getAllDeleteFile,cancel. ");
            return FileManagerService.OperationEventListener.ERROR_CODE_USER_CANCEL;
        }
        //正常文件删除
        if (deleteFile.isDirectory()) {
            deleteList.add(0, deleteFile);
            if (deleteFile.canWrite()) {
                File[] files = deleteFile.listFiles();
                if (files == null) {
                    LogUtils.i(TAG, "getAllDeleteFile,files is null. ");
                    return FileManagerService.OperationEventListener.ERROR_CODE_UNSUCCESS;
                }
                for (File file : files) {
                    getAllDeleteFile(file, deleteList);
                }
            }
        } else {
            deleteList.add(0, deleteFile);
        }
        return FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS;
    }

    protected int getAllFileList(List<FileInfo> srcList, List<File> resultList,
                                 UpdateInfo updateInfo) {
        int ret = FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS;
        for (FileInfo fileInfo : srcList) {
            ret = getAllFile(fileInfo.getFile(), resultList, updateInfo);
            if (ret < 0) {
                break;
            }
        }
        //LogUtils.d(TAG, "getAllFileList,ret = " + ret);

        return ret;
    }

    protected int getAllFile(File srcFile, List<File> fileList, UpdateInfo updateInfo) {
        // LogUtils.d(TAG, "getAllFile...");
        if (isCancelled()) {
            LogUtils.i(TAG, "getAllFile, cancel.");
            return FileManagerService.OperationEventListener.ERROR_CODE_USER_CANCEL;
        }

        fileList.add(srcFile);
        updateInfo.updateTotal(srcFile.length());
        updateInfo.updateTotalNumber(1);
        if (srcFile.isDirectory() && srcFile.canRead()) {
            File[] files = srcFile.listFiles();
            if (files == null) {
                return FileManagerService.OperationEventListener.ERROR_CODE_UNSUCCESS;
            }
            for (File file : files) {
                int ret = getAllFile(file, fileList, updateInfo);
                if (ret < 0) {
                    return ret;
                }
            }
        }
        return FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS;
    }

    protected int copyFile(byte[] buffer, File srcFile, File dstFile, UpdateInfo updateInfo) {
        if ((buffer == null) || (srcFile == null) || (dstFile == null)) {
            LogUtils.i(TAG, "copyFile, invalid parameter.");
            return FileManagerService.OperationEventListener.ERROR_CODE_PASTE_UNSUCCESS;
        }

        FileInputStream in = null;
        FileOutputStream out = null;
        int ret = FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS;
        try {
            if (!srcFile.exists()) {
                LogUtils.i(TAG, "copyFile, src file is not exist.");
                return FileManagerService.OperationEventListener.ERROR_CODE_PASTE_UNSUCCESS;
            }
            if (!dstFile.createNewFile()) {
                LogUtils.i(TAG, "copyFile, create new file fail.");
                return FileManagerService.OperationEventListener.ERROR_CODE_PASTE_UNSUCCESS;
            }
            in = new FileInputStream(srcFile);
            out = new FileOutputStream(dstFile);

            int len = 0;
            while ((len = in.read(buffer)) > 0) {
                // Copy data from in stream to out stream
                if (isCancelled()) {
                    LogUtils.d(TAG, "copyFile,commit copy file cancelled; " + "break while loop "
                            + "thread id: " + Thread.currentThread().getId());
                    if (!dstFile.delete()) {
                        LogUtils.w(TAG, "copyFile,delete fail in copyFile()");
                    }
                    return FileManagerService.OperationEventListener.ERROR_CODE_USER_CANCEL;
                }
                out.write(buffer, 0, len);
                // LogUtils.i(TAG, "copyFile, copyFile,len= " + len);
                updateInfo.updateProgress(len);

                updateProgressWithTime(updateInfo, srcFile);
            }
        } catch (IOException ioException) {
            LogUtils.e(TAG, "copyFile,io exception!");
            ioException.printStackTrace();
            ret = FileManagerService.OperationEventListener.ERROR_CODE_PASTE_UNSUCCESS;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.flush();
                    out.getFD().sync();
                    out.close();
                    LogUtils.e(TAG, "copyFile, close output stream!");
                }
            } catch (IOException ioException) {
                LogUtils.e(TAG, "copyFile,io exception 2!");
                ioException.printStackTrace();
                ret = FileManagerService.OperationEventListener.ERROR_CODE_PASTE_UNSUCCESS;
            } finally {
                LogUtils.d(TAG, "copyFile,update 100%.");
                /*publishProgress(new ProgressInfo(srcFile.getName(), TOTAL, TOTAL, (int) updateInfo
                        .getCurrentNumber(), updateInfo.getTotalNumber()));*/
            }
        }

        return ret;
    }

    File checkFileNameAndRename(File conflictFile) {
        File retFile = conflictFile;
        while (true) {
            if (isCancelled()) {
                LogUtils.i(TAG, "checkFileNameAndRename,cancel.");
                return null;
            }
            if (!retFile.exists()) {
                LogUtils.i(TAG, "checkFileNameAndRename,file is not exist.");
                return retFile;
            }
            retFile = FileUtils.generateNextNewName(retFile);
            if (retFile == null) {
                LogUtils.i(TAG, "checkFileNameAndRename,retFile is null.");
                return null;
            }
        }
    }

    protected void updateProgressWithTime(UpdateInfo updateInfo, File file) {
        if (updateInfo.needUpdate()) {
            int progress = (int) (updateInfo.getProgress() * TOTAL / updateInfo.getTotal());
            //int progress = (int) (updateInfo.getCurrentNumber() * TOTAL / updateInfo.getTotalNumber());
            LogUtils.d(TAG, "updateProgressWithTime: CurrentNumber=" + updateInfo.getCurrentNumber()
                    + ", TotalNumber=" + updateInfo.getTotalNumber()
                    + ", progress=" + updateInfo.getProgress()
                    + ", total=" + updateInfo.getTotal());
            publishProgress(new ProgressInfo(file.getName(), progress, TOTAL,
                    (int) updateInfo.getCurrentNumber(), updateInfo.getTotalNumber()));
        }
    }

    protected void addItem(HashMap<File, FileInfo> fileInfoMap, File file, File addFile) {
        if (fileInfoMap.containsKey(file)) {
            FileInfo fileInfo = new FileInfo(addFile);
            fileInfo.getFileSizeStr();
            mFileInfoManager.addItem(fileInfo);
        }
    }

    protected void removeItem(HashMap<File, FileInfo> fileInfoMap, File file, File removeFile) {
        if (fileInfoMap.containsKey(file)) {
            FileInfo fileInfo = new FileInfo(removeFile);
            mFileInfoManager.removeItem(fileInfo);
        }
    }

    static class DeleteFilesTask extends FileOperationTask {
        private static final String TAG = "DeleteFilesTask";
        private final List<FileInfo> mDeletedFilesInfo;
        private static boolean mIsPicture = false;//如果是图片目录只删文件夹下的图片文件不删除其他

        public static void setIsPicture(boolean isPicture) {
            mIsPicture = isPicture;
        }

        public static boolean isPicture() {
            return mIsPicture;
        }

        public DeleteFilesTask(FileInfoManager fileInfoManager,
                               FileManagerService.OperationEventListener operationEvent, Context context, List<FileInfo> fileInfoList) {
            super(fileInfoManager, operationEvent, context);
            mDeletedFilesInfo = fileInfoList;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            synchronized (mContext.getApplicationContext()) {
                LogUtils.i(TAG, "doInBackground...");
                List<File> deletefileList = new ArrayList<File>();
                FileOperationTask.UpdateInfo updateInfo = new FileOperationTask.UpdateInfo();
                int ret = getAllDeleteFiles(mDeletedFilesInfo, deletefileList);
                if (ret < 0) {
                    LogUtils.i(TAG, "doInBackground,ret = " + ret);
                    return ret;
                }

                DeleteMediaStoreHelper deleteMediaStoreHelper = new MultiMediaStoreHelper.DeleteMediaStoreHelper(
                        mMediaProviderHelper);
                HashMap<File, FileInfo> deleteFileInfoMap = new HashMap<File, FileInfo>();
                for (FileInfo fileInfo : mDeletedFilesInfo) {
                    deleteFileInfoMap.put(fileInfo.getFile(), fileInfo);
                }
                updateInfo.updateTotal(deletefileList.size());
                updateInfo.updateTotalNumber(deletefileList.size());

                publishProgress(new ProgressInfo("", (int) updateInfo.getProgress(), updateInfo
                        .getTotal(), (int) updateInfo.getCurrentNumber(), updateInfo
                        .getTotalNumber()));

                for (File file : deletefileList) {
                    if (isCancelled()) {
                        if (PermissionUtils.hasStorageWritePermission(
                                mContext.getApplicationContext())) {
                            deleteMediaStoreHelper.updateRecords();
                        } else {
                            mMediaProviderHelper.scanPathforMediaStore(
                                    mDeletedFilesInfo.get(0).getFileParentPath());
                        }
                        LogUtils.i(TAG, "doInBackground,user cancel it.");
                        return FileManagerService.OperationEventListener.ERROR_CODE_USER_CANCEL;
                    }
                    if (deleteFile(file)) {
                        deleteMediaStoreHelper.addRecord(file.getAbsolutePath());
                        removeItem(deleteFileInfoMap, file, file);
                    }
                    updateInfo.updateProgress(1);
                    updateInfo.updateCurrentNumber(1);
                    if (updateInfo.needUpdate()) {
                        publishProgress(new ProgressInfo(file.getName(), (int) updateInfo
                                .getProgress(), updateInfo.getTotal(), (int) updateInfo
                                .getCurrentNumber(), updateInfo.getTotalNumber()));
                    }
                }
                if (PermissionUtils.hasStorageWritePermission(
                        mContext.getApplicationContext())) {
                    deleteMediaStoreHelper.updateRecords();
                } else {
                    mMediaProviderHelper.scanPathforMediaStore(
                            mDeletedFilesInfo.get(0).getFileParentPath());
                }
                LogUtils.i(TAG, "doInBackground,return sucsess..");
                return FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS;
            }
        }
    }

    static class UpdateInfo {
        protected static final int NEED_UPDATE_TIME = 200;
        private long mStartOperationTime = 0;
        private long mProgressSize = 0;
        private long mTotalSize = 0;
        private long mCurrentNumber = 0;
        private long mTotalNumber = 0;

        public UpdateInfo() {
            mStartOperationTime = System.currentTimeMillis();
        }

        public long getProgress() {
            return mProgressSize;
        }

        public long getTotal() {
            return mTotalSize;
        }

        public long getCurrentNumber() {
            return mCurrentNumber;
        }

        public long getTotalNumber() {
            return mTotalNumber;
        }

        public void updateProgress(long addSize) {
            mProgressSize += addSize;
        }

        public void updateTotal(long addSize) {
            mTotalSize += addSize;
        }

        public void updateCurrentNumber(long addNumber) {
            mCurrentNumber += addNumber;
        }

        public void updateTotalNumber(long addNumber) {
            mTotalNumber += addNumber;
        }

        public boolean needUpdate() {
            long operationTime = System.currentTimeMillis() - mStartOperationTime;
            if (operationTime > NEED_UPDATE_TIME) {
                mStartOperationTime = System.currentTimeMillis();
                return true;
            }
            return false;
        }

    }

    static class CutPasteFilesTask extends FileOperationTask {
        private static final String TAG = "CutPasteFilesTask";

        static {
            LogUtils.setDebug(TAG, true);
        }

        private final List<FileInfo> mSrcList;
        private final String mDstFolder;
        private int mCutPasteErrorCount = 0;

        /**
         * Buffer size for data read and write.
         */

        public CutPasteFilesTask(FileInfoManager fileInfoManager,
                                 FileManagerService.OperationEventListener operationEvent, Context context, List<FileInfo> src,
                                 String destFolder) {
            super(fileInfoManager, operationEvent, context);

            mSrcList = src;
            mDstFolder = destFolder;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            synchronized (mContext.getApplicationContext()) {
                LogUtils.i(TAG, "doInBackground...");
                if (mSrcList.isEmpty()) {
                    LogUtils.i(TAG, "doInBackground,src list is empty.");
                    return FileManagerService.OperationEventListener.ERROR_CODE_PASTE_UNSUCCESS;
                }

                boolean isSameRoot = true;
                int length = mSrcList.size();
                String srcPath;
                for (int i = 0; i < length; i++) {
                    srcPath = mSrcList.get(i).getFileAbsolutePath();
                    isSameRoot = isSameRoot(srcPath, mDstFolder);
                    LogUtils.d(TAG, "doInBackground: i=" + i + ", srcPath=" + srcPath
                            + ", mDstFolder=" + mDstFolder + ", isSameRoot=" + isSameRoot);
                    if (!isSameRoot) {
                        break;
                    }
                }
                // 剪切后数据库记录删除
                if (isSameRoot) {
                    int result = cutPasteInSameCard();
                    if (result == FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS) {
                        SystemUtils.getDownloadCursorDelete(mContext, mSrcList);
                        SystemUtils.getBluetoothCursorDelete(mContext, mSrcList);
                    }
                    return result;
                } else {
                    int result = cutPasteInDiffCard();
                    if (result == FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS) {
                        SystemUtils.getDownloadCursorDelete(mContext, mSrcList);
                        SystemUtils.getBluetoothCursorDelete(mContext, mSrcList);
                    }
                    return result;
                }
            }
        }

        private boolean isSameRoot(String srcPath, String dstPath) {
            MountPointManager mpm = MountPointManager.getInstance();
            String srcMountPoint = mpm.getRealMountPointPath(srcPath);
            String dstMountPoint = mpm.getRealMountPointPath(dstPath);
            if (srcMountPoint != null && dstMountPoint != null
                    && srcMountPoint.equals(dstMountPoint)) {
                return true;
            }
            return false;
        }

        private Integer cutPasteInSameCard() {
            LogUtils.i(TAG, "cutPasteInSameCard.");
            FileOperationTask.UpdateInfo updateInfo = new FileOperationTask.UpdateInfo();
            updateInfo.updateTotal(mSrcList.size());
            updateInfo.updateTotalNumber(mSrcList.size());
            /** Unnecessary to show process when cut in the same card*/
            publishProgress(new ProgressInfo("", 0, TOTAL, 0, mSrcList.size()));

            PasteMediaStoreHelper pasteMediaStoreHelper = new PasteMediaStoreHelper(
                    mMediaProviderHelper);
            DeleteMediaStoreHelper deleteMediaStoreHelper = new DeleteMediaStoreHelper(
                    mMediaProviderHelper);

            // Set dstFolder so we can scan folder instead of scanning each file one by one.
            pasteMediaStoreHelper.setDstFolder(mDstFolder);

            for (FileInfo fileInfo : mSrcList) {
                File newFile = new File(mDstFolder + MountPointManager.SEPARATOR
                        + fileInfo.getFileName());
                newFile = checkFileNameAndRename(newFile);
                if (isCancelled()) {
                    pasteMediaStoreHelper.updateRecords();
                    if (PermissionUtils.hasStorageWritePermission(
                            mContext.getApplicationContext())) {
                        deleteMediaStoreHelper.updateRecords();
                    } else {
                        mMediaProviderHelper.scanPathforMediaStore(
                                mSrcList.get(0).getFileParentPath());
                    }
                    return FileManagerService.OperationEventListener.ERROR_CODE_USER_CANCEL;
                }

                if (newFile == null) {
                    LogUtils.i(TAG, "cutPasteInSameCard,newFile is null.");
                    publishProgress(new ProgressInfo(
                            FileManagerService.OperationEventListener.ERROR_CODE_PASTE_UNSUCCESS, true));
                    mCutPasteErrorCount++;
                    continue;
                }

                if (fileInfo.getFile().renameTo(newFile)) {
                    updateInfo.updateProgress(1);
                    updateInfo.updateCurrentNumber(1);
                    FileInfo newFileInfo = new FileInfo(newFile);
                    newFileInfo.getFileSizeStr();
                    mFileInfoManager.addItem(newFileInfo);
                    // we need to update the records(uri,is_rintone,etc)in
                    // mediaprovider by recreating them.
                    // mMediaProviderHelper.updateInMediaStore(newFile.getAbsolutePath(),
                    // fileInfo.getFileAbsolutePath());
                    if (/*newFile.isDirectory()*/false) {
                        //if cut directory,update the files in this directory also.
                        //the below code required to be dead because of permission changes
                        mMediaProviderHelper.updateInMediaStore(newFileInfo.getFileAbsolutePath(),
                                fileInfo.getFileAbsolutePath());
                    } else {
                        //if cut file,add it to the pasteMediaStoreHelper
                        deleteMediaStoreHelper.addRecord(fileInfo.getFileAbsolutePath());
                        pasteMediaStoreHelper.addRecord(newFile.getAbsolutePath());
                    }
                } else {
                    publishProgress(new ProgressInfo(
                            FileManagerService.OperationEventListener.ERROR_CODE_PASTE_UNSUCCESS, true));
                    mCutPasteErrorCount++;
                }
                /** Unnecessary to show process when cut in the same card*/
                updateProgressWithTime(updateInfo, fileInfo.getFile());
            }

            pasteMediaStoreHelper.updateRecords();
            if (PermissionUtils.hasStorageWritePermission(
                    mContext.getApplicationContext())) {
                deleteMediaStoreHelper.updateRecords();
            } else {
                mMediaProviderHelper.scanPathforMediaStore(
                        mSrcList.get(0).getFileParentPath());
            }

            int currentNumber = (int) updateInfo.getCurrentNumber();
            long totalNumber = updateInfo.getTotalNumber();
            publishProgress(new ProgressInfo("", TOTAL, TOTAL, currentNumber, totalNumber));
            if (mCutPasteErrorCount < mSrcList.size()) {
                return FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS;
            } else {
                return FileManagerService.OperationEventListener.ERROR_CODE_UNSUCCESS;
            }
        }

        private Integer cutPasteInDiffCard() {
            LogUtils.i(TAG, "cutPasteInDiffCard...");
            int ret = FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS;
            List<File> fileList = new ArrayList<File>();
            FileOperationTask.UpdateInfo updateInfo = new FileOperationTask.UpdateInfo();
            ret = getAllFileList(mSrcList, fileList, updateInfo);
            if (ret < 0) {
                LogUtils.i(TAG, "cutPasteInDiffCard,ret = " + ret);
                return ret;
            }

            if (isGreaterThan4G(updateInfo) && isFat32Disk(mDstFolder)) {
                LogUtils.i(TAG, "cutPasteInDiffCard, destination is FAT32.");
                return FileManagerService.OperationEventListener.ERROR_CODE_COPY_GREATER_4G_TO_FAT32;
            }

            if (!isEnoughSpace(updateInfo, mDstFolder)) {
                LogUtils.i(TAG, "cutPasteInDiffCard,not enough space.");
                return FileManagerService.OperationEventListener.ERROR_CODE_NOT_ENOUGH_SPACE;
            }

            List<File> removeFolderFiles = new LinkedList<File>();
            publishProgress(new ProgressInfo("", 0, TOTAL, 0, fileList.size()));
            byte[] buffer = new byte[BUFFER_SIZE];
            HashMap<String, String> pathMap = new HashMap<String, String>();
            if (!fileList.isEmpty()) {
                pathMap.put(fileList.get(0).getParent(), mDstFolder);
            }

            PasteMediaStoreHelper pasteMediaStoreHelper = new PasteMediaStoreHelper(
                    mMediaProviderHelper);
            DeleteMediaStoreHelper deleteMediaStoreHelper = new DeleteMediaStoreHelper(
                    mMediaProviderHelper);

            // Set dstFolder so we can scan folder instead of scanning each file one by one.
            pasteMediaStoreHelper.setDstFolder(mDstFolder);

            HashMap<File, FileInfo> cutFileInfoMap = new HashMap<File, FileInfo>();
            for (FileInfo fileInfo : mSrcList) {
                cutFileInfoMap.put(fileInfo.getFile(), fileInfo);
            }

            for (File file : fileList) {
                File dstFile = getDstFile(pathMap, file, mDstFolder);
                if (isCancelled()) {
                    pasteMediaStoreHelper.updateRecords();
                    if (PermissionUtils.hasStorageWritePermission(
                            mContext.getApplicationContext())) {
                        deleteMediaStoreHelper.updateRecords();
                    } else {
                        mMediaProviderHelper.scanPathforMediaStore(
                                mSrcList.get(0).getFileParentPath());
                    }
                    LogUtils.i(TAG, "cutPasteInDiffCard,user cancel.");
                    return FileManagerService.OperationEventListener.ERROR_CODE_USER_CANCEL;
                }
                if (dstFile == null) {
                    pasteMediaStoreHelper.updateRecords();
                    if (PermissionUtils.hasStorageWritePermission(
                            mContext.getApplicationContext())) {
                        deleteMediaStoreHelper.updateRecords();
                    } else {
                        mMediaProviderHelper.scanPathforMediaStore(
                                mSrcList.get(0).getFileParentPath());
                    }
                    publishProgress(new ProgressInfo(
                            FileManagerService.OperationEventListener.ERROR_CODE_PASTE_UNSUCCESS, true));
                    return FileManagerService.OperationEventListener.ERROR_CODE_PASTE_UNSUCCESS;
                }

                if (file.isDirectory()) {
                    if (mkdir(pathMap, file, dstFile)) {
                        pasteMediaStoreHelper.addRecord(dstFile.getAbsolutePath());
                        addItem(cutFileInfoMap, file, dstFile);
                        updateInfo.updateProgress(file.length());
                        updateInfo.updateCurrentNumber(1);
                        removeFolderFiles.add(0, file);
                        updateProgressWithTime(updateInfo, file);
                    }
                } else {
                    updateInfo.updateCurrentNumber(1);
                    ret = copyFile(buffer, file, dstFile, updateInfo);
                    LogUtils.i(TAG, "cutPasteInDiffCard ret2 = " + ret);
                    if (ret == FileManagerService.OperationEventListener.ERROR_CODE_USER_CANCEL) {
                        pasteMediaStoreHelper.updateRecords();
                        if (PermissionUtils.hasStorageWritePermission(
                                mContext.getApplicationContext())) {
                            deleteMediaStoreHelper.updateRecords();
                        } else {
                            mMediaProviderHelper.scanPathforMediaStore(
                                    mSrcList.get(0).getFileParentPath());
                        }
                        return ret;
                    } else if (ret < 0) {
                        publishProgress(new ProgressInfo(
                                FileManagerService.OperationEventListener.ERROR_CODE_PASTE_UNSUCCESS, true));
                        updateInfo.updateProgress(file.length());
                        updateInfo.updateCurrentNumber(1);
                        mCutPasteErrorCount++;
                    } else {
                        addItem(cutFileInfoMap, file, dstFile);
                        pasteMediaStoreHelper.addRecord(dstFile.getAbsolutePath());
                        if (deleteFile(file)) {
                            deleteMediaStoreHelper.addRecord(file.getAbsolutePath());
                        }
                    }
                }
            }

            for (File file : removeFolderFiles) {
                if (file.delete()) {
                    deleteMediaStoreHelper.addRecord(file.getAbsolutePath());
                }
            }
            pasteMediaStoreHelper.updateRecords();
            if (PermissionUtils.hasStorageWritePermission(
                    mContext.getApplicationContext())) {
                deleteMediaStoreHelper.updateRecords();
            } else {
                mMediaProviderHelper.scanPathforMediaStore(
                        mSrcList.get(0).getFileParentPath());
            }
            LogUtils.i(TAG, "cutPasteInDiffCard,return success.");
            if (mCutPasteErrorCount < mSrcList.size()) {
                return FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS;
            } else {
                return FileManagerService.OperationEventListener.ERROR_CODE_UNSUCCESS;
            }
        }
    }

    static class CopyPasteFilesTask extends FileOperationTask {
        private static final String TAG = "CopyPasteFilesTask";
        List<FileInfo> mSrcList = null;
        String mDstFolder = null;
        int mCopyFailCount = 0;

        public CopyPasteFilesTask(FileInfoManager fileInfoManager,
                                  FileManagerService.OperationEventListener operationEvent, Context context, List<FileInfo> src,
                                  String destFolder) {
            super(fileInfoManager, operationEvent, context);
            mSrcList = src;
            mDstFolder = destFolder;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            synchronized (mContext.getApplicationContext()) {
                LogUtils.i(TAG, "doInBackground...");
                //final long beforeTime  = System.currentTimeMillis();
                List<File> fileList = new ArrayList<File>();
                FileOperationTask.UpdateInfo updateInfo = new FileOperationTask.UpdateInfo();
                int ret = getAllFileList(mSrcList, fileList, updateInfo);
                if (ret < 0) {
                    LogUtils.i(TAG, "doInBackground,ret = " + ret);
                    return ret;
                }

                PasteMediaStoreHelper pasteMediaStoreHelper = new PasteMediaStoreHelper(
                        mMediaProviderHelper);

                // Set dstFolder so we can scan folder instead of scanning each file one by one.
                pasteMediaStoreHelper.setDstFolder(mDstFolder);

                HashMap<File, FileInfo> copyFileInfoMap = new HashMap<File, FileInfo>();
                for (FileInfo fileInfo : mSrcList) {
                    copyFileInfoMap.put(fileInfo.getFile(), fileInfo);
                }

                if (isGreaterThan4G(updateInfo) && isFat32Disk(mDstFolder)) {
                    LogUtils.i(TAG, "doInBackground, destination is FAT32.");
                    return FileManagerService.OperationEventListener.ERROR_CODE_COPY_GREATER_4G_TO_FAT32;
                }

                if (!isEnoughSpace(updateInfo, mDstFolder)) {
                    LogUtils.i(TAG, "doInBackground, not enough space.");
                    return FileManagerService.OperationEventListener.ERROR_CODE_NOT_ENOUGH_SPACE;
                }

                publishProgress(new ProgressInfo("", 0, TOTAL, 0, updateInfo.getTotalNumber()));

                byte[] buffer = new byte[BUFFER_SIZE];
                HashMap<String, String> pathMap = new HashMap<String, String>();
                if (!fileList.isEmpty()) {
                    pathMap.put(fileList.get(0).getParent(), mDstFolder);
                }
                for (File file : fileList) {
                    File dstFile = getDstFile(pathMap, file, mDstFolder);
                    if (isCancelled()) {
                        pasteMediaStoreHelper.updateRecords();
                        LogUtils.i(TAG, "doInBackground,user cancel.");
                        return FileManagerService.OperationEventListener.ERROR_CODE_USER_CANCEL;
                    }
                    if (dstFile == null) {
                        pasteMediaStoreHelper.updateRecords();
                        publishProgress(new ProgressInfo(
                                FileManagerService.OperationEventListener.ERROR_CODE_PASTE_UNSUCCESS, true));
                        return FileManagerService.OperationEventListener.ERROR_CODE_PASTE_UNSUCCESS;
                    }
                    if (file.isDirectory()) {
                        if (mkdir(pathMap, file, dstFile)) {
                            pasteMediaStoreHelper.addRecord(dstFile.getAbsolutePath());
                            addItem(copyFileInfoMap, file, dstFile);
                            updateInfo.updateProgress(file.length());
                            updateInfo.updateCurrentNumber(1);
                            updateProgressWithTime(updateInfo, file);
                        }
                    } else {
                        if (FileInfo.isDrmFile(file.getName()) || (file.exists() && !file.canRead())) {
                            publishProgress(new ProgressInfo(
                                    FileManagerService.OperationEventListener.ERROR_CODE_COPY_NO_PERMISSION, true));
                            updateInfo.updateProgress(file.length());
                            updateInfo.updateCurrentNumber(1);
                            mCopyFailCount++;
                            continue;
                        }
                        updateInfo.updateCurrentNumber(1);
                        ret = copyFile(buffer, file, dstFile, updateInfo);
                        if (ret == FileManagerService.OperationEventListener.ERROR_CODE_USER_CANCEL) {
                            pasteMediaStoreHelper.updateRecords();
                            return ret;
                        } else if (ret < 0) {
                            publishProgress(new ProgressInfo(ret, true));
                            updateInfo.updateProgress(file.length());
                            mCopyFailCount++;
                        } else {
                            pasteMediaStoreHelper.addRecord(dstFile.getAbsolutePath());
                            addItem(copyFileInfoMap, file, dstFile);
                        }
                    }
                }
                pasteMediaStoreHelper.updateRecords();
                LogUtils.i(TAG, "doInBackground,return success.");
                //final long endTime  = System.currentTimeMillis();
                // LogUtils.i(TAG, "doInBackground, ret 2 = " + ret + ",time cost is:" + (endTime-beforeTime)/1000);
                if (mCopyFailCount < fileList.size()) {
                    return FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS;
                } else {
                    return FileManagerService.OperationEventListener.ERROR_CODE_PASTE_UNSUCCESS;
                }
            }
        }
    }

    static class CreateFolderTask extends FileOperationTask {
        private static final String TAG = "CreateFolderTask";
        private final String mDstFolder;
        int mFilterType;

        public CreateFolderTask(FileInfoManager fileInfoManager,
                                FileManagerService.OperationEventListener operationEvent, Context context, String dstFolder,
                                int filterType) {
            super(fileInfoManager, operationEvent, context);
            mDstFolder = dstFolder;
            mFilterType = filterType;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            synchronized (mContext.getApplicationContext()) {
                int ret = FileManagerService.OperationEventListener.ERROR_CODE_UNSUCCESS;
                LogUtils.i(TAG, "doInBackground...");
                ret = FileUtils.checkFileName(FileUtils.getFileName(mDstFolder));
                if (ret < 0) {
                    LogUtils.i(TAG, "doInBackground, ret = " + ret);
                    return ret;
                }
                String dstFile = mDstFolder.trim();
                LogUtils.d(TAG, "Create a new folder, dstFile=" + dstFile);
                File dir = new File(dstFile);
                LogUtils.d(TAG, "The folder to be created exist: " + dir.exists());
                if (dir.exists()) {
                    LogUtils.i(TAG, "doInBackground,dir is exist.");
                    return FileManagerService.OperationEventListener.ERROR_CODE_FILE_EXIST;
                }

                File path = new File(FileUtils.getFilePath(mDstFolder));
                if (path.getFreeSpace() <= 0) {
                    LogUtils.i(TAG, "doInBackground,not enough space.");
                    return FileManagerService.OperationEventListener.ERROR_CODE_NOT_ENOUGH_SPACE;
                }
                if (dstFile.endsWith(".")) {
                    // our platform support vfat which doesn't allow the
                    // file/folder name end with '.'
                    LogUtils.i(TAG, "doInBackground,end with dot.");
                    while (dstFile.endsWith(".")) {
                        dstFile = dstFile.substring(0, dstFile.length() - 1);
                    }
                    dir = new File(dstFile);
                }

                if (dir.mkdirs()) {
                    FileInfo fileInfo = new FileInfo(dir);
                    fileInfo.getFileSizeStr();
                    if (!fileInfo.isHideFile()
                            || mFilterType == FileManagerService.FILE_FILTER_TYPE_ALL) {
                        mFileInfoManager.addItem(fileInfo);
                    }
                    mMediaProviderHelper.scanPathforMediaStore(
                            fileInfo.getFileAbsolutePath());
                    LogUtils.i(TAG, "doInBackground, mkdir return success.");
                    return FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS;
                }

                return FileManagerService.OperationEventListener.ERROR_CODE_UNSUCCESS;
            }
        }
    }

    static class RenameTask extends FileOperationTask {
        private static final String TAG = "RenameTask";
        private final FileInfo mDstFileInfo;
        private final FileInfo mSrcFileInfo;
        int mFilterType = 0;

        public RenameTask(FileInfoManager fileInfoManager, FileManagerService.OperationEventListener operationEvent,
                          Context context, FileInfo srcFile, FileInfo dstFile, int filterType) {
            super(fileInfoManager, operationEvent, context);
            mDstFileInfo = dstFile;
            mSrcFileInfo = srcFile;
            mFilterType = filterType;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            synchronized (mContext.getApplicationContext()) {
                int ret = FileManagerService.OperationEventListener.ERROR_CODE_UNSUCCESS;
                LogUtils.i(TAG, "doInBackground...");
                String dstFile = mDstFileInfo.getFileAbsolutePath();
                dstFile = dstFile.trim();
                LogUtils.d(TAG, "rename dstFile = " + dstFile);
                ret = FileUtils.checkFileName(FileUtils.getFileName(dstFile));
                if (ret < 0) {
                    LogUtils.i(TAG, "doInBackground, ret = " + ret);
                    return ret;
                }

                File newFile = new File(dstFile);
                File oldFile = new File(mSrcFileInfo.getFileAbsolutePath());

                if (newFile.exists()) {
                    LogUtils.i(TAG, "doInBackground,new file is exist.");
                    return FileManagerService.OperationEventListener.ERROR_CODE_FILE_EXIST;
                    // } else if (oldFile.isFile() && dstFile.endsWith(".")) {
                } else if (dstFile.endsWith(".")) {
                    // our platform support vfat which doesn't allow the
                    // file/folder name end with '.'
                    LogUtils.i(TAG, "doInBackground,end with dot.");
                    while (dstFile.endsWith(".")) {
                        dstFile = dstFile.substring(0, dstFile.length() - 1);
                    }
                    newFile = new File(dstFile);
                    if (newFile.exists()) {
                        return FileManagerService.OperationEventListener.ERROR_CODE_FILE_EXIST;
                    }
                }

                if (oldFile.renameTo(newFile)) {
                    FileInfo newFileInfo = new FileInfo(newFile);
                    newFileInfo.getFileSizeStr();
                    newFileInfo.setDuration(mSrcFileInfo.getDuration());
                    mFileInfoManager.removeItem(mSrcFileInfo);
                    if (!newFileInfo.isHideFile()
                            || mFilterType == FileManagerService.FILE_FILTER_TYPE_ALL) {
                        mFileInfoManager.addItem(newFileInfo);
                    }

                    if (PermissionUtils.hasStorageWritePermission(
                            mContext.getApplicationContext())) {
                        mMediaProviderHelper.updateInMediaStore(newFileInfo.getFileAbsolutePath(),
                                mSrcFileInfo.getFileAbsolutePath());
                    } else {
                        mMediaProviderHelper.scanPathforMediaStore(
                                newFileInfo.getFileParentPath());
                    }
                    //重命名蓝牙文件
                    SystemUtils.getBluetoothCursorUpdate(mContext, mSrcFileInfo, mDstFileInfo);
                    //重命名下载文件
                    SystemUtils.getDownloadCursorUpdate(mContext, mSrcFileInfo, mDstFileInfo);
                    LogUtils.i(TAG, "doInBackground,return success.");
                    return FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS;
                }

                return FileManagerService.OperationEventListener.ERROR_CODE_UNSUCCESS;
            }
        }
    }
}
