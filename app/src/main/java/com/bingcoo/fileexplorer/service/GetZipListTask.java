package com.bingcoo.fileexplorer.service;

import android.os.AsyncTask;
import android.text.TextUtils;

import com.bingcoo.fileexplorer.category.zip.OpenZipsActivity;
import com.bingcoo.fileexplorer.category.zip.ZipInfo;
import com.bingcoo.fileexplorer.util.FileUtils;
import com.bingcoo.fileexplorer.util.RarUtils;
import com.bingcoo.fileexplorer.util.ZipUtilsForZh;
import com.github.junrar.rarfile.FileHeader;

import org.apache.tools.zip.ZipEntry;

import java.io.IOException;
import java.text.CollationKey;
import java.text.Collator;
import java.text.RuleBasedCollator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 作者：zhshh
 * 内容摘要：获取压缩文件列表
 * 创建日期：2017/2/27
 * 修改者， 修改日期， 修改内容
 */

public class GetZipListTask extends AsyncTask<String, Integer, Map<Integer, List<ZipInfo>>> {

    private getZipListCallback mCallback;
    private OpenZipsActivity.FileType mFileType;
    private String mZipPath;
    private String mSeparator;
    private Map<Integer, List<ZipInfo>> mFileMap = new HashMap<>();
    private RuleBasedCollator mCollator;
    private StringComparator mComparator;
    private boolean isBusy=false;
//    public static final int SORT_BY_TYPE = 0;
//    public static final int SORT_BY_NAME = 1;
//    public static final int SORT_BY_SIZE = 2;
//    public static final int SORT_BY_TIME = 3;
//    private int mSortType = 0;

    public GetZipListTask(getZipListCallback mCallback, OpenZipsActivity.FileType fileType, String zipPath) {
        this.mCallback = mCallback;
        this.mFileType = fileType;
        this.mZipPath = zipPath;
    }

    /*public int getmSortType() {
        return mSortType;
    }

    public void setmSortType(int mSortType) {
        this.mSortType = mSortType;
    }*/

    @Override
    protected void onCancelled() {
        super.onCancelled();
        isBusy=false;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        isBusy=true;
    }

    @Override
    protected void onPostExecute(Map<Integer, List<ZipInfo>> integerListMap) {
        super.onPostExecute(integerListMap);
        if (mFileMap != null && mFileMap.size() > 0) {
            mCallback.successCallback(mFileMap);
        } else {
            mCallback.failCallback();
        }
        isBusy=false;
    }

    @Override
    protected Map<Integer, List<ZipInfo>> doInBackground(String... params) {
        switch (mFileType) {
            case ZIP:
                //zip文件
                mSeparator = "/";
                Enumeration<?> entries = null;
                try {
                    entries = ZipUtilsForZh.getEntries(mZipPath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                int tempIndex;//根据分隔符数目判断是第几层目录
                while (entries != null && entries.hasMoreElements()) {
                    ZipEntry zipEntry = (ZipEntry) entries.nextElement();
                    ZipInfo info = new ZipInfo();
                    info.setPath(zipEntry.getName());
                    String fileName;
                    if (zipEntry.isDirectory()) {
                        tempIndex = getSeparatorCount(zipEntry.getName()) - 1;
                        fileName = getFileName(zipEntry.getName(), true);
                        info.setDirectory(true);
                    } else {
                        tempIndex = getSeparatorCount(zipEntry.getName());
                        fileName = getFileName(zipEntry.getName(), false);
                        info.setDirectory(false);
                    }
                    info.setName(fileName);
                    info.setSize(zipEntry.getSize());
                    putListToMap(tempIndex, info);
                }
                //解决取不到第一层目录的问题
                if (mFileMap.get(0) == null || mFileMap.get(0).size() == 0) {
                    List<ZipInfo> secondList = mFileMap.get(1);
                    if (secondList != null && secondList.size() > 0) {
                        List<String> dirList = new ArrayList<>();
                        for (int i = 0; i < secondList.size(); i++) {
                            ZipInfo iInfo = secondList.get(i);
                            String dir = iInfo.getPath().substring(0, iInfo.getPath().indexOf("/") + 1);
                            addDirToList(dirList, dir);
                        }
                        for (int j = 0; j < dirList.size(); j++) {
                            ZipInfo info = new ZipInfo();
                            info.setPath(dirList.get(j));
                            info.setName(dirList.get(j).substring(0, dirList.get(j).length() - 1));
                            info.setDirectory(true);
                            putListToMap(0, info);
                        }
                    }
                }
                for (int i = 0; i < mFileMap.size(); i++) {
                    sortList(mFileMap.get(i));
                }
                break;
            case RAR:
                //rar文件
                mSeparator = "\\";
                List<FileHeader> fhList = RarUtils.getEntries(mZipPath);
                if (fhList == null) {
                    break;
                }
                for (int i = 0; i < fhList.size(); i++) {
                    FileHeader fileHeader = fhList.get(i);
                    ZipInfo info = new ZipInfo();
                    String fileName;
                    if (TextUtils.isEmpty(fileHeader.getFileNameW())) {
                        fileName = fileHeader.getFileNameString();
                    } else {
                        fileName = fileHeader.getFileNameW();
                    }
                    tempIndex = getSeparatorCount(fileName);
                    if (fileHeader.isDirectory()) {
                        info.setPath(fileName + mSeparator);
                        fileName = getFileName(fileName, true);
                        info.setDirectory(true);
                    } else {
                        info.setPath(fileName);
                        fileName = getFileName(fileName, false);
                        info.setDirectory(false);
                    }
                    info.setName(fileName);
                    info.setSize(fhList.get(i).getFullUnpackSize());
                    putListToMap(tempIndex, info);
                }
                for (int i = 0; i < mFileMap.size(); i++) {
                    sortList(mFileMap.get(i));
                }
                break;
            default:

                break;
        }
        return mFileMap;
    }


    public interface getZipListCallback {
        void successCallback(Map<Integer, List<ZipInfo>> fileMap);

        void failCallback();
    }

    /**
     * 获取文件File.separator的数目
     *
     * @param name
     * @return
     */
    private int getSeparatorCount(String name) {

        int count = 0;
        if (name == null) {
            return 0;
        }
        while (name.contains(mSeparator)) {
            count++;
            name = name.substring(name.indexOf(mSeparator) + 1, name.length());
        }
        return count;
    }

    /**
     * 获取文件名字带扩展名
     *
     * @param fileName
     * @param isDirectory
     * @return
     */
    private String getFileName(String fileName, boolean isDirectory) {

        if (TextUtils.isEmpty(fileName)) {
            return null;
        }
        String name = fileName;
        if (isDirectory) {
            //文件夹
            if (mFileType == OpenZipsActivity.FileType.ZIP) {
                //zip文件夹名字最后有“/”，rar没有
                name = fileName.substring(0, fileName.length() - 1);
            }
            int lastIndex = name.lastIndexOf(mSeparator);
            if (lastIndex >= 0) {
                return name.substring(lastIndex + 1, name.length());
            } else {
                return name;
            }
        } else {
            //文件
            int lastIndex = name.lastIndexOf(mSeparator);
            if (lastIndex >= 0) {
                return name.substring(lastIndex + 1, fileName.length());
            } else {
                return name;
            }
        }
    }

    /**
     * 向对应目录中添加文件对象
     *
     * @param index
     * @param info
     * @return
     */
    private Map<Integer, List<ZipInfo>> putListToMap(int index, ZipInfo info) {
        if (mFileMap.containsKey(index)) {
            List<ZipInfo> indexList = mFileMap.get(index);
            if (indexList == null) {
                indexList = new ArrayList<>();
                indexList.add(info);
                mFileMap.put(index, indexList);
            } else {
                if (!indexList.contains(info)) {
                    indexList.add(info);
                }
            }
        } else {
            List<ZipInfo> indexList = new ArrayList<>();
            indexList.add(info);
            mFileMap.put(index, indexList);
        }
        return mFileMap;
    }

    protected void sortList(List<ZipInfo> list) {

        if (mComparator == null) {
            mComparator = new StringComparator();
        }
        if (mCollator == null) {
            mCollator = (RuleBasedCollator) Collator.getInstance(java.util.Locale.CHINA);
        }
        if (list != null) {
            Collections.sort(list, mComparator);
        }
    }

    class StringComparator implements Comparator<ZipInfo> {

        @Override
        public int compare(ZipInfo op, ZipInfo oq) {
            // if only one is directory
            boolean isOpDirectory = op.isDirectory();
            boolean isOqDirectory = oq.isDirectory();
            if (isOpDirectory ^ isOqDirectory) {
                // one is a folder and one is not a folder
                return isOpDirectory ? -1 : 1;
            }
//            return sortByName(op,oq);
            return sortByType(op, oq);
        }
    }

    private int sortByName(ZipInfo op, ZipInfo oq) {

        CollationKey c1 = mCollator.getCollationKey(op.getName());
        CollationKey c2 = mCollator.getCollationKey(oq.getName());
        return mCollator.compare(c1.getSourceString(), c2.getSourceString());
    }

    /**
     * This method compares the files based on their type
     *
     * @param op the first file
     * @param oq the second file
     * @return a negative integer, zero, or a positive integer as the first file is smaller than,
     * equal to, or greater than the second file, ignoring case considerations.
     */
    private int sortByType(ZipInfo op, ZipInfo oq) {
        boolean isOpDirectory = op.isDirectory();
        boolean isOqDirectory = oq.isDirectory();
        if (!isOpDirectory && !isOqDirectory) {
            // both are not directory
            String opExtension = FileUtils.getFileExtension(op.getName());
            String oqExtension = FileUtils.getFileExtension(oq.getName());
            if (opExtension == null && oqExtension != null) {
                return -1;
            } else if (opExtension != null && oqExtension == null) {
                return 1;
            } else if (opExtension != null && oqExtension != null) {
                if (!opExtension.equalsIgnoreCase(oqExtension)) {
                    return opExtension.compareToIgnoreCase(oqExtension);
                }
            }
        }
        return sortByName(op, oq);
    }

    /**
     * This method compares the files based on their sizes
     *
     * @param op the first file
     * @param oq the second file
     * @return a negative integer, zero, or a positive integer as the first file is smaller than,
     * equal to, or greater than the second file, ignoring case considerations.
     */
    private int sortBySize(ZipInfo op, ZipInfo oq) {
        if (!op.isDirectory() && !oq.isDirectory()) {
            long opSize = op.getSize();
            long oqSize = oq.getSize();
            if (opSize != oqSize) {
                return opSize > oqSize ? -1 : 1;
            }
        }
        return sortByName(op, oq);
    }

    /*private int sortByTime(ZipInfo op, ZipInfo oq) {
        long opTime = op.getLastModifiedTime();
        long oqTime = oq.getLastModifiedTime();
        if (opTime != oqTime) {
            return opTime > oqTime ? -1 : 1;
        }
        return sortByName(op, oq);
    }*/

    private List<String> addDirToList(List<String> list, String dir) {
        if (list != null && dir != null && !list.contains(dir)) {
            list.add(dir);
        }
        return list;
    }

    public  boolean isBusy(){
        return isBusy||getStatus().equals(Status.RUNNING);
    }
}