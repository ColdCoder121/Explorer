package com.bingcoo.fileexplorer.service;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;

import com.bingcoo.fileexplorer.interfaces.ItemAdapter;
import com.bingcoo.fileexplorer.util.FileCategoryHelper;
import com.bingcoo.fileexplorer.util.FileInfo;
import com.bingcoo.fileexplorer.util.FileInfoManager;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.SystemUtils;

import static com.bingcoo.fileexplorer.service.FileManagerService.OperationEventListener.ERROR_CODE_UNSUCCESS;
import static com.bingcoo.fileexplorer.service.FileManagerService.OperationEventListener.ERROR_CODE_USER_CANCEL;

/**
 * 类名称：SearchCategoryTask
 * 作者：David
 * 内容摘要：
 * 创建日期：2017/1/12
 * 修改者， 修改日期， 修改内容
 */
public class SearchCategoryTask extends ListCategoryTask {
    private static final String TAG = "SearchCategoryTask";

    static {
        LogUtils.setDebug(TAG, true);
    }

    protected String mFileName;
    protected static final int FIRST_NEED_PROGRESS = 250;
    protected static final int NEXT_NEED_PROGRESS = 200;

    public SearchCategoryTask(Context context, FileInfoManager fileInfoManager,
                              FileManagerService.OperationEventListener operationEvent,
                              FileCategoryHelper.FileCategory fc, int sortType, String fileName) {
        super(context, fileInfoManager, operationEvent, fc, sortType);
        mFileName = fileName;
    }

    @Override
    protected Integer doInBackground(Void... params) {
        int total;
        int progress = 0;
        long startLoadTime = System.currentTimeMillis();
        Cursor cursor;
        if (TextUtils.isEmpty(mFileName)) {
            return ERROR_CODE_USER_CANCEL;
        }
        if (FileCategoryHelper.FileCategory.Download == mFileCategory) {
            cursor = SystemUtils.getDownloadCursor(mContext);
        } else if (FileCategoryHelper.FileCategory.Bluetooth == mFileCategory) {
            cursor = SystemUtils.getBluetoothCursor(mContext);
        } else {
            FileCategoryHelper helper = new FileCategoryHelper(mContext);
            cursor = helper.query(mFileCategory, mSortType);
        }
        if (null != cursor) {
            total = cursor.getCount();
            int searchNumber = 0;//搜索出来的满足条件的数目
            long loadTime;
            int nextUpdateTime = FIRST_NEED_PROGRESS;
            ItemAdapter itemAdapter = getAdapter(cursor);

            for (int i = 0; i < total; i++) {
                if (isCancelled()) {
                    LogUtils.w(TAG, " doInBackground, cancel.");
                    return ERROR_CODE_UNSUCCESS;
                }
                itemAdapter.getItem(i);
                LogUtils.d(TAG, "doInBackground: path=" + itemAdapter.getFilePath());
                loadTime = System.currentTimeMillis() - startLoadTime;
                progress++;

                String filePath = itemAdapter.getFilePath();
                if (!TextUtils.isEmpty(filePath)) {
                    FileInfo item = new FileInfo(filePath);
                    String showName = item.getShowName();
                    if (showName.startsWith(".")) {
                        LogUtils.i(TAG, " doInBackground, start with., continue.");
                        continue;
                    }

                    LogUtils.d(TAG, "doInBackground: showName=" + showName + ", mFileName=" + mFileName);
                    if (!TextUtils.isEmpty(mFileName) && (-1 < showName.toUpperCase().indexOf(mFileName.toUpperCase()))) {
                        searchNumber++;
                        if (FileCategoryHelper.FileCategory.Video == mFileCategory) {
                            item.setDuration(getDuration(item.getFileAbsolutePath()));
                        }
                        item.getFileSizeStr();
//                        publishProgress(new ProgressInfo(item, progress, total, progress, total));
                        publishProgress(new ProgressInfo(item, progress, searchNumber, progress, total));
                    }
                }
            }
            cursor.close();
            LogUtils.d(TAG, "doInBackground ERROR_CODE_SUCCESS");
            //搜索结果为0时也传回一个progress
            if(searchNumber==0){
                publishProgress(new ProgressInfo(FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS, true));
            }
            return FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS;
        }

        LogUtils.w(TAG, " doInBackground, ERROR_CODE_UNSUCCESS");
        return ERROR_CODE_UNSUCCESS;
    }

}
