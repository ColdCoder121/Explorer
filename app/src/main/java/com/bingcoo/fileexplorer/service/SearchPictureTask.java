package com.bingcoo.fileexplorer.service;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;

import com.bingcoo.fileexplorer.interfaces.ItemAdapter;
import com.bingcoo.fileexplorer.util.FileCategoryHelper;
import com.bingcoo.fileexplorer.util.FileInfo;
import com.bingcoo.fileexplorer.util.FileInfoManager;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.SystemUtils;

/**
 * 类名称：SearchPictureTask
 * 作者：David
 * 内容摘要：
 * 创建日期：2017/2/10
 * 修改者， 修改日期， 修改内容
 */
public class SearchPictureTask extends SearchCategoryTask {
    private static final String TAG = "SearchPictureTask";

    static {
        LogUtils.setDebug(TAG, true);
    }

    private String mDir;

    public SearchPictureTask(Context context, FileInfoManager fileInfoManager,
                             FileManagerService.OperationEventListener operationEvent, FileCategoryHelper.FileCategory fc,
                             int sortType, String fileName, String dir) {
        super(context, fileInfoManager, operationEvent, fc, sortType, fileName);
        mDir = dir;
    }

    @Override
    protected Integer doInBackground(Void... params) {
        int total;
        int progress = 0;
        int mSearchNum = 0;
        long startLoadTime = System.currentTimeMillis();
        Cursor cursor;

        if (FileCategoryHelper.FileCategory.Download == mFileCategory) {
            cursor = SystemUtils.getDownloadCursor(mContext);
        } else if (FileCategoryHelper.FileCategory.Bluetooth == mFileCategory) {
            cursor = SystemUtils.getBluetoothCursor(mContext);
        } else {
            FileCategoryHelper helper = new FileCategoryHelper(mContext);
            cursor = helper.query(mFileCategory, mSortType);
        }
        if (null != cursor) {
            total = cursor.getCount();
            long loadTime;
            int nextUpdateTime = FIRST_NEED_PROGRESS;
            ItemAdapter itemAdapter = getAdapter(cursor);

            for (int i = 0; i < total; i++) {
                if (isCancelled()) {
                    LogUtils.w(TAG, " doInBackground, cancel.");
                    return FileManagerService.OperationEventListener.ERROR_CODE_UNSUCCESS;
                }
                itemAdapter.getItem(i);
                LogUtils.d(TAG, "doInBackground: path=" + itemAdapter.getFilePath());
                loadTime = System.currentTimeMillis() - startLoadTime;
                progress++;

                String filePath = itemAdapter.getFilePath();
                if (!TextUtils.isEmpty(filePath)) {
                    FileInfo item = new FileInfo(filePath);
                    String showName = item.getShowName();
                    String parentDir = item.getFileParentPath();
                    LogUtils.d(TAG, "doInBackground: showName=" + showName + ", parentDir=" + parentDir + ", mFileName=" + mFileName);

                    if (showName.startsWith(".")) {
                        LogUtils.i(TAG, " doInBackground, start with., continue.");
                        continue;
                    }

                    if (!TextUtils.isEmpty(mFileName)
                            && !TextUtils.isEmpty(mDir)
                            && (mDir.equals(parentDir))
                            && (-1 < showName.toUpperCase().indexOf(mFileName.toUpperCase()))) {
                        mSearchNum++;
                        if (FileCategoryHelper.FileCategory.Video == mFileCategory) {
                            item.setDuration(getDuration(item.getFileAbsolutePath()));
                        }
                        item.getFileSizeStr();
                        publishProgress(new ProgressInfo(item, progress, mSearchNum, progress, total));
//                        publishProgress(new ProgressInfo(item, progress, total, progress, total));
                    }
                }
            }
            cursor.close();
            LogUtils.d(TAG, "doInBackground ERROR_CODE_SUCCESS");
            return FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS;
        }

        LogUtils.w(TAG, " doInBackground, ERROR_CODE_UNSUCCESS");
        return FileManagerService.OperationEventListener.ERROR_CODE_UNSUCCESS;
    }

}
