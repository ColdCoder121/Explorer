package com.bingcoo.fileexplorer.service;

import android.content.Context;

import com.bingcoo.fileexplorer.service.FileManagerService.OperationEventListener;
import com.bingcoo.fileexplorer.util.FileInfo;
import com.bingcoo.fileexplorer.util.FileInfoManager;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.MountPointManager;

import java.io.File;


class ListFileTask extends BaseAsyncTask {
    private static final String TAG = "ListFileTask";
    static {
        LogUtils.setDebug(TAG, true);
    }
    private final String mPath;
    private final int mFilterType;
    private Context mContext;
    private static final int FIRST_NEED_PROGRESS = 250;
    private static final int NEXT_NEED_PROGRESS = 200;

    /**
     * Constructor for ListFileTask, construct a ListFileTask with certain
     * parameters
     *
     * @param fileInfoManager a instance of FileInfoManager, which manages
     *            information of files in FileManager.
     * @param operationEvent a instance of OperationEventListener, which is a
     *            interface doing things before/in/after the task.
     * @param path ListView will list files included in this path.
     * @param filterType to determine which files will be listed.
     */
    public ListFileTask(Context context, FileInfoManager fileInfoManager,
                        FileManagerService.OperationEventListener operationEvent, String path, int filterType) {
        super(fileInfoManager, operationEvent);
        mContext = context;
        mPath = path;
        mFilterType = filterType;
    }

    @Override
    protected Integer doInBackground(Void... params) {
        synchronized (mContext.getApplicationContext()) {
//            List<FileInfo> fileInfoList = new ArrayList<FileInfo>();
            File[] files = null;
            int total = 0;
            int progress = 0;
            long startLoadTime = System.currentTimeMillis();
            LogUtils.d(TAG, "doInBackground path = " + mPath);
            final boolean isRootPath = MountPointManager.getInstance().isRootPath(mPath);
            if (isRootPath) {
                 MountPointManager.getInstance().updateMountPointSpaceInfo();
            }

            File dir = new File(mPath);
            if (dir.exists()) {
                files = dir.listFiles();
                if (files == null) {
                    LogUtils.w(TAG, "doInBackground,directory is null");
                    return OperationEventListener.ERROR_CODE_UNSUCCESS;
                }
            } else {
                LogUtils.w(TAG, "doInBackground,directory is not exist.");
                return OperationEventListener.ERROR_CODE_UNSUCCESS;
            }
            total = files.length;
            long loadTime = 0;
            int nextUpdateTime = FIRST_NEED_PROGRESS;
            LogUtils.d(TAG, "doInBackground, total = " + total);
            for (int i = 0; i < files.length; i++) {
                LogUtils.d(TAG, "doInBackground, i = " + i + ", fileName=" + files[i].getName());

                if (isCancelled()) {
                    LogUtils.w(TAG, " doInBackground, cancel.");
                    return OperationEventListener.ERROR_CODE_UNSUCCESS;
                }

                if (mFilterType == FileManagerService.FILE_FILTER_TYPE_DEFAULT) {
                    if (files[i].getName().startsWith(".")) {
                        LogUtils.d(TAG, " doInBackground, start with., continue.");
                        continue;
                    }
                }

                if (mFilterType == FileManagerService.FILE_FILTER_TYPE_FOLDER) {
                    if (!files[i].isDirectory()) {
                        LogUtils.d(TAG, " doInBackground, is not directory,continue..");
                        continue;
                    }
                }

                FileInfo item = new FileInfo(files[i]);
                //加载慢
                /*if (!isRootPath) {
                    item.getFileSizeStr();
                }*/
                mFileInfoManager.addItem(item);
                loadTime = System.currentTimeMillis() - startLoadTime;
                progress++;

                if (loadTime > nextUpdateTime) {
                    startLoadTime = System.currentTimeMillis();
                    nextUpdateTime = NEXT_NEED_PROGRESS;
                    LogUtils.d(TAG, "doInBackground, publish progress.");
                    publishProgress(new ProgressInfo("", progress, total, progress, total));

                }
            }
            LogUtils.d(TAG, "doInBackground ERROR_CODE_SUCCESS");
            return OperationEventListener.ERROR_CODE_SUCCESS;
        }
    }
}
