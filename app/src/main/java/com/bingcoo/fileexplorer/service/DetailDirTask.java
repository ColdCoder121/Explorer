package com.bingcoo.fileexplorer.service;

import com.bingcoo.fileexplorer.util.FileInfo;
import com.bingcoo.fileexplorer.util.FileInfoManager;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.MountPointManager;

import java.io.File;
import java.util.List;

/**
 * 类名称：DetailDirTask
 * 作者：David
 * 内容摘要：
 * 创建日期：2017/1/20
 * 修改者， 修改日期， 修改内容
 */
public class DetailDirTask extends BaseAsyncTask {
    private static final String TAG = "DetailDirTask";

    private long mTotal = 0;
    private List<FileInfo> mFileInfos;

    public DetailDirTask(FileInfoManager fileInfoManager,
                         FileManagerService.OperationEventListener listener, List<FileInfo> fileInfos) {
        super(fileInfoManager, listener);
        mFileInfos = fileInfos;
    }


    @Override
    protected Integer doInBackground(Void... params) {
        LogUtils.d(TAG, "doInBackground...");
        if (null != mFileInfos) {
            for (FileInfo fileInfo : mFileInfos) {
                if (fileInfo.isDirectory()) {
                    if (!MountPointManager.getInstance().isRootPath(
                            fileInfo.getFileAbsolutePath())) {
                        final File[] files = fileInfo.getFile().listFiles();
                        int ret = FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS;
                        if (files != null) {
                            for (File file : files) {
                                ret = getContentSize(file);
                                if (ret < 0) {
                                    LogUtils.i(TAG, "doInBackground,ret = " + ret);
                                    return ret;
                                }
                            }
                        }
                    }
                }
            }
        }
        return FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS;
    }

    public int getContentSize(File root) {
        LogUtils.d(TAG, "getContentSize...");
        int ret = FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS;
        if (root.isDirectory()) {
            File[] files = root.listFiles();
            if (files == null) {
                LogUtils.d(TAG, "getContentSize, listFiles is null, return");
                return ret;
            }
            for (File file : files) {
                if (isCancelled()) {
                    return FileManagerService.OperationEventListener.ERROR_CODE_USER_CANCEL;
                }
                ret = getContentSize(file);
                if (ret < 0) {
                    LogUtils.i(TAG, "getContentSize ,ret = " + ret);
                    return ret;
                }
            }
        }
        mTotal += root.length();
        publishProgress(new ProgressInfo("", 0, mTotal, 0, mTotal));
        return FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS;
    }
}
