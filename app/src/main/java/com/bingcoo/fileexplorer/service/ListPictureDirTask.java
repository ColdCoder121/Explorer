package com.bingcoo.fileexplorer.service;

import android.content.Context;
import android.database.Cursor;

import com.bingcoo.fileexplorer.interfaces.ItemAdapter;
import com.bingcoo.fileexplorer.util.FileCategoryHelper;
import com.bingcoo.fileexplorer.util.FileInfo;
import com.bingcoo.fileexplorer.util.FileInfoManager;
import com.bingcoo.fileexplorer.util.FileUtils;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.SystemUtils;

import java.io.File;
import java.util.HashSet;

/**
 * 类名称：ListPictureDirTask
 * 作者：David
 * 内容摘要：
 * 创建日期：2017/1/19
 * 修改者， 修改日期， 修改内容
 */
public class ListPictureDirTask extends ListCategoryTask {
    private static final String TAG = "ListPictureDirTask";
    protected HashSet<String> mDirPaths = new HashSet<>();

    public ListPictureDirTask(Context context, FileInfoManager fileInfoManager,
                              FileManagerService.OperationEventListener operationEvent,
                              FileCategoryHelper.FileCategory fc, int sortType) {
        super(context, fileInfoManager, operationEvent, fc, sortType);
    }

    @Override
    protected Integer doInBackground(Void... params) {
        int total;
        int progress = 0;
        long startLoadTime = System.currentTimeMillis();
        Cursor cursor;

        if (FileCategoryHelper.FileCategory.Download == mFileCategory) {
            cursor = SystemUtils.getDownloadCursor(mContext);
        } else if (FileCategoryHelper.FileCategory.Bluetooth == mFileCategory) {
            cursor = SystemUtils.getBluetoothCursor(mContext);
        } else {
            FileCategoryHelper helper = new FileCategoryHelper(mContext);
            cursor = helper.query(mFileCategory, mSortType);
        }

        if (null != cursor) {
            total = cursor.getCount();
            long loadTime;
            int nextUpdateTime = FIRST_NEED_PROGRESS;
            ItemAdapter itemAdapter = getAdapter(cursor);

            for (int i = 0; i < total; i++) {
                if (isCancelled()) {
                    LogUtils.w(TAG, " doInBackground, cancel.");
                    return FileManagerService.OperationEventListener.ERROR_CODE_UNSUCCESS;
                }
                itemAdapter.getItem(i);

                String path = itemAdapter.getFilePath();
                LogUtils.d(TAG, "doInBackground: item path=" + path);
                File file = new File(path);
                if (!file.exists()) {
                    continue;
                }
                if(FileUtils.isHiddenFile(file)){
                    continue;
                }
                // 获取该图片的目录路径名
                File parentFile = file.getParentFile();
                if (parentFile == null || !parentFile.exists()) {
                    continue;
                }
                if (FileUtils.isHiddenFile(parentFile)) {
                    continue;
                }
                String dirPath = parentFile.getAbsolutePath();
                // 利用一个HashSet防止多次扫描同一个文件夹
                if (mDirPaths.contains(dirPath)) {
                    continue;
                } else {
                    LogUtils.d(TAG, "doInBackground: dirPath=" + dirPath);
                    mDirPaths.add(dirPath);
                }

                if (parentFile.list() == null) {
                    continue;
                }

                FileInfo item = new FileInfo(dirPath);
                item.getFileSizeStr();
                mFileInfoManager.addItem(item);
                loadTime = System.currentTimeMillis() - startLoadTime;
                progress++;

                if (loadTime > nextUpdateTime) {
                    startLoadTime = System.currentTimeMillis();
                    nextUpdateTime = NEXT_NEED_PROGRESS;
                    LogUtils.d(TAG, "doInBackground, publish progress.");
                    publishProgress(new ProgressInfo("", progress, total, progress, total));
                }
            }
            cursor.close();
            LogUtils.d(TAG, "doInBackground ERROR_CODE_SUCCESS");
            return FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS;
        }

        LogUtils.w(TAG, " doInBackground, ERROR_CODE_UNSUCCESS");
        return FileManagerService.OperationEventListener.ERROR_CODE_UNSUCCESS;
    }
}
