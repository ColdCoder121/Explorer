package com.bingcoo.fileexplorer.service;

import android.content.ContentResolver;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;

import com.bingcoo.fileexplorer.service.FileManagerService.OperationEventListener;
import com.bingcoo.fileexplorer.util.FileInfo;
import com.bingcoo.fileexplorer.util.FileInfoManager;
import com.bingcoo.fileexplorer.util.LogUtils;

import static com.bingcoo.fileexplorer.service.FileManagerService.OperationEventListener.ERROR_CODE_USER_CANCEL;


class SearchTask extends BaseAsyncTask {
    private static final String TAG = "SearchTask";

    static {
        LogUtils.setDebug(TAG, true);
    }

    private final String mSearchName;
    private final String mPath;
    private final ContentResolver mContentResolver;

    /**
     * Constructor for SearchTask
     *
     * @param fileInfoManager a instance of FileInfoManager, which manages information of files in
     *                        FileManager.
     * @param operationEvent  a instance of OperationEventListener, which is a interface doing things
     *                        before/in/after the task.
     * @param searchName      the String, which need search
     * @param path            the limitation, which limit the search just in the file represented by the path
     * @param contentResolver the contentResolver for query(search).
     */
    public SearchTask(FileInfoManager fileInfoManager, OperationEventListener operationEvent,
                      String searchName, String path, ContentResolver contentResolver) {
        super(fileInfoManager, operationEvent);
        mContentResolver = contentResolver;
        mPath = path;
        mSearchName = searchName;
    }

    @Override
    protected Integer doInBackground(Void... params) {
        LogUtils.d(TAG, "doInBackground...");
        if (TextUtils.isEmpty(mSearchName)) {
            return ERROR_CODE_USER_CANCEL;
        }
        Uri uri = MediaStore.Files.getContentUri("external");
        int ret = OperationEventListener.ERROR_CODE_SUCCESS;
        String[] projection = {MediaStore.Files.FileColumns.DATA,};
        StringBuilder sb = new StringBuilder();

        sb.append(MediaStore.Files.FileColumns.FILE_NAME + " like ");
        DatabaseUtils.appendEscapedSQLString(sb, "%" + mSearchName + "%");
        sb.append(" and ").append(MediaStore.Files.FileColumns.DATA + " like ");
        DatabaseUtils.appendEscapedSQLString(sb, "%" + mPath + "%");

        String selection = sb.toString();
        Cursor cursor ;
        //偶现不包含file_name列名
        try {
            cursor = mContentResolver.query(uri, projection, selection, null, null);
        }catch (Exception e){
            e.printStackTrace();
            LogUtils.e(TAG,e.toString());
            cursor=null;
        }
        LogUtils.d(TAG, "projection = " + projection[0]);
        LogUtils.d(TAG, "selection = " + selection);
        if (cursor == null) {
            LogUtils.d(TAG, "doInBackground, cursor is null.");
            return OperationEventListener.ERROR_CODE_UNSUCCESS;
        }

        int total = cursor.getCount();
        int mSearchNum = 0;
        publishProgress(new ProgressInfo("", 0, mSearchNum, 0, total));
        int progress = 0;
        cursor.moveToFirst();
        try {
            while (!cursor.isAfterLast()) {
                if (isCancelled()) {
                    LogUtils.d(TAG, "doInBackground,cancel.");
                    ret = OperationEventListener.ERROR_CODE_USER_CANCEL;
                    break;
                }
                String name = cursor.getString(cursor
                        .getColumnIndex(MediaStore.Files.FileColumns.DATA));
                cursor.moveToNext();
                progress++;
                if (!TextUtils.isEmpty(name)) {
                    FileInfo item = new FileInfo(name);

                    String fileName = item.getShowName();
                    if (fileName.startsWith(".")) {
                        LogUtils.i(TAG, " doInBackground, start with., continue.");
                        continue;
                    }
                    mSearchNum++;
                    item.getFileSizeStr();
                    publishProgress(new ProgressInfo(item, progress, mSearchNum, progress, total));
                }
            }
        } finally {
            cursor.close();
        }

        LogUtils.i(TAG, "doInBackground,ret = " + ret);
        return ret;
    }

}
