package com.bingcoo.fileexplorer.service;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;

import com.bingcoo.fileexplorer.interfaces.ItemAdapter;
import com.bingcoo.fileexplorer.util.FileCategoryHelper;
import com.bingcoo.fileexplorer.util.FileInfo;
import com.bingcoo.fileexplorer.util.FileInfoManager;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.SystemUtils;

import java.io.File;

/**
 * 类名称：SearchPictureDirTask
 * 作者：David
 * 内容摘要：
 * 创建日期：2017/1/20
 * 修改者， 修改日期， 修改内容
 */
public class SearchPictureDirTask extends ListPictureDirTask {
    private static final String TAG = "SearchPictureDirTask";

    static {
        LogUtils.setDebug(TAG, true);
    }

    private String mFileName;

    public SearchPictureDirTask(Context context, FileInfoManager fileInfoManager,
                                FileManagerService.OperationEventListener operationEvent,
                                FileCategoryHelper.FileCategory fc, int sortType, String fileName) {
        super(context, fileInfoManager, operationEvent, fc, sortType);
        mFileName = fileName;
    }

    @Override
    protected Integer doInBackground(Void... params) {
        long total;
        long results = 0;
        int progress = 0;
        long startLoadTime = System.currentTimeMillis();
        Cursor cursor;
        LogUtils.d(TAG, "doInBackground: mFileName=" + mFileName);
        if (FileCategoryHelper.FileCategory.Download == mFileCategory) {
            cursor = SystemUtils.getDownloadCursor(mContext);
        } else if (FileCategoryHelper.FileCategory.Bluetooth == mFileCategory) {
            cursor = SystemUtils.getBluetoothCursor(mContext);
        } else {
            FileCategoryHelper helper = new FileCategoryHelper(mContext);
            cursor = helper.query(mFileCategory, mSortType);
        }

        if (null != cursor) {
            total = cursor.getCount();
            long loadTime;
            int nextUpdateTime = FIRST_NEED_PROGRESS;
            ItemAdapter itemAdapter = getAdapter(cursor);

            for (int i = 0; i < total; i++) {
                if (isCancelled()) {
                    LogUtils.w(TAG, " doInBackground, cancel.");
                    return FileManagerService.OperationEventListener.ERROR_CODE_UNSUCCESS;
                }
                itemAdapter.getItem(i);
                progress++;
                String path = itemAdapter.getFilePath();
                LogUtils.d(TAG, "doInBackground: item path=" + path);
                File file = new File(path);
                if (!file.exists()) {
                    continue;
                }
                // 获取该图片的目录路径名
                File parentFile = file.getParentFile();
                if (parentFile == null || !parentFile.exists()) {
                    continue;
                }
                String dirPath = parentFile.getAbsolutePath();
                // 利用一个HashSet防止多次扫描同一个文件夹
                if (mDirPaths.contains(dirPath)) {
                    continue;
                } else {
                    mDirPaths.add(dirPath);
                }
                if (parentFile.list() == null) {
                    continue;
                }
                loadTime = System.currentTimeMillis() - startLoadTime;
                String dirPathName = parentFile.getName();
                LogUtils.d(TAG, "doInBackground: dirPath=" + dirPath + ", dirPathName=" + dirPathName
                            + ", index=" + dirPathName.toUpperCase().indexOf(mFileName.toUpperCase()));

                if (dirPathName.startsWith(".")) {
                    LogUtils.i(TAG, " doInBackground, start with., continue.");
                    continue;
                }

                if (!TextUtils.isEmpty(mFileName)
                        && (-1<dirPathName.toUpperCase().indexOf(mFileName.toUpperCase()))) {
                    results++;
                    FileInfo item = new FileInfo(dirPath);
                    item.getFileSizeStr();
                    LogUtils.d(TAG, "doInBackground: i=" + i + ", progress=" + progress);
                    publishProgress(new ProgressInfo(item, progress, results, progress,total));
                }
            }
            cursor.close();
            LogUtils.d(TAG, "doInBackground ERROR_CODE_SUCCESS");
            return FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS;
        }

        LogUtils.w(TAG, " doInBackground, ERROR_CODE_UNSUCCESS");
        return FileManagerService.OperationEventListener.ERROR_CODE_UNSUCCESS;
    }


}
