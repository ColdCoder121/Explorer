package com.bingcoo.fileexplorer.service;

import com.bingcoo.fileexplorer.util.FileInfo;
import com.bingcoo.fileexplorer.util.FileInfoManager;
import com.bingcoo.fileexplorer.service.FileManagerService.OperationEventListener;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.MountPointManager;

import java.io.File;


class DetailInfoTask extends BaseAsyncTask {
    private static final String TAG = "DetailInfoTask";

    static {
        LogUtils.setDebug(TAG, true);
    }

    private final FileInfo mDetailFileInfo;
    private long mTotal = 0;

    /**
     * Constructor of DetailInfoTask
     *
     * @param fileInfoManager a instance of FileInfoManager, which manages information of files in
     *            FileManager.
     * @param operationEvent a instance of OperationEventListener, which is a interface doing things
     *            before/in/after the task.
     * @param file a instance of FileInfo, which contains all data about a file.
     */
    public DetailInfoTask(FileInfoManager fileInfoManager, OperationEventListener operationEvent,
                          FileInfo file) {
        super(fileInfoManager, operationEvent);
        mDetailFileInfo = file;
    }

    @Override
    protected Integer doInBackground(Void... params) {
        LogUtils.d(TAG, "doInBackground...");
        if (mDetailFileInfo.isDirectory()) {
            if (!MountPointManager.getInstance().isRootPath(
                    mDetailFileInfo.getFileAbsolutePath())) {
                final File[] files = mDetailFileInfo.getFile().listFiles();
                int ret = OperationEventListener.ERROR_CODE_SUCCESS;
                if (files != null) {
                    for (File file : files) {
                        ret = getContentSize(file);
                        if (ret < 0) {
                            LogUtils.i(TAG, "doInBackground,ret = " + ret);
                            return ret;
                        }
                    }
                }
            }
        } else {
            long size = mDetailFileInfo.getFileSize();
            publishProgress(new ProgressInfo("", 0, size, 0, size));
        }

        return OperationEventListener.ERROR_CODE_SUCCESS;
    }

    /**
     * The method calculate a file's size(including its contains, if is a directory).
     *
     * @param root a file which need calculate.
     * @return The file's content size.
     */
    public int getContentSize(File root) {
        LogUtils.d(TAG, "getContentSize...");
        int ret = OperationEventListener.ERROR_CODE_SUCCESS;
        if (root.isDirectory()) {
            File[] files = root.listFiles();
            if (files == null) {
                LogUtils.d(TAG, "getContentSize, listFiles is null, return");
                return ret;
            }
            for (File file : files) {
                if (isCancelled()) {
                    return OperationEventListener.ERROR_CODE_USER_CANCEL;
                }
                ret = getContentSize(file);
                if (ret < 0) {
                    LogUtils.i(TAG, "getContentSize ,ret = " + ret);
                    return ret;
                }
            }
        }
        mTotal += root.length();
        publishProgress(new ProgressInfo("", 0, mTotal, 0, mTotal));
        return OperationEventListener.ERROR_CODE_SUCCESS;
    }
}
