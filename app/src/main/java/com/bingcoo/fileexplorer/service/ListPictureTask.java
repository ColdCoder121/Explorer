package com.bingcoo.fileexplorer.service;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Downloads;
import android.provider.MediaStore;
import android.text.TextUtils;

import com.bingcoo.fileexplorer.interfaces.ItemAdapter;
import com.bingcoo.fileexplorer.util.FileCategoryHelper;
import com.bingcoo.fileexplorer.util.FileInfo;
import com.bingcoo.fileexplorer.util.FileInfoManager;
import com.bingcoo.fileexplorer.util.FileUtils;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.PermissionUtils;
import com.bingcoo.fileexplorer.util.SystemUtils;

/**
 * 类名称：ListPictureTask
 * 作者：David
 * 内容摘要：
 * 创建日期：2017/2/4
 * 修改者， 修改日期， 修改内容
 */
public class ListPictureTask extends ListCategoryTask  {
    private static final String TAG = "ListPictureTask";
    static {
        LogUtils.setDebug(TAG, true);
    }

    private String mDir = null;

    public ListPictureTask(Context context, FileInfoManager fileInfoManager,
                           FileManagerService.OperationEventListener operationEvent,
                           FileCategoryHelper.FileCategory fc, int sortType, String directory) {
        super(context, fileInfoManager, operationEvent, fc, sortType);
        mDir = directory;
        LogUtils.d(TAG, "ListPictureTask: mDir=" + mDir);
    }

    @Override
    protected Integer doInBackground(Void... params) {
        int total;
        int progress = 0;
        long startLoadTime = System.currentTimeMillis();
        Cursor cursor = null;

        if (FileCategoryHelper.FileCategory.Download == mFileCategory) {
            if (PermissionUtils.hasPermission(mContext, Downloads.Impl.PERMISSION_ACCESS_ALL)) {
                cursor = SystemUtils.getDownloadCursor(mContext);
            }
        } else if (FileCategoryHelper.FileCategory.Bluetooth == mFileCategory) {
            cursor = SystemUtils.getBluetoothCursor(mContext);
        } else {
            FileCategoryHelper helper = new FileCategoryHelper(mContext);
            cursor = helper.query(mFileCategory, mSortType);
        }

        if (null != cursor) {
            total = cursor.getCount();
            long loadTime;
            int nextUpdateTime = FIRST_NEED_PROGRESS;
            ItemAdapter itemAdapter = getAdapter(cursor);

            for (int i = 0; i < total; i++) {
                if (isCancelled()) {
                    LogUtils.w(TAG, " doInBackground, cancel.");
                    return FileManagerService.OperationEventListener.ERROR_CODE_UNSUCCESS;
                }
                itemAdapter.getItem(i);

                String filePath = itemAdapter.getFilePath();
                if (!TextUtils.isEmpty(filePath)) {
                    FileInfo item      = new FileInfo(filePath);
                    String   parentDir = item.getFileParentPath();
                    LogUtils.d(TAG, "doInBackground: parentDir=" + parentDir);
                    progress++;
                    if(FileUtils.getFileName(filePath).startsWith(".")){
                        continue;
                    }
                    if (mDir.equals(parentDir)) {
                        try {
                            if (FileCategoryHelper.FileCategory.Picture == mFileCategory) {
                                long id  = getID(cursor);
                                Uri  uri = MediaStore.Images.Media.getContentUri("external");
                                LogUtils.d(TAG, "doInBackground: uri=" + uri);
                                item.setContentUri(Uri.parse(uri + "/" + id));
                            }
                        } catch (Exception e) {
                            LogUtils.e(TAG, "doInBackground: getID ERROR !!!", e);
                        }
                        item.getFileSizeStr();
                        mFileInfoManager.addItem(item);
                        loadTime = System.currentTimeMillis() - startLoadTime;

                        if (loadTime > nextUpdateTime) {
                            startLoadTime = System.currentTimeMillis();
                            nextUpdateTime = NEXT_NEED_PROGRESS;
                            LogUtils.d(TAG, "doInBackground, publish progress.");
                            publishProgress(new ProgressInfo("", progress, total, progress, total));
                        }
                    }
                }
            }
            cursor.close();
            LogUtils.d(TAG, "doInBackground ERROR_CODE_SUCCESS");
            return FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS;
        }

        LogUtils.w(TAG, " doInBackground, ERROR_CODE_UNSUCCESS");
        return FileManagerService.OperationEventListener.ERROR_CODE_UNSUCCESS;
    }

    private long getID(Cursor cursor) {
        return cursor.getLong(FileCategoryHelper.COLUMN_ID);
    }

}
