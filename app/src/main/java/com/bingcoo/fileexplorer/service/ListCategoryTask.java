package com.bingcoo.fileexplorer.service;

import android.content.Context;
import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.provider.Downloads;
import android.text.TextUtils;

import com.bingcoo.fileexplorer.adapter.BluetoothItemAdapter;
import com.bingcoo.fileexplorer.adapter.DownloadItemAdapter;
import com.bingcoo.fileexplorer.adapter.MediaItemAdapter;
import com.bingcoo.fileexplorer.interfaces.ItemAdapter;
import com.bingcoo.fileexplorer.util.FileCategoryHelper;
import com.bingcoo.fileexplorer.util.FileInfo;
import com.bingcoo.fileexplorer.util.FileInfoManager;
import com.bingcoo.fileexplorer.util.FileUtils;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.PermissionUtils;
import com.bingcoo.fileexplorer.util.SystemUtils;

import java.io.File;

/**
 * 类名称：ListCategoryTask
 * 作者：David
 * 内容摘要：
 * 创建日期：2017/1/12
 * 修改者， 修改日期， 修改内容
 */
public class ListCategoryTask extends BaseAsyncTask {
    private static final String TAG = "ListCategoryTask";

    static {
        LogUtils.setDebug(TAG, true);
    }

    protected Context mContext;
    protected FileCategoryHelper.FileCategory mFileCategory;
    protected int mSortType;
    protected static final int FIRST_NEED_PROGRESS = 250;
    protected static final int NEXT_NEED_PROGRESS = 200;

    public ListCategoryTask(Context context, FileInfoManager fileInfoManager,
                            FileManagerService.OperationEventListener operationEvent,
                            FileCategoryHelper.FileCategory fc, int sortType) {
        super(fileInfoManager, operationEvent);
        mContext = context;
        mFileCategory = fc;
        mSortType = sortType;
    }

    @Override
    protected Integer doInBackground(Void... params) {
        int total;
        int progress = 0;
        long startLoadTime = System.currentTimeMillis();
        Cursor cursor = null;

        if (FileCategoryHelper.FileCategory.Download == mFileCategory) {
            if (PermissionUtils.hasPermission(mContext, Downloads.Impl.PERMISSION_ACCESS_ALL)) {
                cursor = SystemUtils.getDownloadCursor(mContext);
            }
        } else if (FileCategoryHelper.FileCategory.Bluetooth == mFileCategory) {
            cursor = SystemUtils.getBluetoothCursor(mContext);
        } else {
            FileCategoryHelper helper = new FileCategoryHelper(mContext);
            cursor = helper.query(mFileCategory, mSortType);
        }

        if (null != cursor) {
            total = cursor.getCount();
            long loadTime;
            int nextUpdateTime = FIRST_NEED_PROGRESS;
            ItemAdapter itemAdapter = getAdapter(cursor);

            for (int i = 0; i < total; i++) {
                if (isCancelled()) {
                    LogUtils.w(TAG, " doInBackground, cancel.");
                    return FileManagerService.OperationEventListener.ERROR_CODE_UNSUCCESS;
                }
                itemAdapter.getItem(i);
                String filePath = itemAdapter.getFilePath();
                progress++;
                if (!TextUtils.isEmpty(filePath)) {
                    //隐藏文件不列出来
                    File file = new File(filePath);
                    if (FileUtils.isHiddenFile(file)) {
                        LogUtils.d(TAG, " doInBackground, file is hidden! continue.");
                        continue;
                    }
                    FileInfo item = new FileInfo(filePath);
                    if (FileCategoryHelper.FileCategory.Video == mFileCategory) {
                        item.setDuration(getDuration(item.getFileAbsolutePath()));
                    }
                    item.getFileSizeStr();
                    mFileInfoManager.addItem(item);
                    loadTime = System.currentTimeMillis() - startLoadTime;

                    if (loadTime > nextUpdateTime) {
                        startLoadTime = System.currentTimeMillis();
                        nextUpdateTime = NEXT_NEED_PROGRESS;
                        LogUtils.d(TAG, "doInBackground, publish progress.");
                        publishProgress(new ProgressInfo("", progress, total, progress, total));
                    }
                }
            }
            cursor.close();
            LogUtils.d(TAG, "doInBackground ERROR_CODE_SUCCESS");
            return FileManagerService.OperationEventListener.ERROR_CODE_SUCCESS;
        }

        LogUtils.w(TAG, " doInBackground, ERROR_CODE_UNSUCCESS");
        return FileManagerService.OperationEventListener.ERROR_CODE_UNSUCCESS;
    }

    protected ItemAdapter getAdapter(Cursor cursor) {
        ItemAdapter itemAdapter = null;
        switch (mFileCategory) {
            case Download:
                itemAdapter = new DownloadItemAdapter(cursor);
                break;

            case Bluetooth:
                itemAdapter = new BluetoothItemAdapter(cursor);
                break;

            case Music:
            case Video:
            case Picture:
            case Doc:
            case Zip:
            case Apk:
            case Recent:
                itemAdapter = new MediaItemAdapter(cursor);
                break;

            default:
                LogUtils.d(TAG, "doInBackground: ERROR Category");
        }
        return itemAdapter;
    }

    private static final int SECONDS_PER_MINUTE = 60;
    private static final int SECONDS_PER_HOUR = 60 * 60;
    private static final int SECONDS_PER_DAY = 24 * 60 * 60;

    String getDuration(String path) {
        try {
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(path);
            String duration = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION); // 毫秒
            LogUtils.d(TAG, "getDuration: duration=" + duration);
            long durationValue = Long.valueOf(duration) / 1000; // 秒
            long days = durationValue / SECONDS_PER_DAY;
            long hours = durationValue % SECONDS_PER_DAY / SECONDS_PER_HOUR;
            long minutes = durationValue % SECONDS_PER_HOUR / SECONDS_PER_MINUTE;
            long seconds = durationValue % SECONDS_PER_MINUTE;
            LogUtils.d(TAG, "getDuration: days=" + days + ", hours=" + hours + ", minutes=" + minutes + ", seconds=" + seconds);
            duration = String.format("%02d:%02d:%02d", hours, minutes, seconds);
            return duration;
        } catch (Exception e) {
            LogUtils.e(TAG, "getDuration: Exception !!", e);
            return String.format("%02d:%02d:%02d", 0, 0, 0);
        }
    }

}
