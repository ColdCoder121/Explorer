package com.bingcoo.fileexplorer.service;

import android.os.AsyncTask;

import com.bingcoo.fileexplorer.service.FileManagerService.OperationEventListener;
import com.bingcoo.fileexplorer.util.FileInfoManager;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.WakeLockUtils;

abstract class BaseAsyncTask extends AsyncTask<Void, ProgressInfo, Integer> {
    private static final String TAG = "BaseAsyncTask";

    static {
        LogUtils.setDebug(TAG, true);
    }

    protected OperationEventListener mListener = null;
    protected FileInfoManager mFileInfoManager = null;
    protected boolean mIsTaskFinished = true;

    /**
     * Constructor of BaseAsyncTask
     *
     * @param fileInfoManager a instance of FileInfoManager, which manages information of files in
     *            FileManager.
     * @param listener a instance of OperationEventListener, which is a interface doing things
     *            before/in/after the task.
     */
    public BaseAsyncTask(FileInfoManager fileInfoManager, OperationEventListener listener) {
        if (fileInfoManager == null) {
            throw new IllegalArgumentException();
        }
        mFileInfoManager = fileInfoManager;
        mListener = listener;
    }

    @Override
    protected void onPreExecute() {
        mIsTaskFinished = false;
        if (mListener != null) {
            LogUtils.d(TAG, "onPreExecute");
            mListener.onTaskPrepare();
        }
    }

    @Override
    protected void onPostExecute(Integer result) {
        WakeLockUtils.getInstance().releaseWakeLock();
        if (mListener != null) {
            LogUtils.d(TAG, "onPostExecute");
            mListener.onTaskResult(result);
            mListener = null;
        }
        mIsTaskFinished = true;
    }

    @Override
    protected void onCancelled() {
        WakeLockUtils.getInstance().releaseWakeLock();
        if (mListener != null) {
            LogUtils.d(TAG, "onCancelled()");
            mListener.onTaskResult(OperationEventListener.ERROR_CODE_USER_CANCEL);
            mListener = null;
        }
        mIsTaskFinished = true;
    };

    @Override
    protected void onProgressUpdate(ProgressInfo... values) {
        WakeLockUtils.getInstance().holdWakeLock();
        if (mListener != null && values != null && values[0] != null) {
            LogUtils.v(TAG, "onProgressUpdate");
            mListener.onTaskProgress(values[0]);
        }
    }

    /**
     * This method remove listener from task. Set listener associate with task to be null.
     */
    protected void removeListener() {
        if (mListener != null) {
            LogUtils.d(TAG, "removeListener");
            mListener = null;
        }
    }

    /**
     * This method set mListener with certain listener.
     *
     * @param listener the certain listener, which will be set to be mListener.
     */
    public void setListener(OperationEventListener listener) {
        mListener = listener;
    }

    /**
     * This method return state of task, whether task is finished
     *
     * @return true if task is executed, otherwise false returned
     */
    public boolean isTaskBusy() {
        LogUtils.d(TAG, "isTaskBusy,task status = " + getStatus());
        if (mIsTaskFinished || getStatus() == Status.FINISHED||isCancelled()) {
            LogUtils.d(TAG, "isTaskBusy, false.");
            return false;
        }
        LogUtils.d(TAG, "isTaskBusy, true.");
        return true;
    }
}
