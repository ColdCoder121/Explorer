/*
 * Copyright (c) 2010-2011, The MiCode Open Source Community (www.micode.net)
 *
 * This file is part of FileExplorer.
 *
 * FileExplorer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FileExplorer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SwiFTP.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.bingcoo.fileexplorer.service;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;
import android.util.LruCache;
import android.widget.ImageView;

import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.RoundCornerBitmapUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Asynchronously loads file icons and thumbnail, mostly single-threaded.
 */
public class FileIconLoader implements Callback {

    private static final String TAG = "FileIconLoader";

    static {
        LogUtils.setDebug(TAG, true);
    }

    private static final String LOADER_THREAD_NAME = "FileIconLoader";

    /**
     * Type of message sent by the UI thread to itself to indicate that some
     * photos need to be loaded.
     */
    private static final int MESSAGE_REQUEST_LOADING = 1;

    /**
     * Type of message sent by the loader thread to indicate that some photos
     * have been loaded.
     */
    private static final int MESSAGE_ICON_LOADED = 2;

    private static long maxMemory = Runtime.getRuntime().maxMemory() / 8;// 模拟器默认是16M
    private static LruCache<String, Bitmap> mLruCache = new LruCache<String, Bitmap>((int) maxMemory) {
        @Override
        protected int sizeOf(String key, Bitmap value) {
            int byteCount = value.getRowBytes() * value.getHeight();
            return byteCount;
        }
    };

    /**
     * 本地页面不同路径下的相同位置的imageview的id竟然是一样的，
     * 导致上一路径的apk图标正在加载时切换路径后apk图标正好显示到
     * 同一位置非apk文件上了
     */
    private static List<ImageView> mApkImageList = new ArrayList<>();

    public void setOtherFileImage(ImageView image) {
        if (mApkImageList.contains(image)) {
            mApkImageList.remove(image);
        }
    }

    public void setApkFileImage(ImageView image) {
        if (!mApkImageList.contains(image)) {
            mApkImageList.add(image);
        }
    }

    private static abstract class ImageHolder {
        public static final int NEEDED = 0;

        public static final int LOADING = 1;

        public static final int LOADED = 2;

        int state;

        public static ImageHolder create() {

            return new BitmapHolder();
        }

        public abstract boolean setImageView(ImageView v, String filePath);

        public abstract void setImage(Bitmap image, String filePath);
    }

    private static class BitmapHolder extends ImageHolder {
//        SoftReference<Bitmap> bitmapRef;

        @Override
        public boolean setImageView(ImageView v, String filePath) {
            if (mLruCache == null) {
                return false;
            }
            Bitmap bitmap = mLruCache.get(filePath);
            if (bitmap == null)
                return false;
            if (mApkImageList.contains(v)) {
                v.setImageBitmap(bitmap);
            }
            return true;
        }

        @Override
        public void setImage(Bitmap image, String filePath) {
            if (image == null || filePath == null) {
                return;
            }
            mLruCache.put(filePath, image);
        }
    }

    /**
     * A soft cache for image thumbnails. the key is file path
     */
    private final static ConcurrentHashMap<String, ImageHolder> mImageCache = new ConcurrentHashMap<>();

    /**
     * A map from ImageView to the corresponding photo ID. Please note that this
     * photo ID may change before the photo loading request is started.
     */
//    private final ConcurrentHashMap<ImageView, FileInfo> mPendingRequests = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<ImageView, String> mPendingRequests = new ConcurrentHashMap<>();

    /**
     * Handler for messages sent to the UI thread.
     */
    private final Handler mMainThreadHandler = new Handler(this);

    /**
     * Thread responsible for loading photos from the database. Created upon the
     * first request.
     */
    private LoaderThread mLoaderThread;

    /**
     * A gate to make sure we only send one instance of MESSAGE_PHOTOS_NEEDED at
     * a time.
     */
    private boolean mLoadingRequested;

    /**
     * Flag indicating if the image loading is paused.
     */
    private Context mContext;

    /**
     * Constructor.
     *
     * @param context content context
     */
    public FileIconLoader(Context context) {
        mContext = context;
    }


    /**
     * Load photo into the supplied image view. If the photo is already cached,
     * it is displayed immediately. Otherwise a request is sent to load the
     * photo from the database.
     */
    public boolean loadIcon(ImageView view, String filePath) {
        boolean loaded = loadCachedIcon(view, filePath);
        if (loaded) {
            mPendingRequests.remove(view);
            mApkImageList.remove(view);
            LogUtils.e(TAG, "icon of " + filePath + " has been loaded");
        } else {
            mPendingRequests.put(view, filePath);
            LogUtils.e(TAG, "icon of " + filePath + " need to load");
            // Send a request to start loading photos
            requestLoading();
        }
        return loaded;
    }

    /**
     * Checks if the photo is present in cache. If so, sets the photo on the
     * view, otherwise sets the state of the photo to
     * {@link BitmapHolder#NEEDED}
     */
    private boolean loadCachedIcon(ImageView view, String filePath) {
        ImageHolder holder = mImageCache.get(filePath);

        if (holder == null) {
            holder = ImageHolder.create();
            if (holder == null) {
                return false;
            }
            mImageCache.put(filePath, holder);
        } else if (holder.state == ImageHolder.LOADED) {
            // failing to set imageview means that the soft reference was
            // released by the GC, we need to reload the photo.
            if (holder.setImageView(view, filePath)) {
                return true;
            }
        }
        holder.state = ImageHolder.NEEDED;
        return false;
    }

    public void clear() {
        mPendingRequests.clear();
        mImageCache.clear();
    }

    /**
     * Sends a message to this thread itself to start loading images. If the
     * current view contains multiple image views, all of those image views will
     * get a chance to request their respective photos before any of those
     * requests are executed. This allows us to load images in bulk.
     */
    private void requestLoading() {
//        if (!mLoadingRequested) {
//            mLoadingRequested = true;
        mMainThreadHandler.sendEmptyMessage(MESSAGE_REQUEST_LOADING);
//        }
    }

    /**
     * Processes requests on the main thread.
     */
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case MESSAGE_REQUEST_LOADING: {
//                mLoadingRequested = false;
                if (mLoaderThread == null) {
                    mLoaderThread = new LoaderThread();
                    mLoaderThread.start();
                }
                synchronized (mLoaderThread) {

                    mLoaderThread.requestLoading();
                }
                return true;
            }

            case MESSAGE_ICON_LOADED: {
                processLoadedIcons();
                return true;
            }
        }
        return false;
    }

    /**
     * Goes over pending loading requests and displays loaded photos. If some of
     * the photos still haven't been loaded, sends another request for image
     * loading.
     */
    private void processLoadedIcons() {
        Iterator<ImageView> iterator = mPendingRequests.keySet().iterator();
        while (iterator.hasNext()) {
            ImageView view = iterator.next();
            String filePath = mPendingRequests.get(view);
            boolean loaded = loadCachedIcon(view, filePath);
            if (loaded) {
                iterator.remove();
            }
        }

        if (!mPendingRequests.isEmpty()) {
            requestLoading();
        }else{
            mApkImageList.clear();
        }
    }

    /**
     * The thread that performs loading of photos from the database.
     */
    private class LoaderThread extends HandlerThread implements Callback {
        private Handler mLoaderThreadHandler;

        public LoaderThread() {
            super(LOADER_THREAD_NAME);
        }

        /**
         * Sends a message to this thread to load requested photos.
         */
        public void requestLoading() {
            if (mLoaderThreadHandler == null) {
                mLoaderThreadHandler = new Handler(getLooper(), this);
            }
            mLoaderThreadHandler.sendEmptyMessage(0);
        }

        /**
         * Receives the above message, loads photos and then sends a message to
         * the main thread to process them.
         */
        public boolean handleMessage(Message msg) {
            Iterator<String> iterator = mPendingRequests.values().iterator();
            while (iterator.hasNext()) {
                String filePath = iterator.next();
                ImageHolder holder = mImageCache.get(filePath);
                if (holder != null && holder.state == ImageHolder.NEEDED) {
                    // Assuming atomic behavior
                    holder.state = ImageHolder.LOADING;
                    Drawable iconDrawable = getApkIcon(mContext, filePath);
                    Bitmap icon = RoundCornerBitmapUtils.GetRoundedCornerBitmap(iconDrawable, mContext);
                    holder.setImage(icon, filePath);
                    holder.state = BitmapHolder.LOADED;
                    mImageCache.put(filePath, holder);
                }
            }

            mMainThreadHandler.sendEmptyMessage(MESSAGE_ICON_LOADED);
            if (mLoaderThread != null) {
                mLoaderThread.quit();
                mLoaderThread = null;
            }
            return true;
        }

    }

    /**
     * 采用了新的办法获取APK图标，之前的失败是因为android中存在的一个BUG,通过
     * appInfo.publicSourceDir = apkPath;来修正这个问题，详情参见:
     * http://code.google.com/p/android/issues/detail?id=9151
     */
    private Drawable getApkIcon(Context context, String apkPath) {
        PackageManager pm = context.getPackageManager();
        PackageInfo info = pm.getPackageArchiveInfo(apkPath, PackageManager.GET_ACTIVITIES);
        if (info != null) {
            ApplicationInfo appInfo = info.applicationInfo;
            appInfo.sourceDir = apkPath;
            appInfo.publicSourceDir = apkPath;
            try {
                return appInfo.loadIcon(pm);
            } catch (OutOfMemoryError e) {
                Log.e(TAG, e.toString());
            }
        }
        return null;
    }

}
