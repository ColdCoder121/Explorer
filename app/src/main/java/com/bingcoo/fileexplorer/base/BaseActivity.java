package com.bingcoo.fileexplorer.base;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.storage.StorageVolume;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bingcoo.fileexplorer.R;
import com.bingcoo.fileexplorer.dialog.fragment.AlertDialogFragment;
import com.bingcoo.fileexplorer.home.FileInfoAdapter;
import com.bingcoo.fileexplorer.receiver.MountReceiver;
import com.bingcoo.fileexplorer.service.FileManagerService;
import com.bingcoo.fileexplorer.service.ProgressInfo;
import com.bingcoo.fileexplorer.ui.SlowHorizontalScrollView;
import com.bingcoo.fileexplorer.util.ConstUtils;
import com.bingcoo.fileexplorer.util.DrmManager;
import com.bingcoo.fileexplorer.util.FileInfoComparator;
import com.bingcoo.fileexplorer.util.FileInfoManager;
import com.bingcoo.fileexplorer.util.IconManager;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.LongStringUtils;
import com.bingcoo.fileexplorer.util.MountPointManager;
import com.bingcoo.fileexplorer.util.SharedPrefUtils;
import com.bingcoo.fileexplorer.util.ToastHelper;
import com.jakewharton.rxbinding.view.RxView;
import com.trello.rxlifecycle.components.RxActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.functions.Action1;

/**
 * 类名称：BaseActivity
 * 作者：David
 * 内容摘要：
 * 创建日期：2017/1/11
 * 修改者， 修改日期， 修改内容
 */
public class BaseActivity extends RxActivity implements MountReceiver.MountListener,
        AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener,
        View.OnClickListener, AlertDialogFragment.OnDialogDismissListener {
    private static final String TAG = "BaseActivity";
    static {
        LogUtils.setDebug(TAG, true);
    }
    public static final String SAVED_PATH_KEY = "saved_path";

    public static final int MSG_DO_MOUNTED = 0;
    public static final int MSG_DO_EJECTED = 1;
    public static final int MSG_DO_UNMOUNTED = 2;
    public static final int MSG_DO_SDSWAP = 3;

    protected Bundle mSavedInstanceState = null;
    protected FileInfoAdapter mAdapter = null;
    protected MountReceiver mMountReceiver = null;
    protected ToastHelper mToastHelper = null;
    protected String mCurrentPath = null;
    protected FileInfoManager mFileInfoManager = null;
    protected MountPointManager mMountPointManager = null;
    protected int mSortType = 0;
    protected FileManagerService mService = null;
    protected boolean mServiceBinded = false;

    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService.disconnected(this.getClass().getName());
            mServiceBinded = false;
            LogUtils.w(TAG, "onServiceDisconnected");
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LogUtils.e(TAG, "base onServiceConnected");
            mService = ((FileManagerService.ServiceBinder) service).getServiceInstance();
            mServiceBinded = true;
            serviceConnected();
        }
    };

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            LogUtils.d(TAG, "handleMessage, msg = " + msg.what);
            switch (msg.what) {
                case MSG_DO_MOUNTED:
                    doOnMounted((String) msg.obj);
                    break;
                case MSG_DO_EJECTED:
                    doOnEjected((String) msg.obj);
                    break;
                case MSG_DO_UNMOUNTED:
                    doOnUnMounted((StorageVolume) msg.obj);
                    break;
                case MSG_DO_SDSWAP:
                    doOnSdSwap();
                    break;
                default:
                    break;
            }
        }
    };

    protected void serviceConnected() {
        LogUtils.e(TAG,"base serviceConnected");
        mFileInfoManager = mService.initFileInfoManager(this.getClass().getName());
        mService.setListType(getPrefsShowHiddenFile() ?
                        FileManagerService.FILE_FILTER_TYPE_ALL : FileManagerService.FILE_FILTER_TYPE_DEFAULT,
                this.getClass().getName());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        LogUtils.e(TAG,"base onCreate");
        super.onCreate(savedInstanceState);

        // register unmount/mount Receiver
       /* if (null == mMountReceiver) {
            mMountReceiver = MountReceiver.registerMountReceiver(this);
            mMountReceiver.registerMountListener(this);
        }*/
//        ActivityManager.getInstance().addActivity(this);
        LogUtils.e(TAG,"base bindService");
        bindService(new Intent(getApplicationContext(), FileManagerService.class),
                mServiceConnection, BIND_AUTO_CREATE);

        mToastHelper = new ToastHelper(this);

        // start watching external storage change.
        mMountPointManager = MountPointManager.getInstance();

        DrmManager.getInstance().init(getApplicationContext());

        mSortType = getPrefsSortBy();
    }

    @Override
    protected void onPause() {
        super.onPause();

        DrmManager.getInstance().init(getApplicationContext());
        IconManager.updateCustomDrawableMap(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mService != null) {
            if (mServiceBinded) {
                unbindService(mServiceConnection);
                mServiceBinded = false;
            }
            if (null != mMountReceiver) {
                mMountReceiver.unregisterMountListener(this);
                unregisterReceiver(mMountReceiver);
                mMountReceiver = null;
            }
        } else {
            LogUtils.w(TAG, "#onDestroy(),the Service hasn't connected yet.");
        }
        DrmManager.getInstance().release();

        super.onDestroy();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        return false;
    }

    @Override
    public void onDialogDismiss() {

    }

    @Override
    public void onMounted(String mountPoint) {
        Message.obtain(mHandler, MSG_DO_MOUNTED, mountPoint).sendToTarget();
    }

    @Override
    public void onUnMounted(StorageVolume volume) {
        Message.obtain(mHandler, MSG_DO_UNMOUNTED, volume).sendToTarget();
    }

    @Override
    public void onEjected(String unMountPoint) {
        Message.obtain(mHandler, MSG_DO_EJECTED, unMountPoint).sendToTarget();
    }

    @Override
    public void onSdSwap() {
        Message.obtain(mHandler, MSG_DO_SDSWAP).sendToTarget();
    }

    protected void doOnMounted(String mountPoint) {

    }

    protected void doOnEjected(String unMountPoint) {

    }

    protected void doOnUnMounted(StorageVolume volume) {

    }

    protected void doOnSdSwap() {

    }

    protected boolean getPrefsShowHiddenFile() {
        return SharedPrefUtils.getInstance(ConstUtils.FILE_EXPLORER_PREF)
                .getBoolean(ConstUtils.PREF_SHOW_HIDDEN_FILE, false);
    }

    protected int getPrefsSortBy() {
        return SharedPrefUtils.getInstance(ConstUtils.FILE_EXPLORER_PREF)
                .getInt(ConstUtils.PREF_SORT_BY, FileInfoComparator.SORT_BY_TYPE);
    }

    protected void setPrefsSortBy(int sort) {
        mSortType = sort;
        SharedPrefUtils.getInstance(ConstUtils.FILE_EXPLORER_PREF)
                .putInt(ConstUtils.PREF_SORT_BY, mSortType);
    }

    public void setViewClickListener(View view, final Runnable runnable) {
        if ((null != view) && (null != runnable)) {
            RxView.clicks(view)
                    .throttleFirst(ConstUtils.THROTTLE_TIME, TimeUnit.MILLISECONDS)
                    .subscribe(new Action1<Void>() {
                        @Override
                        public void call(Void aVoid) {
                            runnable.run();
                        }
                    });
        }
    }

    protected void showDirectoryContent(String path) {

    }

    protected void reloadContent() {
        LogUtils.d(TAG, "****reloadContent******");
        if (mService != null && !mService.isBusy(this.getClass().getName())) {
            if (MountPointManager.getInstance().isRootPath(mCurrentPath)
                    || (mFileInfoManager != null && mFileInfoManager.isPathModified(mCurrentPath))) {
                showDirectoryContent(mCurrentPath);
            } else if (mFileInfoManager != null && mAdapter != null) {
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    protected void restoreDialog() {

    }

    protected class TabManager {
        private final List<String> mTabNameList = new ArrayList<>();
        protected LinearLayout mTabsHolder = null;
        private String mCurFilePath = null;
        private static final int TAB_TET_MAX_LENGTH = 250;
        private SlowHorizontalScrollView mNavigationBar;

        public TabManager(LinearLayout tabsHolder, SlowHorizontalScrollView naviBar) {
            mTabsHolder = tabsHolder;
            mNavigationBar = naviBar;
        }

        protected int getViewDirection() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                return mNavigationBar.getParent().getParent().getLayoutDirection();
            }else{
                return ViewGroup.LAYOUT_DIRECTION_LTR;
            }
        }

        public void refreshTab(String initFileInfo) {
            LogUtils.d(TAG, "refreshTab,initFileInfo = " + initFileInfo);
            int count = mTabsHolder.getChildCount();
            mTabsHolder.removeViews(0, count);
            mTabNameList.clear();

            mCurFilePath = initFileInfo;
            if (mCurFilePath != null) {
                addTab(getString(R.string.local));
                if (!mMountPointManager.isRootPath(mCurFilePath)) {
                    String path = mMountPointManager.getDescriptionPath(mCurFilePath);
                    String[] result = path.split(MountPointManager.SEPARATOR);
                    for (String string : result) {
                        addTab(string);
                    }

                    if (getViewDirection() == ViewGroup.LAYOUT_DIRECTION_LTR) {
                        // scroll to right with slow-slide animation
                        startActionBarScroll();
                    } else if (getViewDirection() == ViewGroup.LAYOUT_DIRECTION_RTL) {
                        // scroll horizontal view to the right
                        mNavigationBar.startHorizontalScroll(-mNavigationBar.getScrollX(),
                                -mNavigationBar.getRight());
                    }
                }
            }
            updateHomeButton();
        }

        private void startActionBarScroll() {
            // scroll to right with slow-slide animation
            // To pass the Launch performance test, avoid the scroll
            // animation when launch.
            int tabHostCount = mTabsHolder.getChildCount();
            int navigationBarCount = mNavigationBar.getChildCount();
            if ((tabHostCount > 2) && (navigationBarCount >= 1)) {
                View view = mNavigationBar.getChildAt(navigationBarCount - 1);
                if (null == view) {
                    LogUtils.d(TAG, "startActionBarScroll, navigationbar child is null");
                    return;
                }
                int width = view.getRight();
                mNavigationBar.startHorizontalScroll(mNavigationBar.getScrollX(), width
                        - mNavigationBar.getScrollX());
            }
        }

        protected void updateHomeButton() {
        }

        /**
         * This method updates the navigation view to the previous view when
         * back button is pressed
         *
         * @param newPath
         *            the previous showed directory in the navigation history
         */
        protected void showPrevNavigationView(String newPath) {
            refreshTab(newPath);
            showDirectoryContent(newPath);
        }

        /**
         * This method creates tabs on the navigation bar
         *
         * @param text
         *            the name of the tab
         */
        protected void addTab(String text) {
            if (!TextUtils.isEmpty(text)) {
                LogUtils.d(TAG, "addTab: text=[" + text + "]");

                TextView button = new TextView(BaseActivity.this);
                button.setClickable(true);
                button.setOnClickListener(BaseActivity.this);
                //button.setBackgroundResource(R.drawable.button_path_bg);
                //button.setTextColor(getResources().getColor(R.color.color999));
                button.setTextColor(getResources().getColorStateList(R.color.nav_tab_text_color));
                button.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                        getResources().getDimensionPixelSize(R.dimen.nav_text_size));
                button.setPadding(getResources().getDimensionPixelOffset(R.dimen.path_padding),
                        0, 0, 0);
                button.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                        R.drawable.path_arrow, 0);
                button.setCompoundDrawablePadding(
                        getResources().getDimensionPixelOffset(R.dimen.path_padding));
                button.setMaxWidth(TAB_TET_MAX_LENGTH);
                LongStringUtils.fadeOutLongString(button);
                button.setText(text.trim());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    button.setOutlineProvider(null);
                }

                LinearLayout.LayoutParams mlp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                mlp.setMargins(0, 0, 0, 0);
                mTabsHolder.addView(button, mlp);

                button.setId(mTabNameList.size());
                mTabNameList.add(text);
            }
        }

        /**
         * The method updates the navigation bar
         *
         * @param id
         *            the tab id that was clicked
         */
        public void updateNavigationBar(int id) {
            LogUtils.d(TAG, "updateNavigationBar, id = " + id + ", listSize=" + mTabNameList.size());

            // click current button do not response
            if (id < mTabNameList.size() - 1) {
                String oldPath = mCurFilePath;
                int count = mTabNameList.size() - id - 1;
                mTabsHolder.removeViews(id + 1, count);

                for (int i = 0; i < count; i++) {
                    // update mTabNameList
                    mTabNameList.remove(mTabNameList.size() - 1);
                }

                if (0 == id) {
                    mCurFilePath = mMountPointManager.getRootPath();
                } else {
                    // get mount point path
                    String mntPointPath = mMountPointManager.getRealMountPointPath(mCurFilePath);
                    LogUtils.d(TAG, "updateNavigationBar, mntPointPath: " + mntPointPath + " for mCurFilepath: " + mCurFilePath);
                    String path = mCurFilePath.substring(mntPointPath.length() + 1);
                    StringBuilder sb = new StringBuilder(mntPointPath);
                    String[] pathParts = path.split(MountPointManager.SEPARATOR);
                    // id=0,1 is for mnt point button, so from id=2 to get other parts of path
                    for (int i = 2; i <= id; i++) {
                        sb.append(MountPointManager.SEPARATOR);
                        sb.append(pathParts[i - 2]);
                    }
                    mCurFilePath = sb.toString();
                }

                showDirectoryContent(mCurFilePath);
                updateHomeButton();
            }
        }

    }

    protected class ListListener implements FileManagerService.OperationEventListener {
        public static final String LIST_DIALOG_TAG = "ListDialogFragment";

        protected void dismissDialogFragment() {
            LogUtils.d(TAG, "ListListener dismissDialogFragment");
            // TODO
        }

        @Override
        public void onTaskPrepare() {

        }

        @Override
        public void onTaskProgress(ProgressInfo progressInfo) {
            LogUtils.d(TAG, "ListListener onTaskProgress: isResumed=" + isResumed());
            if (isResumed()) {
                // TODO
            }
        }

        @Override
        public void onTaskResult(int result) {
            LogUtils.d(TAG, "onTaskResult: result=" + result);

        }
    }

}
