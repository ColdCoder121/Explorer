package com.bingcoo.fileexplorer.dialog.fragment;

//import android.app.AlertDialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Vibrator;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Selection;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bingcoo.fileexplorer.R;
import com.bingcoo.fileexplorer.util.FileInfo;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.ToastHelper;

import bingo.app.AlertDialog;

/**
 * 类名称：AlertDialogFragment
 * 作者：David
 * 内容摘要：
 * 创建日期：2016/12/14
 * 修改者， 修改日期， 修改内容
 */
public class AlertDialogFragment extends DialogFragment implements
        DialogInterface.OnClickListener {
    public static final String TAG = "AlertDialogFragment";

    static {
        LogUtils.setDebug(TAG, true);
    }

    private static final String TITLE = "title";
    private static final String CANCELABLE = "cancelable";
    private static final String ICON = "icon";
    private static final String MESSAGE = "message";
    private static final String LAYOUT = "layout";
    private static final String NEGATIVE_TITLE = "negativeTitle";
    private static final String POSITIVE_TITLE = "positiveTitle";

    private static final String DETAIL_FILE_NAME = "detailFileName";
    private static final String DETAIL_FILE_CNT = "detailFileCnt";//多个图片文件夹文件数
    private static final String DETAIL_MIME_TYPE = "detailMimeType";
    private static final String DETAIL_FILE_SIZE = "detailFileSize";
    private static final String DETAIL_DIR_INCLUDE = "detailDirInclude";
    private static final String DETAIL_MODIFY_TIME = "detailModifyTime";
    private static final String DETAIL_FILE_ROUTE = "detailFileRoute";
    private static final String DETAIL_FILE_RESOLUTION = "detailFileResolution";//图片的分辨率文本
    private static final String DETAIL_FILE_DURATION = "detailFileDuration";//视频时长
    private static final String INCLUDE_VISIBLE = "includeVisible";//是否显示文件夹的包含文件rl
    private static final int FILE_NAME_TV_ID = R.id.tv_detail_name;
    private static final int MIME_TYPE_TV_ID = R.id.tv_detail_type;
    private static final int FILE_SIZE_TV_ID = R.id.tv_detail_size;
    private static final int FILE_CNT_TV_ID = R.id.tv_detail_cnt;
    private static final int DIR_INCLUDE_TV_ID = R.id.tv_detail_include;
    private static final int MODIFY_TIME_TV_ID = R.id.tv_detail_time;
    private static final int FILE_ROUTE_TV_ID = R.id.tv_detail_route;
    private static final int DIR_INCLUDE_RL_ID = R.id.rl_include;
    private static final int FILE_RESOLUTION_TV_ID = R.id.tv_detail_resolution;
    private static final int FILE_DURATION_TV_ID = R.id.tv_detail_duration;

    public static final int INVIND_RES_ID = -1;

    protected DialogInterface.OnClickListener mDoneListener;
    protected DialogInterface.OnDismissListener mDismissListener = null;
    protected ToastHelper mToastHelper = null;
    private OnDialogDismissListener mDialogDismissListener;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putAll(getArguments());

        putSringDetail(outState, DETAIL_FILE_NAME, FILE_NAME_TV_ID);
        putSringDetail(outState, DETAIL_MIME_TYPE, MIME_TYPE_TV_ID);
        putSringDetail(outState, DETAIL_FILE_SIZE, FILE_SIZE_TV_ID);
        putSringDetail(outState, DETAIL_FILE_CNT, FILE_CNT_TV_ID);
        putSringDetail(outState, DETAIL_DIR_INCLUDE, DIR_INCLUDE_TV_ID);
        putSringDetail(outState, DETAIL_MODIFY_TIME, MODIFY_TIME_TV_ID);
        putSringDetail(outState, DETAIL_FILE_ROUTE, FILE_ROUTE_TV_ID);
        putSringDetail(outState, DETAIL_FILE_RESOLUTION, FILE_RESOLUTION_TV_ID);
        putSringDetail(outState, DETAIL_FILE_DURATION, FILE_DURATION_TV_ID);
        putBooleanDetail(outState, INCLUDE_VISIBLE, DIR_INCLUDE_RL_ID);
        super.onSaveInstanceState(outState);
    }

    private void putSringDetail(Bundle outState, String key, int id) {
        Dialog dialog = getDialog();
        if (dialog != null) {
            View tv = dialog.findViewById(id);
            if (tv != null && tv instanceof TextView) {
                outState.putString(key, ((TextView) tv).getText().toString());
            }
        }
    }

    private void putBooleanDetail(Bundle outState, String key, int id) {
        Dialog dialog = getDialog();
        if (dialog != null) {
            View v = dialog.findViewById(id);
            if (v != null) {
                outState.putBoolean(key, v.getVisibility() == View.VISIBLE);
            }
        }
    }

    private void getStringDetail(Bundle args, View view, String key, int id) {

        if (view != null) {
            TextView tv = (TextView) view.findViewById(id);
            if (tv != null) {
                tv.setText((String) args.get(key));
            }
        }
    }

    private void getBooleanDetail(Bundle args, View view, String key, int id) {

        if (view != null) {
            View v = view.findViewById(id);
            if (v != null && args.get(key) != null && args.get(key) instanceof Boolean) {
                v.setVisibility((boolean) args.get(key) ? View.VISIBLE : View.GONE);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getDialog().setCanceledOnTouchOutside(true);
    }

    public static class AlertDialogFragmentBuilder {
        protected final Bundle mBundle = new Bundle();

        /**
         * This method creates AlertDialogFragment with parameter of mBundle.
         *
         * @return AlertDialogFragment
         */
        public AlertDialogFragment create() {
            AlertDialogFragment f = new AlertDialogFragment();
            f.setArguments(mBundle);
            return f;
        }

        /**
         * This method sets TITLE for AlertDialogFragmentBuilder, which responds to title of dialog.
         *
         * @param resId resource id of title
         * @return AlertDialogFragmentBuilder
         */
        public AlertDialogFragmentBuilder setTitle(int resId) {
            mBundle.putInt(TITLE, resId);
            return this;
        }

        /**
         * This method sets LAYOUT for AlertDialogFragmentBuilder, which responds to layout of
         * dialog.
         *
         * @param resId resource id of layout
         * @return AlertDialogFragmentBuilder
         */
        public AlertDialogFragmentBuilder setLayout(int resId) {
            mBundle.putInt(LAYOUT, resId);
            return this;
        }

        /**
         * This method sets CANCELABLE for AlertDialogFragmentBuilder (default value is true), which
         * responds to weather dialog can be canceled.
         *
         * @param cancelable true for can be canceled, and false for can not be canceled
         * @return AlertDialogFragmentBuilder
         */
        public AlertDialogFragmentBuilder setCancelable(boolean cancelable) {
            mBundle.putBoolean(CANCELABLE, cancelable);
            return this;
        }

        /**
         * This method sets ICON for AlertDialogFragmentBuilder.
         *
         * @param resId resource id of icon
         * @return AlertDialogFragmentBuilder
         */
        public AlertDialogFragmentBuilder setIcon(int resId) {
            mBundle.putInt(ICON, resId);
            return this;
        }

        /**
         * This method sets MESSAGE for AlertDialogFragmentBuilder, which is a string.
         *
         * @param resId resource id of message
         * @return AlertDialogFragmentBuilder
         */
        public AlertDialogFragmentBuilder setMessage(int resId) {
            mBundle.putInt(MESSAGE, resId);
            return this;
        }

        /**
         * This method sets NEGATIVE_TITLE for AlertDialogFragmentBuilder, which responds to title
         * of negative button.
         *
         * @param resId resource id of title
         * @return AlertDialogFragmentBuilder
         */
        public AlertDialogFragmentBuilder setCancelTitle(int resId) {
            mBundle.putInt(NEGATIVE_TITLE, resId);
            return this;
        }

        /**
         * This method sets POSITIVE_TITLE for AlertDialogFragmentBuilder, which responds to title
         * of positive button.
         *
         * @param resId resource id of title
         * @return AlertDialogFragmentBuilder
         */
        public AlertDialogFragmentBuilder setDoneTitle(int resId) {
            mBundle.putInt(POSITIVE_TITLE, resId);
            return this;
        }
    }

    /**
     * This method sets doneListenser for AlertDialogFragment
     *
     * @param listener doneListenser for AlertDialogFragment, which will response to press done
     *                 button
     */
    public void setOnDoneListener(DialogInterface.OnClickListener listener) {
        mDoneListener = listener;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (mDoneListener != null) {
            mDoneListener.onClick(dialog, which);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = createAlertDialogBuilder(savedInstanceState);
        return builder.create();
    }

    /**
     * This method gets a instance of AlertDialog.Builder
     *
     * @param savedInstanceState information for AlertDialog.Builder
     * @return
     */
    protected AlertDialog.Builder createAlertDialogBuilder(Bundle savedInstanceState) {
        Bundle args = null;
        if (savedInstanceState == null) {
            args = getArguments();
        } else {
            args = savedInstanceState;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (args != null) {
            int title = args.getInt(TITLE, INVIND_RES_ID);
            if (title != INVIND_RES_ID) {
                builder.setTitle(title);
            }

            int icon = args.getInt(ICON, INVIND_RES_ID);
            if (icon != INVIND_RES_ID) {
                builder.setIcon(icon);
            }

            int message = args.getInt(MESSAGE, INVIND_RES_ID);
            int layout = args.getInt(LAYOUT, INVIND_RES_ID);
            View view = null;
            if (layout != INVIND_RES_ID) {
                view = getActivity().getLayoutInflater().inflate(layout,
                        null);
                builder.setView(view);
            } else if (message != INVIND_RES_ID) {
                builder.setMessage(message);
            }

            int cancel = args.getInt(NEGATIVE_TITLE, INVIND_RES_ID);

            if (cancel != INVIND_RES_ID) {
                builder.setNegativeButton(cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
            }

            int done = args.getInt(POSITIVE_TITLE, INVIND_RES_ID);
            if (done != INVIND_RES_ID) {
                builder.setPositiveButton(done, this);
            }

            //设置中修改字体后返回dialog详情内容为空
            getStringDetail(args, view, DETAIL_FILE_NAME, FILE_NAME_TV_ID);
            getStringDetail(args, view, DETAIL_MIME_TYPE, MIME_TYPE_TV_ID);
            getStringDetail(args, view, DETAIL_FILE_CNT, FILE_CNT_TV_ID);
            getStringDetail(args, view, DETAIL_FILE_SIZE, FILE_SIZE_TV_ID);
            getStringDetail(args, view, DETAIL_DIR_INCLUDE, DIR_INCLUDE_TV_ID);
            getStringDetail(args, view, DETAIL_MODIFY_TIME, MODIFY_TIME_TV_ID);
            getStringDetail(args, view, DETAIL_FILE_ROUTE, FILE_ROUTE_TV_ID);
            getStringDetail(args, view, DETAIL_FILE_RESOLUTION, FILE_RESOLUTION_TV_ID);
            getStringDetail(args, view, DETAIL_FILE_DURATION, FILE_DURATION_TV_ID);
            getBooleanDetail(args, view, INCLUDE_VISIBLE, DIR_INCLUDE_RL_ID);

            mToastHelper = new ToastHelper(getActivity());
            boolean cancelable = args.getBoolean(CANCELABLE, true);
            builder.setCancelable(cancelable);
        }
        return builder;
    }

    /**
     * This method sets dismissListener for AlertDialogFragment, which will response to
     * dismissDialog
     *
     * @param listener OnDismissListener for AlertDialogFragment
     */
    public void setDismissListener(DialogInterface.OnDismissListener listener) {
        mDismissListener = listener;
    }

    /**
     * This method sets dismissListener for AlertDialogFragment, which will
     * response to dismissDialog
     *
     * @param listener OnDismissListener for AlertDialogFragment
     */
    public void setOnDialogDismissListener(OnDialogDismissListener listener) {
        mDialogDismissListener = listener;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (mDismissListener != null) {
            mDismissListener.onDismiss(dialog);
        }
        if (mDialogDismissListener != null) {
            mDialogDismissListener.onDialogDismiss();
        }
        super.onDismiss(dialog);
    }

    public static class EditDialogFragmentBuilder extends
            AlertDialogFragmentBuilder {
        @Override
        public EditTextDialogFragment create() {
            EditTextDialogFragment f = new EditTextDialogFragment();
            f.setArguments(mBundle);
            return f;
        }

        /**
         * This method sets default string and default selection for EditTextDialogFragment.
         *
         * @param defaultString    default string to show on EditTextDialogFragment
         * @param defaultSelection resource id for default selection
         * @return EditDialogFragmentBuilder
         */
        public EditDialogFragmentBuilder setDefault(String defaultString,
                                                    int defaultSelection) {
            mBundle.putString(EditTextDialogFragment.DEFAULT_STRING,
                    defaultString);
            mBundle.putInt(EditTextDialogFragment.DEFAULT_SELECTION,
                    defaultSelection);
            return this;
        }
    }

    public static class EditTextDialogFragment extends AlertDialogFragment {
        public static final String TAG = "EditTextDialogFragment";
        public static final String DEFAULT_STRING = "defaultString";
        public static final String DEFAULT_SELECTION = "defaultSelection";
        private EditText mEditText;
        private EditTextDoneListener mEditTextDoneListener;

        public interface EditTextDoneListener {
            /**
             * This method is used to overwrite by its implement
             *
             * @param text text on EditText when done button is pressed
             */
            void onClick(String text);
        }

        @Override
        public void onSaveInstanceState(Bundle outState) {
            getArguments().putString(DEFAULT_STRING,
                    mEditText.getText().toString());
            getArguments().putInt(DEFAULT_SELECTION,
                    mEditText.getSelectionStart());
            super.onSaveInstanceState(outState);
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            this.setOnDoneListener(this);
            AlertDialog.Builder builder = createAlertDialogBuilder(savedInstanceState);
            Bundle args = null;
            if (savedInstanceState == null) {
                args = getArguments();
            } else {
                args = savedInstanceState;
            }
            if (args != null) {
                String defaultString = args.getString(DEFAULT_STRING, "");
                int selection = args.getInt(DEFAULT_SELECTION, 0);
                View view = getActivity().getLayoutInflater().inflate(
                        R.layout.dialog_edit_text, null);
                builder.setView(view);
                mEditText = (EditText) view.findViewById(R.id.edit_text);
                if (mEditText != null) {
                    mEditText.setText(defaultString);
                    //mEditText.setSelection(selction);
                    Editable editable = mEditText.getText();
                    Selection.setSelection(editable, 0, selection);
                }
            }
            return builder.create();
        }

        @Override
        public void onResume() {
            super.onResume();
            if (mEditText != null && mEditText.getText().length() == 0) {
                final Button button = ((AlertDialog) getDialog())
                        .getButton(DialogInterface.BUTTON_POSITIVE);
                if (button != null) {
                    button.setEnabled(false);
                }
            }
            getDialog().getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            getDialog().setCanceledOnTouchOutside(true);
            setTextChangedCallback(mEditText, (AlertDialog) getDialog());
            restoreOkButtonState();
        }

        /**
         * This method is used to set filter to EditText which is used for user entering filename.
         * This filter will ensure that the inputed filename wouldn't be too long. If so, the
         * inputed info would be rejected.
         *
         * @param edit      The EditText for filter to be registered.
         * @param maxLength limitation of length for input text
         */
        private void setEditTextFilter(final EditText edit, final int maxLength) {
            InputFilter filter = new InputFilter.LengthFilter(maxLength) {
                boolean mHasToasted = false;
                private static final int VIBRATOR_TIME = 100;

                public CharSequence filter(CharSequence source, int start, int end, Spanned dest,
                                           int dstart, int dend) {
                    String oldText = null;
                    String newText = null;
                    int oldSize = 0;
                    int newSize = 0;
                    if (mEditText != null) {
                        oldText = mEditText.getText().toString();
                        oldSize = getStrBytesNum(oldText);
                    }
                    if (source != null) {
                        newText = source.toString();
                        newSize = getStrBytesNum(newText);
                    }
                    newSize = newSize - (dend - dstart);
                    if (source != null && source.length() > 0 && (oldSize + newSize) > maxLength) {
                        LogUtils.d(TAG, "oldSize + newSize) > maxLength,source.length()="
                                + source.length());
                        Vibrator vibrator = (Vibrator) getActivity().getSystemService(
                                Context.VIBRATOR_SERVICE);
                        boolean hasVibrator = vibrator.hasVibrator();
                        if (hasVibrator) {
                            vibrator.vibrate(new long[]{VIBRATOR_TIME, VIBRATOR_TIME},
                                    INVIND_RES_ID);
                        }
                        int addLen = maxLength - oldSize + (dend - dstart);
                        String addStr = "";
                        int i = 0;
                        int j = addLen;
                        boolean flag = false;
                        String tmp = "";
                        for (i = 0; i < newText.length() && j > 0; i++) {
                            tmp = String.valueOf(newText.charAt(i));
                            if (getStrBytesNum(addStr + tmp) > addLen) {
                                LogUtils.d(TAG, "not add char any more because of beyond max length if added this char.");
                                flag = true;
                                break;
                            }
                            addStr += tmp;
                            j -= getStrBytesNum(tmp);
                        }
                        // if there is more chars not add into add string, add the first char into as tmp string.
                        if ((i < newText.length()) && !flag) {
                            tmp = String.valueOf(newText.charAt(i));
                        }
                        if (addStr != null && addStr.length() > 0) {
                            String lastChar = addStr.substring(addStr.length() - 1, addStr.length());
                            if (getStrBytesNum(lastChar) + getStrBytesNum(tmp) != getStrBytesNum(lastChar + tmp)) {
                                LogUtils.d(TAG, "do not seperate one whole code into two part");
                                if (addStr.length() == 1) {
                                    addStr = "";
                                } else {
                                    addStr = addStr.substring(0, addStr.length() - 1);
                                }
                            }
                        }
                        LogUtils.w(TAG, "input out of range,hasVibrator: addLen " + addLen + " addStr " + addStr + " isVibrator " + hasVibrator);
                        return addStr;
                    }
                    if (source != null && source.length() > 0 && !mHasToasted
                            && dstart == 0) {
                        if (source.charAt(0) == '.') {
                            mToastHelper.showToast(R.string.create_hidden_file);
                            mHasToasted = true;
                        }
                    }
                    return super.filter(source, start, end, dest, dstart, dend);
                }
            };
            edit.setFilters(new InputFilter[]{filter});
        }

        /**
         * This method register callback and set filter to Edit, in order to make sure that user
         * input is legal. The input can't be illegal filename and can't be too long.
         *
         * @param editText EditText, which user type on
         * @param dialog   dialog, which EditText associated with
         */
        protected void setTextChangedCallback(EditText editText,
                                              final AlertDialog dialog) {
            setEditTextFilter(editText, FileInfo.FILENAME_MAX_LENGTH);
            editText.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable arg0) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    if (s.toString().trim().length() <= 0
                            || s.toString().matches(".*[/\\\\:*?\"<>|\t].*")) {
                        // characters not allowed
                        if (s.toString().matches(".*[/\\\\:*?\"<>|\t].*")) {
                            mToastHelper
                                    .showToast(R.string.invalid_char_prompt);
                        }
                        Button botton = dialog
                                .getButton(DialogInterface.BUTTON_POSITIVE);
                        if (botton != null) {
                            botton.setEnabled(false);
                        }
                    } else if (s != null && s.charAt(0) == '.') {
                        mToastHelper.showToast(R.string.create_hidden_file);
                        Button botton = dialog
                                .getButton(DialogInterface.BUTTON_POSITIVE);
                        if (botton != null) {
                            botton.setEnabled(true);
                        }
                    } else {
                        Button botton = dialog
                                .getButton(DialogInterface.BUTTON_POSITIVE);
                        if (botton != null) {
                            botton.setEnabled(true);
                        }
                    }
                }
            });
        }

        /**
         * This method gets EditText's content on EditTextDialogFragment
         *
         * @return content of EditText
         */
        public String getText() {
            if (mEditText != null) {
                return mEditText.getText().toString().trim();
            }
            return null;
        }

        public EditText getEditText() {
            return mEditText;
        }

        /**
         * This method sets EditTextDoneListener for EditTextDialogFragment
         *
         * @param listener EditTextDoneListener, which will response press done button
         */
        public void setOnEditTextDoneListener(EditTextDoneListener listener) {
            mEditTextDoneListener = listener;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            // close input method window when click ok button by self
            final InputMethodManager inputMgr = (InputMethodManager) getActivity().
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            if (mEditText != null && inputMgr != null && inputMgr.isActive()) {
                final IBinder token = mEditText.getWindowToken();
                if (token != null) {
                    inputMgr.hideSoftInputFromWindow(token, InputMethodManager.HIDE_NOT_ALWAYS);
                } else {
                    LogUtils.e(TAG, "EditTextDialogFragment onClick(): error cannot get window token");
                }
            }
            if (mEditTextDoneListener != null) {
                mEditTextDoneListener.onClick(getText());
            }
        }

        public void restoreOkButtonState() {
            AlertDialog dialog = (AlertDialog) getDialog();
            if (dialog != null) {
                Button botton = dialog
                        .getButton(DialogInterface.BUTTON_POSITIVE);
                if (botton != null) {
                    if (mEditText != null) {
                        String mtext = mEditText.getText().toString();
                        if (mtext.length() <= 0
                                || mtext.matches(".*[/\\\\:*?\"<>|\t].*")) {
                            // characters not allowed
                            botton.setEnabled(false);
                        } else {
                            botton.setEnabled(true);
                        }
                    }
                }
            }
        }//method end

        private int getStrBytesNum(String inStr) {
            if (inStr == null || inStr.isEmpty()) {
                return 0;
            }
            int strSize = 0;
            int charSize = inStr.length();
            for (int i = 0; i < charSize; i++) {
                int c = inStr.charAt(i);
                if ((c >= 0x0001) && (c <= 0x007E)) {
                    strSize += 1;
                } else if (c > 0x07FF) {
                    strSize += 3;
                } else {
                    strSize += 2;
                }
            }
            LogUtils.d(TAG, "getStrBytesNum, inStr size =" + strSize + ",inStr =" + inStr);
            return strSize;
        }

    }

    public static class ChoiceDialogFragmentBuilder extends
            AlertDialogFragmentBuilder {
        @Override
        public ChoiceDialogFragment create() {
            ChoiceDialogFragment f = new ChoiceDialogFragment();
            f.setArguments(mBundle);
            return f;
        }

        /**
         * This method sets default choice and array for ChoiceDialogFragment.
         *
         * @param arrayId       resource id for array
         * @param defaultChoice resource id for default choice
         * @return ChoiceDialogFragmentBuilder
         */
        public ChoiceDialogFragmentBuilder setDefault(int arrayId,
                                                      int defaultChoice) {
            mBundle.putInt(ChoiceDialogFragment.DEFAULT_CHOICE, defaultChoice);
            mBundle.putInt(ChoiceDialogFragment.ARRAY_ID, arrayId);
            return this;
        }

        public ChoiceDialogFragmentBuilder setDefault(CharSequence[] array,
                                                      int defaultChoice) {
            mBundle.putInt(ChoiceDialogFragment.DEFAULT_CHOICE, defaultChoice);
            mBundle.putCharSequenceArray(ChoiceDialogFragment.ARRAY, array);
            return this;
        }
    }

    public static class ChoiceDialogFragment extends AlertDialogFragment {
        public static final String CHOICE_DIALOG_TAG = "ChoiceDialogFragment";
        public static final String DEFAULT_CHOICE = "defaultChoice";
        public static final String ARRAY_ID = "arrayId";
        public static final String ARRAY = "array";
        public static final String ITEM_LISTENER = "itemlistener";
        private int mArrayId;
        private CharSequence[] mArray;
        private int mDefaultChoice;
        private DialogInterface.OnClickListener mItemLinster = null;

        /**
         * This method sets clickListener for ChoiceDialogFragment
         *
         * @param listener onClickListener, which will response press cancel button
         */
        public void setItemClickListener(DialogInterface.OnClickListener listener) {
            mItemLinster = listener;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            LogUtils.d(CHOICE_DIALOG_TAG, "Show alertSortDialog");
            AlertDialog.Builder builder = createAlertDialogBuilder(savedInstanceState);

            Bundle args = null;
            if (savedInstanceState == null) {
                args = getArguments();
            } else {
                args = savedInstanceState;
            }
            if (args != null) {
                mDefaultChoice = args.getInt(DEFAULT_CHOICE);
                mArrayId = args.getInt(ARRAY_ID);
                mArray = args.getCharSequenceArray(ARRAY);
            }
            if (0 != mArrayId) {
                builder.setSingleChoiceItems(mArrayId, mDefaultChoice, this);
            } else if (null != mArray) {
                builder.setSingleChoiceItems(mArray, mDefaultChoice, this);
            }
            Dialog dialog = builder.create();
            dialog.setCanceledOnTouchOutside(false);
            return dialog;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            if (mItemLinster != null) {
                mItemLinster.onClick(dialog, which);
            }
        }
    }

    public interface OnDialogDismissListener {
        void onDialogDismiss();
    }
}
