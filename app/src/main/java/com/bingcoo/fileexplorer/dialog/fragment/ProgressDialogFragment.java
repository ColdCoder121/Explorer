package com.bingcoo.fileexplorer.dialog.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bingcoo.fileexplorer.R;
import com.bingcoo.fileexplorer.service.ProgressInfo;
import com.bingcoo.fileexplorer.util.ResourceUtils;

import static com.bingcoo.fileexplorer.util.ResourceUtils.TYPE.ID;

/**
 * 类名称：ProgressDialogFragment
 * 作者：David
 * 内容摘要：
 * 创建日期：2016/12/14
 * 修改者， 修改日期， 修改内容
 */
public class ProgressDialogFragment extends DialogFragment {
    public static final String TAG = "ProgressDialogFragment";
    private static final String STYLE = "style";
    private static final String TITLE = "title";
    private static final String CANCEL = "cancel";
    private static final String TOTAL = "total";
    private static final String PROGRESS = "progress";
    private static final String CURRENT_NUM = "currentNumber";
    private static final String TOTAL_NUM = "totalNumber";
    private static final String MESSAGE = "message";
    private int mSavedCurrentNum = 0;
    private int mSavedTotalNum = 0;
    private TextView mProgressNum = null;
    private View.OnClickListener mCancelListener = null;
    private int mViewDirection = 0;

    /**
     * This method gets a instance of ProgressDialogFragment
     *
     * @param style
     *            resource ID of style of DialogFragment
     * @param title
     *            resource ID of title shown on DialogFragment
     * @param message
     *            resource ID of message shown on DialogFragment
     * @param cancel
     *            resource ID of content on cancel button
     * @return a progressDialogFragment
     */
    public static ProgressDialogFragment newInstance(int style, int title, int message, int cancel) {
        ProgressDialogFragment f = new ProgressDialogFragment();
        Bundle args = new Bundle();
        args.putInt(STYLE, style);
        args.putInt(TITLE, title);
        args.putInt(CANCEL, cancel);
        args.putInt(MESSAGE, message);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putAll(getArguments());
        ProgressDialog dialog = (ProgressDialog) getDialog();
        if (dialog != null) {
            outState.putInt(TOTAL, dialog.getMax());
            outState.putInt(PROGRESS, dialog.getProgress());
            outState.putInt(CURRENT_NUM, mSavedCurrentNum);
            outState.putInt(TOTAL_NUM, mSavedTotalNum);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDialog() != null) {
            //int id = ResourceUtils.systemId(getDialog().getContext(), "button3", ID);
            Button mButtonNegative = (Button) getDialog().findViewById(com.bingo.publictheme.R.id.button3);
                    //com.android.internal.R.id.button3);
            mButtonNegative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mCancelListener != null) {
                        mCancelListener.onClick(v);
                    }
                    ProgressDialog dialog = (ProgressDialog) getDialog();
                    if (dialog != null) {
                        dialog.setMessage(getString(R.string.wait));
                    }
                    v.setVisibility(View.INVISIBLE);
                }
            });
        }
    }

    /**
     * This method sets cancel listener to cancel button
     *
     * @param listener
     *            clickListener, which will do proper things when touch cancel
     *            button
     */
    public void setCancelListener(View.OnClickListener listener) {
        mCancelListener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        this.setCancelable(false);
        ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setCancelable(false);
        dialog.setIndeterminate(false);
        Bundle args = null;
        if (savedInstanceState == null) {
            args = getArguments();
        } else {
            args = savedInstanceState;
        }
        if (args != null) {
            int style = args.getInt(STYLE, ProgressDialog.STYLE_HORIZONTAL);
            dialog.setProgressStyle(style);
            int title = args.getInt(TITLE, AlertDialogFragment.INVIND_RES_ID);
            if (title != AlertDialogFragment.INVIND_RES_ID) {
                dialog.setTitle(title);
            }
            int cancel = args.getInt(CANCEL, AlertDialogFragment.INVIND_RES_ID);
            if (cancel != AlertDialogFragment.INVIND_RES_ID) {
                dialog.setButton(ProgressDialog.BUTTON_NEUTRAL, getString(cancel), (Message) null);
            }

            int message = args.getInt(MESSAGE, AlertDialogFragment.INVIND_RES_ID);
            if (message != AlertDialogFragment.INVIND_RES_ID) {
                dialog.setMessage(getString(message));
            }
            int total = args.getInt(TOTAL, -1);
            if (total != -1) {
                dialog.setMax(total);
            }
            int progress = args.getInt(PROGRESS, -1);
            if (progress != -1) {
                dialog.setProgress(progress);
            }
            if (mProgressNum != null) {
                int currentNum = args.getInt(CURRENT_NUM, -1);
                int totalNum = args.getInt(TOTAL_NUM, -1);
                if (currentNum != -1 && totalNum != -1) {
                    mProgressNum.setText(currentNum + "/" + totalNum);
                }
            }
        }

        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_SEARCH) {
                    return true;
                }
                return false;
            }
        });
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        /*
         * in order to customize the Dialog to show file Number, add a new
         * TextView to replace the default TextView of the progressdialog note:
         * cannot put this code section to onCreateDialog, because the Dialog
         * has not being created yet when onCreateDialog is called..
         */
        if (mProgressNum == null) {
            ProgressDialog mDialog = (ProgressDialog) getDialog();
            mProgressNum = new TextView(mDialog.getContext());
            //int id = ResourceUtils.systemId(mDialog.getContext(), "progress_number", ID);
            TextView view = (TextView) mDialog.findViewById(R.id.progress_number);
                    //.findViewById(com.android.internal.R.id.progress_number);
            ViewGroup.LayoutParams params = view.getLayoutParams();
            mProgressNum.setLayoutParams(params);
            view.setVisibility(View.GONE);
            mProgressNum.setTextColor(view.getCurrentTextColor());
            mProgressNum.setSingleLine();

            ViewParent vParent = view.getParent();
            if (vParent instanceof RelativeLayout) {
                RelativeLayout parentLayout = (RelativeLayout) vParent;
                parentLayout.addView(mProgressNum);
            }
        }
    }

    /**
     * This method sets progress of progressDialog according to information of
     * received ProgressInfo.
     *
     * @param progeressInfo
     *            information which need to be updated on progressDialog
     */
    public void setProgress(ProgressInfo progeressInfo) {
        ProgressDialog progressDialog = (ProgressDialog) getDialog();
        if (progressDialog != null && progeressInfo != null) {
            TextView messageView = (TextView) progressDialog
                    .findViewById(com.android.internal.R.id.message);
            if (messageView != null) {
                messageView.setSingleLine();
                messageView.setEllipsize(TextUtils.TruncateAt.MIDDLE);
            }
            progressDialog.setProgress(progeressInfo.getProgeress());
            String message = progeressInfo.getUpdateInfo();
            if (!TextUtils.isEmpty(message)) {
                progressDialog.setMessage(message);
            }
            progressDialog.setMax((int) progeressInfo.getTotal());
            mSavedCurrentNum = progeressInfo.getCurrentNumber();
            mSavedTotalNum = (int) progeressInfo.getTotalNumber();
            if (mProgressNum != null) {
                if (mViewDirection == ViewGroup.LAYOUT_DIRECTION_LTR) {
                    mProgressNum.setText(mSavedCurrentNum + "/" + mSavedTotalNum);
                } else if (mViewDirection == ViewGroup.LAYOUT_DIRECTION_RTL) {
                    mProgressNum.setText(mSavedCurrentNum + "\\" + mSavedTotalNum);
                }
            }
        }
    }

    public void setViewDirection(int direction) {
        mViewDirection = direction;
    }
}
