package com.bingcoo.fileexplorer.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;

import com.mediatek.drm.OmaDrmClient;
import com.mediatek.drm.OmaDrmStore;
import com.mediatek.drm.OmaDrmUiUtils;


/**
 * The class is defined for easily use DrmManagerClient.
 */
public final class DrmManager {
    private static final String TAG = "DrmManager";

    static {
        LogUtils.setDebug(TAG, false);
    }

    public static final String APP_DRM = "application/vnd.oma.drm";
    public static final String EXT_DRM_CONTENT = "dcf";

    public static final int ACTIONID_NOT_DRM = -1;
    public static final int ACTIONID_INVALID_DRM = -2;

    private OmaDrmClient mDrmManagerClient = null;
    private Context mContext = null;

    private static DrmManager sInstance = new DrmManager();

    /**
     * Constructor for DrmManager.
     */
    private DrmManager() {
    }

    /**
     * Initial the DrmManagerClient.
     *
     * @param context The context to use.
     */
    public void init(Context context) {
        mContext = context;
        try {
            if (OptionsUtils.isMtkDrmApp() && (null == mDrmManagerClient)) {
                mDrmManagerClient = new OmaDrmClient(context);
            }
        }catch (NoClassDefFoundError e){
            LogUtils.e(TAG,"DrmManager Init Error:"+e);
        }
    }

    /**
     * Get a DrmManager Object. init() must be called before using it.
     *
     * @return a instance of DrmManager.
     */
    public static DrmManager getInstance() {
        return sInstance;
    }

    /**
     * This method gets Bitmap of DRM file. (Draw a little lock icon at right-down part over
     * original icon)
     *
     * @param resources the resource to use
     * @param path absolute path of the DRM file
     * @param actionId action ID of the file, which is not unique for DRM file
     * @param iconId the ID of background icon, which the new icon draws on
     * @return Bitmap of the DRM file
     */
    public Bitmap overlayDrmIconSkew(Resources resources, String path, int actionId, int iconId) {
        init(mContext);
        if (mDrmManagerClient != null && OptionsUtils.isMtkDrmApp()) {
            return OmaDrmUiUtils.overlayDrmIconSkew(mDrmManagerClient, resources, path, actionId, iconId);
        } else {
            return null;
        }
    }

    /**
     * Get original mimeType of a file.
     *
     * @param path The file's path.
     * @return original mimeType of the file.
     */
    public String getOriginalMimeType(String path) {
        init(mContext);
        if (mDrmManagerClient != null && OptionsUtils.isMtkDrmApp()) {
            String mimeType = mDrmManagerClient.getOriginalMimeType(path);
            if (mimeType == null) {
                LogUtils.w(TAG, "getOriginalMimeType: path=" + path );
                mimeType = "";
            }
            return mimeType;
        } else {
            return "";
        }
    }

    /**
     * This method check weather the rights-protected content has valid right to transfer.
     *
     * @param path path to the rights-protected content.
     * @return true for having right to transfer, false for not having the right.
     */
    public boolean isRightsStatus(String path) {
        init(mContext);
        if (mDrmManagerClient != null && OptionsUtils.isMtkDrmApp()) {
            return mDrmManagerClient.checkRightsStatus(path, OmaDrmStore.Action.TRANSFER)
                != OmaDrmStore.RightsStatus.RIGHTS_VALID;
        } else {
            return false;
        }
    }

    /**
     * This method checks DRM file's MIME type
     *
     * @param path path to content
     * @return true for DRM known type, false for DrmStore.DrmObjectType.UNKNOWN.
     */
    public boolean checkDrmObjectType(String path) {
        init(mContext);
        if (mDrmManagerClient != null && OptionsUtils.isMtkDrmApp()) {
            return mDrmManagerClient.getDrmObjectType(path, null)
                != OmaDrmStore.DrmObjectType.UNKNOWN;
        } else {
            return false;
        }
    }

    /**
     * This method create and show a Dialog, which display the DRM file's protection information.
     *
     * @param activity activity, which the Dialog associated with
     * @param path path to content
     */
    public void showProtectionInfoDialog(Activity activity, String path) {
        init(mContext);
        if (mDrmManagerClient != null && OptionsUtils.isMtkDrmApp()) {
            OmaDrmUiUtils.showProtectionInfoDialog(activity, path);
        }
    }

    /**
     * This method release drm manager client
     */
    public void release() {
        if (mDrmManagerClient != null) {
            mDrmManagerClient.release();
            mDrmManagerClient = null;
            LogUtils.d(TAG, "release: ");
        }
    }
}
