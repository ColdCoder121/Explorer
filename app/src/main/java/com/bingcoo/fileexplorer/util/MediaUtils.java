package com.bingcoo.fileexplorer.util;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.provider.MediaStore.Audio;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Video;
import android.text.TextUtils;

import java.io.File;
import java.util.Locale;

/**
 * 类名称：MediaUtils
 * 作者：David
 * 内容摘要：Utility Methods for Media Library Operations
 * 创建日期：2016/11/17
 * 修改者， 修改日期， 修改内容
 */
@SuppressWarnings("ALL")
public final class MediaUtils {
    private static final String TAG = "MediaUtils";

    public static void sendScanFileBroadcast(Context context, String filePath) {
        File file = new File(filePath);
        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file));
        context.sendBroadcast(intent);
    }

    public static void scanFiles(Context context, String[] paths, String[] mimeTypes, OnScanCompletedListener callback) {
        if (null != paths && paths.length != 0) {
            MediaScannerConnection.scanFile(context, paths, mimeTypes, callback);
        }
    }

    public static void scanFiles(Context context, String[] paths, String[] mimeTypes) {
        scanFiles(context, paths, mimeTypes, null);
    }

    public static void scanFiles(Context context, String[] paths) {
        scanFiles(context, paths, null);
    }

    public static int removeImageFromSD(Context context, String filePath) {
        ContentResolver resolver = context.getContentResolver();
        return resolver.delete(Images.Media.EXTERNAL_CONTENT_URI, Images.Media.DATA + "=?", new String[]{filePath});
    }

    public static int removeAudioFromSD(Context context, String filePath) {
        return context.getContentResolver().delete(Audio.Media.EXTERNAL_CONTENT_URI,
                                                   Audio.Media.DATA + "=?", new String[]{filePath});
    }

    public static int removeVideoFromSD(Context context, String filePath) {
        return context.getContentResolver().delete(Video.Media.EXTERNAL_CONTENT_URI,
                                                   Video.Media.DATA + "=?", new String[]{filePath});

    }

    public static int removeMediaFromSD(Context context, String filePath) {
        String mimeType = FileUtils.getFileMimeType(filePath);
        int affectedRows = 0;
        if (null != mimeType) {
            mimeType = mimeType.toLowerCase(Locale.US);
            if (isImage(mimeType)) {
                affectedRows = removeImageFromSD(context, filePath);
            } else if (isAudio(mimeType)) {
                affectedRows = removeAudioFromSD(context, filePath);
            } else if (isVideo(mimeType)) {
                affectedRows = removeVideoFromSD(context, filePath);
            }
        }
        return affectedRows;
    }

    public static boolean isAudio(String mimeType) {
        return mimeType.startsWith("audio");
    }

    public static boolean isImage(String mimeType) {
        return mimeType.startsWith("image");
    }

    public static boolean isVideo(String mimeType) {
        return mimeType.startsWith("video");
    }


    public static boolean isMediaFile(String filePath) {
        String mimeType = FileUtils.getFileMimeType(filePath);
        return isMediaType(mimeType);
    }

    public static boolean isMediaType(String mimeType) {
        boolean isMedia = false;
        if (!TextUtils.isEmpty(mimeType)) {
            mimeType = mimeType.toLowerCase(Locale.US);
            isMedia = isImage(mimeType) || isAudio(mimeType) || isVideo(mimeType);
        }
        return isMedia;
    }

    public static void renameMediaFile(Context context, String srcPath, String destPath) {
        removeMediaFromSD(context, srcPath);
        sendScanFileBroadcast(context, destPath);
    }

}
