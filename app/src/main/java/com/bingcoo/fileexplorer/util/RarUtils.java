package com.bingcoo.fileexplorer.util;

import android.text.TextUtils;

import com.github.junrar.Archive;
import com.github.junrar.exception.RarException;
import com.github.junrar.rarfile.FileHeader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * 类名称：RarUtils
 * 作者：David
 * 内容摘要：
 * 创建日期：2017/2/16
 * 修改者， 修改日期， 修改内容
 */
public class RarUtils {
    private static final String TAG = "RarUtils";

    public static List<FileHeader> getEntries(String rarFilePath) {
        LogUtils.d(TAG, "getEntries: rarFilePath=" + rarFilePath);
        List<FileHeader> fhList = new ArrayList<>();
        if (!TextUtils.isEmpty(rarFilePath)) {
            File rarFile = new File(rarFilePath);
            if (rarFile.exists()) {
                Archive arch = null;
                try {
                    arch = new Archive(rarFile);
                    if ((null != arch) && !arch.isEncrypted()) {
                        FileHeader fh = arch.nextFileHeader();
                        while (null != fh) {
                            if (!fh.isEncrypted()) {
                                fhList.add(fh);
                            }
                            fh = arch.nextFileHeader();
                        }
                    }
                } catch (RarException e) {
                    LogUtils.e(TAG, "getEntries: RarException !!!", e);
                } catch (IOException e) {
                    LogUtils.e(TAG, "getEntries: IOException !!!", e);
                } catch (Exception e) {
                    LogUtils.e(TAG, "getEntries: Exception !!!", e);
                } finally {
                    CloseUtils.closeIO(arch);
                }
            }
        }

        return fhList;
    }

    public static boolean extractRarFiles(String archivePath, String destinationPath) {

        File archive = new File(archivePath);
        File destination = new File(destinationPath);
        List<FileHeader> fhList = new ArrayList<>();
        Archive arch = null;
        try {
            arch = new Archive(archive);
            if (arch != null) {
                boolean isEncrypted = arch.isEncrypted();
                if (isEncrypted) {
                    return false;
                }
                FileHeader fh = null;
                while (true) {
                    fh = arch.nextFileHeader();
                    if (fh == null) {
                        break;
                    }
                    if (fh.isEncrypted()) {
                        System.out.println("file is encrypted cannot extract: "
                                + fh.getFileNameString());
                        continue;
                    }
                    System.out.println("extracting: " + fh.getFileNameString());
                    fhList.add(fh);
                    if (fh.isDirectory()) {
                        createDirectory(fh, destination);
                    } else {
                        File f = createFile(fh, destination);
                        OutputStream stream = new FileOutputStream(f);
                        arch.extractFile(fh, stream);
                        stream.close();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            CloseUtils.closeIO(arch);
        }
        return fhList != null && fhList.size() > 0;
    }

    /**
     * 解压单独rar文件
     *
     * @param archivePath
     * @param destinationPath
     * @return
     */
    public static boolean extractSingleRarFiles(String archivePath, String childPath, String destinationPath) {

        if (TextUtils.isEmpty(archivePath) || TextUtils.isEmpty(childPath) || TextUtils.isEmpty(destinationPath)) {
            return false;
        }
        childPath.replace("/", "\\");
        File archive = new File(archivePath);
        File destination = new File(destinationPath);
        FileHeader fh = null;
        if (!destination.exists()) {
            destination.mkdirs();
        }
        Archive arch = null;
        try {
            arch = new Archive(archive);
            if (arch != null) {
                boolean isEncrypted = arch.isEncrypted();
                if (isEncrypted) {
                    return false;
                }
                while (true) {
                    fh = arch.nextFileHeader();
                    if (fh == null) {
                        break;
                    }
                    if (fh.isEncrypted()) {
                        continue;
                    }
                    String fileName;
                    if (TextUtils.isEmpty(fh.getFileNameW())) {
                        fileName = fh.getFileNameString();
                    } else {
                        fileName = fh.getFileNameW();
                    }
                    if (fileName.equals(childPath)) {
                        if (!fh.isDirectory()) {
                            File f = createFile(fh, destination);
                            OutputStream stream = new FileOutputStream(f);
                            arch.extractFile(fh, stream);
                            stream.close();
                        }
                    }
                }
            }
        } catch (Exception e) {
            return false;
        } finally {
            CloseUtils.closeIO(arch);
        }
        return true;
    }

    private static File createFile(FileHeader fh, File destination) {
        File f = null;
        String name = null;
        if (fh.isFileHeader() && fh.isUnicode()) {
            name = fh.getFileNameW();
        } else {
            name = fh.getFileNameString();
        }
        f = new File(destination, name);
        if (!f.exists()) {
            try {
                f = makeFile(destination, name);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return f;
    }

    private static File makeFile(File destination, String name) throws IOException {
        String[] dirs = name.split("\\\\");
        if (dirs == null) {
            return null;
        }
        String path = "";
        int size = dirs.length;
        if (size == 1) {
            return new File(destination, name);
        } else if (size > 1) {
            for (int i = 0; i < dirs.length - 1; i++) {
                path = path + File.separator + dirs[i];
                new File(destination, path).mkdir();
            }
            path = path + File.separator + dirs[dirs.length - 1];
            File f = new File(destination, path);
            f.createNewFile();
            return f;
        } else {
            return null;
        }
    }

    private static void createDirectory(FileHeader fh, File destination) {
        File f = null;
        if (fh.isDirectory() && fh.isUnicode()) {
            f = new File(destination, fh.getFileNameW());
            if (!f.exists()) {
                makeDirectory(destination, fh.getFileNameW());
            }
        } else if (fh.isDirectory() && !fh.isUnicode()) {
            f = new File(destination, fh.getFileNameString());
            if (!f.exists()) {
                makeDirectory(destination, fh.getFileNameString());
            }
        }
    }

    private static void makeDirectory(File destination, String fileName) {
        String[] dirs = fileName.split("\\\\");
        if (dirs == null) {
            return;
        }
        String path = "";
        for (String dir : dirs) {
            path = path + File.separator + dir;
            new File(destination, path).mkdir();
        }
    }

}
