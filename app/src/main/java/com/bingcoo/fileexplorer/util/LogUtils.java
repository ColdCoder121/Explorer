package com.bingcoo.fileexplorer.util;

import java.text.MessageFormat;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import android.os.Build;
import android.util.Log;

import com.bingcoo.fileexplorer.BuildConfig;

public final class LogUtils {
    public static Boolean UI_DEBUG = BuildConfig.OPEN_LOG_MODE;

    private static final String LOG_FORMAT = "%s#%s: %s";

    private static Boolean runningInAndroid = true;

    private static Map<String, Boolean> map = new ConcurrentHashMap<>();

    private static boolean isDebug(String tag) {
        synchronized (UI_DEBUG) {
            boolean debug = false;
            if (UI_DEBUG && runningInAndroid) {
                if (map.containsKey(tag)) {
                    debug = map.get(tag);
                } else {
                    map.put(tag, debug);
                }
            }
            return debug;
        }
    }

    public static void setDebug(String tag, boolean isDebug) {
        synchronized (UI_DEBUG) {
            if (runningInAndroid) {
                if (map.containsKey(tag)) {
                    if (map.get(tag) != isDebug) {
                        map.remove(tag);
                        map.put(tag, isDebug);
                    }
                } else {
                    map.put(tag, isDebug);
                }
            }
        }
    }

    public static void clearCache() {
        synchronized (UI_DEBUG) {
            map.clear();
        }
    }

    private LogUtils() {

    }

    public static int v(String tag, String msg) {
        StackTraceElement trace = getInvocationSource();
        msg = String.format(LOG_FORMAT, trace.getClassName(), trace.getLineNumber(), msg);
        if (isDebug(tag)) {
            return Log.v(tag, msg);
        } else {
            //System.out.println(tag + ": " + msg);
            return 0;
        }
    }

    public static int v(String tag, String msg, Throwable tr) {
        StackTraceElement trace = getInvocationSource();
        msg = String.format(LOG_FORMAT, trace.getClassName(), trace.getLineNumber(), msg);
        if (isDebug(tag)) {
            return Log.v(tag, msg, tr);
        } else {
            //System.out.println(tag + ": " + msg);
            tr.printStackTrace();
            return 0;
        }
    }

    public static int d(String tag, String msg) {
        StackTraceElement trace = getInvocationSource();
        msg = String.format(LOG_FORMAT, trace.getClassName(), trace.getLineNumber(), msg);
        if (isDebug(tag)) {
            return Log.d(tag, msg);
        } else {
            //System.out.println(tag + ": " + msg);
            return 0;
        }
    }

    public static int d(String tag, String msg, Throwable tr) {
        StackTraceElement trace = getInvocationSource();
        msg = String.format(LOG_FORMAT, trace.getClassName(), trace.getLineNumber(), msg);
        if (isDebug(tag)) {
            return Log.d(tag, msg, tr);
        } else {
            //System.out.println(tag + ": " + msg);
            tr.printStackTrace();
            return 0;
        }
    }

    public static int i(String tag, String msg) {
        StackTraceElement trace = getInvocationSource();
        msg = String.format(LOG_FORMAT, trace.getClassName(), trace.getLineNumber(), msg);
        if (isDebug(tag)) {
            return Log.i(tag, msg);
        } else {
            //System.out.println(tag + ": " + msg);
            return 0;
        }
    }

    public static int i(String tag, String msg, Throwable tr) {
        StackTraceElement trace = getInvocationSource();
        msg = String.format(LOG_FORMAT, trace.getClassName(), trace.getLineNumber(), msg);
        if (isDebug(tag)) {
            return Log.i(tag, msg, tr);
        } else {
            //System.out.println(tag + ": " + msg);
            tr.printStackTrace();
            return 0;
        }
    }

    public static int w(String tag, String msg) {
        StackTraceElement trace = getInvocationSource();
        msg = String.format(LOG_FORMAT, trace.getClassName(), trace.getLineNumber(), msg);
        if (isDebug(tag)) {
            return Log.w(tag, msg);
        } else {
            //System.out.println(tag + ": " + msg);
            return 0;
        }
    }

    public static int w(String tag, String msg, Throwable tr) {
        StackTraceElement trace = getInvocationSource();
        msg = String.format(LOG_FORMAT, trace.getClassName(), trace.getLineNumber(), msg);
        if (isDebug(tag)) {
            return Log.w(tag, msg, tr);
        } else {
            //System.out.println(tag + ": " + msg);
            tr.printStackTrace();
            return 0;
        }
    }

    public static boolean isLoggable(String tag, int level) {
        if (isDebug(tag)) {
            return Log.isLoggable(tag, level);
        } else {
            return true;
        }
    }

    public static int w(String tag, Throwable tr) {
        if (isDebug(tag)) {
            return Log.w(tag, tr);
        } else {
            tr.printStackTrace();
            return 0;
        }
    }

    public static int e(String tag, String msg) {
        StackTraceElement trace = getInvocationSource();
        msg = String.format(LOG_FORMAT, trace.getClassName(), trace.getLineNumber(), msg);
        if (isDebug(tag)) {
            return Log.e(tag, msg);
        } else {
            //System.out.println(tag + ": " + msg);
            return 0;
        }
    }

    public static int e(String tag, String msg, Throwable tr) {
        StackTraceElement trace = getInvocationSource();
        msg = String.format(LOG_FORMAT, trace.getClassName(), trace.getLineNumber(), msg);
        if (isDebug(tag)) {
            return Log.e(tag, msg, tr);
        } else {
            //System.out.println(tag + ": " + msg);
            tr.printStackTrace();
            return 0;
        }
    }

    public static String getStackTraceString(Throwable tr) {
        if (UI_DEBUG) {
            return Log.getStackTraceString(tr);
        } else {
            return null;
        }
    }

    public static int println(int priority, String tag, String msg) {
        if (isDebug(tag)) {
            return Log.println(priority, tag, msg);
        } else {
            //System.out.println("priority=" + priority + ", tag=" + tag + ": " + msg);
            return 0;
        }
    }

    private static StackTraceElement getInvocationSource() {
        // #0: dalvik.system.VMStack.getThreadStackTrace(Native Method)
        // #1: java.lang.Thread.getStackTrace()
        // #2: .LogUtils.getInvocationSource()
        // #3: .LogUtils.i()
        // #4: <Logger invocation>
        StackTraceElement[] trace = Thread.currentThread().getStackTrace();

//		for (StackTraceElement iter : trace) {
//			Log.d("mtest", "class name=" + iter.getClassName() + ", line number" + iter.getLineNumber());
//		}

        return trace[4];
    }

    /*
     Object[] arguments = {
         new Integer(7),
         new Date(System.currentTimeMillis()),
         "a disturbance in the Force"
     };
     String result = MessageFormat.format(
         "At {1,time} on {1,date}, there was {2} on planet {0,number,integer}.",
         arguments);

     output: At 12:30 PM on Jul 3, 2053, there was a disturbance
           in the Force on planet 7.
     */
    private static String getFormattedLog(String logFormat, Object... logArgs) {
        if (null == logArgs) {
            return logFormat;
        }

        String formattedLog;
        try {
            formattedLog = MessageFormat.format(logFormat, logArgs);
        } catch (Exception e) {
            //Log.e(TAG, "Cannot process given log: " + e.getMessage());
            e.printStackTrace();
            formattedLog = "-- NO LOG TO DISPLAY --";
        }

        return formattedLog;
    }

    private static final String DEVICE_PRODUCT_SONYERICSSON_XPERIA_X10_JP = "SO-01B_1233-7397";

    private static boolean isProblematicDevice() {
        return Build.PRODUCT.equals(DEVICE_PRODUCT_SONYERICSSON_XPERIA_X10_JP);
    }
}
