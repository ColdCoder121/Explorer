package com.bingcoo.fileexplorer.util;

import java.util.HashSet;

/**
 * 类名称：ConstUtils
 * 作者：David
 * 内容摘要：常量定义
 * 创建日期：2016/11/23
 * 修改者， 修改日期， 修改内容
 */
public final class ConstUtils {
    public static final int THROTTLE_TIME = 500;
    public static final String EXTERNAL_VOLUME_NAME = "external";

    public static HashSet<String> sDocMimeTypesSet = new HashSet<String>() {
        {
            add("text/plain");
            add("application/pdf");
            add("text/html");  // html
            add("application/vnd.openxmlformats-officedocument.wordprocessingml.document"); // docx
            add("application/msword"); // docx
            add("application/vnd.openxmlformats-officedocument.wordprocessingml.template"); // docx
            add("application/vnd.openxmlformats-officedocument.presentationml.presentation");   // pptx
            add("application/vnd.ms-powerpoint");   // pptx
            add("application/vnd.openxmlformats-officedocument.presentationml.template");   // pptx
            add("application/vnd.openxmlformats-officedocument.presentationml.slideshow");   // pptx
            add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");  // xlsx
            add("application/vnd.ms-excel");  // xlsx
            add("application/vnd.openxmlformats-officedocument.spreadsheetml.template");  // xlsx
        }
    };

    // pref file name
    public static final String FILE_EXPLORER_PREF = "file_explorer_pref";
    // pref key
    public static final String PREF_SHOW_HIDDEN_FILE = "pref_show_hidden_file";
    public static final String PREF_SORT_BY = "pref_sort_by";

    public static final String CREATE_FOLDER_DIALOG_TAG = "CreateFolderDialog";

}
