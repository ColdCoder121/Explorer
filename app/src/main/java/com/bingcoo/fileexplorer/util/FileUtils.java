package com.bingcoo.fileexplorer.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore.Images.ImageColumns;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;

import com.bingcoo.fileexplorer.service.FileManagerService;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * 类名称：FileUtils
 * 作者：David
 * 内容摘要： 文件工具类
 * 创建日期：2016/11/17
 * 修改者， 修改日期， 修改内容
 */
public class FileUtils {
    private static final String TAG = "FileUtils";

    static {
        LogUtils.setDebug(TAG, true);
    }

    public static final String UNIT_B = "B";
    public static final String UNIT_KB = "KB";
    public static final String UNIT_MB = "MB";
    public static final String UNIT_GB = "GB";
    public static final String UNIT_TB = "TB";
    private static final int UNIT_INTERVAL = 1024;
    private static final double ROUNDING_OFF = 0.005;
    private static final int DECIMAL_NUMBER = 100;
    private static String sDuplicateExt = null;

    public static void setDuplicateExt(String duplicateExt) {
        sDuplicateExt = duplicateExt;
    }

    public static String getFileMimeType(String filename) {
        if (TextUtils.isEmpty(filename)) {
            return null;
        }
        int lastDotIndex = filename.lastIndexOf('.');
        String mimeType =
                MimeTypeMap.getSingleton()
                        .getMimeTypeFromExtension(
                                filename.substring(lastDotIndex + 1).toLowerCase());
        LogUtils.d(TAG, "getFileMimeType: mimeType = " + mimeType);
        return mimeType;
    }

    /**
     * This method check the file name is valid.
     *
     * @param fileName the input file name
     * @return valid or the invalid type
     */
    public static int checkFileName(String fileName) {
        if (TextUtils.isEmpty(fileName) || fileName.trim().length() == 0) {
            return FileManagerService.OperationEventListener.ERROR_CODE_NAME_EMPTY;
        } else {
            try {
                int length = fileName.getBytes("UTF-8").length;
                // int length = fileName.length();
                LogUtils.d(TAG, "checkFileName: fileName=" + fileName + ", length=" + length);
                if (length > FileInfo.FILENAME_MAX_LENGTH) {
                    LogUtils.d(TAG, "checkFileName: fileName is too long, len=" + length);
                    return FileManagerService.OperationEventListener.ERROR_CODE_NAME_TOO_LONG;
                } else {
                    return FileManagerService.OperationEventListener.ERROR_CODE_NAME_VALID;
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return FileManagerService.OperationEventListener.ERROR_CODE_NAME_EMPTY;
            }
        }
    }

    /**
     * This method gets extension of certain file.
     *
     * @param fileName name of a file
     * @return Extension of the file's name
     */
    public static String getFileExtension(String fileName) {
        if (fileName == null) {
            return null;
        }
        String extension = null;
        final int lastDot = fileName.lastIndexOf('.');
        if ((lastDot >= 0)) {
            extension = fileName.substring(lastDot + 1).toLowerCase();
        }
        return extension;
    }

    /**
     * This method gets name of certain file from its path.
     *
     * @param absolutePath the file's absolute path
     * @return name of the file
     */
    public static String getFileName(String absolutePath) {
        int sepIndex = absolutePath.lastIndexOf(MountPointManager.SEPARATOR);
        if (sepIndex >= 0) {
            return absolutePath.substring(sepIndex + 1);
        }
        return absolutePath;

    }

    /**
     * This method gets path to directory of certain file(or folder).
     *
     * @param filePath path to certain file
     * @return path to directory of the file
     */
    public static String getFilePath(String filePath) {
        int sepIndex = filePath.lastIndexOf(MountPointManager.SEPARATOR);
        if (sepIndex >= 0) {
            return filePath.substring(0, sepIndex);
        }
        return "";

    }

    /**
     * This method generates a new suffix if a name conflict occurs, ex: paste a file named
     * "stars.txt", the target file name would be "stars(1).txt"
     *
     * @param file the conflict file
     * @return a new name for the conflict file
     */
//    private static String sDuplicateEn = "duplicate";
//    private static String sDuplicateCn = "复件";
    public static File generateNextNewName(File file) {
        String parentDir = file.getParent();
        String fileName = file.getName();
        String ext = "";
        int newNumber = 0;
        if (file.isFile()) {
            int extIndex = fileName.lastIndexOf(".");
            if (extIndex != -1) {
                ext = fileName.substring(extIndex);
                fileName = fileName.substring(0, extIndex);
            }
        }

        if (fileName.startsWith(sDuplicateExt)) {
            int leftBracketIndex = fileName.indexOf("(");
            int rightBracketIndex = fileName.indexOf(") ");
            LogUtils.d(TAG, "generateNextNewName: leftBracketIndex=" + leftBracketIndex
                    + ", rightBracketIndex=" + rightBracketIndex
                    + ", fileName=" + fileName);
            if ((leftBracketIndex != -1) && (rightBracketIndex != -1) &&
                    (leftBracketIndex+1<rightBracketIndex)) {
                String numeric = fileName.substring(leftBracketIndex + 1, rightBracketIndex);
                LogUtils.d(TAG, "generateNextNewName: numeric=" + numeric);
                if ((null != numeric) && numeric.matches("[0-9]+")) {
                    LogUtils.d(TAG, "generateNextNewName: Conflict folder name already contains (): " + fileName
                            + ", thread id: " + Thread.currentThread().getId());
                    try {
                        newNumber = Integer.parseInt(numeric);
                        newNumber++;
                        fileName = fileName.substring(rightBracketIndex + 2).trim();
                    } catch (Exception e) {
                        LogUtils.e(TAG, "generateNextNewName: Exception !!", e);
                        fileName = fileName.substring(rightBracketIndex + 1).trim();
                    }
                }
            } else {
                newNumber = 2;
//                if (fileName.startsWith(sDuplicateExt)) {
//                    fileName = fileName.substring(sDuplicateExt.length());
//                } else if (fileName.startsWith(sDuplicateExt)) {
                fileName = fileName.substring(sDuplicateExt.length());
//                }
            }
        }

        StringBuffer sb = new StringBuffer();
        if (0 == newNumber) {
            sb.append(sDuplicateExt).append(" ").append(fileName).append(ext);
        } else {
            sb.append(sDuplicateExt).append("(").append(newNumber).append(") ").append(fileName).append(ext);
        }
        if (FileUtils.checkFileName(sb.toString()) < 0) {
            return null;
        }

        LogUtils.d(TAG, "generateNextNewName: parentDir=" + parentDir + ", fileName=" + fileName
                + ", ext=" + ext + ", newName=" + sb);

        return new File(parentDir, sb.toString());
    }

    /**
     * This method converts a size to a string
     *
     * @param size the size of a file
     * @return the string represents the size
     */
    public static String sizeToString(long size) {
        String unit = UNIT_B;
        if (size < DECIMAL_NUMBER) {
            LogUtils.d(TAG, "sizeToString: size=" + size);
            return Long.toString(size) + "" + unit;
        }

        unit = UNIT_KB;
        double sizeDouble = (double) size / (double) UNIT_INTERVAL;
        if (sizeDouble > UNIT_INTERVAL) {
            sizeDouble = (double) sizeDouble / (double) UNIT_INTERVAL;
            unit = UNIT_MB;
        }
        if (sizeDouble > UNIT_INTERVAL) {
            sizeDouble = (double) sizeDouble / (double) UNIT_INTERVAL;
            unit = UNIT_GB;
        }
        if (sizeDouble > UNIT_INTERVAL) {
            sizeDouble = (double) sizeDouble / (double) UNIT_INTERVAL;
            unit = UNIT_TB;
        }

        // Add 0.005 for rounding-off.
        long sizeInt = (long) ((sizeDouble + ROUNDING_OFF) * DECIMAL_NUMBER); // strict to two
        // decimal places
        double formatedSize = ((double) sizeInt) / DECIMAL_NUMBER;
        LogUtils.d(TAG, "sizeToString: formatedSize=" + formatedSize + ", unit=" + unit);

        if (formatedSize == 0) {
            return "0" + "" + unit;
        } else {
            return Double.toString(formatedSize) + "" + unit;
        }
    }

    public static List<File> getFileList(final File rootDir,
                                         final boolean ignoreHiddenFile) {
        final List<File> list = new ArrayList<>();
        final File[] files = rootDir.listFiles();
        if (null != files) {
            for (int i = 0; i < files.length; i++) {
                File file = files[i];
                if (file.isHidden() && ignoreHiddenFile) {
                    //noinspection UnnecessaryContinue
                    continue;
                } else if (file.isDirectory()) {
                    list.addAll(getFileList(file, ignoreHiddenFile));
                } else if (file.isFile()) {
                    list.add(file);
                }
            }
        }
        return list;
    }

    /**
     * This method gets the MIME type from multiple files (order to return: image->video->other)
     *
     * @param service        service of FileManager
     * @param currentDirPath the current directory
     * @param files          a list of files
     * @return the MIME type of the multiple files
     */
    public static String getMultipleMimeType(FileManagerService service, String currentDirPath,
                                             List<FileInfo> files) {
        String mimeType = null;

        for (FileInfo info : files) {
            mimeType = info.getFileMimeType(service);
            if ((null != mimeType)
                    && (mimeType.startsWith("image/") || mimeType.startsWith("video/"))) {
                break;
            }
        }

        if (mimeType == null || mimeType.startsWith("unknown")) {
            mimeType = FileInfo.MIMETYPE_UNRECOGNIZED;
        }
        LogUtils.d(TAG, "getMultipleMimeType: mimeType=" + mimeType);
        return mimeType;
    }

    /**
     * This method checks weather extension of certain file(not folder) is changed.
     *
     * @param newFilePath path to file before modified.(Here modify means rename).
     * @param oldFilePath path to file after modified.
     * @return true for extension changed, false for not changed.
     */
    public static boolean isExtensionChange(String newFilePath, String oldFilePath) {
        File oldFile = new File(oldFilePath);
        if (oldFile.isDirectory()) {
            return false;
        }
        String origFileExtension = FileUtils.getFileExtension(oldFilePath);
        String newFileExtension = FileUtils.getFileExtension(newFilePath);
        if (((origFileExtension != null) && (!origFileExtension.equals(newFileExtension)))
                || ((newFileExtension != null) && (!newFileExtension.equals(origFileExtension)))) {
            return true;
        }
        return false;
    }

    public static String formatModifiedTime(Context context, long millis) {
        String modifiedTime;
        Locale locale = context.getResources().getConfiguration().locale;
        String language = locale.getLanguage();
        SimpleDateFormat formatter;
        Date time = new Date();
        time.setTime(millis);
        if (language.endsWith("zh")) {
            formatter = new SimpleDateFormat("yyyy.MM.dd  HH:mm");
        } else {
            formatter = new SimpleDateFormat("dd MMM yyyy  HH:mm");
        }
        modifiedTime = formatter.format(time);
        return modifiedTime;
    }

    public static String getMimeTypeBySpecialExtension(String fileName, String extension) {
        if (!TextUtils.isEmpty(fileName)) {
            int lastDot = fileName.lastIndexOf('.');
            if (-1 == lastDot) {
                lastDot = fileName.length();
            }
            String modFileName = fileName.substring(0, lastDot) + extension;
            String mimeType = FileUtils.getFileMimeType(modFileName);
            LogUtils.d(TAG, "getMimeTypeBySpecialExtension: fileName=" + fileName
                    + ", extension=" + extension + ", modFileName=" + modFileName
                    + ", mimeType=" + mimeType);
            return mimeType;
        }
        return "";
    }

    /**
     * 根据文件路径获取文件
     *
     * @param filePath 文件路径
     * @return 文件
     */
    public static File getFileByPath(String filePath) {
        return TextUtils.isEmpty(filePath) ? null : new File(filePath);
    }

    /**
     * 判断目录是否存在，不存在则判断是否创建成功
     *
     * @param file 文件
     * @return {@code true}: 存在或创建成功<br>{@code false}: 不存在或创建失败
     */
    public static boolean createOrExistsDir(File file) {
        // 如果存在，是目录则返回true，是文件则返回false，不存在则返回是否创建成功
        return file != null && (file.exists() ? file.isDirectory() : file.mkdirs());
    }


    /**
     * 判断文件是否存在，不存在则判断是否创建成功
     *
     * @param filePath 文件路径
     * @return {@code true}: 存在或创建成功<br>{@code false}: 不存在或创建失败
     */
    public static boolean createOrExistsFile(String filePath) {
        return createOrExistsFile(getFileByPath(filePath));
    }

    /**
     * 判断文件是否存在，不存在则判断是否创建成功
     *
     * @param file 文件
     * @return {@code true}: 存在或创建成功<br>{@code false}: 不存在或创建失败
     */
    public static boolean createOrExistsFile(File file) {
        if (file == null) return false;
        // 如果存在，是文件则返回true，是目录则返回false
        if (file.exists()) return file.isFile();
        if (!createOrExistsDir(file.getParentFile())) return false;
        try {
            return file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String getRealFilePath(final Context context, final Uri uri) {
        if (null == uri) return null;
        final String scheme = uri.getScheme();
        String data = null;
        if (scheme == null)
            data = uri.getPath();
        else if (ContentResolver.SCHEME_FILE.equals(scheme)) {
            data = uri.getPath();
        } else if (ContentResolver.SCHEME_CONTENT.equals(scheme)) {
            Cursor cursor = context.getContentResolver().query(uri, new String[]{ImageColumns.DATA}, null, null, null);
            if (null != cursor) {
                if (cursor.moveToFirst()) {
                    int index = cursor.getColumnIndex(ImageColumns.DATA);
                    if (index > -1) {
                        data = cursor.getString(index);
                    }
                }
                cursor.close();
            }
        }
        return data;
    }

    /**
     * 只判断该文件是否以"."开头，不包括隐藏目录下的文件
     *
     * @param file
     * @return
     */
    public static boolean isHiddenFile(File file) {
        if (file == null) {
            return false;
        }
        return file.getName().startsWith(".");
    }

 /*   *//**
     * 根据文件路径获取视频长度
     * @param path
     * @return
     *//*
    public static String getVideoDuration(String path) {
        final int SECONDS_PER_MINUTE = 60;
        final int SECONDS_PER_HOUR = 60 * 60;
        final int SECONDS_PER_DAY = 24 * 60 * 60;
        try {
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(path);
            String duration = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION); // 毫秒
            LogUtils.d(TAG, "getDuration: duration=" + duration);
            long durationValue = Long.valueOf(duration) / 1000; // 秒
            long days = durationValue / SECONDS_PER_DAY;
            long hours = durationValue % SECONDS_PER_DAY / SECONDS_PER_HOUR;
            long minutes = durationValue % SECONDS_PER_HOUR / SECONDS_PER_MINUTE;
            long seconds = durationValue % SECONDS_PER_MINUTE;
            LogUtils.d(TAG, "getDuration: days=" + days + ", hours=" + hours + ", minutes=" + minutes + ", seconds=" + seconds);
            duration = String.format("%02d:%02d:%02d", hours, minutes, seconds);
            return duration;
        } catch (Exception e) {
            LogUtils.e(TAG, "getDuration: Exception !!", e);
            return String.format("%02d:%02d:%02d", 0, 0, 0);
        }
    }*/
}
