package com.bingcoo.fileexplorer.util;

import android.util.LruCache;

import java.util.List;

/**
 * 类名称：SystemUtils
 * 作者：David
 * 内容摘要：系统工具类。
 * 创建日期：2016/11/22
 * 修改者， 修改日期， 修改内容
 */
public final class LruCacheUtils {
    private static final String TAG = "LruCacheUtils";

    static {
        LogUtils.setDebug(TAG, true);
    }
    private static long maxMemory = Runtime.getRuntime().maxMemory() / 8;// 模拟器默认是16M
    private static LruCache<String, List<FileInfo>> mLruCache = new LruCache<String, List<FileInfo>>((int) maxMemory) {
        @Override
        protected int sizeOf(String key, List<FileInfo> value) {
            int byteCount = 0;
            for(FileInfo info:value){
                byteCount+=info.getFileSize();
            }
            return byteCount;
        }
    };

    public static void putToCache(String key, List<FileInfo> value) {
        mLruCache.put(key,value);
    }

    public static List<FileInfo> getCache(String key) {
        return  mLruCache.get(key);
    }

}
