package com.bingcoo.fileexplorer.util;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Process;
import android.support.v4.content.LocalBroadcastManager;

/**
 * 类名称：PermissionUtils
 * 作者：David
 * 内容摘要：
 * 创建日期：2016/11/24
 * 修改者， 修改日期， 修改内容
 */
public final class PermissionUtils {
    public static boolean hasPermission(Context context, String permission) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            return context.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
        } else {
            return context.checkPermission(permission, Process.myPid(), Process.myUid()) == PackageManager.PERMISSION_GRANTED;
        }
    }

    public static void registerPermissionReceiver(Context context, BroadcastReceiver receiver,
                                                  String permission) {
        final IntentFilter filter = new IntentFilter(permission);
        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, filter);
    }

    public static void unregisterPermissionReceiver(Context context, BroadcastReceiver receiver) {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver);
    }

    public static void notifyPermissionGranted(Context context, String permission) {
        final Intent intent = new Intent(permission);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static boolean hasStorageWritePermission(Context ctx) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return (ctx.checkSelfPermission(Manifest.permission.
                                                    WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        }else{
            return ctx.checkPermission(Manifest.permission.
                    WRITE_EXTERNAL_STORAGE, Process.myPid(), Process.myUid()) == PackageManager.PERMISSION_GRANTED;
        }
    }

    public static boolean hasStorageReadPermission(Context ctx) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return (ctx.checkSelfPermission(Manifest.permission.
                                                    READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        }else{
            return ctx.checkPermission(Manifest.permission.
                    READ_EXTERNAL_STORAGE, Process.myPid(), Process.myUid()) == PackageManager.PERMISSION_GRANTED;
        }
    }

    public static void requestPermission(Activity ctx, String permission, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ctx.requestPermissions(new String[]{permission}, requestCode);
        }
    }

    @TargetApi(23)
    public static void requestPermission(Fragment fragment, String permission, int requestCode) {
        fragment.requestPermissions(new String[]{permission}, requestCode);
    }


    @TargetApi(23)
    public static boolean showWriteRational(Activity ctx){
        return ctx.shouldShowRequestPermissionRationale(
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    @TargetApi(23)
    public static boolean showWriteRational(Fragment fragment){
        return fragment.shouldShowRequestPermissionRationale(
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    @TargetApi(23)
    public static boolean showReadRational(Activity ctx){
        return ctx.shouldShowRequestPermissionRationale(
                Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    @TargetApi(23)
    public static boolean showReadRational(Fragment fragment){
        return fragment.shouldShowRequestPermissionRationale(
                Manifest.permission.READ_EXTERNAL_STORAGE);
    }

}
