package com.bingcoo.fileexplorer.util;

import android.support.annotation.NonNull;
import android.webkit.MimeTypeMap;

import java.util.Comparator;
import java.util.HashMap;

/**
 * 类名称：FileSortHelper
 * 作者：David
 * 内容摘要： 文件排序工具类
 * 创建日期：2016/11/23
 * 修改者， 修改日期， 修改内容
 */
@SuppressWarnings("all")
public class FileSortHelper {
    public enum SortMethod {
        NAME, SIZE, DATE, TYPE
    }

    private SortMethod mSort;
    private boolean mFileFirst;
    private HashMap<SortMethod, Comparator> mComparatorList = new HashMap<>();

    public FileSortHelper() {
        mSort = SortMethod.NAME;
       /* mComparatorList.put(SortMethod.NAME, cmpName);
        mComparatorList.put(SortMethod.SIZE, cmpSize);
        mComparatorList.put(SortMethod.DATE, cmpDate);
        mComparatorList.put(SortMethod.TYPE, cmpType);*/
    }

    public void setSortMethod(SortMethod s) {
        mSort = s;
    }

    public SortMethod getSortMethod() {
        return mSort;
    }

    public Comparator getComparator() {
        return mComparatorList.get(mSort);
    }

    private abstract class FileComparator implements Comparator<FileInfo> {

        @Override
        public int compare(@NonNull FileInfo object1, @NonNull FileInfo object2) {
            return doCompare(object1, object2);
        }

        protected abstract int doCompare(FileInfo object1, FileInfo object2);
    }

    /*private Comparator cmpName = new FileComparator() {
        @Override
        public int doCompare(FileInfo object1, FileInfo object2) {
            return object1.fileName.compareToIgnoreCase(object2.fileName);
        }
    };

    private Comparator cmpSize = new FileComparator() {
        @Override
        public int doCompare(FileInfo object1, FileInfo object2) {
            return longToCompareInt(object1.fileSize - object2.fileSize);
        }
    };

    private Comparator cmpDate = new FileComparator() {
        @Override
        public int doCompare(FileInfo object1, FileInfo object2) {
            return longToCompareInt(object2.modifiedDate - object1.modifiedDate);
        }
    };

    private int longToCompareInt(long result) {
        return result > 0 ? 1 : (result < 0 ? -1 : 0);
    }

    private Comparator cmpType = new FileComparator() {
        @Override
        public int doCompare(FileInfo object1, FileInfo object2) {
            int result = MimeTypeMap.getFileExtensionFromUrl(object1.fileName).compareToIgnoreCase(
                    MimeTypeMap.getFileExtensionFromUrl(object2.fileName));
            if (result != 0) {
                return result;
            }

            return MimeTypeMap.getFileExtensionFromUrl(object1.fileName).compareToIgnoreCase(
                    MimeTypeMap.getFileExtensionFromUrl(object2.fileName));
        }
    };*/
}
