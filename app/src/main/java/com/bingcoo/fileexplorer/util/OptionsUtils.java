package com.bingcoo.fileexplorer.util;

import android.os.SystemProperties;


/**
 * The class is defined for easily use options.
 */
public final class OptionsUtils {
    private static final String TAG = "OptionsManager";

    /**
     * Get the option value of FeatureOption.MTK_DRM_APP.
     *
     * @return is MtkDrmApp, or not.
     */
    public static boolean isMtkDrmApp() {
        return SystemProperties.getBoolean("ro.mtk_oma_drm_support", false);
    }
/*
    public static boolean isMtkThemeSupported() {
        //LogUtils.d(TAG, "FeatureOption.MTK_THEMEMANAGER_APP: " + FeatureOption.MTK_THEMEMANAGER_APP);
        return FeatureOption.MTK_THEMEMANAGER_APP;
    }
*/

    public static boolean isMtkBeamSurpported() {
        return SystemProperties.getBoolean("ro.mtk_beam_plus_support", false);
    }

    public static boolean isMtkSDSwapSurpported() {
        return SystemProperties.getBoolean("ro.mtk_2sdcard_swap", false);
    }

    public static boolean isMtkHotKnotSupported() {
        return SystemProperties.getBoolean("ro.mtk_hotknot_support", false);
    }
    public static boolean isOP01Surported() {
        return "OP01".equals(SystemProperties.get("ro.operator.optr"));
    }
}
