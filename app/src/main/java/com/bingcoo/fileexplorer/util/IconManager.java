package com.bingcoo.fileexplorer.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;

import com.bingcoo.fileexplorer.R;
import com.bingcoo.fileexplorer.service.FileManagerService;
import com.mediatek.common.MPlugin;
import com.mediatek.drm.OmaDrmUtils;

import java.util.HashMap;


public final class IconManager {
    public static final String TAG = "IconManager";

    static {
        LogUtils.setDebug(TAG, true);
    }

    private static IconManager sInstance = new IconManager();
    /**
     * the sdcard2 head image
     */
    // -------------- Bitmaps Cache for creating files ICON ---------------------

    private static final int OFFX = 4;
    /**
     * Cache the default icons
     */
    protected HashMap<Integer, Bitmap> mDefIcons = null;
    /**
     * Cache the sdcard2 icons, <all the icons has a sdcard2 head>
     */
    protected HashMap<Integer, Bitmap> mSdcard2Icons = null;
    protected HashMap<String, Bitmap> mMime2Bitmap = new HashMap<>();
    private Resources mRes;
    protected Bitmap mIconsHead = null;

    private IIconExtension mExt = null;
    private int mCurrentDirection = 0;
    private boolean mDirectionChanged = false;
    /**
     * The map used to record custom icons.
     * The key is file type,the Value is drawable-id.
     * If want to add more file types ,just add it's file type & drawable-id to be show  in this Map!
     */
    private static HashMap<Integer, Integer> sCustomDrawableIdsMap = new HashMap<Integer, Integer>();

    private IconManager() {

    }

    /**
     * This method gets instance of IconManager
     *
     * @return instance of IconManager
     */
    public static IconManager getInstance() {
        return sInstance;
    }

    /**
     * This method gets the drawable id based on the mimetype
     *
     * @param mimeType the mimeType of a file/folder
     * @return the drawable icon id based on the mimetype
     */
    public static int getDrawableId(Context context, String mimeType) {
        //直接根据后缀名判断，不管系统是否支持
       /* boolean isSupported = isSupportedByCurrentSystem(context, mimeType, uri);
        LogUtils.d(TAG, "getDrawableId: mimeType=" + mimeType + ", isSupported=" + isSupported + ", uri=" + uri);
        if (!isSupported) {
            return R.drawable.fm_unknown;
        }*/

        if (TextUtils.isEmpty(mimeType)) {
            return R.drawable.fm_unknown;
        } else if (mimeType.startsWith("application/vnd.android.package-archive")) {
            // TODO change "application/vnd.android.package-archive" to static final string
            return R.drawable.fm_apk;
        } else if (mimeType.startsWith("application/zip")) {
            return R.drawable.fm_zip;
        } else if (mimeType.startsWith("application/rar")) {
            return R.drawable.fm_rar;
        } else if (mimeType.startsWith("video/3gpp")) {
            return R.drawable.fm_3gp;
        } else if (mimeType.startsWith("audio/aac")) {
            return R.drawable.fm_aac;
        } else if (mimeType.startsWith("audio/amr")) {
            return R.drawable.fm_amr;
        } else if (mimeType.startsWith("audio/ape")) {
            return R.drawable.fm_ape;
        } else if (mimeType.startsWith("video/avi")) {
            return R.drawable.fm_avi;
        } else if (mimeType.startsWith("application/msword") ||
                mimeType.startsWith("application/vnd.openxmlformats-officedocument.wordprocessingml.document") ||
                mimeType.startsWith("application/vnd.openxmlformats-officedocument.wordprocessingml.template")) {
            return R.drawable.fm_docx;
        } else if (mimeType.startsWith("audio/flac")) {
            return R.drawable.fm_flac;
        } else if (mimeType.startsWith("video/x-flv")) {
            return R.drawable.fm_flv;
        } else if (mimeType.startsWith("text/html")) {
            return R.drawable.fm_html;
        } else if (mimeType.startsWith("audio/m4a")) {
            return R.drawable.fm_m4a;
        } else if (mimeType.startsWith("video/x-matroska")) {
            return R.drawable.fm_mkv;
        } else if (mimeType.startsWith("audio/mp3")) {
            return R.drawable.fm_mp3;
        } else if (mimeType.startsWith("video/mp4")) {
            return R.drawable.fm_mp4;
        } else if (mimeType.startsWith("video/mp2p")) {
            return R.drawable.fm_mpg;
        } else if (mimeType.startsWith("audio/ogg") || mimeType.startsWith("application/ogg")) {
            return R.drawable.fm_ogg;
        } else if (mimeType.startsWith("application/pdf")) {
            return R.drawable.fm_pdf;
        } else if (mimeType.startsWith("image/")) {
            return R.drawable.fm_picture;
        } else if (mimeType.startsWith("application/vnd.ms-powerpoint") ||
                mimeType.startsWith("application/vnd.openxmlformats-officedocument.presentationml.presentation") ||
                mimeType.startsWith("application/vnd.openxmlformats-officedocument.presentationml.template") ||
                mimeType.startsWith("application/vnd.openxmlformats-officedocument.presentationml.slideshow")) {
            return R.drawable.fm_ppt;
        } else if (mimeType.startsWith("text/plain")) {
            return R.drawable.fm_txt;
        } else if (mimeType.startsWith("audio/x-wav")) {
            return R.drawable.fm_wav;
        } else if (mimeType.startsWith("audio/x-ms-wma")) {
            return R.drawable.fm_wma;
        } else if (mimeType.startsWith("video/x-ms-wmv")) {
            return R.drawable.fm_wmv;
        } else if (mimeType.startsWith("application/vnd.ms-excel") ||
                mimeType.startsWith("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") ||
                mimeType.startsWith("application/vnd.openxmlformats-officedocument.spreadsheetml.template")) {
            return R.drawable.fm_xlxs;
        } else if (mimeType.startsWith("audio/")) {
            return R.drawable.fm_music_default;
        } else if (mimeType.startsWith("video/")) {
            return R.drawable.fm_video_default;
        } else if (mimeType.startsWith("text/")) {
            return R.drawable.fm_txt_default;
        }
        return getCustomDrawableId(context, mimeType);
    }

    public static int getUnknownTypeDrawableId() {
        return R.drawable.fm_unknown;
    }

    /**
     * If the mime type not support by the default system,can customer the file icon here.
     *
     * @param context
     * @param mimeType
     * @return The resource id that use to curtome
     */
    private static int getCustomDrawableId(Context context, String mimeType) {
        /*if (!OptionsUtils.isOP01Surported()) {
            // just supported on OP01
            *//*OP01表示中国移动（CMCC）
            OP02表示中国联通（CU）
            OP03表示Orange
            OP06表示Vodafone
            OP07表示AT&T
            OP08表示TMO-US
            OP09表示中国电信（CT）
            OP10表示Tier-2 operator*//*
            return getUnknownTypeDrawableId();
        }*/

        int fileType = MediaFileManager.getFileTypeForMimeType(mimeType);

        if (!sCustomDrawableIdsMap.containsKey(fileType)) {
            return getUnknownTypeDrawableId();
        }

        return sCustomDrawableIdsMap.get(fileType);
    }

//    /**
//     * @param context
//     * @param mimeType
//     * @return true if have some APs support this mime type,false else.
//     */
   /* private static boolean isSupportedByCurrentSystem(Context context, String mimeType) {
        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setType(mimeType);
        ResolveInfo info = packageManager.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return info != null;
    }*/

    /*private static boolean isSupportedByCurrentSystem(Context context, String mimeType, Uri uri) {
        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(uri, mimeType);
        ResolveInfo info = packageManager.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return info != null;
    }*/
    public static void updateCustomDrawableMap(Context context) {
        sCustomDrawableIdsMap.clear();

        // for excel mime type
        /*if (isSupportedByCurrentSystem(context, "application/vnd.ms-excel")
                || isSupportedByCurrentSystem(context,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                || isSupportedByCurrentSystem(context,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.template")) {*/
        sCustomDrawableIdsMap.put(MediaFileManager.FILE_TYPE_MS_EXCEL, R.drawable.fm_excel);
        LogUtils.d(TAG, "updateCustomDrawableMap: add excel type drawable");
//        }

        // for ppt mime type
        /*if (isSupportedByCurrentSystem(context, "application/vnd.ms-powerpoint")
                || isSupportedByCurrentSystem(context,
                        "application/vnd.openxmlformats-officedocument.presentationml.presentation")
                || isSupportedByCurrentSystem(context,
                        "application/vnd.openxmlformats-officedocument.presentationml.template")
                || isSupportedByCurrentSystem(context,
                        "application/vnd.openxmlformats-officedocument.presentationml.slideshow")) {*/
        sCustomDrawableIdsMap.put(MediaFileManager.FILE_TYPE_MS_POWERPOINT, R.drawable.fm_ppt);
        LogUtils.d(TAG, "updateCustomDrawableMap: add ppt type drawable");
//        }

        // for word mime type
        /*if (isSupportedByCurrentSystem(context, "application/msword")
                || isSupportedByCurrentSystem(context,
                        "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                || isSupportedByCurrentSystem(context,
                        "application/vnd.openxmlformats-officedocument.wordprocessingml.template")) {*/
        sCustomDrawableIdsMap.put(MediaFileManager.FILE_TYPE_MS_WORD, R.drawable.fm_docx);
        LogUtils.d(TAG, "updateCustomDrawableMap: add word type drawable");
//        }

        // for pdf mime type
       /* if (isSupportedByCurrentSystem(context, "application/pdf")) {*/
        sCustomDrawableIdsMap.put(MediaFileManager.FILE_TYPE_PDF, R.drawable.fm_pdf);
        LogUtils.d(TAG, "updateCustomDrawableMap: add pdf type drawable");
//        }
    }

    /**
     * This method gets icon from resources according to file's information.
     *
     * @param res      Resources to use
     * @param fileInfo information of file
     * @param service  FileManagerService, which will provide function to get file's Mimetype
     * @return bitmap(icon), which responds the file
     */
    public Bitmap getIcon(Resources res, FileInfo fileInfo, FileManagerService service, int viewDirection) {
        if (mCurrentDirection != viewDirection) {
            mDirectionChanged = true;
            mCurrentDirection = viewDirection;
        }

        Bitmap icon = null;
        boolean isExternal = false; //MountPointManager.getInstance().isExternalFile(fileInfo);
        //LogUtils.d(TAG, "getIcon: isExternal =" + isExternal);
        if (fileInfo.isDirectory()) {
            icon = getFolderIcon(fileInfo, isExternal);
        } else {
            String mimeType = fileInfo.getFileMimeType(service);
            LogUtils.e(TAG, fileInfo.getFileName() + "*****" + mimeType);
            if (mMime2Bitmap.containsKey(mimeType)) {
                return mMime2Bitmap.get(mimeType);
            }

            int iconId = getDrawableId(service, mimeType);
            if (fileInfo.isDrmFile()) {
                int actionId = OmaDrmUtils.getMediaActionType(mimeType);
                LogUtils.d(TAG, "getIcon: actionId=" + actionId);
                if (actionId != DrmManager.ACTIONID_NOT_DRM) {
                    // try to get the DRM file icon.
                    icon = DrmManager.getInstance().overlayDrmIconSkew(res,
                            fileInfo.getFileAbsolutePath(), actionId, iconId);
                    if (icon != null && isExternal) {
                        icon = createExternalIcon(icon);
                    }
                }
            }
            if (icon == null) {
                icon = getFileIcon(iconId, isExternal);
                mMime2Bitmap.put(mimeType, icon);
            }
        }

        return icon;
    }

    private Bitmap getFileIcon(int iconId, boolean isExternal) {
        if (isExternal) {
            return getExternalIcon(iconId);
        } else {
            return getDefaultIcon(iconId);
        }
    }

    private Bitmap getFolderIcon(FileInfo fileInfo, boolean isExternal) {
        String path = fileInfo.getFileAbsolutePath();
        if (MountPointManager.getInstance().isInternalMountPath(path)) {
            return getDefaultIcon(R.drawable.phone_storage);
        } else if (MountPointManager.getInstance().isExternalMountPath(path)) {
            return getDefaultIcon(R.drawable.sdcard);
        } else if (mExt != null && mExt.isSystemFolder(path)) {
            Bitmap icon = mExt.getSystemFolderIcon(path);
            if (icon != null) {
                if (isExternal) {
                    return createExternalIcon(icon);
                } else {
                    return icon;
                }
            }
        } else if (OptionsUtils.isMtkHotKnotSupported() && fileInfo.getShowName().equalsIgnoreCase("HotKnot") &&
                (MountPointManager.getInstance().isInternalMountPath(fileInfo.getFile().getParent()) ||
                        MountPointManager.getInstance().isExternalMountPath(fileInfo.getFile().getParent()))) {
            /*add icon for HotKnot folder*/
            //return getFileIcon(R.drawable.ic_hotknot_folder, isExternal);
            return getFileIcon(R.drawable.fm_folder, isExternal);
        }
        return getFileIcon(R.drawable.fm_folder, isExternal);
    }

    /**
     * This method initializes variable mExt of IIconExtension type, and create system folder.
     *
     * @param context Context to use
     * @param path    create system folder under this path
     */
    public void init(Context context, String path) {
        try {
            mRes = context.getResources();
            mExt = (IIconExtension) MPlugin.createInstance(
                    IIconExtension.class.getName(), context);
        } catch (Throwable t) {
            LogUtils.e(TAG, "init: Exception !!", t);
        } finally {
            if (mExt == null) {
                mExt = new DefaultIconExtension();
            }
            mExt.createSystemFolder(path);
        }
    }

    /**
     * This method checks weather certain file is system folder.
     *
     * @param fileInfo certain file to be checked
     * @return true for system folder, and false for not system folder
     */
    public boolean isSystemFolder(FileInfo fileInfo) {
        if (fileInfo == null || mExt == null) {
            return false;
        }
        return mExt.isSystemFolder(fileInfo.getFileAbsolutePath());
    }

    /**
     * Get the sdcard2 icon . icon.
     *
     * @param resId resource ID for external icon
     * @return external icon for certain item
     */
    public Bitmap getExternalIcon(int resId) {
        if (mSdcard2Icons == null) {
            mSdcard2Icons = new HashMap<>();
        }
        if (mDirectionChanged) {
            if (mDefIcons != null) {
                mDefIcons.clear();
            }
            mSdcard2Icons.clear();
            mIconsHead = null;
            mDirectionChanged = false;
        }
        Bitmap icon = null;
        if (mSdcard2Icons.containsKey(resId)) {
            icon = mSdcard2Icons.get(resId);
        } else {
            icon = createExternalIcon(getDefaultIcon(resId));
            mSdcard2Icons.put(resId, icon);
        }
        return icon;
    }

    /**
     * Merge the mIconsHead with bitmap together to get the SDCard2 icon.
     *
     * @param bitmap base icon for external icon
     * @return created external icon
     */
    public Bitmap createExternalIcon(Bitmap bitmap) {
        if (bitmap == null) {
            throw new IllegalArgumentException("parameter bitmap is null");
        }
        return bitmap;
        /*if (mIconsHead == null) {
            mIconsHead = BitmapFactory.decodeResource(mRes,
                    R.drawable.fm_sdcard2_header);
        }
        if (mCurrentDirection == ViewGroup.LAYOUT_DIRECTION_LTR) {
            int offx = mIconsHead.getWidth() / OFFX;
            int width = offx + bitmap.getWidth();
            int height = bitmap.getHeight();
            Bitmap icon = Bitmap.createBitmap(width, height, Config.ARGB_8888);
            Canvas c = new Canvas(icon);
            c.drawBitmap(bitmap, offx, 0, null);
            c.drawBitmap(mIconsHead, 0, 0, null);
            return icon;
        } else if (mCurrentDirection == ViewGroup.LAYOUT_DIRECTION_RTL) {
            int offx = mIconsHead.getWidth() / OFFX;
            int width = offx + bitmap.getWidth();
            int height = bitmap.getHeight();
            Bitmap icon = Bitmap.createBitmap(width, height, Config.ARGB_8888);
            Canvas c = new Canvas(icon);
            c.drawBitmap(bitmap, 0, 0, null);
            c.drawBitmap(mIconsHead, width - mIconsHead.getWidth(), 0, null);
            return icon;
        } else {
            LogUtils.d(TAG, "createExternalIcon: unknown direction...");
            return null;
        }*/
    }

    /**
     * Get the default bitmap and cache it in memory.
     *
     * @param resId resource ID for default icon
     * @return default icon
     */
    public Bitmap getDefaultIcon(int resId) {
        if (mDefIcons == null) {
            mDefIcons = new HashMap<>();
        }
        if (mDirectionChanged) {
            mDefIcons.clear();
            if (mSdcard2Icons != null) {
                mSdcard2Icons.clear();
            }
            mIconsHead = null;
            mDirectionChanged = false;
        }
        Bitmap icon = null;

        LogUtils.d(TAG, "getDefaultIcon: resId=" + resId);
        if (mDefIcons.containsKey(resId)) {
            icon = mDefIcons.get(resId);
        } else {
            icon = BitmapFactory.decodeResource(mRes, resId);
            if (icon == null) {
                throw new IllegalArgumentException(
                        "decodeResource() fail, or invalid resId! resId=" + resId);
            }
            mDefIcons.put(resId, icon);
        }
        return icon;
    }
}
