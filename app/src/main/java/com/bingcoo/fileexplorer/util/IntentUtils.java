package com.bingcoo.fileexplorer.util;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;

import com.bingcoo.fileexplorer.R;

import java.io.File;
import java.util.List;

/**
 * 类名称：IntentUtils
 * 作者：David
 * 内容摘要：
 * 创建日期：2017/1/10
 * 修改者， 修改日期， 修改内容
 */
public class IntentUtils {
    private static final String TAG = "IntentUtils";

    static {
        LogUtils.setDebug(TAG, true);
    }

    public static boolean shareText(final Context context,
                                    final String text,
                                    final String pickerTitle) {
        final Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        if (!TextUtils.isEmpty(text))
            sharingIntent.putExtra(Intent.EXTRA_TEXT, text);
        sharingIntent.setType("text/plain");
        return startIntent(context, sharingIntent, pickerTitle);
    }

    public static boolean shareImage(final Context context,
                                     final File imageFile) {
        return shareImage(context, imageFile, null, null);
    }

    public static boolean shareImage(final Context context,
                                     final File imageFile,
                                     final String shareMessage,
                                     final String pickerTitle) {
        final Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("image/*");
        sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(imageFile));
        if (!TextUtils.isEmpty(shareMessage))
            sharingIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
        return startIntent(context, sharingIntent, pickerTitle);
    }

    public static boolean shareAudio(final Context context,
                                     final File messageFileMp3) {
        return shareAudio(context, messageFileMp3, null, null);
    }

    public static boolean shareAudio(final Context context,
                                     final File messageFileMp3,
                                     final String shareMessage,
                                     final String pickerTitle) {
        final Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(messageFileMp3));
        if (!TextUtils.isEmpty(shareMessage))
            sharingIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
        sharingIntent.setType("audio/*");
        return startIntent(context, sharingIntent, pickerTitle);
    }

    public static boolean share(final Context context,
                                final String path) {
        LogUtils.d(TAG, "share: path=" + path);
        File shareFile = new File(path);
        FileInfo shareInfo = new FileInfo(path);
        String extension=FileUtils.getFileExtension(path);
        final Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(shareFile));
        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        String mimeType;
        if (shareInfo.isDrmFile()) {
            mimeType = DrmManager.getInstance().getOriginalMimeType(path);
        }else{
//            mimeType=MediaFile.getMimeTypeForFile(path);
            mimeType= MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        //原生就是这么干的
        if(TextUtils.isEmpty(mimeType)){
            mimeType="application/zip";
        }
        shareIntent.setType(mimeType);
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(shareIntent, PackageManager.MATCH_DEFAULT_ONLY);
        if ((null != resolveInfos)) {
            LogUtils.d(TAG, "share: resolveInfos=" + resolveInfos.size());
        }
        return startIntent(context, shareIntent, null);
    }

    private static boolean startIntent(final Context context, final Intent sharingIntent, String pickerTitle) {
        boolean isSharingIntentStarted = true;
        try {
            if (TextUtils.isEmpty(pickerTitle)) {
                pickerTitle = context.getResources().getString(R.string.edit_share);
                context.startActivity(Intent.createChooser(sharingIntent, pickerTitle));
            } else {
                context.startActivity(Intent.createChooser(sharingIntent, pickerTitle));
            }
        } catch (ActivityNotFoundException e) {
//                Toast.makeText(context, noAssociatedAppErrorMessage, Toast.LENGTH_LONG).show();
            LogUtils.d(TAG, "startIntent: ActivityNotFoundException");
            isSharingIntentStarted = false;
        } catch (SecurityException e) {
//                Toast.makeText(context, securityExceptionMessage, Toast.LENGTH_LONG).show();
            LogUtils.d(TAG, "startIntent: SecurityException");
            isSharingIntentStarted = false;
        } catch (Exception e) {
            LogUtils.d(TAG, "startIntent: Exception");
            isSharingIntentStarted = false;
        }
        return isSharingIntentStarted;
    }
}
