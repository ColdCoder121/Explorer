package com.bingcoo.fileexplorer.util;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.webkit.MimeTypeMap;

import com.bingcoo.fileexplorer.service.FileManagerService;

import java.io.File;
import java.util.List;

/**
 * 类名称：FileInfo
 * 作者：David
 * 内容摘要：文件信息封装类
 * 创建日期：2016/11/23
 * 修改者， 修改日期， 修改内容
 */
@SuppressWarnings("all")
public class FileInfo implements Parcelable {
    private static final String TAG = "FileInfo";

    static {
        LogUtils.setDebug(TAG, true);
    }

    public static final String MIMETYPE_EXTENSION_NULL = "unknown_ext_null_mimeType";
    public static final String MIMETYPE_EXTENSION_UNKONW = "unknown_ext_mimeType";
    public static final String MIMETYPE_3GPP_VIDEO = "video/3gpp";
    public static final String MIMETYPE_3GPP2_VIDEO = "video/3gpp2";
    public static final String MIMETYPE_3GPP_UNKONW = "unknown_3gpp_mimeType";
    public static final String MIMETYPE_3GPP_AUDIO = "audio/3gpp";
    public static final String MIMETYPE_UNRECOGNIZED = "application/zip";
    public static final String MIMETYPE_RAR = "application/rar";
    public static final String MIMETYPE_MP3 = "audio/mp3";
    public static final String MIMETYPE_M4A = "audio/m4a";

    public static final String MIME_HAED_IMAGE = "image/";
    public static final String MIME_HEAD_VIDEO = "video/";

    /**
     * File name's max length
     */
    public static final int FILENAME_MAX_LENGTH = 255;

    private final File mFile;
    private String mParentPath = null;
    private final String mName;
    private final String mAbsolutePath;
    private String mFileSizeStr = null;
    private final boolean mIsDir;
    private long mLastModifiedTime = -1;
    private long mSize = -1;
    private Uri mContentUri = null;
    private String mDuration = null;

    /**
     * Used in FileInfoAdapter to indicate whether the file is selected
     */
    private boolean mIsChecked = false;

    ///M:@#3gp#@{the follow code is to handle the 3gp file.not do special handle now
//  private static HashMap<String, String> sMimeType3GPPMap = new HashMap<String, String>();
    ///M:@{the top code is to handle the 3gp file.not do special handle now

    /**
     * Constructor of FileInfo, which restore details of a file.
     *
     * @param file the file associate with the instance of FileInfo.
     * @throws IllegalArgumentException when the parameter file is null, will
     *                                  throw the Exception.
     */
    public FileInfo(File file) throws IllegalArgumentException {
        if (file == null) {
            throw new IllegalArgumentException();
        }
        mFile = file;
        mName = mFile.getName();
        mAbsolutePath = mFile.getAbsolutePath();
        mLastModifiedTime = mFile.lastModified();
        mIsDir = mFile.isDirectory();
        if (!mIsDir) {
            mSize = mFile.length();
        }
    }

    /**
     * Constructor of FileInfo, which restore details of a file.
     *
     * @param absPath the absolute path of a file which associated with the
     *                instance of FileInfo.
     */
    public FileInfo(String absPath) {
        if (absPath == null) {
            throw new IllegalArgumentException();
        }
        mAbsolutePath = absPath;
        mFile = new File(absPath);
        mName = mFile.getName();
        mLastModifiedTime = mFile.lastModified();
        mIsDir = mFile.isDirectory();
        if (!mIsDir) {
            mSize = mFile.length();
        }
    }

    /**
     * This method gets a file's parent path
     *
     * @return file's parent path.
     */
    public String getFileParentPath() {
        if (mParentPath == null) {
            mParentPath = FileUtils.getFilePath(mAbsolutePath);
            // LogUtils.d(TAG, "getFileParentPath = " + mParentPath);
        }
        return mParentPath;
    }

    /**
     * This method gets a file's parent path's description, which will be shown
     * on the NavigationBar.
     *
     * @return the path's parent path's description path.
     */
    public String getShowParentPath() {
        LogUtils.d(TAG, "getShowParentPath: ");
        return MountPointManager.getInstance().getDescriptionPath(getFileParentPath());
    }

    /**
     * This method gets a file's description path, which will be shown on the
     * NavigationBar.
     *
     * @return the path's description path.
     */
    public String getShowPath() {
        LogUtils.d(TAG, "getShowPath: ");
        return MountPointManager.getInstance().getDescriptionPath(getFileAbsolutePath());
    }

    /**
     * This method gets a file's real name.
     *
     * @return file's name on FileSystem.
     */
    public String getFileName() {
        LogUtils.d(TAG, "getFileName, name = " + mName);
        return mName;
    }

    /**
     * This method gets the file's description name.
     *
     * @return file's description name for show.
     */
    public String getShowName() {
        final String strShowName = FileUtils.getFileName(getShowPath());
        LogUtils.d(TAG, "getShowName: name = " + strShowName);
        return strShowName;
    }

    /**
     * This method gets the file's size(including its contains).
     *
     * @return file's size in long format.
     */
    public long getFileSize() {
        return mSize;
    }

    /**
     * This method gets transform the file's size from long to String.
     *
     * @return file's size in String format.
     */
    public String getFileSizeStr() {
        if (mFileSizeStr == null) {
            if (mFile.isDirectory()) {
//                List<File> fileList = FileUtils.getFileList(mFile, false);
                List<File> fileList = FileUtils.getFileList(mFile, true);
                int sum = 0;
                for (File item : fileList) {
                    sum += item.length();
                }
                mFileSizeStr = FileUtils.sizeToString(sum);
            } else {
                mFileSizeStr = FileUtils.sizeToString(mSize);
            }
        }
        return mFileSizeStr;
    }

    /**
     * This method check the file is directory, or not.
     *
     * @return true for directory, false for not directory.
     */
    public boolean isDirectory() {
        return mIsDir;
    }

    /**
     * This method get the file's MIME type.
     *
     * @param service the FileManager Service for update the 3gpp File
     * @return the file's MIME type.
     */
    public String getFileMimeType(FileManagerService service) {
        LogUtils.d(TAG, "getFileMimeType: ");
        String mimeType = null;

        if (!isDirectory()) {
            if (!isDrmFile()) {
                mimeType = getMimeType(mFile);
                LogUtils.d(TAG, "getFileMimeType: mimetype=" + mimeType);
            } else {
                mimeType = DrmManager.getInstance().getOriginalMimeType(mAbsolutePath);
                LogUtils.d(TAG, "getFileMimeType: mimetype=" + mimeType);
            }

            ///M:@#3gp#@{the follow code is to handle the 3gp file.not do special handle now
//            if (mimeType.equalsIgnoreCase(FileInfo.MIMETYPE_3GPP_UNKONW)) {
//                mimeType = service.update3gppMimetype(this);
//            }
//
//            if (mimeType.equalsIgnoreCase(FileInfo.MIMETYPE_3GPP_AUDIO)
//                    || mimeType.equalsIgnoreCase(FileInfo.MIMETYPE_3GPP_VIDEO)
//                    || mimeType.equalsIgnoreCase(FileInfo.MIMETYPE_3GPP2_VIDEO)) {
//                String name = null;
//                int sepIndex = mName.lastIndexOf("(");
//                if (sepIndex > 0) {
//                    name = mName.substring(0, sepIndex);
//                } else {
//                    name = mName.substring(0, mName.indexOf("."));
//                }
//                LogUtils.d(TAG, "getFileMimeType ,record the mimetype: " + mimeType + ",name = "
//                        + name + ",mName = " + mName);
//
//                sMimeType3GPPMap.put(name, mimeType);
//            }
            ///@the top code is to handle the 3gp file.not do special handle now}
        }

        return mimeType;
    }

    /**
     * The method check the file is DRM file, or not.
     *
     * @return true for DRM file, false for not DRM file.
     */
    public boolean isDrmFile() {
        if (mIsDir) {
            return false;
        }
        return isDrmFile(mAbsolutePath);
    }

    /**
     * This static method check a file is DRM file, or not.
     *
     * @param fileName the file which need to be checked.
     * @return true for DRM file, false for not DRM file.
     */
    public static boolean isDrmFile(String fileName) {
        if (OptionsUtils.isMtkDrmApp()) {
            String extension = FileUtils.getFileExtension(fileName);
            if (extension != null && extension.equalsIgnoreCase(DrmManager.EXT_DRM_CONTENT)) {
                return true; // all drm files cannot be copied
            }
        }
        return false;
    }

    /**
     * This method gets the MIME type based on the extension of a file
     *
     * @param file the target file
     * @return the MIME type of the file
     */
    private String getMimeType(File file) {
        String fileName = getFileName();
        String extension = FileUtils.getFileExtension(fileName);
        LogUtils.d(TAG, "getMimeType: fileName=" + fileName + ",extension = " + extension);

        if (extension == null) {
            return FileInfo.MIMETYPE_EXTENSION_NULL;
        }

        String mimeType;
        if (extension.equalsIgnoreCase("mp3")) {
            mimeType = MIMETYPE_MP3;
        } else if (extension.equalsIgnoreCase("m4a")) {
            mimeType = MIMETYPE_M4A;
        } else {
//            mimeType = MediaFile.getMimeTypeForFile(fileName);
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        LogUtils.d(TAG, "getMimeType: mimeType =" + mimeType);
        if (mimeType == null) {
            if (fileName.endsWith(".rar")) {
                return FileInfo.MIMETYPE_RAR;
            }
            return FileInfo.MIMETYPE_EXTENSION_UNKONW;
        }
        ///M:#3gp#@{the follow code is to handle the 3gp file.not do special handle now
        // sp ecial solution for checking 3gpp original mimetype
        // 3gpp extension could be video/3gpp or audio/3gpp
//        if (mimeType.equalsIgnoreCase(FileInfo.MIMETYPE_3GPP_VIDEO)
//                || mimeType.equalsIgnoreCase(FileInfo.MIMETYPE_3GPP2_VIDEO)) {
//            LogUtils.d(TAG, "getMimeType, a 3gpp or 3g2 file,mimeType=" + mimeType);
//            return FileInfo.MIMETYPE_3GPP_UNKONW;
//        }
        ///M:@{the top code is to handle the 3gp file.not do special handle now
        return mimeType;
    }

    /**
     * This method sets the MIME type of a file.
     *
     * @param mimeType MIME type which will be set
     */
    /*
     * public void setFileMimeType(String mimeType) { mMimeType = mimeType; }
     */

    /**
     * This method gets last modified time of the file.
     *
     * @return last modified time of the file.
     */
    public long getFileLastModifiedTime() {
        return mLastModifiedTime;
    }

    /**
     * This method update mLastModifiedTime(the file's last modified time).
     *
     * @return updated mLastModifiedTime(the file's last modified time).
     */
    public long getNewModifiedTime() {
        mLastModifiedTime = mFile.lastModified();
        return mLastModifiedTime;
    }

    /**
     * This method gets the file's absolute path.
     *
     * @return the file's absolute path.
     */
    public String getFileAbsolutePath() {
        return mAbsolutePath;
    }

    /**
     * This method gets the file packaged in FileInfo.
     *
     * @return the file packaged in FileInfo.
     */
    public File getFile() {
        return mFile;
    }

    /**
     * This method gets the file packaged in FileInfo.
     *
     * @return the file packaged in FileInfo.
     */
    public Uri getUri() {
        return Uri.fromFile(mFile);
    }

    public Uri getContentUri() {
        return mContentUri;
    }

    public void setContentUri(Uri uri) {
        mContentUri = uri;
    }

    public String getDuration() {
        return mDuration;
    }

    public void setDuration(String duration) {
        mDuration = duration;
    }

    @Override
    public int hashCode() {
        return getFileAbsolutePath().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (super.equals(o)) {
            return true;
        } else {
            if (o instanceof FileInfo) {
                if (((FileInfo) o).getFileAbsolutePath().equals(this.getFileAbsolutePath())) {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * This method checks that the file is selected, or not.
     *
     * @return true for selected, false for not selected.
     */
    public boolean isChecked() {
        return mIsChecked;
    }

    /**
     * This method sets variable mIsChecked, which present the file is selected
     * or not.
     *
     * @param checked the checked flag for the file.
     */
    public void setChecked(boolean checked) {
        mIsChecked = checked;
    }

    /**
     * This method checks that weather the file is hide file, or not.
     *
     * @return true for hide file, and false for not hide file
     */
    public boolean isHideFile() {
        if (getFileName().startsWith(".")) {
            return true;
        }
        return false;
    }

    /**
     * This method update this file info if this file is modified
     */
    public void updateFileInfo() {
        LogUtils.d(TAG, "updateFileInfo: ");
        if (!mIsDir) {
            mSize = mFile.length();
            mFileSizeStr = null;
            LogUtils.d(TAG, "updateFileInfo: mDesc=" + mSize);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(mFile);
        dest.writeString(mName);
        dest.writeString(mAbsolutePath);
        dest.writeString(mFileSizeStr);
        dest.writeInt(mIsDir ? 1 : 0);
        dest.writeLong(mLastModifiedTime);
        dest.writeLong(mSize);
        dest.writeParcelable(mContentUri, flags);
        dest.writeString(mDuration);
        dest.writeInt(mIsChecked ? 1 : 0);
    }

    public FileInfo(Parcel in) {
        mFile = (File) in.readSerializable();
        mName = in.readString();
        mAbsolutePath = in.readString();
        mFileSizeStr = in.readString();
        mIsDir = (1 == in.readInt()) ? true : false;
        mLastModifiedTime = in.readLong();
        mSize = in.readLong();
        mContentUri = in.readParcelable(Uri.class.getClassLoader());
        mDuration = in.readString();
        mIsChecked = (1 == in.readInt()) ? true : false;
    }

    public static final Parcelable.Creator<FileInfo> CREATOR = new Parcelable.Creator<FileInfo>() {
        public FileInfo createFromParcel(Parcel in) {
            return new FileInfo(in);
        }

        public FileInfo[] newArray(int size) {
            return new FileInfo[size];
        }
    };

    ///M:@#3gp#{the follow code is to handle the 3gp file.not do special handle now
//    public String getFileOriginMimeType() {
//        String key = null;
//        LogUtils.d(TAG, "getFileOriginMimeType, mName =" + mName);
//        int sepIndex = mName.lastIndexOf("(");
//        if (sepIndex > 0) {
//            key = mName.substring(0, sepIndex);
//        } else {
//            key = mName.substring(0, mName.indexOf("."));
//        }
//        LogUtils.d(TAG, "getFileOriginMimeType,key is:" + key);
//        String orginalMimeType = sMimeType3GPPMap.get(key);
//        LogUtils.d(TAG, "getFileOriginMimeType, OrginalMimeType=" + orginalMimeType);
//        if (orginalMimeType != null) {
//            sMimeType3GPPMap.remove(key);
//        }
//        return orginalMimeType;
//
//    }
    ///M:@{the top code is to handle the 3gp file.not do special handle now
}
