package com.bingcoo.fileexplorer.util;

import android.graphics.Bitmap;


public class DefaultIconExtension implements IIconExtension {

    @Override
    public void createSystemFolder(String defaultPath) {
        return;
    }

    @Override
    public Bitmap getSystemFolderIcon(String path) {
        return null;
    }

    @Override
    public boolean isSystemFolder(String path) {
        return false;
    }
}
