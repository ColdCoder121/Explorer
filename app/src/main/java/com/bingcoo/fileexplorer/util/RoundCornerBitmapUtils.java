package com.bingcoo.fileexplorer.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.Drawable;

import com.bingcoo.fileexplorer.R;

/**
 * 作者：zhshh
 * 内容摘要：圆角图片
 * 创建日期：2017/3/9
 * 修改者， 修改日期， 修改内容
 */

public class RoundCornerBitmapUtils {

    private static final String TAG = "RoundCornerBitmapUtils";
    private static final int mTargetDensity=Resources.getSystem().getDisplayMetrics().densityDpi;
    private static final int mIconWidth=mTargetDensity*87/480;
    /*Drawable bg = mContext.getDrawable(R.drawable.bingcoo_ic_circle_back_normal);
    if (null != bg) {
        centerViewSize = bg.getIntrinsicWidth();
    }*/
    //    private static final int mIconRadius= (int) (mIconWidth*0.6);
    private static Bitmap mMask = null;

    static {
        LogUtils.setDebug(TAG, true);
    }

    public static Bitmap GetRoundedCornerBitmap(Drawable drawable, Context context) {
        if (drawable == null) {
            return null;
        }
        Bitmap bitmap = drawableToBitmap(drawable);
        if(bitmap==null){
            return null;
        }
        if(mMask==null){
            mMask = BitmapFactory.decodeResource(context.getResources(),R.drawable.apk_mask);
        }
        Bitmap bitmapScale=scaleBitmap(bitmap,mIconWidth,mIconWidth);
//        Bitmap bitmapScale=compressBySize(bitmap,mIconWidth,mIconWidth);
        final Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStrokeJoin(Paint.Join.ROUND);
        Bitmap target = Bitmap.createBitmap(bitmapScale.getWidth(), bitmapScale.getHeight(), Bitmap.Config.ARGB_8888);
        /**
         * 产生一个同样大小的画布
         */
        Canvas canvas = new Canvas(target);
        /**
         * 首先绘制圆形
         */
//        canvas.drawCircle(bitmapScale.getWidth() / 2,  bitmapScale.getHeight() / 2, mIconRadius, paint);
        canvas.drawBitmap(mMask,0,0,paint);

        /**
         * 使用SRC_IN
         */
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        /**
         * 绘制图片
         */
        canvas.drawBitmap(bitmapScale, 0, 0, paint);
        return target;

    }

    public static Bitmap drawableToBitmap(Drawable drawable) {

        if(drawable==null){
            return null;
        }
        Bitmap bitmap = Bitmap.createBitmap(
                drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(),
                drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888
                        : Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bitmap);
        //canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static Bitmap scaleBitmap(Bitmap bitmap, int mNewWith, int mNewHeight) {
        if(bitmap==null){
            return null;
        }
        //获取这个图片的宽和高
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        //定义预转换成的图片的宽度和高度
        int newWidth = mNewWith;//200
        int newHeight = mNewHeight;//200
        //计算缩放率，新尺寸除原始尺寸
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // 创建操作图片用的matrix对象
        Matrix matrix = new Matrix();
        // 缩放图片动作
        matrix.postScale(scaleWidth, scaleHeight);
        //旋转图片 动作
//        matrix.postRotate(45);
        // 创建新的图片
        Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
                width, height, matrix, true);
        return resizedBitmap;
    }

}
