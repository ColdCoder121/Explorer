package com.bingcoo.fileexplorer.util;

import android.util.Log;

import com.bingcoo.fileexplorer.BuildConfig;

/**
 * 类名称：TraceUtils
 * 作者：David
 * 内容摘要：
 * 创建日期：2016/11/22
 * 修改者， 修改日期， 修改内容
 */
@SuppressWarnings("all")
public final class TraceUtils {
    private static final String TAG = "TraceUtils";
    private static final int FIRST = 1;

    public static void findCaller() {
        final Throwable mThrowable = new Throwable();
        final StackTraceElement[] elements = mThrowable.getStackTrace();
        final int len = elements.length;
        if (FIRST > len) {
            return;
        }
        StackTraceElement item;
        for (int i = FIRST; i < len; i++) {
            item = elements[i];
            if (BuildConfig.OPEN_TRACE_MODE) {
                Log.d(TAG, "StackTrace: " +
                           item.getClassName() + "." + item.getMethodName()
                           + " ---" + item.getLineNumber() + " line");
            }
        }
    }

    public static void findCallerByMethodName() {
        final Throwable mThrowable = new Throwable();
        final StackTraceElement[] elements = mThrowable.getStackTrace();
        final int len = elements.length;
        if (FIRST > len) {
            return;
        }

        StackTraceElement item = elements[FIRST];
        String TAG = item.getMethodName();
        for (int i = FIRST; i < len; i++) {
            item = elements[i];
            if (BuildConfig.OPEN_TRACE_MODE) {
                Log.d(TAG, "StackTrace: " +
                           item.getClassName() + "." + item.getMethodName()
                           + " ---" + item.getLineNumber() + " line");
            }
        }
    }
}
