package com.bingcoo.fileexplorer.util;

import android.content.Context;
import android.content.res.Resources;

/**
 * 类名称：ResourceUtils
 * 作者：David
 * 内容摘要：
 * 创建日期：2016/12/21
 * 修改者， 修改日期， 修改内容
 */
public class ResourceUtils {
    /**
     * 根据资源名称和类型,得到资源ID
     *
     * @param context
     * @param resourceName
     * @param type
     * @return
     */
    public static int id(Context context, String resourceName, TYPE type) {
        Resources resources = context.getResources();
        return resources.getIdentifier(resourceName, type.getString(), context.getPackageName());
    }

    public static int systemId(Context context, String resourceName, TYPE type) {
        Resources resources = context.getResources();
        return resources.getIdentifier(resourceName, type.getString(), "android");
    }

    /**
     * 定义资源枚举类型
     */
    public enum TYPE {
        ATTR("attr"),
        ARRAY("array"),
        ANIM("anim"),
        BOOL("bool"),
        COLOR("color"),
        DIMEN("dimen"),
        DRAWABLE("drawable"),
        ID("id"),
        INTEGER("integer"),
        LAYOUT("layout"),
        MENU("menu"),
        MIPMAP("mipmap"),
        RAW("raw"),
        STRING("string"),
        STYLE("style"),
        STYLEABLE("styleable");

        private String string;

        TYPE(String string) {
            this.string = string;
        }

        public String getString() {
            return string;
        }
    }
}
