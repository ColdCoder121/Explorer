package com.bingcoo.fileexplorer.util;

import android.content.Context;
import android.os.PowerManager;

/**
 * 类名称：WakeLockUtils
 * 作者：David
 * 内容摘要：
 * 创建日期：2016/12/27
 * 修改者， 修改日期， 修改内容
 */
public class WakeLockUtils {
    private static final String TAG = "WakeLockUtils";

    private PowerManager.WakeLock mWakeLock;
    private Context mCxt;
    private static WakeLockUtils sWakeLockUtils;

    private WakeLockUtils() {

    }

    public static synchronized WakeLockUtils getInstance() {
        if (null == sWakeLockUtils) {
            sWakeLockUtils = new WakeLockUtils();
        }
        return sWakeLockUtils;
    }

    public void setContext(Context context) {
        mCxt = context;
    }

    /**
     * Hold wake lock.
     */
    public void holdWakeLock(Context context) {
        if ((null!=mWakeLock) && !mWakeLock.isHeld()) {
            PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
            mWakeLock.acquire();
        }
    }

    public void holdWakeLock() {
        LogUtils.d(TAG, "holdWakeLock: isHeld=" + (null!=mWakeLock? mWakeLock.isHeld(): "null"));
        if ((null!=mCxt) && (null!=mWakeLock) && !mWakeLock.isHeld()) {
            PowerManager powerManager = (PowerManager) mCxt.getSystemService(Context.POWER_SERVICE);
            mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
            mWakeLock.acquire();
        }
    }

    /**
     * Release wake lock.
     */
    public void releaseWakeLock() {
        LogUtils.d(TAG, "releaseWakeLock: isHeld=" + (null!=mWakeLock? mWakeLock.isHeld(): "null"));
        if ((null!=mWakeLock) && mWakeLock.isHeld()) {
            mWakeLock.release();
        }
    }


}
