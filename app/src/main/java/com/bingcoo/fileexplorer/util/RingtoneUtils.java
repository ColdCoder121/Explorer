package com.bingcoo.fileexplorer.util;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.io.File;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 类名称：RingtoneUtils
 * 作者：David
 * 内容摘要：铃声（来电、短信、闹钟）设置工具类。
 * 创建日期：2016/11/16
 * 修改者， 修改日期， 修改内容
 */
public final class RingtoneUtils {
    private static final String TAG = "RingtoneUtils";
    static {
        LogUtils.setDebug(TAG, true);
    }

    @SuppressWarnings("unused")
    public static void setRingtone(@NonNull Context context, @RingType int ringType, @NonNull File file) {
        LogUtils.d(TAG, "setRingtone: ringType=" + ringType + ", file=" + file);

        try {
            Uri ringUri = null;
            ContentValues values = new ContentValues();
            // 获取系统音频文件的Uri
            Uri uri = MediaStore.Audio.Media.getContentUriForPath(file.getAbsolutePath());

            // 查询音乐文件在媒体库是否存在
            Cursor cursor = context.getContentResolver().query(uri, null, MediaStore.MediaColumns.DATA + "=?",
                                                            new String[] { file.getAbsolutePath() }, null);
            if (cursor.moveToFirst() && cursor.getCount() > 0) {
                String _id = cursor.getString(0);

                values.put(MediaStore.Audio.Media.IS_RINGTONE,
                           (RingtoneManager.TYPE_RINGTONE == ringType));                // 来电
                values.put(MediaStore.Audio.Media.IS_NOTIFICATION,
                           (RingtoneManager.TYPE_NOTIFICATION == ringType));         // 短信
                values.put(MediaStore.Audio.Media.IS_ALARM,
                           (RingtoneManager.TYPE_ALARM == ringType));                      // 闹钟
                values.put(MediaStore.Audio.Media.IS_MUSIC, false);

                context.getContentResolver().update(uri, values, MediaStore.MediaColumns.DATA + "=?",
                                                    new String[] { file.getAbsolutePath() });
                ringUri = ContentUris.withAppendedId(uri, Long.valueOf(_id));
                LogUtils.d(TAG, "setRingtone: ringUri=" + ringUri + ", _id=" + _id);

                // 设置铃声
                RingtoneManager.setActualDefaultRingtoneUri(context, ringType, ringUri);

            } else {
                values.put(MediaStore.MediaColumns.DATA, file.getAbsolutePath());
                values.put(MediaStore.MediaColumns.TITLE, file.getName());
                values.put(MediaStore.MediaColumns.MIME_TYPE, "audio*//*");
                values.put(MediaStore.Audio.Media.IS_RINGTONE,
                           (RingtoneManager.TYPE_RINGTONE == ringType));                // 来电
                values.put(MediaStore.Audio.Media.IS_NOTIFICATION,
                           (RingtoneManager.TYPE_NOTIFICATION == ringType));         // 短信
                values.put(MediaStore.Audio.Media.IS_ALARM,
                           (RingtoneManager.TYPE_ALARM == ringType));                      // 闹钟
                values.put(MediaStore.Audio.Media.IS_MUSIC, false);
                // 获取系统音频文件的Uri
                uri = MediaStore.Audio.Media.getContentUriForPath(file.getAbsolutePath());
                // 将文件插入系统媒体库，并获取新的Uri
                ringUri = context.getContentResolver().insert(uri, values);
                LogUtils.d(TAG, "setRingtone: uri=" + uri + ", ringUri=" + ringUri + ", path=" + (null!=ringUri? ringUri.getPath(): ""));

                // 设置铃声
                RingtoneManager.setActualDefaultRingtoneUri(context, ringType, ringUri);
            }

            if (null != cursor) {
                cursor.close();
            }
        } catch (Exception e) {
            LogUtils.e(TAG, "setRingtone: ERROR!!", e);
        }

    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({RingtoneManager.TYPE_RINGTONE, RingtoneManager.TYPE_NOTIFICATION, RingtoneManager.TYPE_ALARM})
    public @interface RingType {
    }
}
