package com.bingcoo.fileexplorer.util;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.provider.MediaStore;

import com.bingcoo.fileexplorer.R;

import java.util.HashMap;
import java.util.Iterator;

/**
 * 类名称：FileCategoryHelper
 * 作者：David
 * 内容摘要：分类检索工具类。
 * 创建日期：2016/11/23
 * 修改者， 修改日期， 修改内容
 */
public final class FileCategoryHelper {
    public static final int COLUMN_ID = 0;

    public static final int COLUMN_PATH = 1;

    public static final int COLUMN_SIZE = 2;

    public static final int COLUMN_DATE = 3;

    private static final String TAG = "FileCategoryHelper";

    static {
        LogUtils.setDebug(TAG, true);
    }

    public enum FileCategory {
        All, Music, Video, Picture, Doc, Zip, Apk, Recent, Download, Bluetooth
    }

    private static String APK_EXT = "apk";
    private static String THEME_EXT = "mtz";
    private static String[] ZIP_EXTS = new String[]{
            "zip", "rar"
    };

    //public static HashMap<FileCategory, FilenameExtFilter> filters = new HashMap<FileCategory, FilenameExtFilter>();

    public static HashMap<FileCategory, Integer> categoryNames = new HashMap<FileCategory, Integer>();

    static {
        categoryNames.put(FileCategory.All, R.string.category_all);
        categoryNames.put(FileCategory.Music, R.string.category_music);
        categoryNames.put(FileCategory.Video, R.string.category_video);
        categoryNames.put(FileCategory.Picture, R.string.category_picture);
        categoryNames.put(FileCategory.Doc, R.string.category_document);
        categoryNames.put(FileCategory.Zip, R.string.category_zip);
        categoryNames.put(FileCategory.Apk, R.string.category_apk);
    }

    public static FileCategory[] sCategories = new FileCategory[]{
            FileCategory.Music, FileCategory.Video, FileCategory.Picture,
            FileCategory.Doc, FileCategory.Zip, FileCategory.Apk
    };

    private FileCategory mCategory;

    private Context mContext;

    public FileCategoryHelper(Context context) {
        mContext = context;

        mCategory = FileCategory.All;
    }

    public FileCategory getCurCategory() {
        return mCategory;
    }

    public void setCurCategory(FileCategory c) {
        mCategory = c;
    }

    public int getCurCategoryNameResId() {
        return categoryNames.get(mCategory);
    }

    /*public void setCustomCategory(String[] exts) {
        mCategory = FileCategory.Custom;
        if (filters.containsKey(FileCategory.Custom)) {
            filters.remove(FileCategory.Custom);
        }

        filters.put(FileCategory.Custom, new FilenameExtFilter(exts));
    }

    public FilenameFilter getFilter() {
        return filters.get(mCategory);
    }*/

    private HashMap<FileCategory, CategoryInfo> mCategoryInfo = new HashMap<>();

    public HashMap<FileCategory, CategoryInfo> getCategoryInfos() {
        return mCategoryInfo;
    }

    public CategoryInfo getCategoryInfo(FileCategory fc) {
        if (mCategoryInfo.containsKey(fc)) {
            return mCategoryInfo.get(fc);
        } else {
            CategoryInfo info = new CategoryInfo();
            mCategoryInfo.put(fc, info);
            return info;
        }
    }

    public class CategoryInfo {
        public long count;

        public long size;
    }

    private void setCategoryInfo(FileCategory fc, long count, long size) {
        CategoryInfo info = mCategoryInfo.get(fc);
        if (info == null) {
            info = new CategoryInfo();
            mCategoryInfo.put(fc, info);
        }
        info.count = count;
        info.size = size;
    }


    private String buildDocSelection() {
        StringBuilder selection = new StringBuilder();
        Iterator<String> iter = ConstUtils.sDocMimeTypesSet.iterator();
        while (iter.hasNext()) {
            selection.append("(" + MediaStore.Files.FileColumns.MIME_TYPE + "=='" + iter.next() + "') OR ");
        }
        return selection.substring(0, selection.lastIndexOf(")") + 1);
    }

    private String buildZipSelection() {
        StringBuilder selection = new StringBuilder();
        selection.append("(" + MediaStore.Files.FileColumns.DATA + " LIKE '%.zip" + "') OR ("
                + MediaStore.Files.FileColumns.DATA + " LIKE '%.rar"
                + "')");
        return selection.toString();
    }

    private String buildSelectionByCategory(FileCategory cat) {
        String selection = null;
        switch (cat) {
            case Doc:
                selection = buildDocSelection();
                LogUtils.d(TAG, "buildSelectionByCategory: doc selection=" + selection);
                break;
            case Zip:
                //selection = "(" + FileColumns.MIME_TYPE + " == '" + Util.sZipFileMimeType + "')";
                selection = buildZipSelection();
                LogUtils.d(TAG, "buildSelectionByCategory: zip selection=" + selection);
                break;
            case Apk:
                selection = MediaStore.Files.FileColumns.DATA + " LIKE '%.apk'";
                break;
            default:
                selection = null;
        }
        return selection;
    }

    private Uri getContentUriByCategory(FileCategory cat) {
        Uri uri;
        String volumeName = ConstUtils.EXTERNAL_VOLUME_NAME;
        switch (cat) {
            case Doc:
            case Zip:
            case Apk:
                uri = MediaStore.Files.getContentUri(volumeName);
                break;
            case Music:
                uri = MediaStore.Audio.Media.getContentUri(volumeName);
                break;
            case Video:
                uri = MediaStore.Video.Media.getContentUri(volumeName);
                break;
            case Picture:
                uri = MediaStore.Images.Media.getContentUri(volumeName);
                break;
            default:
                uri = null;
        }
        return uri;
    }

    private String buildSortOrder(int sort) {
        String sortOrder = null;
        switch (sort) {
            case FileInfoComparator.SORT_BY_NAME:
                sortOrder = MediaStore.Files.FileColumns.TITLE + " asc";
                break;
            case FileInfoComparator.SORT_BY_SIZE:
                sortOrder = MediaStore.Files.FileColumns.SIZE + " asc";
                break;
            case FileInfoComparator.SORT_BY_TIME:
                sortOrder = MediaStore.Files.FileColumns.DATE_MODIFIED + " desc";
                break;
            case FileInfoComparator.SORT_BY_TYPE:
                sortOrder = MediaStore.Files.FileColumns.MIME_TYPE + " asc, " + MediaStore.Files.FileColumns.TITLE + " asc";
                break;
        }
        return sortOrder;
    }

    public Cursor query(FileCategory fc, int sort) {
        if (PermissionUtils.hasStorageReadPermission(mContext)) {
            Uri uri = getContentUriByCategory(fc);
            String selection = buildSelectionByCategory(fc);
            String sortOrder = buildSortOrder(sort);
            if (uri == null) {
                LogUtils.d(TAG, "query: category:" + fc.name());
                return null;
            }
            String[] columns = new String[]{
                    MediaStore.Files.FileColumns._ID, MediaStore.Files.FileColumns.DATA, MediaStore.Files.FileColumns.SIZE, MediaStore.Files.FileColumns.DATE_MODIFIED
            };
            return mContext.getContentResolver().query(uri, columns, selection, null, sortOrder);
        } else {
            return null;
        }
    }

    public Cursor query(FileCategory fc, int sort, String fileName) {
        Uri uri = getContentUriByCategory(fc);

        String selection = buildSelectionByCategory(fc);
        StringBuilder sb = new StringBuilder();

        if (FileCategory.Music != fc) {
            sb.append(MediaStore.Files.FileColumns.FILE_NAME + " like ");
            DatabaseUtils.appendEscapedSQLString(sb, "%" + fileName + "%");
            sb.append(" and (").append(selection).append(")");
        } else {
            sb.append(MediaStore.MediaColumns.DISPLAY_NAME + " like ");
            DatabaseUtils.appendEscapedSQLString(sb, "%" + fileName + "%");
            //sb.append(" and (").append(selection).append(")");
        }
        LogUtils.d(TAG, "query: selection=" + sb.toString());

        String sortOrder = buildSortOrder(sort);

        if (uri == null) {
            LogUtils.d(TAG, "query: category:" + fc.name());
            return null;
        }

        String[] columns = new String[]{
                MediaStore.Files.FileColumns._ID, MediaStore.Files.FileColumns.DATA, MediaStore.Files.FileColumns.SIZE, MediaStore.Files.FileColumns.DATE_MODIFIED
        };
        try {
            return mContext.getContentResolver().query(uri, columns, sb.toString(), null, sortOrder);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public void refreshCategoryInfo() {
        // clear
        for (FileCategory fc : sCategories) {
            setCategoryInfo(fc, 0, 0);
        }

        // query database
        String volumeName = ConstUtils.EXTERNAL_VOLUME_NAME;

        Uri uri = MediaStore.Audio.Media.getContentUri(volumeName);
        refreshMediaCategory(FileCategory.Music, uri);

        uri = MediaStore.Video.Media.getContentUri(volumeName);
        refreshMediaCategory(FileCategory.Video, uri);

        uri = MediaStore.Images.Media.getContentUri(volumeName);
        refreshMediaCategory(FileCategory.Picture, uri);

        uri = MediaStore.Files.getContentUri(volumeName);
        refreshMediaCategory(FileCategory.Doc, uri);
        refreshMediaCategory(FileCategory.Zip, uri);
        refreshMediaCategory(FileCategory.Apk, uri);
    }

    private boolean refreshMediaCategory(FileCategory fc, Uri uri) {
        String[] columns = new String[]{
                "COUNT(*)", "SUM(_size)"
        };
        Cursor c = mContext.getContentResolver().query(uri, columns, buildSelectionByCategory(fc), null, null);
        if (c == null) {
            LogUtils.d(TAG, "refreshMediaCategory: fail to query uri=" + uri);
            return false;
        }

        if (c.moveToNext()) {
            setCategoryInfo(fc, c.getLong(0), c.getLong(1));
            LogUtils.d(TAG, "refreshMediaCategory: name=" + fc.name() + ", info >>> count:" + c.getLong(0) + ", SIZE:" + c.getLong(1));
            c.close();
            return true;
        }

        return false;
    }

    /*public static FileCategory getCategoryFromPath(String path) {
        MediaFileType TYPE = MediaFile.getFileType(path);
        if (TYPE != null) {
            if (MediaFile.isAudioFileType(TYPE.fileType)) return FileCategory.Music;
            if (MediaFile.isVideoFileType(TYPE.fileType)) return FileCategory.Video;
            if (MediaFile.isImageFileType(TYPE.fileType)) return FileCategory.Picture;
            if (Util.sDocMimeTypesSet.contains(TYPE.mimeType)) return FileCategory.Doc;
        }

        int dotPosition = path.lastIndexOf('.');
        if (dotPosition < 0) {
            return FileCategory.Other;
        }

        String ext = path.substring(dotPosition + 1);
        if (ext.equalsIgnoreCase(APK_EXT)) {
            return FileCategory.Apk;
        }
        if (ext.equalsIgnoreCase(THEME_EXT)) {
            return FileCategory.Theme;
        }

        if (matchExts(ext, ZIP_EXTS)) {
            return FileCategory.Zip;
        }

        return FileCategory.Other;
    }*/

    private static boolean matchExts(String ext, String[] exts) {
        for (String ex : exts) {
            if (ex.equalsIgnoreCase(ext))
                return true;
        }
        return false;
    }
}
