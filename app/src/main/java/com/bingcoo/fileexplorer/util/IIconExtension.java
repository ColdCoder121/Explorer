package com.bingcoo.fileexplorer.util;

import android.graphics.Bitmap;


public interface IIconExtension {

    /**
     * This method used for creating system folder under the default path and the default path is suuplied by Storage Manager.
     * IconManager Init function initiate calls this method.
     *
     * @param defaultPath Storae manager default path under which the system folders are to be created.
     */
    void createSystemFolder(String defaultPath);

    /**
     * Uses the input parameter path to check if it represents one of the system folder created by void createSystemFolder(String defaultPath).
     *
     * @param path path of the folder whcih is to be checked if it is system folder
     * @return true or false depending upon if it is a system folder or not
     */
    boolean isSystemFolder(String path);

    /**
     * This methos is used to get the Bitmap icon for the system folder. all system folder bitmaps are created and stored in a hashmap while call to void createSystemFolder(String defaultPath).
     *
     * @param path path of system folder
     * @return Bitmap icon for the system folder
     */
    Bitmap getSystemFolderIcon(String path);
}
