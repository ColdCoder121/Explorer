package com.bingcoo.fileexplorer.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bingcoo.fileexplorer.R;

/**
 * 类名称：ToastHelper
 * 作者：David
 * 内容摘要：
 * 创建日期：2016/12/14
 * 修改者， 修改日期， 修改内容
 */
public class ToastHelper {
    private final Context mContext;
    private Toast mToast = null;
    private View mLayout = null;
    private TextView mText = null;

    /**
     * Constructor for ToastHelper, construct a ToastHelper with certain context
     *
     * @param context The context to use, there will be the associated activity.
     */
    public ToastHelper(Context context) {
        if (context == null) {
            throw new IllegalArgumentException();
        }
        mContext = context;
    }

    /**
     * Show a Toast(Toast.LENGTH_SHORT).
     *
     * @param text the content shown on the Toast.
     */
    public void showToast(String text) {
        if (null == mToast) {
            mLayout = LayoutInflater.from(mContext).inflate(R.layout.toast, null);
            mText = (TextView) mLayout.findViewById(R.id.message);
            mText.setText(text);

            mToast = new Toast(mContext);
            mToast.setDuration(Toast.LENGTH_SHORT);
            mToast.setView(mLayout);
        } else {
            mText.setText(text);
        }
        mToast.show();
    }

    /**
     * Show a Toast(Toast.LENGTH_SHORT).
     *
     * @param resId the content from Resource(strings.xml) shown on the Toast.
     */
    public void showToast(int resId) {
        if (null == mToast) {
            mLayout = LayoutInflater.from(mContext).inflate(R.layout.toast, null);
            mText = (TextView) mLayout.findViewById(R.id.message);
            mText.setText(mContext.getString(resId));

            mToast = new Toast(mContext);
            mToast.setDuration(Toast.LENGTH_SHORT);
            mToast.setView(mLayout);
        } else {
            mText.setText(mContext.getString(resId));
        }
        mToast.show();
    }


}
