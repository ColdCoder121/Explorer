package com.bingcoo.fileexplorer.util;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 类名称：SharedPrefUtils
 * 作者：David
 * 内容摘要：
 * 创建日期：2016/12/13
 * 修改者， 修改日期， 修改内容
 */
public class SharedPrefUtils {
    private static final String TAG = "SharedPrefUtils";
    static {
        LogUtils.setDebug(TAG, true);
    }

    private SharedPreferences settings = null;

    private SharedPrefUtils(String pref) {
        settings = cxt.getSharedPreferences(pref, Context.MODE_APPEND);
    }

    private SharedPrefUtils(String pref, Context context) {
        if (null != context) {
            settings = context.getSharedPreferences(pref, Context.MODE_APPEND);
        } else {
            settings = cxt.getSharedPreferences(pref, Context.MODE_APPEND);
        }
    }

    private static Context cxt;
    private final static Map<String, SharedPrefUtils> dic = new ConcurrentHashMap<>();

    public static synchronized void init(Context context) {
        cxt = context;
    }

    public static synchronized SharedPrefUtils getInstance(String pref) {
        SharedPrefUtils sp = null;
        if (!dic.containsKey(pref)) {
            sp = new SharedPrefUtils(pref);
            dic.put(pref, sp);
        }

        sp = dic.get(pref);
        return sp;
    }

    public static synchronized SharedPrefUtils getInstance(String pref, Context context) {
        SharedPrefUtils sp = null;
        if (!dic.containsKey(pref)) {
            sp = new SharedPrefUtils(pref, context);
            dic.put(pref, sp);
        }

        sp = dic.get(pref);
        return sp;
    }

    public boolean getBoolean(String key, boolean defValue) {
        return settings.getBoolean(key, defValue);
    }

    public synchronized void putBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public int getInt(String key, int defValue) {
        return settings.getInt(key, defValue);
    }

    public synchronized void putInt(String key, int value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public long getLong(String key, long defValue) {
        return settings.getLong(key, defValue);
    }

    public synchronized void putLong(String key, long value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    public String getString(String key, String defValue) {
        return settings.getString(key, defValue);
    }

    public synchronized void putString(String key, String value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public float getFloat(String key, float defValue) {
        return settings.getFloat(key, defValue);
    }

    public synchronized void putFloat(String key, float value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putFloat(key, value);
        editor.commit();
    }

    public synchronized void remove(String key) {
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(key);
        editor.commit();
    }

    public synchronized void remove(List<String> keyList) {
        SharedPreferences.Editor editor = settings.edit();
        for (String key : keyList) {
            editor.remove(key);
        }
        editor.commit();
    }

    public void registerOnPrefChangeListener(
            SharedPreferences.OnSharedPreferenceChangeListener listener) {
        try {
            settings.registerOnSharedPreferenceChangeListener(listener);
        } catch (Exception ignored) {
        }
    }

    public void unregisterOnPrefChangeListener(
            SharedPreferences.OnSharedPreferenceChangeListener listener) {
        try {
            settings.unregisterOnSharedPreferenceChangeListener(listener);
        } catch (Exception ignored) {
        }
    }

}
