package com.bingcoo.fileexplorer.util;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.StrictMode;
import android.provider.Downloads;
import android.support.annotation.Nullable;
import android.view.WindowManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.bingcoo.fileexplorer.util.PermissionUtils.hasPermission;

/**
 * 类名称：SystemUtils
 * 作者：David
 * 内容摘要：系统工具类。
 * 创建日期：2016/11/22
 * 修改者， 修改日期， 修改内容
 */
public final class SystemUtils {
    private static final String TAG = "SystemUtils";

    static {
        LogUtils.setDebug(TAG, true);
    }

    public static void openStrictMode() {
        StrictMode.setThreadPolicy(
                new StrictMode.ThreadPolicy.Builder()
                        .detectAll()
                        .penaltyLog()
                        .build());

        StrictMode.VmPolicy.Builder vmPolicyBuilder =
                new StrictMode.VmPolicy.Builder()
                        .detectActivityLeaks()
                        .detectLeakedSqlLiteObjects()
                        .detectLeakedClosableObjects();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            vmPolicyBuilder.detectLeakedRegistrationObjects();
        }
        vmPolicyBuilder.penaltyLog();
        StrictMode.setVmPolicy(vmPolicyBuilder.build());
    }

    @Nullable
    public static Cursor getDownloadCursor(Context ctx) {
//        DownloadManager dm = (DownloadManager) ctx.getSystemService(Context.DOWNLOAD_SERVICE);
        if (hasPermission(ctx, Downloads.Impl.PERMISSION_ACCESS_ALL)) {
//            dm.setAccessAllDownloads(true);
//            DownloadManager.Query query = new DownloadManager.Query();
//            query.setFilterByStatus(DownloadManager.STATUS_SUCCESSFUL);
//            Cursor cursor = dm.query(query);
            Cursor cursor = ctx.getContentResolver().query(Downloads.Impl.ALL_DOWNLOADS_CONTENT_URI, null,
                    Downloads.Impl.COLUMN_STATUS + " >= '200' AND " + Downloads.Impl.COLUMN_STATUS + " <'300' ",
                    null, Downloads.Impl.COLUMN_LAST_MODIFICATION + " DESC");
            return cursor;
        }
        return null;
    }

    public static int getDownloadCursorDelete(Context ctx, File file) {

        if (hasPermission(ctx, Downloads.Impl.PERMISSION_ACCESS_ALL) && file != null) {
            Cursor cursor = null;
            try {
                cursor = getDownloadCursor(ctx);
                while (cursor.moveToNext()) {
                    String path = cursor.getString(cursor.getColumnIndex(Downloads.Impl._DATA));
                    if (path != null && path.equals(file.getAbsolutePath())) {
                        LogUtils.e(TAG, "the path to delete=:" + path);
                        DownloadManager dm = (DownloadManager) ctx.getSystemService(Context.DOWNLOAD_SERVICE);
                        long id = cursor.getLong(cursor.getColumnIndex(DownloadManager.COLUMN_ID));
                        LogUtils.e(TAG, "*******需要删除的id=：" + id);
                        return dm.remove(id);
                    }
                }

            } catch (Exception e) {
                LogUtils.e(TAG, "***getDownloadCursorDelete*****异常：" + e);
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }

        }
        return 0;
    }

    public static long getDownloadCursorUpdate(Context ctx, FileInfo mOldFileInfo, FileInfo mNewFileInfo) {
        if (mOldFileInfo == null || mNewFileInfo == null) {
            return 0;
        }
        if (hasPermission(ctx, Downloads.Impl.PERMISSION_ACCESS_ALL)) {
            Cursor mCursor = null;
            try {
                mCursor = getDownloadCursor(ctx);
                String oldPath = mOldFileInfo.getFileAbsolutePath();
                String newPath = mNewFileInfo.getFileAbsolutePath();
                ContentResolver contentResolver = ctx.getContentResolver();
                DownloadManager dm = (DownloadManager) ctx.getSystemService(Context.DOWNLOAD_SERVICE);

                while (mCursor.moveToNext()) {
                    String path = mCursor.getString(mCursor.getColumnIndex(Downloads.Impl._DATA));
                    if (path != null && path.equals(oldPath)) {
                        LogUtils.e(TAG, "***getDownloadCursorUpdate需要更新的path=：" + path);
                        long id = mCursor.getLong(mCursor.getColumnIndex(Downloads.Impl._ID));
                        LogUtils.e(TAG, "*****需要更新的id=:" + id);
//                        String mediaUri = mCursor.getString(mCursor.getColumnIndex(Downloads.Impl.COLUMN_MEDIAPROVIDER_URI));
//                        LogUtils.e(TAG, "****mediaUri=:" + mediaUri);
                        String mMimeType = dm.getMimeTypeForDownloadedFile(id);
                        long totalBytes = mCursor.getLong(mCursor.getColumnIndex(Downloads.Impl.COLUMN_TOTAL_BYTES));
                        Uri deleteUri = ContentUris.withAppendedId(Downloads.Impl.ALL_DOWNLOADS_CONTENT_URI, id);
                        int deleteRows = contentResolver.delete(deleteUri, null, null);
                        LogUtils.e(TAG, "*****deleteRows=:" + deleteRows);
//                        boolean noNotificationPermission = PermissionUtils.hasPermission(ctx, Downloads.Impl.PERMISSION_NO_NOTIFICATION);
//                        LogUtils.e(TAG, "******noNotificationPermission=:" + noNotificationPermission);
                        long newRowId = dm.addCompletedDownload(FileUtils.getFileName(newPath), FileUtils.getFileName(newPath),
                                true, mMimeType, newPath, totalBytes, true);
                        LogUtils.e(TAG, "*****新增的id newRowId=:" + newRowId);
                        return newRowId;
                    }
                }
            } catch (Exception e) {
                LogUtils.e(TAG, "*****DownloadUpdateException=*****" + e);
                return -1;
            } finally {
                if (mCursor != null) {
                    mCursor.close();
                }
            }
        }
        return 0;
    }

    public static void getDownloadCursorDelete(Context ctx, List<FileInfo> list) {

        if (hasPermission(ctx, Downloads.Impl.PERMISSION_ACCESS_ALL)) {
            if (list != null) {
                List<String> datas = new ArrayList<>();
                long[] idAttay = new long[list.size()];
                Cursor cursor = null;
                for (FileInfo info : list) {
                    datas.add(info.getFileAbsolutePath());
                }
                DownloadManager dm = (DownloadManager) ctx.getSystemService(Context.DOWNLOAD_SERVICE);
                int index = 0;
                try {
                    cursor = getDownloadCursor(ctx);
                    while (cursor.moveToNext()) {
                        String path = cursor.getString(cursor.getColumnIndex(Downloads.Impl._DATA));
                        if (path != null && datas.contains(path)) {
                            LogUtils.e(TAG, "****需要删除的path=：****" + path);
                            long id = cursor.getLong(cursor.getColumnIndex(Downloads.Impl._ID));
                            LogUtils.e(TAG, "*******需要删除的id=：" + id);
                            idAttay[index] = id;
                            LogUtils.e(TAG, "*****idAttay长度是：" + idAttay.length);
                            index++;
                        }
                    }
                    if (index >= 1) {
                        int cnt = dm.remove(idAttay);
                        LogUtils.e(TAG, "*****getDownloadCursorDelete=" + cnt);
                    }
                } catch (Exception e) {
                    LogUtils.e(TAG, "*****Exception=*****" + e);
                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                }
            }
        }
    }

    public static Cursor getBluetoothCursor(Context ctx) {
        String visible = "(" + BluetoothShare.VISIBILITY + " IS NULL OR "
                + BluetoothShare.VISIBILITY + " == '" + BluetoothShare.VISIBILITY_VISIBLE + "'" + ")";
        String not_through_handover = "(" + BluetoothShare.USER_CONFIRMATION + " != '"
                + BluetoothShare.USER_CONFIRMATION_HANDOVER_CONFIRMED + "'" + ")";
        String WHERE_COMPLETED = BluetoothShare.STATUS + " >= '200' AND " + visible +
                " AND " + not_through_handover; // Don't show handover-initiated transfers
        String WHERE_COMPLETED_INBOUND = WHERE_COMPLETED + " AND " + "("
                + BluetoothShare.DIRECTION + " == " + BluetoothShare.DIRECTION_INBOUND + ")";
        Cursor cursor = null;
        if (hasPermission(ctx, BluetoothShare.PERMISSION_ACCESS)) {
            cursor = ctx.getContentResolver().query(BluetoothShare.CONTENT_URI, null,
                    WHERE_COMPLETED_INBOUND, null, BluetoothShare.TIMESTAMP + " DESC");
//            cursor.moveToFirst();
        }
        return cursor;
    }

    /**
     * 删除数据库记录
     *
     * @param ctx
     * @param file
     * @return 删除的条数
     */
    public static int getBluetoothCursorDelete(Context ctx, File file) {
        ContentResolver contentResolver = ctx.getContentResolver();
        if (hasPermission(ctx, BluetoothShare.PERMISSION_ACCESS)) {
            if (file != null) {
                String filePath = file.getAbsolutePath();
                LogUtils.e(TAG, "filePath=" + filePath);
                LogUtils.e(TAG, "fileName=" + file.getName());
                try {
                    int cnt = contentResolver.delete(BluetoothShare.CONTENT_URI, BluetoothShare._DATA + "=='" + filePath + "'", null);
                    LogUtils.e(TAG, "getBluetoothCursorDelete返回=" + cnt);
                    return cnt;
                } catch (Exception e) {
                    LogUtils.e(TAG, "*****Exception=*****" + e);
                }
            }
        }
        return 0;
    }

    public static void getBluetoothCursorDelete(Context ctx, List<FileInfo> list) {

        List<String> datas = new ArrayList<>();
        StringBuilder whereClause = new StringBuilder();
        whereClause.append("?");

        ContentResolver contentResolver = ctx.getContentResolver();
        if (hasPermission(ctx, BluetoothShare.PERMISSION_ACCESS)) {
            if (list != null) {
                for (int i = 0; i < list.size() - 1; i++) {
                    whereClause.append(",?");
                }
                String where = BluetoothShare._DATA + " IN(" + whereClause.toString() + ")";
                for (FileInfo info : list) {
                    datas.add(info.getFileAbsolutePath());
                }
                String[] whereArgs = new String[list.size()];
                datas.toArray(whereArgs);
                try {
                    int cnt = contentResolver.delete(BluetoothShare.CONTENT_URI, where, whereArgs);
                    LogUtils.e(TAG, "getBluetoothCursorDelete返回=" + cnt);
                } catch (Exception e) {
                    LogUtils.e(TAG, "*****Exception=*****" + e);
                }
            }
        }
    }

    public static void getBluetoothCursorUpdate(Context ctx, FileInfo mOldFileInfo, FileInfo mNewFileInfo) {

        if (mOldFileInfo == null || mNewFileInfo == null) {
            return;
        }
        String oldPath = mOldFileInfo.getFileAbsolutePath();
        String newPath = mNewFileInfo.getFileAbsolutePath();
        ContentResolver contentResolver = ctx.getContentResolver();
        if (hasPermission(ctx, BluetoothShare.PERMISSION_ACCESS)) {
            ContentValues values = new ContentValues();
            values.put(BluetoothShare._DATA, newPath);
            try {
                int cnt = contentResolver.update(BluetoothShare.CONTENT_URI, values, BluetoothShare._DATA + "=='" + oldPath + "'", null);
                LogUtils.e(TAG, "getBluetoothCursorUpdate=" + cnt);
            } catch (Exception e) {
                LogUtils.e(TAG, "*****Exception=*****" + e);
            }

        }
    }

    // storage, G M K B
    public static String convertStorage(long size) {
        long kb = 1024;
        long mb = kb * 1024;
        long gb = mb * 1024;

        if (size >= gb) {
            return String.format("%.1f GB", (float) size / gb);
        } else if (size >= mb) {
            float f = (float) size / mb;
            return String.format(f > 100 ? "%.0f MB" : "%.1f MB", f);
        } else if (size >= kb) {
            float f = (float) size / kb;
            return String.format(f > 100 ? "%.0f KB" : "%.1f KB", f);
        } else
            return String.format("%d B", size);
    }

    public static int getScreenWidth(Activity activity) {
        WindowManager wm = activity.getWindowManager();
        return wm.getDefaultDisplay().getWidth();
    }

    public static int dp2px(float dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density + 0.5f);
    }

}
