package com.bingcoo.fileexplorer.adapter;

import android.database.Cursor;

import com.bingcoo.fileexplorer.interfaces.ItemAdapter;
import com.bingcoo.fileexplorer.util.FileCategoryHelper;
import com.bingcoo.fileexplorer.util.FileUtils;
import com.bingcoo.fileexplorer.util.LogUtils;

/**
 * 类名称：MediaItemAdapter
 * 作者：David
 * 内容摘要：
 * 创建日期：2016/12/26
 * 修改者， 修改日期， 修改内容
 */
public class MediaItemAdapter implements ItemAdapter {
    private static final String TAG = "MediaItemAdapter";
    static {
        LogUtils.setDebug(TAG, false);
    }

    private Cursor mCursor;

    public MediaItemAdapter(Cursor cursor, int pos) {
        mCursor = cursor;
        getItem(pos);
    }

    public MediaItemAdapter(Cursor cursor) {
        mCursor = cursor;
    }

    @Override
    public String getFileName() {
        if (null != mCursor) {
            String path = mCursor.getString(FileCategoryHelper.COLUMN_PATH);
            LogUtils.d(TAG, "getFileName: path=" + path);
            return FileUtils.getFileName(path);
        }
        return "";
    }

    @Override
    public long getFileModifiedTime() {
        if (null != mCursor) {
            return mCursor.getLong(FileCategoryHelper.COLUMN_DATE);
        }
        return 0;
    }

    @Override
    public String getFileSize() {
        if (null != mCursor) {
            long size = mCursor.getLong(FileCategoryHelper.COLUMN_SIZE);
            return FileUtils.sizeToString(size);
        }
        return "0";
    }

    @Override
    public String getFilePath() {
        if (null != mCursor) {
            String path = mCursor.getString(FileCategoryHelper.COLUMN_PATH);
            LogUtils.d(TAG, "getFileName: name=" + path);
            return path;
        }
        return "";
    }


    @Override
    public Cursor getItem(int pos) {
        LogUtils.d(TAG, "getItem: pos=" + pos + ", mCursor=" + mCursor);
        if (null != mCursor) {
            mCursor.moveToPosition(pos);
            return mCursor;
        }
        return null;
    }
}
