package com.bingcoo.fileexplorer.adapter;

import android.database.Cursor;
import android.provider.Downloads;

import com.bingcoo.fileexplorer.interfaces.ItemAdapter;
import com.bingcoo.fileexplorer.util.FileUtils;

/**
 * 类名称：DownloadItemAdapter
 * 作者：David
 * 内容摘要：
 * 创建日期：2016/12/26
 * 修改者， 修改日期， 修改内容
 */
public class DownloadItemAdapter implements ItemAdapter {
    private Cursor mCursor;

    public DownloadItemAdapter(Cursor cursor, int pos) {
        mCursor = cursor;
        getItem(pos);
    }

    public DownloadItemAdapter(Cursor cursor) {
        mCursor = cursor;
    }

    @Override
    public String getFileName() {
        if (null != mCursor) {
            int nameColumnId = mCursor.getColumnIndex(Downloads.Impl._DATA);//DownloadManager.COLUMN_LOCAL_FILENAME
            String name = mCursor.getString(nameColumnId);
            return FileUtils.getFileName(name);
        }
        return "";
    }

    @Override
    public long getFileModifiedTime() {
        if (null != mCursor) {
            int modifiedColumnId = mCursor.getColumnIndex(Downloads.Impl.COLUMN_LAST_MODIFICATION);//DownloadManager.COLUMN_LAST_MODIFIED_TIMESTAMP
            return mCursor.getLong(modifiedColumnId);
        }
        return 0;
    }

    @Override
    public String getFileSize() {
        if (null != mCursor) {
            int sizeColumnId = mCursor.getColumnIndex(Downloads.Impl.COLUMN_TOTAL_BYTES);//DownloadManager.COLUMN_TOTAL_SIZE_BYTES
            long size = mCursor.getLong(sizeColumnId);
            return FileUtils.sizeToString(size);
        }
        return "";
    }

    @Override
    public String getFilePath() {
        if (null != mCursor) {
            int nameColumnId = mCursor.getColumnIndex(Downloads.Impl._DATA);//DownloadManager.COLUMN_LOCAL_FILENAME
            String path = mCursor.getString(nameColumnId);
            return path;
        }
        return "";
    }

    @Override
    public Cursor getItem(int pos) {
        if (null != mCursor) {
            mCursor.moveToPosition(pos);
            return mCursor;
        }
        return null;
    }
}
