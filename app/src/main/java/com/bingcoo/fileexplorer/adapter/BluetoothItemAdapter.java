package com.bingcoo.fileexplorer.adapter;

import android.database.Cursor;

import com.bingcoo.fileexplorer.interfaces.ItemAdapter;
import com.bingcoo.fileexplorer.util.BluetoothShare;
import com.bingcoo.fileexplorer.util.FileCategoryHelper;
import com.bingcoo.fileexplorer.util.FileUtils;

/**
 * 类名称：BluetoothItemAdapter
 * 作者：David
 * 内容摘要：
 * 创建日期：2016/12/26
 * 修改者， 修改日期， 修改内容
 */
public class BluetoothItemAdapter implements ItemAdapter {
    private Cursor mCursor;

    public BluetoothItemAdapter(Cursor cursor, int pos) {
        mCursor = cursor;
        getItem(pos);
    }

    public BluetoothItemAdapter(Cursor cursor) {
        mCursor = cursor;
    }

    @Override
    public String getFileName() {
        if (null != mCursor) {
            int nameColumnId = mCursor.getColumnIndexOrThrow(BluetoothShare.FILENAME_HINT);
            String fileName = mCursor.getString(nameColumnId);
            return FileUtils.getFileName(fileName);
        }
        return "";
    }

    @Override
    public long getFileModifiedTime() {
        if (null != mCursor) {
            int dateColumnId = mCursor.getColumnIndexOrThrow(BluetoothShare.TIMESTAMP);
            long time = mCursor.getLong(dateColumnId);
            return time;
        }
        return 0;
    }

    @Override
    public String getFileSize() {
        if (null != mCursor) {
            int sizeColumnId = mCursor.getColumnIndexOrThrow(BluetoothShare.TOTAL_BYTES);
            long totalBytes = mCursor.getLong(sizeColumnId);
            return FileUtils.sizeToString(totalBytes);
        }
        return null;
    }

    @Override
    public String getFilePath() {
        if (null != mCursor) {
            int nameColumnId = mCursor.getColumnIndexOrThrow(BluetoothShare._DATA);
            String filePath = mCursor.getString(nameColumnId);
            return filePath;
        }
        return "";
    }

    @Override
    public Cursor getItem(int pos) {
        if (null != mCursor) {
            mCursor.moveToPosition(pos);
            return mCursor;
        }
        return null;
    }
}
