package com.bingcoo.fileexplorer.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.bingcoo.fileexplorer.util.LogUtils;

/**
 * 类名称：BootBroadcastReceiver
 * 作者：David
 * 内容摘要：说明主要功能。
 * 创建日期：2016/11/25
 * 修改者， 修改日期， 修改内容
 */
public class BootBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "BootBroadcastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        LogUtils.d(TAG, "onReceive: ");
    }
}
