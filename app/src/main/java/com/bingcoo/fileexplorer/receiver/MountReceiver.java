package com.bingcoo.fileexplorer.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.storage.StorageVolume;

import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.MountPointManager;
import com.bingcoo.fileexplorer.util.OptionsUtils;

import java.util.ArrayList;


public class MountReceiver extends BroadcastReceiver {
    private static final String TAG = "MountReceiver";
    static {
        LogUtils.setDebug(TAG, true);
    }

    private final MountPointManager mMountPointManager;
    private final ArrayList<MountListener> mMountListenerList = new ArrayList<MountListener>();
    private static final String INTENT_SD_SWAP = "com.mediatek.SD_SWAP";

    public interface MountListener {
        /**
         * This method will be called to do things before MountPoint init.
         */
        // void prepareForMount(String mountPoint);

        /**
         * This method will be called when receive a mounted intent.
         */
        void onMounted(String mountPoint);

        /**
         * This method will be implemented by its class who implements this
         * interface, and called when receive a unMounted intent.
         *
         * @param mountPoint the path of mount point
         */
        @SuppressWarnings("JavadocReference")
        void onUnMounted(StorageVolume volume);

        /**
         * This method cancel the current action on the SD card which will be
         * unmounted.
         */
        void onEjected(String unMountPoint);

        /**
         * This method re-load volume info when sd swap on/off
         *
         */
        void onSdSwap();
    }

    /**
     * This method gets MountPointManager's instance
     */
    public MountReceiver() {
        mMountPointManager = MountPointManager.getInstance();
    }

    /**
     * This method adds listener for activities
     *
     * @param listener listener of certain activity to respond mounted and
     *            unMounted intent
     */
    public void registerMountListener(MountListener listener) {
        mMountListenerList.add(listener);
    }

    public void unregisterMountListener(MountListener listener) {
        mMountListenerList.remove(listener);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        String mountPoint = null;
        StorageVolume volume = null;
        Uri mountPointUri = intent.getData();
        if (mountPointUri != null) {
            mountPoint = mountPointUri.getPath();
        }
        LogUtils.d(TAG, "onReceive: " + action + " mountPoint: " + mountPoint);
        if (INTENT_SD_SWAP.equals(action)) {
            synchronized (this) {
                for (MountListener listener : mMountListenerList) {
                    LogUtils.d(TAG, "onReceive,handle SD_SWAP ");
                    listener.onSdSwap();
                }
            }
        }

        if (mountPoint == null || mountPointUri == null) {
            return;
        }

        if (Intent.ACTION_MEDIA_MOUNTED.equals(action)) {
            // cancel the current operation before MountPointManager init
            // to avoid concurrently access to mountpoint arraylist.{
            for (MountListener listener : mMountListenerList) {
                listener.onMounted(mountPoint);
            }
        } else if (Intent.ACTION_MEDIA_UNMOUNTED.equals(action)) {
            volume = (StorageVolume)intent.getExtra(StorageVolume.EXTRA_STORAGE_VOLUME);
            mMountPointManager.changeMountState(mountPoint, false);
            for (MountListener listener : mMountListenerList) {
                listener.onUnMounted(volume);
            }
        } else if (Intent.ACTION_MEDIA_EJECT.equals(action)) {
            for (MountListener listener : mMountListenerList) {
                listener.onEjected(mountPoint);
            }
        }
    }

    /**
     * Register a MountReceiver for context. See
     * {@link Intent.ACTION_MEDIA_MOUNTED} {@link Intent.ACTION_MEDIA_UNMOUNTED}
     *
     * @param context Context to use
     * @return A mountReceiver
     */
    @SuppressWarnings("JavadocReference")
    public static MountReceiver registerMountReceiver(Context context) {
        MountReceiver receiver = new MountReceiver();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        intentFilter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        intentFilter.addAction(Intent.ACTION_MEDIA_EJECT);
        intentFilter.addDataScheme("file");
        context.registerReceiver(receiver, intentFilter);

        if (OptionsUtils.isMtkSDSwapSurpported()) {
           IntentFilter intentFilterSDSwap = new IntentFilter();
           intentFilterSDSwap.addAction(INTENT_SD_SWAP);
           context.registerReceiver(receiver, intentFilterSDSwap);
        }
        return receiver;
    }
}
