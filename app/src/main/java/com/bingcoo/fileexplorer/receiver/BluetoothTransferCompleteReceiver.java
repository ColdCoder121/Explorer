package com.bingcoo.fileexplorer.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;

import com.bingcoo.fileexplorer.util.BluetoothShare;
import com.bingcoo.fileexplorer.util.LogUtils;

/**
 * 类名称：BluetoothTransferCompleteReceiver
 * 作者：David
 * 内容摘要：
 * 创建日期：2016/12/26
 * 修改者， 修改日期， 修改内容
 */
public class BluetoothTransferCompleteReceiver extends BroadcastReceiver {
    private static final String TAG = "BluetoothTransferComple";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(BluetoothShare.TRANSFER_COMPLETED_ACTION)) {
            Uri uri = intent.getData();
            try {
                Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        int statusColumnId = cursor.getColumnIndexOrThrow(BluetoothShare.STATUS);
                        int status = cursor.getInt(statusColumnId);
                        LogUtils.d(TAG, "onReceive: status=" + status);
                        if (null != mTransferCompleteListener) {
                            mTransferCompleteListener.transfer(status);
                        }
                    }
                    cursor.close();
                }
            } catch (Exception e) {
                LogUtils.e(TAG, "onReceive: Exception !!", e);
            }
        }
    }

    private TransferCompleteListener mTransferCompleteListener;

    public void setTransferCompleteListener(TransferCompleteListener transferCompleteListener) {
        mTransferCompleteListener = transferCompleteListener;
    }

    public interface TransferCompleteListener {
        void transfer(int status);
    }
}
