package com.bingcoo.fileexplorer.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.bingcoo.fileexplorer.home.HomeActivity;

/**
 * 作者：zhshh
 * 内容摘要：
 * 创建日期：2017/3/27
 * 修改者， 修改日期， 修改内容
 */

public class PermissionReceiver extends BroadcastReceiver {

    private onPermissionGranted mPermissionGrantedListener;
    private static final String MY_REQUEST_TO_READ_EXTERNAL_STORAGE_PERMISSIONS_GRANTED = "my_request_to_read_external_storage_permissions_granted";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action != null && action.equals(HomeActivity.MY_REQUEST_TO_READ_EXTERNAL_STORAGE_PERMISSIONS_GRANTED)) {
            if (mPermissionGrantedListener != null) {
                mPermissionGrantedListener.doPermissionGranted();
            }
        }
    }

    public interface onPermissionGranted {
        void doPermissionGranted();
    }

    public static PermissionReceiver registerPermissionReceiver(Context context) {
        PermissionReceiver receiver = new PermissionReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MY_REQUEST_TO_READ_EXTERNAL_STORAGE_PERMISSIONS_GRANTED);
        context.registerReceiver(receiver, intentFilter);
        return receiver;
    }

    public void registerPermissionListener(onPermissionGranted listener) {
        this.mPermissionGrantedListener = listener;
    }

}
