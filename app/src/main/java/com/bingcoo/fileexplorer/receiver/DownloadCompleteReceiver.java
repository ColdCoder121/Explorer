package com.bingcoo.fileexplorer.receiver;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;

import com.bingcoo.fileexplorer.util.LogUtils;

import static android.content.Context.DOWNLOAD_SERVICE;

/**
 * 类名称：DownloadCompleteReceiver
 * 作者：David
 * 内容摘要：
 * 创建日期：2016/12/26
 * 修改者， 修改日期， 修改内容
 */
public class DownloadCompleteReceiver extends BroadcastReceiver {
    private static final String TAG = "DownloadCompleteReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
            long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
            DownloadManager.Query query = new DownloadManager.Query();
            query.setFilterById(downloadId);
            DownloadManager dm = (DownloadManager) context.getSystemService(DOWNLOAD_SERVICE);
            Cursor c = dm.query(query);
            if ((null!=c) && c.moveToFirst()) {
                int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);
                int status = c.getInt(columnIndex);
                LogUtils.d(TAG, "onReceive: status=" + status);
                if (null != mDownloadCompleteListener) {
                    mDownloadCompleteListener.download(status);
                }
                c.close();
            }
        }
    }

    private DownloadCompleteListener mDownloadCompleteListener;

    public void setDownloadCompleteListener(DownloadCompleteListener downloadCompleteListener) {
        mDownloadCompleteListener = downloadCompleteListener;
    }

    public interface DownloadCompleteListener {
        void download(int status);
    }

}
