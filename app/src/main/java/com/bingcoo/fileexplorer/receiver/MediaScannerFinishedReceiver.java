package com.bingcoo.fileexplorer.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.bingcoo.fileexplorer.util.LogUtils;

/**
 * 类名称：MediaScannerFinishedReceiver
 * 作者：David
 * 内容摘要：
 * 创建日期：2016/12/26
 * 修改者， 修改日期， 修改内容
 */
public class MediaScannerFinishedReceiver extends BroadcastReceiver {
    private static final String TAG = "MediaScannerFinishedRec";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        LogUtils.d(TAG, "onReceive: action=" + action);
        if (Intent.ACTION_MEDIA_SCANNER_FINISHED.equals(action)) {
            if (null != mScannerFinishedListener) {
                mScannerFinishedListener.finishScan();
            }
        }
    }

    private ScannerFinishedListener mScannerFinishedListener;

    public void setScannerFinishedListener(ScannerFinishedListener scannerFinishedListener) {
        mScannerFinishedListener = scannerFinishedListener;
    }

    public interface ScannerFinishedListener {
        void finishScan();
    }

}
