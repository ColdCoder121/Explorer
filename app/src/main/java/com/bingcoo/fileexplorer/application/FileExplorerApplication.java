package com.bingcoo.fileexplorer.application;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;

import com.bingcoo.fileexplorer.BuildConfig;
import com.bingcoo.fileexplorer.R;
import com.bingcoo.fileexplorer.application.components.AppComponent;
import com.bingcoo.fileexplorer.service.FileManagerService;
import com.bingcoo.fileexplorer.util.DrmManager;
import com.bingcoo.fileexplorer.util.FileUtils;
import com.bingcoo.fileexplorer.util.IconManager;
import com.bingcoo.fileexplorer.util.LogUtils;
import com.bingcoo.fileexplorer.util.MountPointManager;
import com.bingcoo.fileexplorer.util.SharedPrefUtils;
import com.bingcoo.fileexplorer.util.SystemUtils;
import com.bingcoo.fileexplorer.util.WakeLockUtils;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import java.util.Locale;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;

import static java.util.Locale.ENGLISH;

//import com.bingcoo.fileexplorer.application.components.DaggerAppComponent;
//import com.bingcoo.fileexplorer.application.modules.AppModule;

/**
 * 类名称：FileExplorerApplication
 * 作者：David
 * 内容摘要：文件管理器application
 * 创建日期：2016/11/11
 * 修改者， 修改日期， 修改内容
 */
public class FileExplorerApplication extends Application {
    private final static String TAG = "FileExplorerApplication";

    static {
        LogUtils.setDebug(TAG, true);
    }

    private RefWatcher mRefWatcher;
    private AppComponent mAppComponent;
    private Locale mLocale = ENGLISH;

    @Override
    public void onCreate() {
        super.onCreate();
        mLocale = getResources().getConfiguration().locale;
        init();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        //MultiDex.install(this);
    }

    public RefWatcher getRefWatcher() {
        return mRefWatcher;
    }

    @SuppressWarnings("unused")
    public AppComponent getAppComponent() {
        return mAppComponent;
    }

    private void init() {
        //mAppComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
        if (!BuildConfig.OPEN_RELEASE_MODE) {
            mRefWatcher = LeakCanary.install(this);
            CustomActivityOnCrash.install(this);
        }
        //切换语言后如果应用在后台没有销毁，application并不会重新执行
        MountPointManager.getInstance().init(this);
        DrmManager.getInstance().init(getApplicationContext());
        SharedPrefUtils.init(this);
        FileUtils.setDuplicateExt(getString(R.string.duplicate));
        WakeLockUtils.getInstance().setContext(this);
        openStrictMode();
        startService(new Intent(this.getApplicationContext(),
                FileManagerService.class));
        IconManager.updateCustomDrawableMap(this);
    }

    private void openStrictMode() {
        LogUtils.d(TAG, "openStrictMode: BuildConfig.OPEN_STRICT_MODE=" + BuildConfig.OPEN_STRICT_MODE);
        if (BuildConfig.OPEN_STRICT_MODE) {
            SystemUtils.openStrictMode();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.locale != mLocale) {
            MountPointManager.getInstance().init(this);
            FileUtils.setDuplicateExt(getString(R.string.duplicate));
            LogUtils.e(TAG, "***duplicate=:" + getString(R.string.duplicate));
            mLocale = newConfig.locale;
        }

    }
}
