package com.bingcoo.fileexplorer.application.modules;

import com.bingcoo.fileexplorer.application.FileExplorerApplication;
import com.squareup.leakcanary.RefWatcher;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * 类名称：AppModule
 * 作者：David
 * 内容摘要：提供FileExplorerApplication、RefWatcher的Module
 * 创建日期：2016/11/14
 * 修改者， 修改日期， 修改内容
 */
@Module
public class AppModule {
    private FileExplorerApplication mApp;

    public AppModule(FileExplorerApplication app) {
        mApp = app;
    }

    @Provides
    @Singleton
    public FileExplorerApplication provideFileExplorerApplication() {
        return mApp;
    }

    @Provides
    @Singleton
    public RefWatcher provideRefWatcher() {
        return (null != mApp) ? mApp.getRefWatcher() : null;
    }

}
