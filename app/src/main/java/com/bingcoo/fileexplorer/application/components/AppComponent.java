package com.bingcoo.fileexplorer.application.components;

import com.bingcoo.fileexplorer.application.FileExplorerApplication;
import com.bingcoo.fileexplorer.application.modules.AppModule;
import com.squareup.leakcanary.RefWatcher;

import javax.inject.Singleton;

import dagger.Component;

/**
 * 类名称：AppComponent
 * 作者：David
 * 内容摘要：提供FileExplorerApplication、RefWatcher的Component
 * 创建日期：2016/11/14
 * 修改者， 修改日期， 修改内容
 */
@Singleton
@Component(modules={AppModule.class})
public interface AppComponent {
    FileExplorerApplication getApp();
    RefWatcher getRefWatcher();
}
