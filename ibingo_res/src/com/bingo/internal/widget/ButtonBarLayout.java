/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bingo.internal.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.bingo.publictheme.R;

/**
 * An extension of LinearLayout that automatically switches to vertical
 * orientation when it can't fit its child views horizontally.
 */
public class ButtonBarLayout extends LinearLayout {
	/** Whether the current configuration allows stacking. */
	private boolean mAllowStacking;

	private int mLastWidthSize = -1;
	private final Paint mSelectedUnderlinePaint;
	private final int diverTopHeight = 1;
	private int dividerPadding = 0;

	private Context context;
	public ButtonBarLayout(Context context, AttributeSet attrs) {
		super(context, attrs);

		// final TypedArray ta = context.obtainStyledAttributes(attrs,
		// R.styleable.ButtonBarLayout);
		// mAllowStacking =
		// ta.getBoolean(R.styleable.ButtonBarLayout_allowStacking, false);
		// ta.recycle();

		mSelectedUnderlinePaint = new Paint();
		mSelectedUnderlinePaint.setColor(Build.VERSION.SDK_INT>=23?getContext().getColor(R.color.bingo_dialog_material_divider):
			getContext().getResources().getColor(R.color.bingo_dialog_material_divider));
		dividerPadding = getContext().getResources().getDimensionPixelSize(R.dimen.bingo_dialog_material_divider_padding);

		mSelectedUnderlinePaint.setStrokeWidth(1);
		//mSelectedUnderlinePaint.setAntiAlias(true);

		setWillNotDraw(false);
		this.context = context;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);

//		 Bitmap topDiver = BitmapFactory.decodeResource(getResources(),
//		 R.drawable.bingo_dialog_background_material);
		//
		// canvas.drawBitmap(topDiver, 0, 0, mSelectedUnderlinePaint);
//		 drawImage(canvas, topDiver, 0, 0, getWidth(), px2dip(context, diverTopHeight), 0, 0);
		 int startX = 0;
		 
		 //support RTL
		 boolean rtl = false;
		 if (getLayoutDirection() == View.LAYOUT_DIRECTION_RTL) {
			 rtl = true;
			 startX = getMeasuredWidth();
		 }
		  
		 boolean firstChild = true;
		 for (int i = 0; i < getChildCount(); i++) {
			 View child = this.getChildAt(i);
			 if(child instanceof android.widget.Space)
				 continue;
			 
			 if(child.getVisibility() == View.GONE || child.getVisibility() == View.INVISIBLE)
				 continue;
			 
			 if(firstChild) {
				 startX += rtl ? (-1 * child.getMeasuredWidth()): child.getMeasuredWidth();
				 firstChild = false;
				 continue;
			 }
			 
			 
			 canvas.drawLine(startX, 0+dividerPadding,  startX, getMeasuredHeight()-dividerPadding, mSelectedUnderlinePaint);
			 startX += rtl ? (-1 * child.getMeasuredWidth()): child.getMeasuredWidth();
			
		}
		canvas.drawLine(0, 0, getMeasuredWidth(),0, mSelectedUnderlinePaint);
		

	}

	public static void drawImage(Canvas canvas, Bitmap blt, int x, int y,
			int w, int h, int bx, int by) {
		Rect src = new Rect();// ͼƬ >>ԭ����
		Rect dst = new Rect();// ��Ļ >>Ŀ�����

		src.left = bx;
		src.top = by;
		src.right = bx + w;
		src.bottom = by + h;

		dst.left = x;
		dst.top = y;
		dst.right = x + w;
		dst.bottom = y + h;
		// ����ָ����λͼ��λͼ���Զ�--������/�Զ�ת�������Ŀ�����
		// �����������˼���� ��һ��λͼ���������ػ�һ�飬�����λͼ����������Ҫ����
		canvas.drawBitmap(blt, null, dst, null);
		src = null;
		dst = null;
	}

	public static int px2dip(Context context, float pxValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (pxValue / scale + 0.5f);
	}
	
	  public static int dip2px(Context context, float dpValue) {  
	        final float scale = context.getResources().getDisplayMetrics().density;  
	        return (int) (dpValue * scale + 0.5f);  
	    }  

}
