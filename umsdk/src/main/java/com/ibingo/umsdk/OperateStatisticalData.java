package com.ibingo.umsdk;

import android.content.Context;

import com.umeng.analytics.MobclickAgent;
import com.umeng.analytics.MobclickAgent.EScenarioType;
import com.umeng.analytics.MobclickAgent.UMAnalyticsConfig;
import com.umeng.analytics.social.UMPlatformData;
import com.umeng.analytics.social.UMPlatformData.UMedia;

import java.util.HashMap;
import java.util.Map;

import javax.microedition.khronos.opengles.GL10;

/**
 * @author 蔡健
 * @version 2016-08-12 类说明：封装了销量统计（友盟）接口
 */

public class OperateStatisticalData {

    /**
     * 是否开启友盟统计。
     */
    public static boolean bOpened = true;

    /**
     * 设置场景类型。 etype是官方场景，有如下场景: EScenarioType.E_UM_NORMAL 普通统计场景类型
     * EScenarioType.E_UM_GAME 游戏场景类型 EScenarioType.E_UM_ANALYTICS_OEM 统计盒子场景类型
     * EScenarioType. E_UM_GAME_OEM 游戏盒子场景类型。
     *
     * @param context
     * @param etype
     */
    public static void setScenarioType(Context context, EScenarioType etype) {
        if (!bOpened)
            return;
        MobclickAgent.setScenarioType(context, etype);
    }

    /**
     * 在程序的入口处,设置Appkey/Token/Channel。 调用如下方法代码中集成,需要在程序入口调用此接口。
     * UMAnalyticsConfig初始化参数类，提供多参数构造方式： UMAnalyticsConfig(Context context,
     * String appkey, String channelId) UMAnalyticsConfig(Context context,
     * String appkey, String channelId, EScenarioType eType)
     * UMAnalyticsConfig(Context context, String appkey, String channelId,
     * EScenarioType eType,boolean isCrashEnable) 构造意义： String
     * appkey:官方申请的Appkey String channel: 渠道号 EScenarioType eType:
     * 场景模式，包含统计、游戏、统计盒子、游戏盒子 Boolean isCrashEnable: 可选初始化. 是否开启crash模式。
     *
     * @param config
     */
    public static void startWithConfigure(UMAnalyticsConfig config) {
        if (!bOpened)
            return;
        MobclickAgent.startWithConfigure(config);
    }

    /**
     * 设置Secret Key 防止appkey被盗用,secretkey网站申请。
     *
     * @param context
     * @param secretkey
     */
    public static void setSecret(Context context, String secretkey) {
        if (!bOpened)
            return;
        MobclickAgent.setSecret(context, secretkey);
    }

    // 确保在所有的Activity中都调用 MobclickAgent.onResume()
    // 和MobclickAgent.onPause()方法，这两个调用将不会阻塞应用程序的主线程，也不会影响应用程序的性能。
    // 注意 如果您的Activity之间有继承或者控制关系请不要同时在父和子Activity中重复添加onPause和onResume方法，否则会造成重复统计，导致启动次数异常增高。(eg：使用TabHost、TabActivity、ActivityGroup时)。
    // 当应用在后台运行超过30秒（默认）再回到前端，将被认为是两个独立的session(启动)，例如用户回到home，或进入其他程序，经过一段时间后再返回之前的应用。可通过接口：MobclickAgent.setSessionContinueMillis(long
    // interval) 来自定义这个间隔（参数单位为毫秒）。
    // 如果开发者调用Process.kill或者System.exit之类的方法杀死进程，请务必在此之前调用MobclickAgent.onKillProcess(Context
    // context)方法，用来保存统计数据。
    // 必须调用
    // MobclickAgent.onResume()和MobclickAgent.onPause()方法，才能够保证获取正确的新增用户、活跃用户、启动次数、使用时长等基本数据。

    /**
     * Session统计 请在onResume()中调用此方法。
     *
     * @param context
     */
    public static void onResume(Context context) {
        if (!bOpened)
            return;
        MobclickAgent.onResume(context);
    }

    /**
     * Session统计 请在onPause()中调用此方法。
     *
     * @param context
     */
    public static void onPause(Context context) {
        if (!bOpened)
            return;
        MobclickAgent.onPause(context);
    }

    /**
     * Session统计 请在onPageStart()中调用此方法。
     *
     * @param paramString
     */
    public static void onPageStart(String paramString) {
        if (!bOpened)
            return;
        MobclickAgent.onPageStart(paramString);
    }

    /**
     * Session统计 请在onPageEnd()中调用此方法。
     *
     * @param paramString
     */
    public static void onPageEnd(String paramString) {
        if (!bOpened)
            return;
        MobclickAgent.onPageEnd(paramString);
    }

    /**
     * 是否自动统计Activity。 需要在程序入口处调用。
     *
     * @param enable
     */
    public static void openActivityDurationTrack(boolean enable) {
        if (!bOpened)
            return;
        MobclickAgent.openActivityDurationTrack(enable);
    }

    /**
     * 账号的统计 ID：用户账号ID，长度小于64字节。
     *
     * @param ID
     */
    public static void onProfileSignIn(String ID) {
        if (!bOpened)
            return;
        MobclickAgent.onProfileSignIn(ID);
    }

    /**
     * ID：用户账号ID，长度小于64字节。
     * Provider：账号来源。如果用户通过第三方账号登陆，可以调用此接口进行统计。支持自定义，不能以下划线"_"
     * 开头，使用大写字母和数字标识，长度小于32 字节; 如果是上市公司，建议使用股票代码。
     *
     * @param Provider
     * @param ID
     */
    public static void onProfileSignIn(String Provider, String ID) {
        if (!bOpened)
            return;
        MobclickAgent.onProfileSignIn(Provider, ID);
    }

    /**
     * 账号登出时需调用此接口，调用之后不再发送账号相关内容。
     */
    public static void onProfileSignOff() {
        if (!bOpened)
            return;
        MobclickAgent.onProfileSignOff();
    }

    /**
     * 设置是否对日志信息进行加密, 默认false(不加密)。
     * 如果enable为true，SDK会对日志进行加密。加密模式可以有效防止网络攻击，提高数据安全性。
     * 如果enable为false，SDK将按照非加密的方式来传输日志。 如果您没有设置加密模式，SDK的加密模式为false（不加密）。
     *
     * @param enable
     */
    public static void enableEncrypt(boolean enable) {
        if (!bOpened)
            return;
        MobclickAgent.enableEncrypt(enable);
    }

    /**
     * 统计发生次数 context指当前的Activity eventId为当前统计的事件ID。
     *
     * @param context
     * @param eventId
     */
    public static void onEvent(Context context, String eventId) {
        if (!bOpened)
            return;
        MobclickAgent.onEvent(context, eventId);
    }

    /**
     * 统计点击行为各属性被触发的次数 map为当前事件的属性和取值（Key-Value键值对）。
     *
     * @param context
     * @param eventId
     * @param map
     */
    public static void onEvent(Context context, String eventId,
                               HashMap<String, String> map) {
        if (!bOpened)
            return;
        MobclickAgent.onEvent(context, eventId, map);
    }

    /**
     * 统计数值型变量的值的分布 id为事件ID map为当前事件的属性和取值
     * du为当前事件的数值为当前事件的数值，取值范围是-2,147,483,648 到 +2,147,483,647
     * 之间的有符号整数，即int32类型，如果数据超出了该范围，会造成数据丢包，影响数据统计的准确性。
     *
     * @param context
     * @param id
     * @param m
     * @param du
     */
    public static void onEventValue(Context context, String id,
                                    Map<String, String> m, int du) {
        if (!bOpened)
            return;
        MobclickAgent.onEventValue(context, id, m, du);
    }

    /**
     * 社交统计 针对社交行为的垂直统计，可以非常详尽地统计应用中发生的各种社交行为。 只需要调用一行代码，便可享用到丰富的社交行为和社交用户分析报表
     * 把分享信息发送到友盟服务器, 我们会通过这些信息创建社交行为报表。 相关参数说明： UMPlatformData： UMeida
     * meida平台枚举类型（必填） String user_id用户的id（必填） String weiboId 微博id String
     * name用户姓名 GENDER gender 用户性别。
     *
     * @param context
     * @param paramUMedia
     * @param user_id
     */
    public static void onSocialEvent(Context context, UMedia paramUMedia,
                                     String user_id) {
        if (!bOpened)
            return;
        UMPlatformData platform = new UMPlatformData(paramUMedia, user_id);
        // platform.setGender(GENDER.MALE); //optional
        // platform.setWeiboId("weiboId"); //optional
        MobclickAgent.onSocialEvent(context, platform);
    }

    /**
     * 设置openGL信息，辅助统计GPU信息。
     *
     * @param gl
     */
    public static void setOpenGLContext(GL10 gl) {
        if (!bOpened)
            return;
        MobclickAgent.setOpenGLContext(gl);
    }

    /**
     * 如果开发者调用Process.kill()或者System.exit()之类的方法杀死进程，请务必在此之前调用此方法，用来保存统计数据。
     *
     * @param context
     */
    public static void onKillProcess(Context context) {
        if (!bOpened)
            return;
        MobclickAgent.onKillProcess(context);
    }

    /**
     * android6.0中采集mac方式变更。
     * 该接口默认参数是true，即采集mac地址，但如果开发者需要在googleplay发布，考虑到审核风险，可以调用该接口，参数设置为 false
     * 就不会采集mac地址。
     *
     * @param enable
     */
    public static void setCheckDevice(boolean enable) {
        if (!bOpened)
            return;
        MobclickAgent.setCheckDevice(enable);
    }

    /**
     * 是否打开调试模式 使用普通测试流程，请先在程序入口添加以下代码打开调试模式。
     * 打开调试模式后，您可以在logcat中查看您的数据是否成功发送到友盟服务器
     * ，以及集成过程中的出错原因等，友盟相关log的tag是MobclickAgent。
     *
     * @param enable
     */
    public static void setDebugMode(boolean enable) {
        if (!bOpened)
            return;
        MobclickAgent.setDebugMode(enable);
    }

    /**
     * 初始化（带参）
     *
     * @param context
     * @param enable1
     * @param enable2
     * @param mType
     */
    public static void init(Context context, boolean enable1, boolean enable2,
                            EScenarioType mType) {
        if (!bOpened)
            return;
        MobclickAgent.setDebugMode(enable1);
        MobclickAgent.openActivityDurationTrack(enable2);
        MobclickAgent.setScenarioType(context, mType);
    }

    /**
     * 初始化（默认）
     *
     * @param context
     */
    public static void init(Context context) {
        init(context, false, false, EScenarioType.E_UM_NORMAL);
    }
}
